
  create or replace  view datalake_dev.semantic.RPT_BPM_CONDO_SALES_PERFORMANCE  as (
    
with 
 Total_Net_Sales_Proceeds  --sub1
as
(SELECT  opp.building 
			   ,opp.Space_Id
 
			   	,CASE  WHEN   spc.Property_ID IN ('a001Y00000p4WzaQAE'--Koula
												,'a00i000000gGeW9AAK'--Ke Kilohana
												,'a00i000000nbCAIAA2'--,A'ali'i
												,'a002a000004FGDlAAO')--Victoria Place
				       THEN SUM(ifnull(spc.Unit_Price,0))
				       WHEN   spc.Property_ID IN ('a00i0000007XwkmAAC'--Waiea
					                    ,'a00i0000007XwhIAAS'--Anaha
										,'a00i000000dQBgGAAW'--Ae'o
										)
				       THEN SUM(ifnull(Purchase_Price_Net_of_Credits ,0)) 
					   ELSE SUM(ifnull(Purchase_Price_Net_of_Credits ,0))-- tmp
					   END  Total_Net_Sales_Proceeds 
from datalake_dev.semantic.DIM_SF_OPPORTUNITY opp
left join datalake_dev.semantic.DIM_SF_PROPSPACE spc on opp.space_id =spc.space_id
left join datalake_dev.semantic.DIM_SF_PROPERTY  prop on spc.property_id = prop.property_id
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE  T ON spc.RECORD_TYPE_ID = T.RECORD_TYPE_ID
 WHERE  opp.IsWon ='TRUE'
	     	AND t.Name='Ward Residential'				--
		    AND spc.Space_Status <> 'Not Available'		--					--
		 GROUP BY opp.building
		         ,opp.Space_Id
				 ,spc.Property_ID
 )
 ,net_sale_proceeds_Price_PSF_achieved   --sub2
as
(
  select 
  spc.Property_id,spc.building
		  ,spc.space_id
		  ,sum(spc.SQ_FOOTAGE)  SqFootage
 				,CASE  WHEN spc.Property_id IN ('a001Y00000p4WzaQAE','a00i000000nbCAIAA2')--Koula,A'ali'i
				       THEN SUM(ifnull(Proforma_Unit_Price,0) - ifnull(Furniture,0) + ifnull(Furniture_Option,0))
				       WHEN spc.Property_id IN ('a00i0000007XwkmAAC'--Waiea
					                    ,'a00i0000007XwhIAAS'--Anaha
										,'a00i000000gGeW9AAK'--Ke Kilohana
										,'a00i000000dQBgGAAW'--Ae'o
										,'a002a000004FGDlAAO')--Victoria Place
				       THEN SUM(ifnull(Proforma_Unit_Price,0))
					   ELSE SUM(ifnull(Proforma_Unit_Price,0))-- tmp
				END  Pro_forma_net_sale_proceeds
				
				,CASE  WHEN spc.Property_id IN ('a001Y00000p4WzaQAE','a00i000000nbCAIAA2')--Koula,A'ali'i
				       THEN AVG((ifnull(Turn_Key_option_Unit_Price,0) + ifnull(Furniture_Option,0))/ nullif(ifnull(Est_Net_Living_Area_SF,0),0))
				       WHEN spc.Property_id  IN ('a00i0000007XwkmAAC'--Waiea
					                    ,'a00i0000007XwhIAAS'--Anaha
										,'a00i000000gGeW9AAK'--Ke Kilohana
										,'a00i000000dQBgGAAW'--Ae'o
										,'a002a000004FGDlAAO')--Victoria Place
				       THEN AVG(ifnull(spc.UNIT_PRICE,0)/ nullif(ifnull(Est_Net_Living_Area_SF,0),0))
					   ELSE AVG(ifnull(spc.Unit_Price,0)/ nullif(ifnull(Est_Net_Living_Area_SF,0),0))-- tmp
 				END
				 Price_PSF_achieved
from  datalake_dev.semantic.DIM_SF_PROPSPACE spc  
left join datalake_dev.semantic.DIM_SF_PROPERTY prop on spc.property_id = prop.property_id
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE  T ON spc.RECORD_TYPE_ID = T.RECORD_TYPE_ID
   where 
		  spc.Space_Status in ( 'Contracted' , '1st Deposit Hard') 
	     AND t.Name='Ward Residential'				--
		 AND spc.Space_Status <> 'Not Available'		--					--
		 GROUP BY   spc.Property_id  ,spc.building
				  ,spc.space_id
)
,Pro_forma_Price_PSF  --sub3
as
(
  select 
 spc. Property_id,spc.building
			  ,spc.space_id
		      ,AVG(ifnull(Pro_Forma_Price_PSF,0))  Pro_forma_Price_PSF
from datalake_dev.semantic.DIM_SF_PROPSPACE spc  
left join datalake_dev.semantic.DIM_SF_PROPERTY  prop on spc.property_id = prop.property_id
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE   T ON spc.RECORD_TYPE_ID = T.RECORD_TYPE_ID
   where   t.Name='Ward Residential'				--
		 AND spc.Space_Status <> 'Not Available'		--					--
		 GROUP BY   spc.Property_id ,spc.building 
				  ,spc.space_id
)
, Price_PSF_in_unsold_inventory_Pro_forma_Price_PSF_unsold  --ssub4
as
(
  select 
 spc. Property_id,spc.building
			  ,spc.space_id
					,AVG (ifnull(Unit_Price_PSF,0)) Price_PSF_in_unsold_inventory
 					,CASE WHEN spc. Property_id IN ('a001Y00000p4WzaQAE'--Koula
					                       ,'a00i000000nbCAIAA2')--A'ali'i
                         THEN AVG((ifnull(Proforma_Unit_Price,0) - ifnull(Furniture_Option ,0))/Est_Net_Living_Area_SF )
					     ELSE  AVG(ifnull(Proforma_Unit_Price,0)/Est_Net_Living_Area_SF )
				   END Pro_forma_Price_PSF_unsold 
from  datalake_dev.semantic.DIM_SF_PROPSPACE spc  
left join datalake_dev.semantic.DIM_SF_PROPERTY  prop on spc.property_id = prop.property_id
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE   T ON spc.RECORD_TYPE_ID = T.RECORD_TYPE_ID
   where 
		  spc.Space_Status in ( 'Available' , 'Held','Reserved') 
	     AND t.Name='Ward Residential'				--
		 AND spc.Space_Status <> 'Not Available'		--					--
		 GROUP BY   spc.Property_id  ,spc.building
				  ,spc.space_id
)
select 
spc.Building Building,
spc.space_id 
			  ,spc.NAME space
			  ,spc. Est_Net_Living_Area_SF 
			 
			  ,Total_Net_Sales_Proceeds.Total_Net_Sales_Proceeds
			  ,sub2.Pro_forma_net_sale_proceeds
			  ,sub2.Price_PSF_achieved
			  ,	CASE WHEN Total_Net_Sales_Proceeds.Total_Net_Sales_Proceeds  IS NULL THEN NULL
			  ELSE sub3.Pro_forma_Price_PSF END Pro_forma_Price_PSF
			  ,sub4.Price_PSF_in_unsold_inventory
			  ,sub4.Pro_forma_Price_PSF_unsold
			  ,prop.MCRP24 Property_CC24
from 
datalake_dev.semantic.DIM_SF_PROPSPACE spc  
left join datalake_dev.semantic.DIM_SF_PROPERTY prop on spc.property_id = prop.property_id
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE     T ON spc.RECORD_TYPE_ID = T.RECORD_TYPE_ID
LEFT JOIN Total_Net_Sales_Proceeds ON spc.Building =Total_Net_Sales_Proceeds.Building 
									 and Total_Net_Sales_Proceeds.Space_Id=spc.space_id
LEFT JOIN net_sale_proceeds_Price_PSF_achieved sub2 on spc.Building =sub2.Building
                                                    and spc.space_id =sub2.space_id
left join Pro_forma_Price_PSF sub3 on spc.Building =sub3.Building
                                                    and spc.space_id =sub3.space_id
left join Price_PSF_in_unsold_inventory_Pro_forma_Price_PSF_unsold sub4 on spc.Building =sub4.Building
                                                    and spc.space_id =sub4.space_id

   where   t.Name='Ward Residential'				 
		 AND spc.Space_Status <> 'Not Available'	
and spc.building is not null
  );
