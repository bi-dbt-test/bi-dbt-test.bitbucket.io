
  create or replace  view datalake_dev.semantic.VIEW_BIRPT_HHC_COMMENT  as (
    

SELECT
  cmt.commentId,
  lkp1.shortDescription AS entityType,
  lkp2.shortDescription AS request,
  prj.name AS project,
  cnt.title AS content,
  app.approvalTaskType AS approval,
  msg.createdBy AS messageSentBy,
  onb.employeeNumber AS onboardingEmployeeNumber,
  ofb.appianUsername AS offboardingEmployee,
  dtt.name,
  cmt.comment,
  cmt.isActive,
  cmt.createdBy,
  cmt.createdDate,
  cmt.lastUpdatedBy,
  cmt.lastUpdatedDate,
  cmt.isEveryoneTagged,
  cmt.isEdited,
  cmt.SysStartTime,
  cmt.SysEndTime
FROM  datalake_dev.governed.APPIAN_HHC_COMMENT     cmt
LEFT JOIN  datalake_dev.governed.APPIAN_HHC_LOOKUP   lkp1 ON lkp1.lookupId=cmt.entityTypeLookupId
LEFT JOIN  datalake_dev.governed.APPIAN_DRM_REQUEST     req ON req.requestId=cmt.entityId AND cmt.entityTypeLookupId=1
LEFT JOIN  datalake_dev.governed.APPIAN_HHC_LOOKUP   lkp2 ON lkp2.lookupId=req.requestTypeLookupId
LEFT JOIN datalake_dev.governed.APPIAN_WSP_PROJECT      prj ON prj.projectId=cmt.entityId AND cmt.entityTypeLookupId=3
LEFT JOIN datalake_dev.governed.APPIAN_HOME_CONTENT    cnt ON cnt.contentId=cmt.entityId AND cmt.entityTypeLookupId=5
LEFT JOIN datalake_dev.governed.APPIAN_CAP_APPROVAL      app ON app.approvalId=cmt.entityId AND cmt.entityTypeLookupId=8
LEFT JOIN  datalake_dev.governed.APPIAN_HHC_MESSAGETHREAD    msg ON msg.messageThreadId=cmt.entityId AND cmt.entityTypeLookupId=11
LEFT JOIN datalake_dev.governed.APPIAN_EOB_EMPLOYEEONBOARD   onb ON onb.employeeOnboardId=cmt.entityId AND cmt.entityTypeLookupId=14
LEFT JOIN datalake_dev.governed.APPIAN_EOB_EMPLOYEEOFFBOARD     ofb ON ofb.employeeOffboardId=cmt.entityId AND cmt.entityTypeLookupId=15
LEFT JOIN datalake_dev.governed.APPIAN_CRD_CRITICALDATE   crd ON crd.criticalDateId=cmt.entityId AND cmt.entityTypeLookupId=17
LEFT JOIN datalake_dev.governed.APPIAN_CRD_DATETYPE     dtt ON dtt.dateTypeId=crd.dateTypeId
WHERE entityTypeLookupId IS NOT NULL
  );
