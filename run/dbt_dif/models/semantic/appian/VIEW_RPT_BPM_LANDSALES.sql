
  create or replace  view datalake_dev.semantic.VIEW_RPT_BPM_LANDSALES  as (
    

with base
as
(
select f.HOME_BUILDER_AREA area
,dl.company
,MPC_AREA uds_description
,f.COMMUNITY
,dl.LCC06 lcc06_land_dev_stage
,dl.LCC06_DESCRIPTION lcc06_land_dev_stage_desc
,f.BUYER_NUMBER builder_number
,t.TENANT_NAME builder_name
,f.PHASE cmcphase_NeighCode
,f.LOT_NUMBER 
,dl.LCC03 lcc03_product_code
,dl.LCC03_DESCRIPTION lcc03_product_code_desc
,dl.LCC05_LAND_TYPE lcc05_land_type
,dl.LCC05_DESCRIPTION lcc05_land_type_desc
,dl.LCC07_PRODUCT_CODE lcc07_land_type_desc
,dl.LCC07_DESCRIPTION lcc07_land_type_desc_desc
,dl.LCC02 unit_type
,dl.NET_ACREAGE
,case when dl.NET_SQ_FEET <> 0 then  dl.NET_SQ_FEET else dl.NET_ACREAGE/0.00002295684 end as net_sq_feet
,dl.GROSS_ACREAGE
,dl.NO_OF_UNITS
,t.TENANT_NAME buyer_name
,f.SALES_ACTIVITY_CODE_LAST  SalesActCD
,f.SALES_ACTIVITY_LAST SalesActCD_desc
,f.SALES_ACTIVITY_CODE_NEXT 
,f.SALES_ACTIVITY_NEXT  SALES_ACTIVITY_CODE_NEXT_desc
,f.SALES_CONTRACT_NUMBER
,f.SALES_CONTRACT_STATUS_CODE
,f.SALES_CONTRACT_STATUS SALES_CONTRACT_STATUS_desc
,f.SALES_DATE SMSDJ_ContractDate
,f.CLOSE_DATE
,f.TAKE_DOWN_DATE 
,f.BASE_PRICE
,f.TOTAL_SALES_PRICE
,f.MORTGAGE_APPROVAL_DATE SMMAD_ContractSignedDate
,f.GROSS_SALES_PRICE
,f.SID_ASSUMPTION
,f.NET_REVENUE
,f.COST_OF_SALES
,f.GROSS_MARGIN
,f.BUSINESS_UNIT
,bu.BU_CAT_CD24
,bu.BU_CAT_CD11 OperPropCD
from FACT_MPC_SALES   f
inner join DIM_LOT dl on f.LOT_KEY =dl.LOT_KEY
 inner join datalake_dev.semantic.DIM_BUSINESS_UNIT   bu on f.business_unit =bu.business_unit
left join  datalake_dev.semantic.DIM_TENANT     t on t.tenant_id =f.BUYER_NUMBER
 )
 select * 
 ,case when NET_ACREAGE = 0 then 0 else (ifnull(GROSS_SALES_PRICE, 0) + ifnull(SID_ASSUMPTION, 0)) /NET_ACREAGE end as  Price_Per_Acre
	,case when NET_SQ_FEET = 0 then 0 else (ifnull(GROSS_SALES_PRICE, 0) + ifnull(SID_ASSUMPTION, 0)) / NET_SQ_FEET end as Price_Per_Sqft
 from 
 base
 where CLOSE_DATE>'01/01/2019'::date
  );
