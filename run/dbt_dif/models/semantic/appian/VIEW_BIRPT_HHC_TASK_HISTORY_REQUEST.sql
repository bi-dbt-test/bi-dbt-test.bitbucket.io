
  create or replace  view datalake_dev.semantic.VIEW_BIRPT_HHC_TASK_HISTORY_REQUEST  as (
    
SELECT
  tsk.taskHistoryId,
  lkp6.shortDescription AS taskType,
  lkp7.shortDescription AS taskStatus,
  lkp1.shortDescription AS approvalDecision,
  tsk.taskName,
  tsk.startTime,
  tsk.endTime,
  tsk.assignees,
  tsk.isActive,
  tsk.createdBy,
  tsk.createdDate,
  tsk.lastUpdatedBy,
  tsk.lastUpdatedDate,
  tsk.dueDate,
  lkp8.shortDescription AS priority,
  tsk.description,
  req.requestId,
  lkp2.shortDescription AS requestType,
  lkp3.shortDescription AS requestStatus,
  lkp4.shortDescription AS requestOutcome,
  req.description AS requestDescription,
  req.isParent,
  req.parentRequestId,
  lkp5.shortDescription AS workflowStatus,
  wkf.createdDate AS workflowStartDate,
  (CASE WHEN wkf.statusId=7002 THEN wkf.lastUpdatedDate ELSE NULL END) AS workflowEndDate
FROM  datalake_dev.governed.APPIAN_HHC_TASKHISTORY   tsk
LEFT JOIN  datalake_dev.governed.APPIAN_DRM_REQUEST     req ON req.requestId=tsk.entityId
LEFT JOIN  datalake_dev.governed.APPIAN_HHC_LOOKUP      lkp1 ON lkp1.lookupId=tsk.approvalDecisionLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP  lkp2 ON lkp2.lookupId=req.requestTypeLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP   lkp3 ON lkp3.lookupId=req.requestStatusLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP   lkp4 ON lkp4.lookupId=req.requestOutcomeLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_WORKFLOW     wkf ON wkf.entityId=req.requestId AND wkf.entityTypeId=1
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP     lkp5 ON lkp5.lookupId=wkf.statusId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP     lkp6 ON lkp6.lookupId=tsk.taskTypeLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP      lkp7 ON lkp7.lookupId=tsk.taskStatusLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP      lkp8 ON lkp8.lookupId=tsk.priorityId
WHERE tsk.entityTypeLookupId=1 AND req.requestId IS NOT NULL
 and  req.isActive=1
  );
