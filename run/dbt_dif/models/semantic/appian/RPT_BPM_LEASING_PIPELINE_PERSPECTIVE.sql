
  create or replace  view datalake_dev.semantic.RPT_BPM_LEASING_PIPELINE_PERSPECTIVE  as (
    
select ifnull(a.NAME,ls.LEASE_NAME) TENANT_NAME ,
 spc.NAME unit
 ,ls.lease_stage stage_c
 ,ifnull(ls.SQ_FOOTAGE_OVERRIDE,ls.SQFOOTAGE)  sq_footage
 ,ls.RATE_COMMENCEMENT_DT
 ,ifnull(TA_Rate_PSF_Achieved,0) Actual_TA
 ,'??' "Sq footage%"
 ,right('00'|| prop.MCRP24,3) Property_CC24 
-- ,lease_stage
 from datalake_dev.semantic.DIM_SF_LEASE   ls  
 inner join datalake_dev.semantic.DIM_SF_ACCOUNT   a on a.account_id = ls.tenant_id
 inner join datalake_dev.semantic.BRG_SF_LEASE_SPACE   lsp on lsp.lease_id = ls.lease_id
 inner join datalake_dev.semantic.DIM_SF_PROPSPACE    spc on spc.space_id = lsp.space_id
 inner join datalake_dev.semantic.DIM_SF_PROPERTY  prop on prop.property_id = spc.property_id
 WHERE spc.Space_Type  IS NOT NULL
            and ls.lease_stage = 'Legal Review'
			and
				(ls.Fully_Executed_Dt BETWEEN  DATEADD(DAY, -60,CURRENT_TIMESTAMP::date) AND CURRENT_TIMESTAMP::date
			 OR ls.Assigned_to_Legal_Dt BETWEEN DATEADD(DAY, -60,CURRENT_TIMESTAMP::date ) AND CURRENT_TIMESTAMP::date
			 OR ls.LOI_to_Tenant_Dt BETWEEN DATEADD(DAY, -60,CURRENT_TIMESTAMP::date) AND CURRENT_TIMESTAMP::date
			 OR ls.SF_Created_Dt BETWEEN DATEADD(DAY, -60,CURRENT_TIMESTAMP::date ) AND CURRENT_TIMESTAMP::date
				)
 order by PROPERTY_CC24,ls.lease_stage
  );
