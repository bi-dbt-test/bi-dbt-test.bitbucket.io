
  create or replace  view datalake_dev.semantic.VIEW_BIRPT_HOME_FEEDBACK  as (
    
SELECT
  fbk.feedbackId,
  fbk.starRating,
  fbk.description,
  lkp.shortDescription AS feedbackType,
  fbk.createdBy,
  fbk.createdDate,
  fbk.lastUpdatedBy,
  fbk.lastUpdatedDate,
  fbk.isActive
FROM  datalake_dev.governed.APPIAN_HOME_FEEDBACK    fbk
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP   lkp ON lkp.lookupId=fbk.feedbackTypeLookupId
  );
