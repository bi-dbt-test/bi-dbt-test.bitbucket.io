
  create or replace  view datalake_dev.semantic.RPT_BPM_LOTAVAILABILITY  as (
    
With account_list 
As
(
Select   distinct case when CHARINDEX('.', DESCRIPTION) > 1 then LEFT(DESCRIPTION, CHARINDEX('.', DESCRIPTION) - 1) else DESCRIPTION end ObjectAccount
,case when CHARINDEX('.', DESCRIPTION) > 1 then SUBSTRING(DESCRIPTION, CHARINDEX('.', DESCRIPTION) + 1, length(DESCRIPTION)) else '' end SubAccount
from  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES  
 Where ProductCode = '55' And UserDefinedCodes = 'H3' And SpecialHandlingCode = 4
)
, costofsales As
(Select BusinessUnit, FiscalYear, sum(amtbeginningbalancepy + amountwtd)/100 as CostOfSales 
FROM  datalake_dev.governed.JDE_ACCOUNT_BALANCES      b
inner join account_list al on b.ObjectAccount=al.OBJECTACCOUNT and b.subsidiary=al.SUBACCOUNT
where  LedgerType = 'AA'
Group by BusinessUnit, FiscalYear
)--select * from costofsales
, LotAvailability 
AS
(select sm.salesactivitycodelast as SalesActCD, c2.description as last_status, sm.Community  as Community
 , lm.lotcategory03 as lcc03_product_code,c6.description as lcc03_product_code_desc
 , lm.lotcategory01 as lmlcc01_ProductType,
sm.Phase, mbu.description as BU_Desc_Full, lm.legallotnumber as legal_lot
 ,lm.homebuilderblock as Block,lm.ParcelNumber as LMPARC_ParcelNumber
 ,lm.homebuildertract as SectionNumber
 ,sm.homebuilderlotnumber as smhblot_LotNumber, 
case when sm.homebuilderlotnumber is null then 0 else 1 end as lot_count,  sm.homebuilderarea as Area,c4.description as Area_Code_Desc, 						
lm.lotcategory05 as lcc05_land_type,c7.description as lcc05_land_type_desc,			
lm.lotcategory06 as  lcc06_land_usage 
 , lm.amountuserdefinedamount02/100 as UAMT02_Net_acreage, lm.lotsquarefootage as LotSqFt
 , lm.amountuserdefinedamount04/100 as UAMT04_numofLots,					
sm.salesactivitycodenext as SalesActCDNxt, c3.description as SalesActCDNxt_desc, sm.SalesContractStatus
 , c1.description as sales_contract_status_desc,					
sm.salesdate SMSDJ_ContractDate,					
sm.closedate SMCDJ_CloseDate,			
lm.amountbaseprice/100 as bsp_baseprice,sm.basehousesalesprice/100 as F44H501_bhprice_base_price,
sm.smlotprem/100 as smlotprem_LotPremium,pm.amountbaseprice/100 as F44H301_BasePrice,
case when sm.basehousesalesprice = 0 then pm.amountbaseprice/100 else sm.basehousesalesprice/100 end as Base_Lot_Sales_Price,
case 
	when CostOfSales is not null then CostOfSales 
	when CostOfSales is null then sm.amountuserdefinedamount10/10000
End as Cost_of_Sales,
lm.amountuserdefinedamount02/100 as net_acreage, 
case when lm.lotsquarefootage <> 0 then lm.lotsquarefootage else (lm.amountuserdefinedamount02/100)/0.00002295684 end as net_sq_feet, 		
sm.mortgageapprovaldate SMMAD_ContractSignedDate,		
bu.categorycodecostcenter24 BUCatCD24,mbu.company, bu.categorycodecostct011 OperPropCD
from  datalake_dev.governed.JDE_SALES_MASTER_TABLE    sm			
left outer join datalake_dev.governed.JDE_BUSINESS_UNIT_MASTER     mbu on LTRIM(rtriM(sm.Community)) = LTRIM(rtrim(mbu.BusinessUnit))			
left outer join  datalake_dev.governed.JDE_LOT_MASTER      lm on  sm.Community=lm.Community and sm.homebuilderlotnumber = lm.homebuilderlotnumber 			
left outer join   datalake_dev.governed.JDE_PLAN_MASTER      pm on   sm.Community=pm.Community  and sm.Phase = pm.Phase	and sm.homebuilderarea = pm.homebuilderarea and lm.plan = pm.plan	
left outer join    datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES      c1 on ltrim(rtrim(sm.SalesContractStatus)) = ltrim(rtrim(c1.userdefinedcode)) and c1.productcode = '44H5' and c1.userdefinedcodes = 'ST' 			
left  outer join    datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     c2 on ltrim(rtrim(sm.salesactivitycodelast)) = ltrim(rtrim(c2.userdefinedcode)) and c2.productcode = '44H0' and c2.userdefinedcodes = 'RL' 			
left  outer join datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES      c3 on ltrim(rtrim(sm.salesactivitycodenext)) = ltrim(rtrim(c3.userdefinedcode)) and c3.productcode = '44H0' and c3.userdefinedcodes = 'RL' 			
left outer join datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES    c4 on LTRIM(rtrim(sm.homebuilderarea)) = LTRIM(rtrim(c4.userdefinedcode)) and c4.productcode = '00' and c4.userdefinedcodes = '05'			
left outer join  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES   c6 on LTRIM(rtrim(lm.lotcategory03)) = LTRIM(rtrim(c6.userdefinedcode)) and c6.productcode = '44H2' and c6.userdefinedcodes = '03'			
left outer join  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES    c7 on LTRIM(rtrim(lm.lotcategory05)) = LTRIM(rtrim(c7.userdefinedcode)) and c7.productcode = '44H2' and c7.userdefinedcodes = '05'			
left outer join  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES      c8 on LTRIM(rtrim(lm.lotcategory06)) = LTRIM(rtrim(c8.userdefinedcode)) and c8.productcode = '44H2' and c8.userdefinedcodes = '06'			
--left outer join "GOVERNED"."JDE_ADDRESS_BOOK_MASTER" t on sm.buyernumbera = t.addressnumber and t.addresstype1 = 'BD'	
left outer join datalake_dev.governed.JDE_BUSINESS_UNIT_MASTER    bu on sm.BusinessUnit = bu.BusinessUnit		
left outer join costofsales CO on sm.BusinessUnit = CO.BusinessUnit 
	and RIGHT(YEAR(CURRENT_DATE), 2) = CO.FiscalYear
where sm.salesactivitycodelast in (500,590,600,650,925,950) and sm.closedate is null and lm.lotcategory03 <> 'NOS'	
--and bu.categorycodecostct011 = case when $bucatcd24 is null then bu.categorycodecostct011 else $bucatcd24 end
)-- select * from LotAvailability
Select *,
	(Base_Lot_Sales_Price +  smlotprem_LotPremium ) as  total_sales_price , 
	( Base_Lot_Sales_Price +  smlotprem_LotPremium)/ lot_count as Avg_Lot_Price ,
	 net_sq_feet  /  lot_count  as  avg_sqft ,
	         net_acreage  /  lot_count  as  avg_acreage ,
	case when net_sq_feet = 0 then 0 else ROUND(( Base_Lot_Sales_Price  +  smlotprem_LotPremium ) / ( net_sq_feet  /  lot_count ), 2) end as  Avg_Price_per_Sqft ,
	case when  net_acreage  = 0 then 0 else ROUND(( Base_Lot_Sales_Price  +  smlotprem_LotPremium )/ ( net_acreage /  lot_count ), 0) end as  Avg_Price_per_acreage 
From LotAvailability
  );
