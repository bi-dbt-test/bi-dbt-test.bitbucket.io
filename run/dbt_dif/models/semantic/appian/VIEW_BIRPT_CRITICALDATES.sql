
  create or replace  view datalake_dev.semantic.VIEW_BIRPT_CRITICALDATES  as (
    
SELECT APPIAN_CRD.criticalDateId,
      APPIAN_CRD.criticalDate,
      APPIAN_CRD.dateTypeId,
      APPIAN_CRD.context,
      APPIAN_CRD.entityId,
      dateType.externalEntityTypeId,
COALESCE(NULLIF(APPIAN_CRD.titleOverride,''), dateType.name) AS dateTypeName,
      dateType.description AS dateTypeDescription,
      dateTypeCustom.dateTypeCustomId,
      COALESCE(dateTypeCustom.categoryId, externalEntityType.categoryId) AS categoryId,
      COALESCE(dateTypeCustomCategory.shortDescription, externalEntityTypeCat.shortDescription) AS category,
      dateTypeCustom.catCode24,
      dateTypeCustom.frequencyId,
      --coalesce(dateTypeCustom.isPrivate, cast(0 AS bit)) AS isPrivate,
      dateTypeCustom.createdBy AS customCreatedBy,
      frequency.shortDescription AS frequency,
      externalEntityType.sourceSystemLookupId,
      sourceSystem.shortDescription AS sourceSystem,
      dateType.isActive AS dateTypeIsActive,
      APPIAN_CRD.isActive AS criticalDateIsActive,
      CASE WHEN task.taskId IS NULL THEN 0 ELSE 1 END AS hasTask,
      taskStatus.shortDescription AS taskStatus
  FROM  datalake_dev.governed.APPIAN_CRD_CRITICALDATE   APPIAN_CRD
  INNER JOIN  datalake_dev.governed.APPIAN_CRD_DATETYPE    dateType on APPIAN_CRD.dateTypeId = dateType.dateTypeId
  LEFT JOIN datalake_dev.governed.APPIAN_CRD_DATETYPECUSTOM    dateTypeCustom on APPIAN_CRD.dateTypeId = dateTypeCustom.dateTypeCustomId
  LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP      dateTypeCustomCategory on dateTypeCustom.categoryId = dateTypeCustomCategory.lookupId
  LEFT JOIN datalake_dev.governed.APPIAN_CRD_EXTERNALENTITYTYPE      externalEntityType on dateType.externalEntityTypeId = externalEntityType.externalEntityTypeId
  LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP    sourceSystem on externalEntityType.sourceSystemLookupId = sourceSystem.lookupId
  LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP      externalEntityTypeCat on externalEntityType.categoryId = externalEntityTypeCat.lookupId
  LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP    frequency on dateTypeCustom.frequencyId = frequency.lookupId
  LEFT JOIN datalake_dev.governed.APPIAN_HHC_TASKHISTORY     task ON task.entityId = APPIAN_CRD.criticalDateId AND task.entityTypeLookupId = 17
  LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP     taskStatus ON taskStatus.lookupId = task.taskStatusLookupId
  );
