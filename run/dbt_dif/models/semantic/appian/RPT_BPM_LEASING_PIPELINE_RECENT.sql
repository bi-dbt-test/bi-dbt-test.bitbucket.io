
  create or replace  view datalake_dev.semantic.RPT_BPM_LEASING_PIPELINE_RECENT  as (
    
with base
as
(
select ls.Fully_Executed_Dt execution_date
  , spc.NAME unit
  ,ifnull(a.TRADE_NAME,ls.LEASE_NAME) TENANT_NAME
  ,ifnull(ls.SQ_FOOTAGE_OVERRIDE,ls.SQFOOTAGE) sq_footage
   ,ls.RATE_COMMENCEMENT_DT
   ,ifnull(TA_Rate_PSF_Achieved,0) Actual_TA
   ,'??' "Sq footage%"
   ,right('00'|| prop.MCRP24,3) Property_CC24
  
  from datalake_dev.semantic.DIM_SF_LEASE  ls  
 inner join datalake_dev.semantic.DIM_SF_ACCOUNT  a on a.account_id = ls.tenant_id
 inner join datalake_dev.semantic.BRG_SF_LEASE_SPACE   lsp on lsp.lease_id = ls.lease_id
 inner join datalake_dev.semantic.DIM_SF_PROPSPACE   spc on spc.space_id = lsp.space_id
 inner join datalake_dev.semantic.DIM_SF_PROPERTY    prop on prop.property_id = spc.property_id
 WHERE spc.Space_Type  IS NOT NULL
  and ls.Fully_Executed_Dt >= DATEADD(DAY, -60,CURRENT_TIMESTAMP::date ) 
)
 
 select min(execution_date) execution_date
 ,TENANT_NAME 
 ,sum(sq_footage) sq_footage
 ,sum(Actual_TA) Actual_TA
,max(RATE_COMMENCEMENT_DT) RATE_COMMENCEMENT_DT
,"Sq footage%"
,max(Property_CC24) Property_CC24
 FROM BASE
			GROUP BY TENANT_NAME, "Sq footage%" 
 order by execution_date
  );
