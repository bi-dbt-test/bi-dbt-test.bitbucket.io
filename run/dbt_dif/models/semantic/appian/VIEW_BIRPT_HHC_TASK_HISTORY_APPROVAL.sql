
  create or replace  view datalake_dev.semantic.VIEW_BIRPT_HHC_TASK_HISTORY_APPROVAL  as (
    
SELECT
tsk.taskHistoryId,
lkp2.shortDescription AS taskType,
lkp3.shortDescription AS taskStatus,
lkp1.shortDescription AS approvalDecision,
tsk.taskName,
tsk.startTime,
tsk.endTime,
tsk.assignees,
tsk.isActive,
tsk.createdBy,
tsk.createdDate,
tsk.lastUpdatedBy,
tsk.lastUpdatedDate,
tsk.dueDate,
lkp4.shortDescription AS priority,
tsk.description,
NULL AS approvalId,
NULL AS coreSystemId,
NULL AS coreSystem,
NULL AS approver,
NULL AS coreSystemWorkflowId,
NULL AS coreSystemTaskId,
NULL AS approvalTaskType
FROM  datalake_dev.governed.APPIAN_HHC_TASKHISTORY     tsk
INNER JOIN  datalake_dev.governed.APPIAN_HHC_LOOKUP      tskType ON tsk.taskTypeLookupId = tskType.lookupId
LEFT JOIN   datalake_dev.governed.APPIAN_HHC_LOOKUP     lkp1 ON lkp1.lookupId=tsk.approvalDecisionLookupId
LEFT JOIN  datalake_dev.governed.APPIAN_HHC_LOOKUP      lkp2 ON lkp2.lookupId=tsk.taskTypeLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP      lkp3 ON lkp3.lookupId=tsk.taskStatusLookupId
LEFT JOIN datalake_dev.governed.APPIAN_HHC_LOOKUP    lkp4 ON lkp4.lookupId=tsk.priorityId
WHERE tskType.longDescription = 'Approval'
  );
