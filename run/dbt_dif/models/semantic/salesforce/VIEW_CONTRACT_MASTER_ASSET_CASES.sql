
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_MASTER_ASSET_CASES  as (
    

select distinct case when replace(cs.BUILDING_C, '''','') = '' then 'Unknown' else replace(cs.BUILDING_C, '''','')  end as Building
,cs.BUILDING_C as Building_Actual
FROM  datalake_dev.governed.SALESFORCE_CASE     cs
order by Building
  );
