
  create or replace  view datalake_dev.semantic.VIEW_WARD_VILLAGE_UNITS  as (
    

SELECT 
    'Total' as Unit_Type,
	Property_id as PropertyID,
	Building as Building, 
    Name as Unit_Numer,
	1 AS Unit_Count,
	IFNULL(Unit_Price,0) AS Rev,
	IFNULL(Turn_Key_option_Unit_Price,0)Turn_Key_option_Unit_Price, 
	Furniture_Option,
	EST_NET_LIVING_AREA_SF AS Est_Living_Area,
    CASE    WHEN Property_ID in ('a00i0000007XwkmAAC', 'a00i0000007XwhIAAS','a00i000000dQBgGAAW','a001Y00000p4WzaQAE') then 1
            WHEN Property_ID in ('a00i000000gGeW9AAK','a00i000000nbCAIAA2') then 2
            ELSE 0 END as SubtotalGroupBy,
    NULL as CONTRACT_SIGNED_BY_SELLER,
    Null AS Daily,
    Null AS PrevTwoDays,
    Null AS Weekly,
    Null AS ThirtyDays,
    Null AS MTD_BeginMo,
    Null AS NinteyDays,
    Null AS YTD_BeginYr

from (
SELECT DISTINCT s.PROPERTY_ID,s.NAME ,s.Building, s.Unit_Price, s.Furniture_Option,Turn_Key_option_Unit_Price,EST_NET_LIVING_AREA_SF
  FROM datalake_dev.semantic.DIM_SF_PROPSPACE   s
INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE  r on s.Record_Type_ID = r.Record_Type_ID
WHERE r.Record_Type_ID = '012i0000000mNx7AAE'
and s.Space_Status <> 'Not Available'
and s.Building is not NULL
and s.IsDeleted = 'False'
and s.Building <> 'Gateway Cylinder')

 
Union ALL

  SELECT 
    'Sold' as Unit_Type,
	Property_ID as PropertyID,
	Building as Building, 
    Name as Unit_Numer,
	1 AS Unit_Count,
	IFNULL(Unit_Price,0) AS Rev,
	IFNULL(Turn_Key_option_Unit_Price,0)Turn_Key_option_Unit_Price, 
	Furniture_Option,
	EST_NET_LIVING_AREA_SF AS Est_Living_Area,
    CASE    WHEN Property_ID in ('a00i0000007XwkmAAC', 'a00i0000007XwhIAAS','a00i000000dQBgGAAW','a001Y00000p4WzaQAE') then 1
            WHEN Property_ID in ('a00i000000gGeW9AAK','a00i000000nbCAIAA2') then 2
            ELSE 0 END as SubtotalGroupBy,
    CONTRACT_SIGNED_BY_SELLER as CONTRACT_SIGNED_BY_SELLER,
    DATEADD(dd,-1,CURRENT_DATE) AS Daily,
    DATEADD(dd,-2,CURRENT_DATE) AS PrevTwoDays,
    DATEADD(dd,-7,CURRENT_DATE) AS Weekly,
    DATEADD(dd,-30,CURRENT_DATE) AS ThirtyDays,
    DATEADD(dd,-(Day(CURRENT_DATE)-1), CURRENT_DATE) AS MTD_BeginMo,
    DATEADD(dd,-90,CURRENT_DATE) AS NinteyDays,
    DATE_TRUNC(year, CURRENT_DATE()) AS YTD_BeginYr
  
from (
SELECT DISTINCT s.Property_ID,s.NAME ,s.Building, s.Unit_Price, s.Furniture_Option,Turn_Key_option_Unit_Price,EST_NET_LIVING_AREA_SF,CONTRACT_SIGNED_BY_SELLER
FROM  datalake_dev.semantic.DIM_SF_OPPORTUNITY   o
INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE  s on o.SPACE_ID = s.SPACE_ID and o.IsDeleted = s.IsDeleted
INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE  r on s.Record_Type_ID = r.Record_Type_ID
WHERE r.Record_Type_ID = '012i0000000mNx7AAE'
and s.Space_Status <> 'Not Available'
and o.Building is not NULL
and s.IsDeleted = 'False'
and o.IsDeleted = 'False'
and o.IsWon = 1 
and s.Building <> 'Gateway Cylinder'
order by s.name desc
)

Union ALL


SELECT 
    'Unsold' as Unit_Type,
	Property_ID as PropertyID,
	Building as Building,
    Name as Unit_Numer,
	1 AS Unit_Count,
	IFNULL(Unit_Price,0) AS Revenue,
	IFNULL(Turn_Key_option_Unit_Price,0)Turn_Key_option_Unit_Price, 
	Furniture_Option,
	EST_NET_LIVING_AREA_SF AS Est_Living_Area,
    CASE    WHEN Property_ID in ('a00i0000007XwkmAAC', 'a00i0000007XwhIAAS','a00i000000dQBgGAAW','a001Y00000p4WzaQAE') then 1
            WHEN Property_ID in ('a00i000000gGeW9AAK','a00i000000nbCAIAA2') then 2
            ELSE 0 END as SubtotalGroupBy,
    NULL as CONTRACT_SIGNED_BY_SELLER,
    Null AS Daily,
    Null AS PrevTwoDays,
    Null AS Weekly,
    Null AS ThirtyDays,
    Null AS MTD_BeginMo,
    Null AS NinteyDays,
    Null AS YTD_BeginYr
  
from (
SELECT DISTINCT s.PROPERTY_ID,s.NAME ,s.Building, s.Unit_Price, s.Furniture_Option,Turn_Key_option_Unit_Price,EST_NET_LIVING_AREA_SF
FROM SEMANTIC.DIM_SF_PROPSPACE s
INNER JOIN SEMANTIC.LUP_SF_RECORD_TYPE r on s.Record_Type_ID = r.Record_Type_ID
WHERE r.Record_Type_ID = '012i0000000mNx7AAE'
and s.Space_Status <> 'Not Available'
and s.Building is not NULL
and s.IsDeleted = 'False'
and s.Building <> 'Gateway Cylinder'
and not exists (SELECT s2.name ,s2.Building
FROM datalake_dev.semantic.DIM_SF_OPPORTUNITY  o2
INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE   s2 on o2.SPACE_ID = s2.SPACE_ID and o2.IsDeleted = s2.IsDeleted
INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE    r2 on s2.Record_Type_ID = r2.Record_Type_ID
WHERE r2.Record_Type_ID = '012i0000000mNx7AAE'
and s2.Space_Status <> 'Not Available'
and s2.Building is not NULL
and s2.IsDeleted = 'False'
and o2.IsDeleted = 'False'
and o2.IsWon = 1 
and s2.Building <> 'Gateway Cylinder' 
and s.Name = s2.Name 
and s.Building = s2.Building)  
)
  );
