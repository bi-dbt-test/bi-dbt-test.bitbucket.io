
  create or replace  view datalake_dev.semantic.VIEW_SF_LEASING_ACTIVITY_COMBINED  as (
    
SELECT PL.Opportunity_Id,
       PL.Space_Id,
       L.Lease_Id,
       PL.Tenant_Id,
       PL.Property_Id,
       L.Lease_Stage AS LeaseStage_Descr,
       L.New_Renewal_Amendment,  
       L.Lease_Term Lease_Term,
       L.SqFootage,
       L.TA_RATE_PSF_ACHIEVED,
       L.TA_RATE_PSF_BUDGETED,
       L.MIN_RENT_ACHIEVED_PSF,
       L.MIN_RENT_BUDGETED_PSF,
       L.LANDLORD_WORK_EST_ACHIEVED,
       L.CAM_ACHIEVED,
       L.CAM_BUDGETED,
       L.BROKER_COMMISSIONS_ACHIEVED,
       L.BROKER_COMMISSIONS_BUDGETED,
       L.Fully_Executed_Dt,
       L.Assigned_to_Legal_Dt,
       L.LOI_to_Tenant_Dt,
       L.Tenant_REC_Dt,
       NULL Cancelled_Dt,
       L.SF_Created_Dt
FROM datalake_dev.semantic.FACT_LEASING_ACTIVITY     PL
JOIN datalake_dev.semantic.DIM_SF_LEASE   L ON L.LEASE_ID = PL.LEASE_ID
WHERE PL.LEASE_RNK = 1

UNION ALL

SELECT O.Opportunity_Id,
       OS.Space_Id,  
       NULL Lease_Id,
       O.Account_Id,
       O.Property_Id,
       O.Stage_Name,        
       O.New_Renewal_Amendment,
       O.Lease_Term,
       OS.Sq_Footage,
       O.TA_RATE_PSF_ACHIEVED,
       O.TA_RATE_PSF_BUDGETED,
       O.MIN_RENT_ACHIEVED_PSF,
       O.MIN_RENT_BUDGETED_PSF,
       O.LANDLORD_WORK_EST_ACHIEVED,
       O.CAM_ACHIEVED,
       O.CAM_BUDGETED,
       O.BROKER_COMMISSIONS_ACHIEVED,
       O.BROKER_COMMISSIONS_BUDGETED,
       NULL Fully_Executed_Dt,
       NULL Assigned_to_Legal_Dt,
       CV1.Tenant_Rec_Date LOI_to_Tenant_Dt,
       CV2.Tenant_Rec_Date Tenant_REC_Dt,
       O.Cancelled_Date,
       O.Created_Date
FROM datalake_dev.semantic.DIM_SF_OPPORTUNITY    O
INNER JOIN datalake_dev.semantic.BRG_SF_OPPORTUNITY_SPACE    OS ON O.Opportunity_Id = OS.Opportunity_Id
LEFT JOIN datalake_dev.semantic.DIM_SF_LEASE     L ON O.Opportunity_ID = L.Opportunity_ID
LEFT JOIN datalake_dev.semantic.DIM_SF_CONTENT_VERSION   CV1 ON O.Opportunity_ID = CV1.First_Publish_Location_Id AND CV1.NAMED_FILE_UPLOAD_TYPE_METADATA_NAME = 'Proposal_LOI_Sent'
LEFT JOIN datalake_dev.semantic.DIM_SF_CONTENT_VERSION    CV2 ON O.Opportunity_ID = CV2.First_Publish_Location_Id AND CV2.NAMED_FILE_UPLOAD_TYPE_METADATA_NAME = 'Proposal_LOI_Received'
WHERE L.Opportunity_ID IS NULL
AND O.Is_Leasing_Opportunity = TRUE
AND O.Stage_Name <> 'Closed Lost'
  );
