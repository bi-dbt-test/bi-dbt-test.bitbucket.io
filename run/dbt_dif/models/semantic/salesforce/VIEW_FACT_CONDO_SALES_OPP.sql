
  create or replace  view datalake_dev.semantic.VIEW_FACT_CONDO_SALES_OPP  as (
    

select cast(spc.id as varchar) space_id,cast(spc.Property_c as varchar) prop_id
,cast(IFNULL(opp.id,'UKN') as varchar) Oppty_id,cast(IFNULL(opp.ownerid,'UKN') as varchar) UserID
,cast(IFNULL(opp.AccountId,'UKN') as varchar) AccountID,
IFNULL(opp.Unit_Price_c,0) Unit_Price_c,IFNULL(opp.Initial_Deposit_Amount_c,0) Initial_Deposit_Amount_c, 
IFNULL(opp.Initial_Deposit_Received_Amount_c,0) Initial_Deposit_Received_Amount_c, IFNULL(opp.X2nd_Deposit_Amount_c,0) X2nd_Deposit_Amount_c,IFNULL(opp.X2nd_Deposit_Amount_Received_c,0) X2nd_Deposit_Amount_Received_c,
IFNULL(opp.X2nd_Deposit_Percentage_c,0) X2nd_Deposit_Percentage_c,IFNULL(opp.X3rd_Deposit_Amount_c,0) X3rd_Deposit_Amount_c,IFNULL(opp.X3rd_Deposit_Amount_Received_c,0) X3rd_Deposit_Amount_Received_c,
IFNULL(opp.X3rd_Deposit_Percentage_c,0) X3rd_Deposit_Percentage_c, IFNULL(spc.Proforma_Unit_Price_c,0) Proforma_Price, IFNULL(Est_Net_Living_Area_SF_c,0) Est_Net_Living_Area_SF,IFNULL(opp.Total_Credits_c,0) Total_Credits_c,
IFNULL(opp.Primary_Owner_Share_c,0) Primary_Owner_Share_c,IFNULL(opp.Secondary_Owner_Share_c,0) Secondary_Owner_Share_c, IFNULL(spc.Loan_List_Price_c,0) Loan_List_Price, IFNULL(opp.Tertiary_Owner_Share_c,0) Tertiary_Owner_Share_c
from  datalake_dev.governed.SALESFORCE_PROPERTY__C    prop
inner join datalake_dev.governed.SALESFORCE_SPACE__C     spc on prop.id = spc.Property_c
left outer join (select * from (select a.*,row_number() over (partition by unit_number_c order by stagename,cancelled_date_c desc) rn 
from datalake_dev.governed.SALESFORCE_OPPORTUNITY       a) as b where rn=1) opp
on spc.id = opp.Unit_Number_c
where prop.Property_Type_c = 'Residential'
and spc.space_Status_c not in ('Not Available')
  );
