

      create or replace  table datalake_dev.semantic.FACT_LEASING_ACTIVITY  as
      (-- CREATE OR REPLACE TABLE SEMANTIC. _STG_JDE_SF_FACT_LEASING_ACTIVITY  AS

SELECT 
       LS.LEASE_ID,
       PS.space_id, 
       PR.PROPERTY_ID ,
       LES.opportunity_ID,
      MP.PROPERTY_ID AS PARENT_PROPERTY_ID ,
       LES.TENANT_ID,
    RT.RECORD_TYPE_ID,
--    NPV.NPV_ID,
  PS.NAME AS SF_UNIT_NBR ,
    PS.FLOOR,
   CASE WHEN LENGTH(PS.FLOOR) <4 THEN LPAD(CAST(PS.FLOOR AS VARCHAR(4)),4 ,'0') ELSE PS.FLOOR END AS SF_FLOOR_NBR,
       PR.COMPANY_CODE AS COMPANY,
   PR.JDE_PROPERTY_ID, 
     LPAD(PR.MCRP24, 3, '0') AS BU_CC24, 
   BU.BUSINESS_UNIT AS BASE_BUSINESS_UNIT,
      DU.BUSINESS_UNIT,
      BU.DESCRIPTION AS JDE_COMPANY_DESC, 
      DU.FLOOR_NBR AS JDE_FLOOR,  
      DU.UNIT_KEY,LES.LEASE_STAGE ,
      -- Check and confirm where STAGE_NAME is coming from
      oppo.STAGE_NAME,REASON_FOR_PURCHASE,
       case LES.LEASE_STAGE when 'Submitted for Approval' then 2
                    WHEN 'Legal Review' THEN 2
                    WHEN 'Deviation' THEN 2
                    WHEN 'Submitted for Approval' THEN 2
                    WHEN 'Executed' THEN 1
                    WHEN 'Ended' THEN 3
                    ELSE 5 END STAGE_sort ,
       ROW_NUMBER() OVER (PARTITION BY PS.SPACE_ID 
                   ORDER BY 
                   STAGE_sort, 
                    CASE WHEN LES.NEW_RENEWAL_AMENDMENT = 'New' THEN 1 
                                                                ELSE 2 
                     END, 
                     LES.FULLY_EXECUTED_DT  DESC NULLS LAST
 			  ) AS LEASE_RNK    ,    
 				ROW_NUMBER() OVER (PARTITION BY PS.SPACE_ID 
                   ORDER BY 
                     STAGE_sort,  
                   LES.FULLY_EXECUTED_DT DESC NULLS LAST, SF_CREATED_DT desc
                                       
				  )AS Stacking_Plan_RNK,
     CURRENT_TIMESTAMP() AS CREATE_DT 
FROM  datalake_dev.semantic.DIM_SF_PROPERTY PR
LEFT OUTER JOIN datalake_dev.semantic.DIM_SF_PROPERTY MP
             ON PR.MASTER_PROPERTY_ID = MP.PROPERTY_ID
LEFT OUTER JOIN datalake_dev.semantic.DIM_BUSINESS_UNIT    BU
             ON PR.COMPANY_CODE  = BU.COMPANY
            AND LPAD(PR.MCRP24, 3, '0')  = BU.BU_CAT_CD24
            AND BU.BUSINESS_UNIT = BU.COMPANY_RPT_BUSINESS_UNIT
            AND PR.COMPANY_CODE  = SUBSTR(BU.BUSINESS_UNIT, 1, 5)  
LEFT OUTER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE PS  
             ON PS.PROPERTY_ID = PR.PROPERTY_ID    
LEFT OUTER JOIN    datalake_dev.semantic.DIM_UNIT   DU
             ON PR.COMPANY_CODE  = SUBSTR(DU.BUSINESS_UNIT, 1, 5)
            AND CURRENT_DATE() BETWEEN UNIT_BEGIN_EFF_DATE AND IFNULL(UNIT_END_EFF_DATE, CURRENT_DATE()+5000)               
            AND PS.NAME = DU.UNIT_NBR
LEFT OUTER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE RT
             ON MP.RECORD_TYPE_ID = RT.RECORD_TYPE_ID            
LEFT OUTER JOIN  datalake_dev.semantic.BRG_SF_LEASE_SPACE LS
             ON LS.SPACE_ID = PS.SPACE_ID
--LEFT OUTER JOIN SEMANTIC.DIM_SF_NPV_ANALYSIS NPV 
--             ON LS.LEASE_ID = NPV.LEASE_ID 
LEFT OUTER JOIN datalake_dev.semantic.DIM_SF_LEASE LES
             ON LS.LEASE_ID = LES.LEASE_ID 
left join datalake_dev.semantic.DIM_SF_OPPORTUNITY  oppo on oppo.opportunity_ID=LES.opportunity_ID
      );
    