
  create or replace  view datalake_dev.semantic.VIEW_CONDO_ALL_AGENTS_JAPAN  as (
    
SELECT DISTINCT 
            CASE WHEN U.Country = 'Japan'  THEN 'Japan'
            ELSE
			concat(U.First_Name,' ', U.LastName) END AS WV_Agent, 
			U.Country AS WV_Agent_Country,
            CAST(U.WV_INTERNAL_AGENT AS INT) as  WV_INTERNAL_AGENT,
            3 as WV_INTERNAL_AGENT_ALL,
            U.ISACTIVE
FROM  datalake_dev.semantic.DIM_SF_USER   U
WHERE U.FIRST_NAME != 'UNKNOWN'
  );
