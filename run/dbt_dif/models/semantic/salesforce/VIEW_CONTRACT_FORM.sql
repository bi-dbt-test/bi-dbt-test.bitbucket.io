
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_FORM  as (
    
WITH Contract_Form_By_Record_Type
AS
(
  SELECT
  prop.id AS PropertyId2
  ,prop.Name AS PropertyName2
  ,CASE WHEN rec2.Name = 'Ward Residential' THEN 'a00i0000003VDdSAAW' -- Assign Ward Village Master property
    ELSE prop.MasterProperty_c END  AS MasterPropertyId2
  ,prop.RecordTypeId AS PropertyRecordTypeId
  ,rec2.Name AS PropertyRecordTypeDesc
  ,CASE WHEN rec2.name  = 'HHC Master Property'
		THEN prop.MasterProperty_c  
		ELSE CASE WHEN rec2.name = 'Ward Residential' THEN 'a00i0000003VDdSAAW' END -- Assign Ward Village Master property
		END AS MasterPropertyId3
  ,CASE WHEN rec2.name in ('HHC Development Property','HHC Operating Property','HHC Property')
		THEN prop.Id END AS PropertyId3
  ,rec.name AS ContractFormType   
  ,cf.BUDGETED_C
  ,cf.CAM_Date_Submitted_c
  ,cf.CAM_Decision_Maker_Approval_Date_c
  ,cf.Contract_Effective_Date_c
  ,cf.Contract_Expiration_Date_c
  ,cf.Contract_Out_For_Signature_c
  ,cf.Contract_Status_C
  ,cf.CONTRACT_TYPE_C
  ,cf.COUNTERPARTY_COMMENT_RCVD_HISTORY_C
  ,cf.COUNTERPARTY_NAME_C
  ,cf.DATE_ASSIGNED_TO_LEGAL_C
  ,cf.DATE_SUBMITTED_C
  ,cf.DECISION_MAKER_C
  ,cf.Department_c
  ,cf.Execution_Comments_c
  ,cf.Fully_Approved_Date_c
  ,cf.Fully_Executed_Date_c
  ,cf.ID
  ,cf.INITIAL_CONTRACT_SENT_TO_COUNTERPARTY_C
  ,cf.Is_the_Amount_Payable_or_Receivable_c
  ,cf.LEGAL_COMMENTS_HISTORY_C
  ,cf.Legal_Duration_c
  ,cf.Legal_Review_Date_c
  ,cf.LEGAL_TEAM_MEMBER_C
  ,cf.LL_s_Response_Sent_c
  ,cf.Master_Asset_c 
  ,cf.Name
  ,cf.Property_c
  ,cf.RecordTypeID
  ,cf.Signatory_c
  ,cf.Submitted_to_Designee_c
  ,cf.Total_Receivable_Amount_c
  ,cf.TOTALAMOUNTPAYABLEUNDERCONTRACT_c
  ,cf.DECISION_MAKER_APPROVAL_DATE_C
  FROM    datalake_dev.governed.SALESFORCE_FORMS__C   cf 
  INNER JOIN datalake_dev.governed.SALESFORCE_RECORDTYPE    rec ON rec.ID = cf.RecordTypeId
  LEFT JOIN  datalake_dev.governed.SALESFORCE_PROPERTY__C     prop ON prop.id = cf.Property_c
  LEFT JOIN datalake_dev.governed.SALESFORCE_RECORDTYPE   rec2 ON rec2.ID = prop.RecordTypeId
)

SELECT 
	 CASE WHEN (MasterPropertyID3 IS NULL AND PropertyId3 IS NOT NULL)
		THEN prop3.MasterProperty_c
		ELSE CASE 
				WHEN PropertyRecordTypeDesc = 'HHC Master Property' AND PropertyID2 = 'a001Y00000p4JWgQAM' THEN MasterPropertyId2 -- Ward Entertainment Center - Garage
				WHEN PropertyRecordTypeDesc = 'HHC Master Property' THEN PropertyID2				
				WHEN PropertyRecordTypeDesc = 'Ward Residential' THEN MasterPropertyId2
			END 
		END MasterPropertyID  
	 ,CASE WHEN (cf.MasterPropertyID3 IS NULL AND cf.PropertyId3 IS NOT NULL)
		THEN prop4.Name
		ELSE CASE 
				WHEN PropertyRecordTypeDesc = 'HHC Master Property' AND PropertyID2 = 'a001Y00000p4JWgQAM' THEN prop5.Name -- Ward Entertainment Center - Garage
				WHEN PropertyRecordTypeDesc = 'HHC Master Property' THEN prop6.Name				
				WHEN PropertyRecordTypeDesc = 'Ward Residential' THEN prop5.Name
			END 
			END 
		AS MasterPropertyName
	,prop3.name  AS PropertyName
	,cf.*
    ,usr.Name AS Decision_MakerName
	,usr2.Name AS Legal_Team_MemberName
	,usr3.Name AS SignatoryName
	,cf.Name AS ContractFormRecType
	FROM Contract_Form_By_Record_Type cf
    LEFT JOIN datalake_dev.governed.SALESFORCE_PROPERTY__C   prop3 ON prop3.id = cf.PropertyID3
    LEFT JOIN datalake_dev.governed.SALESFORCE_PROPERTY__C    prop4 ON prop4.id = prop3.MasterProperty_c
    LEFT JOIN  datalake_dev.governed.SALESFORCE_PROPERTY__C    prop5 ON prop5.id = cf.MasterPropertyID2
    LEFT JOIN datalake_dev.governed.SALESFORCE_PROPERTY__C   prop6 ON prop6.id = cf.PropertyID2
    LEFT JOIN datalake_dev.governed.SALESFORCE_USER     usr ON usr.id = cf.Decision_Maker_c
    LEFT JOIN datalake_dev.governed.SALESFORCE_USER     usr2 ON usr2.id = cf.Legal_Team_Member_c
    LEFT JOIN datalake_dev.governed.SALESFORCE_USER     usr3 ON usr3.id = cf.Signatory_c
    LEFT JOIN datalake_dev.governed.SALESFORCE_RECORDTYPE      REC ON REC.ID = CF.RECORDTYPEID
  );
