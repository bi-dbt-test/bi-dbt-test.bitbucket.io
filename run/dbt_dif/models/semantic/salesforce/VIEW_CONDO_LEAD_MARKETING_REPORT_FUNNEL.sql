
  create or replace  view datalake_dev.semantic.VIEW_CONDO_LEAD_MARKETING_REPORT_FUNNEL  as (
    

SELECT IFNULL(NULLIF(LEAD.First_Contact,''), 'N/A') as LeadSource
,LEAD.CREATEDDATE AS LEAD_CREATE_DATE, Lead_ID
,CAST(USER1.WV_INTERNAL_AGENT AS INT) WV_INTERNAL_AGENT
,3 as WV_INTERNAL_AGENT_ALL
FROM datalake_dev.semantic.DIM_SF_LEAD  LEAD
INNER JOIN datalake_dev.semantic.DIM_SF_USER   USER1 on LEAD.OWNERID = USER1.USER_ID
where  LEAD.ISDELETED = 'FALSE'
and LEAD.Name <>'[not provided]'
  );
