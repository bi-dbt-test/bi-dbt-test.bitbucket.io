
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_LEASE_TYPE  as (
    
Select 
'Ground' as Label, 'Ground' as LeaseType
UNION
Select 'Office' as Label, 'Permanent (Office)' as LeaseType
UNION
Select 'Retail' as Label, 'Permanent (Retail)' as LeaseType
UNION
Select 'Development' as Label, 'Development' as LeaseType

Order by Label
  );
