
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_STAGE_TYPE  as (
    

select 'Submitted for Approval' as StageType
union
select 'Recalled by Dealmaker' as StageType
union
select 'Executed' as StageType
union
select 'Legal Review' as StageType
union
select 'Waiting for Legal Assignment' as StageType
  );
