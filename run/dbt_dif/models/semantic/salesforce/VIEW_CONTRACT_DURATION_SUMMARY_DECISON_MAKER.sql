
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_DURATION_SUMMARY_DECISON_MAKER  as (
    
SELECT IFNULL(Decision_MakerName,'Unknown') as Decision_MakerName, SUM(Number_of_Contracts) as Number_of_Contracts, 
sum(CRF_Approval_Duration)/SUM(Number_of_Contracts) as CRF_Duration,sum(Contract_Approval_Memo_Duration)/SUM(Number_of_Contracts) as CAM_Duration
FROM (
SELECT cf.Decision_MakerName, 1 as Number_of_Contracts
,DATEDIFF(day, cf.Date_Submitted_c, IFNULL(cf.Decision_Maker_Approval_Date_c,IFNULL(cf.Date_Assigned_to_Legal_c,IFNULL(Legal_Review_Date_c,IFNULL(CAM_Decision_Maker_Approval_Date_c,IFNULL(CAM_Date_Submitted_C,IFNULL(Fully_Approved_Date_C,Fully_Executed_Date_c))))))) AS CRF_Approval_Duration
,CASE WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.CAM_Decision_Maker_Approval_Date_c is null and CAM_Date_Submitted_c is not null and cf.Fully_Approved_Date_c is null
      THEN DATEDIFF(day, cf.CAM_Date_Submitted_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is null and cf.Fully_Approved_Date_c is null
      THEN DATEDIFF(day, cf.CAM_Decision_Maker_Approval_Date_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is not null and cf.Fully_Approved_Date_c is not null
      THEN DATEDIFF(day, CAM_Date_Submitted_c, cf.Fully_Approved_Date_c)
	  ELSE 0--DATEDIFF(day, cf.Date_Submitted_c,IFNULL(Fully_Approved_Date_c, getdate()))
	  END as Contract_Approval_Memo_Duration
 ,deu.NAME 
FROM datalake_dev.semantic.VIEW_CONTRACT_FORM  cf
LEFT JOIN datalake_dev.semantic.DIM_SF_USER  deu on cf.Decision_Maker_c = deu.User_ID
WHERE cf.Contract_Status_c NOT IN ('X: Terminated','X: Dead','S13: Dead','S12: Fully Executed','S0: Preparing Form','')
AND cf.Decision_MakerName is not null

UNION ALL

SELECT cf.Decision_MakerName, 1 as Number_of_Contracts
,DATEDIFF(day, cf.Date_Submitted_c, IFNULL(cf.Decision_Maker_Approval_Date_c,IFNULL(cf.Date_Assigned_to_Legal_c,IFNULL(Legal_Review_Date_c,IFNULL(CAM_Decision_Maker_Approval_Date_c,IFNULL(CAM_Date_Submitted_C,IFNULL(Fully_Approved_Date_C,Fully_Executed_Date_c))))))) AS CRF_Approval_Duration
,CASE WHEN cf.CAM_Decision_Maker_Approval_Date_c is null and CAM_Date_Submitted_c is not null and cf.Fully_Approved_Date_c is not null
      THEN DATEDIFF(day, cf.CAM_Date_Submitted_c, cf.Fully_Approved_Date_c)
	  WHEN cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is null and cf.Fully_Approved_Date_c is not null
      THEN DATEDIFF(day, cf.CAM_Decision_Maker_Approval_Date_c, cf.Fully_Approved_Date_c)
	  WHEN cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is not null and cf.Fully_Approved_Date_c is not null
      THEN DATEDIFF(day, CAM_Date_Submitted_c, cf.CAM_Decision_Maker_Approval_Date_c)
	  WHEN cf.CAM_Decision_Maker_Approval_Date_c is null and CAM_Date_Submitted_c is null
	  THEN 0
	  ELSE 0
	  END as Contract_Approval_Memo_Duration
,deu.NAME  
FROM datalake_dev.semantic.VIEW_CONTRACT_FORM   cf
LEFT JOIN  datalake_dev.semantic.DIM_SF_USER     deu on cf.Decision_Maker_c = deu.User_ID
WHERE cf.Contract_Status_c IN ('S12: Fully Executed','X: Terminated')
AND cf.Decision_MakerName is not null
) tab_a
WHERE
tab_a.NAME NOT IN (
SELECT NAMEEXCLUDED
FROM  datalake_dev.governed.STG_REPORT_EXCLUSION    -- datalake_dev.governed.STG_REPORT_EXCLUSION   
WHERE REPORTNAME='Contract Approval Duration Summary' AND REPORTSECTION='Decision Maker'
) 
GROUP BY Decision_MakerName
ORDER BY Decision_MakerName
  );
