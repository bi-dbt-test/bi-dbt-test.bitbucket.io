
  create or replace  view datalake_dev.semantic.VIEW_LEASES_LEGAL  as (
    
 WITH Lease_Form_By_Record_Type
AS
(
  SELECT
  prop.id AS PropertyId2
  ,prop.Name AS PropertyName2
  ,CASE WHEN rec2.Name = 'Ward Residential' THEN 'a00i0000003VDdSAAW' -- Assign Ward Village Master property
    ELSE prop.MasterProperty_c END  AS MasterPropertyId2
  ,prop.RecordTypeId AS PropertyRecordTypeId
  ,rec2.Name AS PropertyRecordTypeDesc
  ,CASE WHEN rec2.name  = 'HHC Master Property'
		THEN prop.MasterProperty_c  
		ELSE CASE WHEN rec2.name = 'Ward Residential' THEN 'a00i0000003VDdSAAW' END -- Assign Ward Village Master property
		END AS MasterPropertyId3
  ,CASE WHEN rec2.name = 'HHC Property' 
		THEN prop.Id END AS PropertyId3
  ,rec.name AS ContractFormType   
  , cf.MASTER_ASSET_C
  , cf.ACCOUNT_C TENANT_C
  --, cf.TENANT_TRADE_NAME_C as TENANT_NAME
  , A.TRADE_NAME_C TENANT_NAME
  , cf.LEASE_TYPE_C
  , cf.NEW_RENEWAL_AMENDMENT_C
  , ifnull(cf.SQ_FOOTAGE_OVERRIDE_C,cf.SQFOOTAGE_C) SQ_FOOTAGE_REPORTING_C
  , cf.DEALMAKER_C
  , cf.ASSIGNED_LEGAL_C
  , cf.DATE_ASSIGNED_TO_LEGAL_C
  , cf.LEASE_SENT_TO_TENANT_C
  , cf.TENANT_COMMENT_RCVD_HISTORY_C
  , cf.LL_S_RESPONSE_SENT_HISTORY_C
  , cf.TENANT_EXECUTED_RECEIVED_C
  , cf.AGMT_TO_EXEC_FOR_SIG_C
  , cf.FULLY_EXE_AGMT_DISTRIBUTED_C
  , cf.Salesforce_Lease_ID_c
  , cf.ID as FormNum
  , cf.RecordTypeId
  , cf.STAGE_C
  , cf.STAGE_REASON_C lease_STAGE_REASON
  , cf.LEGAL_COMMENTS_C
  , cf.DEVIATION_REQUIRED_C 
  FROM  datalake_dev.governed.SALESFORCE_LEASE_2__C    cf 
  INNER JOIN datalake_dev.governed.SALESFORCE_RECORDTYPE     rec ON rec.ID = cf.RecordTypeId
  LEFT JOIN datalake_dev.governed.SALESFORCE_PROPERTY__C      prop ON prop.ID = cf.PROPERTY_C
  LEFT JOIN datalake_dev.governed.SALESFORCE_RECORDTYPE       rec2 ON rec2.ID = prop.RecordTypeId
  LEFT JOIN datalake_dev.governed.SALESFORCE_ACCOUNT     A ON CF.ACCOUNT_C=A.ID
)
SELECT 
    cf.*
    ,usr.Name AS Decision_MakerName
	,usr2.Name AS Legal_Team_MemberName
FROM Lease_Form_By_Record_Type cf
    LEFT JOIN  datalake_dev.governed.SALESFORCE_USER    usr ON usr.id = cf.DEALMAKER_C
    LEFT JOIN datalake_dev.governed.SALESFORCE_USER     usr2 ON usr2.id = cf.ASSIGNED_LEGAL_C
    LEFT JOIN datalake_dev.governed.SALESFORCE_RECORDTYPE     REC ON REC.ID = CF.RECORDTYPEID
  );
