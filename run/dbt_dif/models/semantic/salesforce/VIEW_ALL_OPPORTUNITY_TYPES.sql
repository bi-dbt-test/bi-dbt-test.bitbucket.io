
  create or replace  view datalake_dev.semantic.VIEW_ALL_OPPORTUNITY_TYPES  as (
    
Select distinct  cj.OPPORTUNITY_TYPE,vc.BUILDING, vc.PROPERTY_ID, cj.GROUPNUM, cj.SORT_ORDER
FROM  (select distinct py.TRADE_NAME as BUILDING, py.PROPERTY_ID
from datalake_dev.semantic.DIM_SF_PROPERTY   py
where py.RECORD_TYPE_ID = '012i0000000mNrZAAU'
and TRADE_NAME <> 'Gateway Cylinder') vc
CROSS JOIN (Select 'New Sales' as Opportunity_Type, 1 as GROUPNUM, 1 as SORT_ORDER
Union Select 'Transfers' as Opportunity_Typee, 1 as GROUPNUM, 2 as SORT_ORDER
Union Select 'Cancellations' as Opportunity_Typee, 1 as GROUPNUM, 3 as SORT_ORDER
Union Select 'Reserved' as Opportunity_Type, 2 as GROUPNUM, 4 as SORT_ORDER
Union Select 'Contract Out' as Opportunity_Type, 2 as GROUPNUM, 5 as SORT_ORDER
Union Select 'Transfer Out' as Opportunity_Type, 2 as GROUPNUM, 6 as SORT_ORDER
Union Select 'Transfer/Cancel' as Opportunity_Type, 2 as GROUPNUM, 7 as SORT_ORDER
Union Select 'X: Closed Lost' as Opportunity_Type,  2 as GROUPNUM, 8 as SORT_ORDER) cj
  );
