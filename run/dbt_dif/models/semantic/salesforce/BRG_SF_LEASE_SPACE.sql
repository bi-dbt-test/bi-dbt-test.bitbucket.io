

      create or replace  table datalake_dev.semantic.BRG_SF_LEASE_SPACE  as
      (-- create or replace table semantic.BRG_SF_LEASE_SPACE 
-- as
select ID,LEASE_C LEASE_id,LEASE_STAGE_C LEASE_STAGE,NAME,SPACE_C SPACE_id,TENANT_C TENANT_ID
,SQ_FOOTAGE_C SQ_FOOTAGE,EXPIRATION_DATE_C EXPIRATION_DATE
,LEASABLE_SQ_FOOTAGE_C LEASABLE_SQ_FOOTAGE
,LEASABLE_SQ_FOOTAGE_ON_SPACE_C LEASABLE_SQ_FOOTAGE_ON_SPACE
,EFFECTIVE_DATE_C EFFECTIVE_DATE
,BASE_RENT_C BASE_RENT
,BASE_RENT_PSF_C BASE_RENT_PSF
,CREATEDBYID CREATED_BY_ID
,CREATEDDATE CREATED_DATE
,LASTMODIFIEDBYID LAST_MODIFIED_BY_ID
,LASTMODIFIEDDATE LAST_MODIFIED_DATE
,ISDELETED IS_DELETED
from  datalake_dev.governed.SALESFORCE_LEASE_SPACE__C
      );
    