

      create or replace  table datalake_dev.semantic.FACT_SF_CONDO_SALES_OPPORTUNITY  as
      (WITH  BASE_DATE 
AS 
( 
SELECT DAY         AS FULLDATE, 
       DATE_ID     AS DATEKEY, 
	   MONTH_START, 
	   MONTH_END, 
	   MONTH_NUM   AS MONTHOFYEAR 
  FROM  datalake_dev.semantic.DIM_DATE  
 WHERE    
       MONTH_START BETWEEN '01/01/2013' AND '12/01/2022' 
),  
BASE_OPPORTUNITY_TYPE 
AS 
(  
SELECT REPLACE(VALUE,'"','') AS OPPORTUNITY_TYPE 
  FROM 
      ( 
        SELECT 'New Sales,Cancellations,Transfers,Reserved,Contract Out,X: Closed Lost,Transfer/Cancel,Transfer Out' 
                AS OPPORTUNITY_TYPE 
      ) TST 
    , LATERAL FLATTEN (INPUT => SPLIT(TST.OPPORTUNITY_TYPE,',')) S 
), 
GET_BASE 
AS 
( 
SELECT BASE_DATE.FULLDATE,  
       CASE WHEN OPPORTUNITY_TYPE='New Sales'       THEN  1  
            WHEN OPPORTUNITY_TYPE='Transfers'       THEN  2 
            WHEN OPPORTUNITY_TYPE='Cancellations'   THEN  3 
            WHEN OPPORTUNITY_TYPE='Reserved'        THEN  4   
            WHEN OPPORTUNITY_TYPE='Contract Out'    THEN  5    
            WHEN OPPORTUNITY_TYPE='Transfer Out'    THEN  6 
            WHEN OPPORTUNITY_TYPE='Transfer/Cancel' THEN  7 
            WHEN OPPORTUNITY_TYPE='X: Closed Lost'  THEN  8    
	END                                   AS SORT_ORDER,  
       OPPORTUNITY_TYPE 
  FROM BASE_DATE 
 CROSS JOIN  BASE_OPPORTUNITY_TYPE 
),  
OPP_FACT 
AS 
( 
SELECT CAST(SPC.SPACE_ID AS VARCHAR)                     AS SPACE_ID, 
       CAST(SPC.PROPERTY_ID AS VARCHAR)                  AS PROP_ID, 
       CAST(IFNULL(OPP.OPPORTUNITY_ID,'UKN') AS VARCHAR) AS OPPTY_ID, 
       CAST(IFNULL(OPP.OWNER_ID,'UKN') AS VARCHAR)       AS USERID, 
       CAST(IFNULL(OPP.ACCOUNT_ID,'UKN') AS VARCHAR)     AS ACCOUNTID,
       CASE WHEN RN = 1 OR RN IS NULL THEN 'TRUE' 
	                              ELSE 'FALSE' 
	END                                              AS LATEST_OPPORTUNITY,
       IFNULL(OPP.UNIT_PRICE,0)                          AS UNIT_PRICE_C
 
  FROM  datalake_dev.semantic.DIM_SF_PROPERTY PROP 
 INNER JOIN  datalake_dev.semantic.DIM_SF_PROPSPACE SPC 
         ON PROP.PROPERTY_ID = SPC.PROPERTY_ID
  LEFT JOIN (SELECT * 
               FROM (SELECT A.*,
                            ROW_NUMBER() OVER (PARTITION BY SPACE_ID 
					       ORDER BY STAGE_NAME,
					       CANCELLED_DATE DESC) RN 
                       FROM  datalake_dev.semantic.DIM_SF_OPPORTUNITY A) M 
		      WHERE RN=1) OPP
         ON SPC.SPACE_ID = OPP.SPACE_ID
 WHERE PROP.PROPERTY_TYPE = 'Residential'
   AND SPC.SPACE_STATUS NOT IN ('Not Available')
), 
NEW_SALES
AS 
(
SELECT SPACE.PROPERTY_ID, 
       SPACE.SPACE_ID                              AS SPACE_ID, 
       OPP.OPPORTUNITY_ID                          AS OPPORTUNITY_ID, 
       'New Sales'                                 AS OPPORTUNITY_TYPE, 
       CAST(OPP.CONTRACT_SIGNED_BY_SELLER AS DATE) AS TRANSACTION_DATE, 
       IFNULL(SPACE.UNIT_PRICE,0)                  AS SPACE_UNIT_PRICE_C,
       IFNULL(OPP.UNIT_PRICE,0) OPP_UNIT_PRICE_C,
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0) AS CREDIT_TOWARDS_PURCHASE_PRICE_C,
       IFNULL(SPACE.UNIT_PRICE,0) - IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0) AS NET_PRICE
  FROM datalake_dev.semantic.DIM_SF_OPPORTUNITY  OPP  
 INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE   SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE   RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 WHERE RECTYPE.NAME = 'Ward Residential'
   AND OPP.BUILDING IS NOT NULL
   AND SPACE.SPACE_STATUS <> 'Not Available'
   AND OPP.CONTRACT_SIGNED_BY_BUYER IS NOT NULL
   AND OPP.TRANSFER_COMPLETE_DATE IS  NULL 
   AND OPP.CONTRACT_SIGNED_BY_SELLER IS NOT NULL
), 
CONDOCANCELLATIONS 
AS 
 (
SELECT SPACE.PROPERTY_ID, 
       SPACE.SPACE_ID                               AS SPACE_ID, 
       OPP.OPPORTUNITY_ID                           AS OPPORTUNITY_ID, 
       'Cancellations'                              AS OPPORTUNITY_TYPE, 
       CAST(OPP.CANCELLED_DATE  AS DATE)            AS TRANSACTION_DATE, 
       IFNULL(SPACE.UNIT_PRICE ,0)                  AS SPACE_UNIT_PRICE_C,  
       IFNULL(OPP.UNIT_PRICE ,0)                    AS OPP_UNIT_PRICE_C, 
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE ,0) AS CREDIT_TOWARDS_PURCHASE_PRICE_C, 
       (IFNULL(SPACE.UNIT_PRICE ,0) - IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE ,0)) * -1 AS NET_PRICE
  FROM  datalake_dev.semantic.DIM_SF_OPPORTUNITY   OPP 
 INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE   SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE   RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 WHERE RECTYPE.NAME = 'Ward Residential'  
   AND OPP.BUILDING IS NOT NULL
   AND SPACE.SPACE_STATUS  <> 'Not Available'
   AND OPP.CONTRACT_SIGNED_BY_SELLER  IS NOT NULL
   AND OPP.CANCELLED_DATE  IS NOT NULL
   AND OPP.CANCELLED_DATE  IS NOT NULL 
), 
TRANSFER
AS
(
SELECT SPACE.PROPERTY_ID, 
       SPACE.SPACE_ID                                                         AS SPACE_ID, 
       OPP.OPPORTUNITY_ID                                                     AS OPPORTUNITY_ID, 
       'Transfers'                                                            AS OPPORTUNITY_TYPE, 
       CAST(OPP.TRANSFER_COMPLETE_DATE AS DATE)                               AS TRANSACTION_DATE, 
       IFNULL(SPACE.UNIT_PRICE,0)                                             AS SPACE_UNIT_PRICE_C, 
       IFNULL(OPP.UNIT_PRICE,0)                                               AS OPP_UNIT_PRICE_C, 
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0)                            AS CREDIT_TOWARDS_PURCHASE_PRICE_C, 
       IFNULL(SPACE.UNIT_PRICE,0)-IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0) AS NET_PRICE, 
       OPP.TRANSFERRED_FROM                                                   AS TRANSFERRED_FROM, 
       OPP.TRANSFER_COMPLETE_DATE, 
       OPP.CONTRACT_SIGNED_BY_SELLER
  FROM OPP_FACT  
 INNER JOIN datalake_dev.semantic.DIM_SF_OPPORTUNITY   OPP  
         ON OPP_FACT.OPPTY_ID=OPP.OPPORTUNITY_ID
 INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE    SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE   RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 INNER JOIN datalake_dev.semantic.DIM_SF_ACCOUNT ACCT 
         ON ACCT.ACCOUNT_ID = OPP.ACCOUNT_ID
 WHERE RECTYPE.NAME =  'Ward Residential'  
   AND OPP.BUILDING <> 'Gateway Cylinder'
   AND OPP.TRANSFERRED_FROM       IS NOT NULL 
   AND OPP.TRANSFER_COMPLETE_DATE IS NOT NULL

),  
TRANSFER_FROM 
AS
(
SELECT SPACE.PROPERTY_ID, 
       SPACE.SPACE_ID                                                                  AS SPACE_ID, 
       OPP.OPPORTUNITY_ID                                                              AS OPPORTUNITY_ID, 
       'Transfers'                                                                     AS OPPORTUNITY_TYPE, 
       CAST(OPP.CANCELLED_DATE AS DATE)                                                AS TRANSACTION_DATE, 
       IFNULL(SPACE.UNIT_PRICE,0)                                                      AS SPACE_UNIT_PRICE_C, 
       IFNULL(OPP.UNIT_PRICE,0)                                                        AS OPP_UNIT_PRICE_C,  
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0)                                     AS CREDIT_TOWARDS_PURCHASE_PRICE_C, 
       (IFNULL(SPACE.UNIT_PRICE,0) - IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0)) * -1 AS NET_PRICE, 
       OPP.TRANSFERRED_FROM                                                            AS TRANSFERRED_FROM, 
       OPP.TRANSFER_COMPLETE_DATE, 
       OPP.CONTRACT_SIGNED_BY_SELLER
  FROM  datalake_dev.semantic.DIM_SF_OPPORTUNITY   OPP   
 INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE  SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE    RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 INNER JOIN TRANSFER 
         ON OPP.OPPORTUNITY_ID=TRANSFER.TRANSFERRED_FROM
), 
UNION_TRANSFER
AS 
( 
SELECT * 
  FROM TRANSFER_FROM
UNION ALL 
SELECT * 
  FROM TRANSFER
), 
TMPSALES 
AS 
( 
SELECT * 
  FROM NEW_SALES
UNION ALL
SELECT * 
  FROM CONDOCANCELLATIONS
), 
TRANSFER_OUT 
AS 
( 
SELECT SPACE.PROPERTY_ID,
       SPACE.SPACE_ID                                                             AS SPACE_ID,
       OPP.OPPORTUNITY_ID                                                         AS OPPORTUNITY_ID,
       'Transfer Out'                                                             AS OPPORTUNITY_TYPE,
       CAST(OPP.CREATED_DATE AS DATE)                                             AS TRANSACTION_DATE,
       IFNULL(SPACE.UNIT_PRICE ,0)                                                AS SPACE_UNIT_PRICE_C,
       IFNULL(OPP.UNIT_PRICE,0)                                                   AS OPP_UNIT_PRICE_C,
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE ,0)                               AS CREDIT_TOWARDS_PURCHASE_PRICE_C,
       IFNULL(SPACE.UNIT_PRICE ,0) - IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE ,0) AS NET_PRICE,
       OPP.TRANSFERRED_FROM                                                       AS TRANSFERRED_FROM,
       OPP.TRANSFER_COMPLETE_DATE 
  FROM OPP_FACT
 INNER JOIN  datalake_dev.semantic.DIM_SF_OPPORTUNITY    OPP 
         ON OPP_FACT.OPPTY_ID = OPP.OPPORTUNITY_ID
 INNER JOIN  datalake_dev.semantic.DIM_SF_PROPSPACE   SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE   RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 INNER JOIN datalake_dev.semantic.DIM_SF_ACCOUNT    ACCT 
         ON ACCT.ACCOUNT_ID = OPP.ACCOUNT_ID
 WHERE  RECTYPE.NAME = 'Ward Residential'  
   AND OPP.BUILDING <> 'Gateway Cylinder'
   AND OPP.TRANSFERRED_FROM        IS NOT NULL  AND LENGTH(OPP.TRANSFERRED_FROM ) > 0
   AND OPP.TRANSFER_COMPLETE_DATE  IS   NULL  
   AND  OPP.CREATED_DATE           IS NOT NULL 
 
), 
CLOASED_LOST 
AS 
 (
SELECT SPACE.PROPERTY_ID,
       SPACE.SPACE_ID                                                                  AS SPACE_ID,
       OPP.OPPORTUNITY_ID                                                              AS OPPORTUNITY_ID,
       'X: Closed Lost'                                                                AS OPPORTUNITY_TYPE,
       CAST(OPP.CANCELLED_DATE AS DATE)                                                AS TRANSACTION_DATE,
       IFNULL(SPACE.UNIT_PRICE,0)                                                      AS SPACE_UNIT_PRICE_C,
       IFNULL(OPP.UNIT_PRICE,0)                                                        AS OPP_UNIT_PRICE_C,
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0)                                     AS CREDIT_TOWARDS_PURCHASE_PRICE_C,
       (IFNULL(SPACE.UNIT_PRICE,0) - IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0)) * -1 AS NET_PRICE
  FROM  datalake_dev.semantic.DIM_SF_OPPORTUNITY    OPP 
 INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE   SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 WHERE RECTYPE.NAME        = 'Ward Residential' 
   AND OPP.BUILDING       IS NOT NULL
   AND SPACE.SPACE_STATUS <> 'Not Available'
   AND OPP.CANCELLED_DATE IS NOT NULL
   AND OPP.BUILDING       <> 'Gateway Cylinder' 
   AND STAGE_NAME          = 'X: Closed Lost'
),  
CONTRACT_OUT 
AS   
(
SELECT SPACE.PROPERTY_ID, 
       SPACE.SPACE_ID                              AS SPACE_ID, 
       OPP.OPPORTUNITY_ID                          AS OPPORTUNITY_ID, 
       'Contract Out'                              AS OPPORTUNITY_TYPE, 
       NULL                                        AS TRANSACTION_DATE, 
       IFNULL(SPACE.UNIT_PRICE,0)                  AS SPACE_UNIT_PRICE_C, 
       IFNULL(OPP.UNIT_PRICE,0)                    AS OPP_UNIT_PRICE_C, 
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0) AS CREDIT_TOWARDS_PURCHASE_PRICE_C, 
       IFNULL(SPACE.UNIT_PRICE,0)                  AS NET_PRICE 
  FROM datalake_dev.semantic.DIM_SF_OPPORTUNITY  OPP 
 INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE  RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 WHERE RECTYPE.NAME        ='Ward Residential'  
   AND OPP.BUILDING       IS NOT NULL
   AND SPACE.SPACE_STATUS  ='Contract Out'
   AND OPP.CANCELLED_DATE IS   NULL
   AND OPP.BUILDING       <> 'Gateway Cylinder' 
),  
RESERVED 
AS 
(
SELECT SPACE.PROPERTY_ID, 
       SPACE.SPACE_ID                              AS SPACE_ID, 
       OPP.OPPORTUNITY_ID                          AS OPPORTUNITY_ID, 
       'Reserved'                                  AS OPPORTUNITY_TYPE, 
       NULL                                        AS TRANSACTION_DATE, 
       IFNULL(SPACE.UNIT_PRICE,0)                  AS SPACE_UNIT_PRICE_C, 
       IFNULL(OPP.UNIT_PRICE,0)                    AS OPP_UNIT_PRICE_C, 
       IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0) AS CREDIT_TOWARDS_PURCHASE_PRICE_C, 
       IFNULL(SPACE.UNIT_PRICE,0)                  AS NET_PRICE
  FROM datalake_dev.semantic.DIM_SF_OPPORTUNITY  OPP 
 INNER JOIN  datalake_dev.semantic.DIM_SF_PROPSPACE  SPACE 
         ON  OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN   datalake_dev.semantic.LUP_SF_RECORD_TYPE  RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 WHERE RECTYPE.NAME        = 'Ward Residential'   
   AND OPP.BUILDING       IS NOT NULL
   AND SPACE.SPACE_STATUS  ='Reserved'
   AND OPP.CANCELLED_DATE IS   NULL
   AND OPP.BUILDING       <> 'Gateway Cylinder'    
), 
UNION_CATEGORY
AS
( 
SELECT * 
  FROM TMPSALES
UNION ALL
SELECT PROPERTY_ID,
       SPACE_ID,
       OPPORTUNITY_ID, 
       OPPORTUNITY_TYPE,
       TRANSACTION_DATE,
       SPACE_UNIT_PRICE_C,
       OPP_UNIT_PRICE_C, 
       CREDIT_TOWARDS_PURCHASE_PRICE_C,
       NET_PRICE 
  FROM TRANSFER_OUT
 UNION ALL
SELECT PROPERTY_ID,
       SPACE_ID,
       OPPORTUNITY_ID, 
       OPPORTUNITY_TYPE,
       TRANSACTION_DATE,
       SPACE_UNIT_PRICE_C,
       OPP_UNIT_PRICE_C, 
       CREDIT_TOWARDS_PURCHASE_PRICE_C,
       NET_PRICE 
  FROM CLOASED_LOST
),   
RESULT_PRE
AS
(
SELECT BASE.FULLDATE,
       PROPERTY_ID,
       SPACE_ID,
       OPPORTUNITY_ID,
       BASE.OPPORTUNITY_TYPE,
       SPACE_UNIT_PRICE_C,
       OPP_UNIT_PRICE_C,
       CREDIT_TOWARDS_PURCHASE_PRICE_C,
       NET_PRICE,
       CASE WHEN BASE.OPPORTUNITY_TYPE IN ('New Sales','Transfers','Cancellations') 
	        THEN 'TRUE' 
		    ELSE 'FALSE' 
		END AS PART_OF_TOTAL_CALCULATION,
       SORT_ORDER
  FROM GET_BASE BASE
  LEFT JOIN UNION_CATEGORY 
         ON BASE.FULLDATE = UNION_CATEGORY.TRANSACTION_DATE
        AND BASE.OPPORTUNITY_TYPE = UNION_CATEGORY.OPPORTUNITY_TYPE
UNION ALL
SELECT NULL AS FULLDATE,
       PROPERTY_ID, 
	   SPACE_ID, 
	   OPPORTUNITY_ID,  
	   OPPORTUNITY_TYPE, 
	   SPACE_UNIT_PRICE_C, 
	   OPP_UNIT_PRICE_C, 
	   CREDIT_TOWARDS_PURCHASE_PRICE_C, 
	   NET_PRICE,
	   'FALSE' AS PART_OF_TOTAL_CALCULATION, 
	   5 AS SORT_ORDER 
  FROM CONTRACT_OUT
UNION ALL
SELECT NULL AS FULLDATE,
       PROPERTY_ID, 
	   SPACE_ID, 
	   OPPORTUNITY_ID, 
	   OPPORTUNITY_TYPE,
	   SPACE_UNIT_PRICE_C, 
	   OPP_UNIT_PRICE_C, 
	   CREDIT_TOWARDS_PURCHASE_PRICE_C,
	   NET_PRICE,
	   'FALSE' AS PART_OF_TOTAL_CALCULATION,
	   4 AS SORT_ORDER   
  FROM RESERVED
)
SELECT FULLDATE,
       PROPERTY_ID, 
	   SPACE_ID, 
	   OPPORTUNITY_ID, 
	   OPPORTUNITY_TYPE,
	   SPACE_UNIT_PRICE_C, 
	   OPP_UNIT_PRICE_C, 
	   CREDIT_TOWARDS_PURCHASE_PRICE_C,
	   NET_PRICE,
	   PART_OF_TOTAL_CALCULATION,
	   SORT_ORDER, 
       CURRENT_TIMESTAMP::DATETIME AS CREATED_DATE 
  FROM RESULT_PRE
      );
    