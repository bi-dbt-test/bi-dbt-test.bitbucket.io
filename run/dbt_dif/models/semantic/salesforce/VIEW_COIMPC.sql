
  create or replace  view datalake_dev.semantic.VIEW_COIMPC  as (
    

with propmaster as (
  select p.id m_Prop_id, p.name m_prop_name, p.id masterproperty_c, p.name masterproperty
  from  datalake_dev.governed.SALESFORCE_PROPERTY__C  p where masterproperty_c = ''
  union all
  select p1.id, p1.name, m.masterproperty_c, m.masterproperty
  from datalake_dev.governed.SALESFORCE_PROPERTY__C   p1
  join propmaster m
  on m.m_Prop_id = p1.masterproperty_c
)

SELECT g.masterproperty AS Master_Property
,a.property_name_c
,a.id COI_ID
,a.name COI_Name
,a.CREATEDDATE AS SF_Created_Dt
,a.COI_Status_c
,a.RecordTypeID
,e.Name RecordTypeName
,a.ownerID
,f.name Owner_name
,a.counterparty_name_c Vendor_Name
,a.CGL_Policy_Expiration_Date_c
,a.CGL_Each_Occurence_Limit_c
,a.CGL_General_Aggregate_Limit_c
,a.WC_EL_Policy_Expiration_c
,a.Employer_s_Liability_Limit_c
,a.BA_Policy_Expiration_Date_c
,a.Automobile_Policy_Limit_c
,a.UL_Policy_Expiration_Date_c
,a.Umbrella_Limit_c
,b.id FID
,b.name Form_ID
,b.Counterparty_Name_c Form_Vendor_Name
,b.Counterparty_Email_c
,b.Counterparty_Phone_c
,b.Counterparty_Contact_c
,b.Contract_Status_c
,c.id Lease_id
,c.name Lease_name
,c.salesforce_lease_id_c
//replacing the contact email and Phone from the contact table instead of the lease table
,d.EMAIL Contact_Email_c 
,d.phone Contact_Phone_c
,c.Stage_c Lease_Status
,d.name Tenant_Contact_Name
,a.Counterparty_Tenant_COI_Contact_c
,a.Property_Policy_Expiration_Date_c
,b.Department_c
,b.New_Amendment_c
,b.Contract_Type_c
,b.Contract_Expiration_Date_c
,b.Insurance_Needed_c
,b.Insurance_Needed_Date_c
,g.masterproperty_c AS Master_Property_id
,a.Property_c AS Property_Id
FROM datalake_dev.governed.SALESFORCE_COI__C     A
LEFT OUTER JOIN datalake_dev.governed.SALESFORCE_FORMS__C   B ON A.FORM_ID_C = B.ID
LEFT OUTER JOIN datalake_dev.governed.SALESFORCE_LEASE_2__C     C ON A.LEASE_2_C = C.ID --updated to new table
LEFT OUTER JOIN  datalake_dev.governed.SALESFORCE_OPPORTUNITY     opp on C.OPPORTUNITY_C = opp.ID
LEFT OUTER JOIN datalake_dev.governed.SALESFORCE_OPPORTUNITYCONTACTROLE    OCR  --bridge
ON opp.ID = ocr.OPPORTUNITYID and isPrimary = 'true'
LEFT OUTER JOIN datalake_dev.governed.SALESFORCE_CONTACT     D ON OCR.CONTACTID = D.ID
LEFT OUTER JOIN datalake_dev.governed.SALESFORCE_RECORDTYPE      E ON A.RECORDTYPEID = E.ID
LEFT OUTER JOIN datalake_dev.governed.SALESFORCE_USER     F ON A.OWNERID = F.ID
INNER JOIN propmaster G ON G.m_Prop_id = A.Property_c
  );
