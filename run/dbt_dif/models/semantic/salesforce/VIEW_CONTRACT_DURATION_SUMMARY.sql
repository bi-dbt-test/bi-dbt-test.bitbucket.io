
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_DURATION_SUMMARY  as (
    
SELECT a.Fully_Executed_Flag, AVG(a.CRF_Approval_Duration) as CRF_Approval_Duration, AVG(a.Legal_Duration) as Legal_Duration,
AVG(a.Contract_Approval_Memo_Duration) as Contract_Approval_Memo_Duration, AVG(Total_Contract_Duration) as Total_Contract_Duration,
TO_NUMBER(sum(a.Total_Receivable_Amount_c)) as Total_Receivable_Amount_c,TO_NUMBER(sum(a.TotalAmountPayableUnderContract_c)) as TotalAmountPayableUnderContract_c, SUM(Number_of_Contracts) as Number_of_Contracts
from (
SELECT 
'In Progress' as Fully_Executed_Flag, 1 as Number_of_Contracts, 
		cf.Contract_Status_c, cf.Name,
CASE WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c >= cf.TotalAmountPayableUnderContract_c
		  THEN cf.Total_Receivable_Amount_c
	WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c >= 0
		  THEN cf.Total_Receivable_Amount_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c = 0 and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c is null and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c

	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Credit' and cf.Total_Receivable_Amount_c >= cf.TotalAmountPayableUnderContract_c
		  THEN cf.Total_Receivable_Amount_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Credit' and cf.Total_Receivable_Amount_c >= 0
		  THEN cf.Total_Receivable_Amount_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Credit' and cf.Total_Receivable_Amount_c = 0 and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Credit' and cf.Total_Receivable_Amount_c is null and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c
	 ELSE 0
	 END as Total_Receivable_Amount_c,cf.TotalAmountPayableUnderContract_c
,CASE 
      WHEN cf.Contract_Status_c in ('On Hold') 
			AND cf.Date_Assigned_to_Legal_c is null
	  THEN DATEDIFF(day, cf.Date_Submitted_c ,getdate () )
	  WHEN cf.Date_Submitted_c <= cf.Date_Assigned_to_Legal_c
	  THEN DATEDIFF(day, cf.Date_Submitted_c, cf.Date_Assigned_to_Legal_c)
	  WHEN cf.Date_Submitted_c <= cf.Legal_Review_Date_c
	  THEN DATEDIFF(day, cf.Date_Submitted_c, cf.Date_Submitted_c)
	  WHEN cf.Contract_Status_c in ('On Hold','S1: Decision Maker Approval','S1a: Risk Management Approval','S1b: Executive Approval','S2: CFO Approval','S3: General Counsel Approval') 
			AND cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null
	  THEN DATEDIFF(day, cf.Date_Submitted_c ,getdate () )
	  WHEN cf.Date_Submitted_c <= cf.Date_Assigned_to_Legal_c or cf.Date_Submitted_c <= cf.Legal_Review_Date_c or cf.Date_Submitted_c <= cf.Submitted_to_Designee_c
	  THEN DATEDIFF(day, cf.Date_Submitted_c, cf.Submitted_to_Designee_c)
	  WHEN cf.Contract_Status_c in ('On Hold','S1: Decision Maker Approval','S1a: Risk Management Approval','S1b: Executive Approval','S2: CFO Approval','S3: General Counsel Approval') 
	  THEN DATEDIFF(day, cf.Date_Submitted_c, getdate ())
      ELSE NULL
	  END AS CRF_Approval_Duration
,CASE WHEN cf.Contract_Status_c in ('On Hold') 
			AND cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,getdate () )
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review') 
	  and (cf.Date_Assigned_to_Legal_c is not null or cf.Legal_Review_Date_c is not null)
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: CAM Decision Maker Approval','S8a: Risk Management Approval', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is not null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
      WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: CAM Decision Maker Approval','S8a: Risk Management Approval', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
      WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: CAM Decision Maker Approval','S8a: Risk Management Approval', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: CAM Decision Maker Approval','S8a: Risk Management Approval', 'S11: Out for Signatures','On Hold') 
	  and cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null and cf.Date_Submitted_c is not null
	  THEN DATEDIFF(day, cf.Date_Submitted_c, getdate())
	  WHEN cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
	  WHEN cf.Date_Assigned_to_Legal_c is not null and cf.Submitted_to_Designee_c is not null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
	  WHEN cf.Contract_Status_c in ('S1: Decision Maker Approval','S1a: Risk Management Approval','S1b: Executive Approval','S2: CFO Approval','S3: General Counsel Approval') 
	  THEN NULL
	  ELSE 0
	  END as Legal_Duration
,CASE WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: CAM Decision Maker Approval','S8a: Risk Management Approval', 'S11: Out for Signatures','On Hold') 
	  and cf.CAM_Decision_Maker_Approval_Date_c is null and CAM_Date_Submitted_c is not null
      THEN DATEDIFF(day, cf.CAM_Date_Submitted_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: CAM Decision Maker Approval','S8a: Risk Management Approval', 'S11: Out for Signatures','On Hold') 
	  and cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is null
      THEN DATEDIFF(day, cf.CAM_Decision_Maker_Approval_Date_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: CAM Decision Maker Approval','S8a: Risk Management Approval', 'S11: Out for Signatures','On Hold') 
	  and cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is not null
      THEN DATEDIFF(day, CAM_Date_Submitted_c, cf.CAM_Decision_Maker_Approval_Date_c)
	  ELSE NULL--DATEDIFF(day, cf.Date_Submitted_c,IFNULL(Fully_Approved_Date_c, getdate()))
	  END as Contract_Approval_Memo_Duration
,DATEDIFF(day, cf.Date_Submitted_c, IFNULL(cf.CAM_Decision_Maker_Approval_Date_c,IFNULL(Fully_Approved_Date_c,getdate()))) AS Total_Contract_Duration
,cf.Legal_Duration_c
, cf.Date_Submitted_c,cf.Date_Assigned_to_Legal_c, cf.Legal_Review_Date_c, cf.Submitted_to_Designee_c
,cf.CAM_Decision_Maker_Approval_Date_c,cf.CAM_Date_Submitted_c,cf.Fully_Approved_Date_c,cf.Fully_Executed_Date_c
FROM datalake_dev.semantic.VIEW_CONTRACT_FORM  cf
WHERE cf.Contract_Status_c NOT IN ('X: Dead','X: Terminated','S13: Dead','S12: Fully Executed','S0: Preparing Form','')


) a


GROUP BY a.Fully_Executed_Flag

UNION


SELECT a.Fully_Executed_Flag, AVG(a.CRF_Approval_Duration) as CRF_Approval_Duration, AVG(a.Legal_Duration) as Legal_Duration,
AVG(a.Contract_Approval_Memo_Duration) as Contract_Approval_Memo_Duration, AVG(Total_Contract_Duration) as Total_Contract_Duration,
sum(a.Total_Receivable_Amount_c) as Total_Receivable_Amount_c,sum(a.TotalAmountPayableUnderContract_c) as TotalAmountPayableUnderContract_c, SUM(Number_of_Contracts) as Number_of_Contracts
from (

SELECT --TOP 100 
'Fully Executed' as Fully_Executed_Flag, 1 as Number_of_Contracts, cf.Name,
CASE WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c >= cf.TotalAmountPayableUnderContract_c
		  THEN cf.Total_Receivable_Amount_c
	WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c >= 0
		  THEN cf.Total_Receivable_Amount_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c = 0 and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c is null and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c

	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Credit' and cf.Total_Receivable_Amount_c >= cf.TotalAmountPayableUnderContract_c
		  THEN cf.Total_Receivable_Amount_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Receivable' and cf.Total_Receivable_Amount_c >= 0
		  THEN cf.Total_Receivable_Amount_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Credit' and cf.Total_Receivable_Amount_c = 0 and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c
	 WHEN cf.Is_the_Amount_Payable_or_Receivable_c = 'Credit' and cf.Total_Receivable_Amount_c is null and cf.TotalAmountPayableUnderContract_c >0
	      THEN cf.TotalAmountPayableUnderContract_c
	 ELSE 0
	 END as Total_Receivable_Amount_c
,cf.TotalAmountPayableUnderContract_c
,CASE WHEN cf.Date_Submitted_c is not null and cf.Date_Assigned_to_Legal_c is not null
	  THEN DATEDIFF(day, cf.Date_Submitted_c, cf.Date_Assigned_to_Legal_c)
	  WHEN cf.Date_Submitted_c is not null and cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is not null
	  THEN DATEDIFF(day, cf.Date_Submitted_c, cf.Legal_Review_Date_c)
	  ELSE NULL
	  END AS CRF_Approval_Duration
,CASE WHEN cf.Submitted_to_Designee_c is not null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
      WHEN cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
      WHEN cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null
	  THEN NULL
	  WHEN cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null and cf.CAM_Decision_Maker_Approval_Date_c is null and cf.CAM_Decision_Maker_Approval_Date_c is null
	  THEN NULL
	  ELSE NULL
	  END as Legal_Duration
,CASE WHEN cf.CAM_Decision_Maker_Approval_Date_c is null and CAM_Date_Submitted_c is not null
      	  THEN DATEDIFF(day, cf.CAM_Date_Submitted_c, cf.Fully_Executed_Date_c)
	  WHEN cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is null
      	  THEN DATEDIFF(day, cf.CAM_Decision_Maker_Approval_Date_c, cf.Fully_Executed_Date_c)
	  WHEN cf.CAM_Decision_Maker_Approval_Date_c is not null and CAM_Date_Submitted_c is not null
      	  THEN DATEDIFF(day, CAM_Date_Submitted_c, cf.Fully_Executed_Date_c)
	  WHEN cf.CAM_Decision_Maker_Approval_Date_c is null and CAM_Date_Submitted_c is null and CAM_Date_Submitted_c is null
	  THEN NULL
	  ELSE NULL
	  END as Contract_Approval_Memo_Duration
,DATEDIFF(day, cf.Date_Submitted_c, IFNULL(cf.Fully_Executed_Date_c,getdate())) AS Total_Contract_Duration
, cf.Date_Submitted_c,cf.Date_Assigned_to_Legal_c, cf.Legal_Review_Date_c, cf.Submitted_to_Designee_c
,cf.CAM_Decision_Maker_Approval_Date_c,cf.CAM_Date_Submitted_c
,cf.Fully_Executed_Date_c, cf.Fully_Approved_Date_c


FROM datalake_dev.semantic.VIEW_CONTRACT_FORM   cf
WHERE cf.Contract_Status_c IN ('S12: Fully Executed','X: Terminated')



) a



GROUP BY a.Fully_Executed_Flag

ORDER BY Fully_Executed_Flag
  );
