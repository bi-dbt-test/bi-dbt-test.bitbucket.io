

      create or replace  table datalake_dev.semantic.FACT_SF_CONDO_SALES  as
      (WITH DIMDATE 
AS
(
SELECT DAY       AS FULLDATE,
       DATE_ID   AS DATEKEY, 
       MONTH_START, 
       MONTH_END, 
       MONTH_NUM AS MONTHOFYEAR 
  FROM  datalake_dev.semantic.DIM_DATE
 WHERE MONTH_START BETWEEN '01/01/2013' AND DATEADD(YEAR,2,CURRENT_DATE())
), 
ALREADYSOLD  --2676,2675
AS
(
SELECT DISTINCT SPACE.PROPERTY_ID,
                SPACE.SPACE_ID                AS SPACE_ID,
		OPP.CONTRACT_SIGNED_BY_SELLER
  FROM  datalake_dev.semantic.DIM_SF_OPPORTUNITY    OPP
 INNER JOIN datalake_dev.semantic.DIM_SF_PROPSPACE    SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE    RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
WHERE 
      RECTYPE.NAME = 'Ward Residential'
  AND SPACE.SPACE_STATUS <> 'Not Available'
  AND OPP.BUILDING IS NOT NULL 
  AND OPP.ISWON = 'TRUE'
), 
TOTALUNITS  --3170
AS
(
SELECT   SPACE.SPACE_ID                   AS SPACE_ID,  
         SPACE.NAME                       AS SPACE_NAME, 
	 SPACE.SQ_FOOTAGE                 AS SPACE_SQ_FOOTAGE,
         SPACE.UNIT_PRICE                 AS UNIT_PRICE,
         SPACE.PROPERTY_ID                AS PROPERTY_ID,
         SPACE.BUILDING                   AS BUILDING,
         SPACE.EST_NET_LIVING_AREA_SF     AS LIVING_AREA, 
         0.0                              AS LIVING_AREA_SOLD,
         0.0                              AS LIVING_AREA_UNSOLD,
         SPACE.GROUP_TYPE, 
         SPACE.TURN_KEY_OPTION_UNIT_PRICE AS TURN_KEY_OPTION_UNIT_PRICE, 
         SPACE.FURNITURE_OPTION           AS FURNITURE_OPTION 
  FROM 
       datalake_dev.semantic.DIM_SF_PROPSPACE   SPACE  
 INNER JOIN   datalake_dev.semantic.LUP_SF_RECORD_TYPE   RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 WHERE 
       RECTYPE.NAME = 'Ward Residential'
   AND SPACE.SPACE_STATUS  <> 'Not Available'
   AND SPACE.BUILDING IS NOT NULL 
),  
UNIT_LIST
AS
(
SELECT  TOTALUNITS.SPACE_ID,
        TOTALUNITS.BUILDING,
	TOTALUNITS.PROPERTY_ID,
	ALREADYSOLD.CONTRACT_SIGNED_BY_SELLER
  FROM  TOTALUNITS
  LEFT JOIN ALREADYSOLD 
         ON TOTALUNITS.SPACE_ID = ALREADYSOLD.SPACE_ID
), 
BUILDINGSALES
AS
(
SELECT 
       PROPERTY_ID,
	   BUILDING, 
	   NULL                                      AS TOTAL_UNITS_SOLD,
	   NULL                                      AS TOTAL_UNITS_UNSOLD,
	   COUNT(*)                                  AS TOTAL_UNITS,
	   NULL                                      AS PCT_SOLD,
	   NULL                                      AS SOLD_REV,
	   NULL                                      AS UNSOLD_REV,
	   SUM(IFNULL(UNIT_PRICE,0))                 AS TOTAL_REV,
	   NULL                                      AS PCT_SOLD_REV,
	   NULL                                      AS SOLD_TURN_KEY_OPTION_UNIT_PRICE , 
	   NULL                                      AS SOLD_FURNITURE_OPTION ,
	   NULL                                      AS UNSOLD_TURN_KEY_OPTION_UNIT_PRICE , 
	   NULL                                      AS UNSOLD_FURNITURE_OPTION ,
	   SUM(IFNULL(TURN_KEY_OPTION_UNIT_PRICE,0)) AS TURN_KEY_OPTION_UNIT_PRICE , 
	   SUM(FURNITURE_OPTION)                     AS FURNITURE_OPTION ,
	   NULL                                      AS PCT_SOLD_TURN_KEY_OPTION_UNIT_PRICE ,
	   NULL                                      AS PCT_SOLD_FURNITURE_OPTION ,
	   SUM(LIVING_AREA)                          AS EST_LIVING_AREA
  FROM TOTALUNITS
 GROUP BY PROPERTY_ID,	
          BUILDING
),               
TOTALSOLDDETAILS
AS
(
SELECT DIMDATE.FULLDATE,
       SUM(CASE WHEN SPACE.SPACE_ID IS NOT NULL 
	            THEN 1 ELSE 0 
	    END)                                        AS SOLD_UNIT,
       SUM(IFNULL( SPACE.SQ_FOOTAGE,0))                 AS SPACE_SQ_FOOTAGE,
       SUM(IFNULL(SPACE.UNIT_PRICE,0))                  AS UNIT_PRICE,
       SPACE.PROPERTY_ID                                AS PROPERTY_ID,
       SPACE.BUILDING                                   AS BUILDING,
       SUM(CASE WHEN OPP.X30_DAY_RESCISSION_DATE >DIMDATE.FULLDATE    
	            THEN 1 ELSE 0 
	    END)                                        AS RECISION_COUNT,
       SUM(IFNULL(SPACE.EST_NET_LIVING_AREA_SF,0))      AS LIVING_AREA,
       SUM(IFNULL(OPP.CREDIT_TOWARDS_PURCHASE_PRICE,0)) AS TOTAL_CREDITS,
       SUM(IFNULL(SPACE.TURN_KEY_OPTION_UNIT_PRICE,0))  AS TURN_KEY_OPTION_UNIT_PRICE 
  FROM DIMDATE
  LEFT JOIN  datalake_dev.semantic.DIM_SF_OPPORTUNITY    OPP 
         ON CAST(OPP.CONTRACT_SIGNED_BY_SELLER AS DATE) = DIMDATE.FULLDATE
  LEFT JOIN  datalake_dev.semantic.DIM_SF_PROPSPACE   SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID 
 INNER JOIN  datalake_dev.semantic.LUP_SF_RECORD_TYPE   RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 WHERE OPP.BUILDING IS NOT NULL
   AND SPACE.SPACE_STATUS <> 'Not Available'
   AND OPP.ISWON = 'TRUE'
   AND RECTYPE.NAME = 'Ward Residential'
 GROUP BY DIMDATE.FULLDATE,
          SPACE.PROPERTY_id,
	  SPACE.BUILDING
), 
MISSING_BASE
AS
(
SELECT DISTINCT DIMDATE.FULLDATE, 
                OPP.BUILDING AS BUILDING,
		SPACE.PROPERTY_ID  AS PROPERTY_ID
  FROM 
       datalake_dev.semantic.DIM_SF_OPPORTUNITY   OPP
 INNER JOIN  datalake_dev.semantic.DIM_SF_PROPSPACE SPACE 
         ON OPP.SPACE_ID = SPACE.SPACE_ID
 INNER JOIN datalake_dev.semantic.LUP_SF_RECORD_TYPE   RECTYPE 
         ON RECTYPE.RECORD_TYPE_ID = SPACE.RECORD_TYPE_ID
 CROSS JOIN DIMDATE
 WHERE 
       RECTYPE.NAME = 'Ward Residential'
   AND SPACE.SPACE_STATUS <> 'Not Available'
   AND OPP.BUILDING IS NOT NULL
   AND OPP.ISWON = 'TRUE'
  ORDER BY DIMDATE.FULLDATE
),           
MISSING_DATE_ADDED
AS
(
SELECT M.FULLDATE,
       M.BUILDING,
       IFNULL(SOLD_UNIT,0)                  AS MTD_SOLD_UNIT,       
       IFNULL(UNIT_PRICE,0)                 AS MTD_UNIT_PRICE, 
       M.PROPERTY_ID, 
       IFNULL(RECISION_COUNT,0)             AS MTD_RECISION_COUNT, 
       IFNULL(LIVING_AREA,0)                AS MTD_LIVING_AREA,
       IFNULL(TOTAL_CREDITS,0)              AS MTD_TOTAL_CREDITS,
       IFNULL(TURN_KEY_OPTION_UNIT_PRICE,0) AS MTD_TURN_KEY_OPTION_UNIT_PRICE
  FROM 
       MISSING_BASE M
  LEFT JOIN TOTALSOLDDETAILS R 
         ON M.FULLDATE = R.FULLDATE 
	AND M.BUILDING = R.BUILDING
 ), 
RUNNING
AS
(
SELECT FULLDATE,
       PROPERTY_ID,
       BUILDING,
       SUM(MTD_SOLD_UNIT)                           OVER(PARTITION BY MONTH(FULLDATE),YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS MTD_SOLD_UNIT,
       SUM(MTD_SOLD_UNIT)                           OVER(PARTITION BY YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS YTD_SOLD_UNIT,
       SUM(MTD_SOLD_UNIT)                           OVER(PARTITION BY PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS PTD_SOLD_UNIT,    
       MTD_UNIT_PRICE, 
       SUM(MTD_UNIT_PRICE)                          OVER(PARTITION BY YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS YTD_SOLD_REVENUE,
       SUM(MTD_UNIT_PRICE)                          OVER(PARTITION BY PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS PTD_SOLD_REVENUE,
       MTD_RECISION_COUNT, 
       SUM(MTD_RECISION_COUNT)                      OVER(PARTITION BY YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS YTD_RECISION_COUNT,
       SUM(MTD_RECISION_COUNT)                      OVER(PARTITION BY PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS PTD_RECISION_COUNT,
       SUM(MTD_LIVING_AREA)                         OVER(PARTITION BY MONTH(FULLDATE),YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS MTD_LIVING_AREA,
       SUM(MTD_LIVING_AREA)                         OVER(PARTITION BY YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS YTD_LIVING_AREA,
       SUM(MTD_LIVING_AREA)                         OVER(PARTITION BY PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS PTD_LIVING_AREA,
       SUM(MTD_TOTAL_CREDITS)                       OVER(PARTITION BY MONTH(FULLDATE),YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS MTD_TOTAL_CREDITS,
       SUM(MTD_TOTAL_CREDITS)                       OVER(PARTITION BY YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS YTD_TOTAL_CREDITS,
       SUM(MTD_TOTAL_CREDITS)                       OVER(PARTITION BY PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS PTD_TOTAL_CREDITS,     
       MTD_TURN_KEY_OPTION_UNIT_PRICE,
       SUM(IFNULL(MTD_TURN_KEY_OPTION_UNIT_PRICE,0))OVER(PARTITION BY YEAR(FULLDATE),PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS YTD_SOLD_TURN_KEY,
       SUM(IFNULL(MTD_TURN_KEY_OPTION_UNIT_PRICE,0))OVER(PARTITION BY PROPERTY_ID 
	                                                     ORDER BY FULLDATE)                                   AS PTD_SOLD_TURN_KEY 
  FROM MISSING_DATE_ADDED
),  
BUILDING_FINAL
AS
(
SELECT FULLDATE,
       RUNNING.PROPERTY_ID,
       RUNNING.BUILDING,
       MTD_SOLD_UNIT,
       YTD_SOLD_UNIT, 
       PTD_SOLD_UNIT,
       TOTAL_UNITS,
       MTD_UNIT_PRICE                AS MTD_SOLD_REVENUE, 
       YTD_SOLD_REVENUE, 
       PTD_SOLD_REVENUE, 
       TOTAL_REV, 
       MTD_RECISION_COUNT, 
       YTD_RECISION_COUNT, 
       PTD_RECISION_COUNT,
       MTD_LIVING_AREA, 
       YTD_LIVING_AREA, 
       PTD_LIVING_AREA, 
       EST_LIVING_AREA                AS TOTAL_EST_LIVING_AREA,
       MTD_TOTAL_CREDITS, 
       YTD_TOTAL_CREDITS,
       PTD_TOTAL_CREDITS,
       MTD_TURN_KEY_OPTION_UNIT_PRICE, 
       YTD_SOLD_TURN_KEY, 
       PTD_SOLD_TURN_KEY
  FROM RUNNING
 INNER JOIN BUILDINGSALES TAL 
    ON RUNNING.PROPERTY_ID = TAL.PROPERTY_ID
)
SELECT BUILDING_FINAL.FULLDATE,
       BUILDING_FINAL.PROPERTY_ID,
       BUILDING_FINAL.BUILDING,
       BUILDING_FINAL.MTD_SOLD_UNIT,
       BUILDING_FINAL.YTD_SOLD_UNIT,
       BUILDING_FINAL.PTD_SOLD_UNIT,
       BUILDING_FINAL.TOTAL_UNITS,
       BUILDING_FINAL.MTD_SOLD_REVENUE, 
       BUILDING_FINAL.YTD_SOLD_REVENUE, 
       BUILDING_FINAL.PTD_SOLD_REVENUE, 
       BUILDING_FINAL.TOTAL_REV, 
       UNIT_LIST.SPACE_ID, 
       UNIT_LIST.CONTRACT_SIGNED_BY_SELLER,
       CASE WHEN CONTRACT_SIGNED_BY_SELLER BETWEEN DATE_TRUNC(MONTH, BUILDING_FINAL.FULLDATE::DATE) AND BUILDING_FINAL.FULLDATE 
	        THEN 1 ELSE  0 
	END                            AS MTD_UNIT_SOLD_FLAG,
       CASE WHEN CONTRACT_SIGNED_BY_SELLER BETWEEN DATE_TRUNC(YEAR, BUILDING_FINAL.FULLDATE::DATE) AND BUILDING_FINAL.FULLDATE 
	        THEN 1 ELSE  0 
	END                            AS YTD_UNIT_SOLD_FLAG,
       CASE WHEN CONTRACT_SIGNED_BY_SELLER <= BUILDING_FINAL.FULLDATE 
	        THEN 1 ELSE  0 
	END                            AS PTD_UNIT_SOLD_FLAG, 
       BUILDING_FINAL.MTD_LIVING_AREA, 
       BUILDING_FINAL.YTD_LIVING_AREA, 
       BUILDING_FINAL.PTD_LIVING_AREA, 
       BUILDING_FINAL.TOTAL_EST_LIVING_AREA, 
       BUILDING_FINAL.MTD_TOTAL_CREDITS, 
       BUILDING_FINAL.YTD_TOTAL_CREDITS, 
       BUILDING_FINAL.PTD_TOTAL_CREDITS, 
       BUILDING_FINAL.MTD_TURN_KEY_OPTION_UNIT_PRICE, 
       BUILDING_FINAL.YTD_SOLD_TURN_KEY, 
       BUILDING_FINAL.PTD_SOLD_TURN_KEY, 
       CURRENT_TIMESTAMP::DATETIME CREATED_DATE
  FROM BUILDING_FINAL
  LEFT JOIN UNIT_LIST 
         ON BUILDING_FINAL.PROPERTY_ID = UNIT_LIST.PROPERTY_ID
      );
    