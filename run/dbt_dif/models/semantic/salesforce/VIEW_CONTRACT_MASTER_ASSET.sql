
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_MASTER_ASSET  as (
    
SELECT Distinct CASE WHEN IFNULL(MASTER_ASSET_C, '') = '' then 'Unknown'
                     ELSE MASTER_ASSET_C
                     END as Master_Asset
,MASTER_ASSET_C as MASTER_ASSET_ACTUAL                     
FROM datalake_dev.semantic.VIEW_CONTRACT_FORM   cf
WHERE (cf.CONTRACT_STATUS_C IS NOT NULL and cf.CONTRACT_STATUS_C <> ''
AND cf.CONTRACT_STATUS_C NOT IN ('S13: Dead','X: Terminated','X: Dead'))
AND cf.Master_Asset_C not in ('Alameda','Bridges at Mint Hill','Circle T','Century Plaza','Cottonwood Square','Cottonwood Mall','Elk Grove'
,'Fashion Show Air Rights','Landmark Mall', 'Monarch City','Kendall','Park West','Redlands','The Club at Carlton Woods','West Windsor')
ORDER BY 1
  );
