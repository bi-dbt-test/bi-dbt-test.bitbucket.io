

      create or replace  table datalake_dev.semantic.LUP_SF_RECORD_TYPE  as
      (-- USED in FACT_LEASING_ACTIVITY
-- CREATE OR REPLACE TABLE 
-- with DIM_SF_RECORDTYPE AS 
-- SELECT 'UNKNOWN' AS RECORD_TYPE_ID,
--       'UNKNOWN' AS NAME,
--       'UNKNOWN' DESCRIPTION
-- UNION ALL
SELECT ID RECORD_TYPE_ID, 
       NAME, 
       DESCRIPTION   
 FROM  datalake_dev.governed.SALESFORCE_RECORDTYPE
      );
    