
  create or replace  view datalake_dev.semantic.VIEW_CONDO_LEADMARKETING_CHANNELS  as (
    
SELECT 
LEAD.LEAD_ID,
CASE WHEN LEAD.MARKETING_CHANNELS = '' then 'N/A'
            WHEN LEAD.MARKETING_CHANNELS IS NULL then 'N/A'
            ELSE LEAD.MARKETING_CHANNELS
            END as MARKETING_CHANNELS
,case when DAYOFWEEK(LEAD.CREATEDDATE) = 0 then 1
      when DAYOFWEEK(LEAD.CREATEDDATE) = 1 then 2
      when DAYOFWEEK(LEAD.CREATEDDATE) = 2 then 3
      when DAYOFWEEK(LEAD.CREATEDDATE) = 3 then 4
      when DAYOFWEEK(LEAD.CREATEDDATE) = 4 then 5
      when DAYOFWEEK(LEAD.CREATEDDATE) = 5 then 6
      when DAYOFWEEK(LEAD.CREATEDDATE) = 6 then 7
      END as DayOrder
,case when DAYOFWEEK(LEAD.CREATEDDATE) = 0 then 'Sunday'
      when DAYOFWEEK(LEAD.CREATEDDATE) = 1 then 'Monday'
      when DAYOFWEEK(LEAD.CREATEDDATE) = 2 then 'Tuesday'
      when DAYOFWEEK(LEAD.CREATEDDATE) = 3 then 'Wednesday'
      when DAYOFWEEK(LEAD.CREATEDDATE) = 4 then 'Thursday'
      when DAYOFWEEK(LEAD.CREATEDDATE) = 5 then 'Friday'
      when DAYOFWEEK(LEAD.CREATEDDATE) = 6 then 'Saturday'
      END as WeekDayName
,LEAD.CREATEDDATE AS LEAD_CREATE_DATE
,CAST(USER1.WV_INTERNAL_AGENT AS INT) WV_INTERNAL_AGENT
,3 as WV_INTERNAL_AGENT_ALL
FROM  datalake_dev.semantic.DIM_SF_LEAD  LEAD
INNER JOIN datalake_dev.semantic.DIM_SF_USER   USER1 on LEAD.OWNERID = USER1.USER_ID
where DAYOFWEEK(LEAD.CREATEDDATE) is not null
and LEAD.ISDELETED = 'FALSE'
and LEAD.Name <>'[not provided]'
  );
