

      create or replace  table datalake_dev.semantic.STG_ALL_AGENTS_SUMMARY  as
      (select ifnull(s.FULLDATE,co.Contract_Signed_by_Seller) as FULLDATE,s.PROPERTY_ID, case when s.Sort_Order = 1 then 'New Sales'  
    when s.Sort_Order = 2 then 'Transfer'  
    when s.Sort_Order = 3 then 'Cancellations' end as SalesType  
    ,co.Opportunity_ID,co.Building, concat(acct.FIRST_NAME, ' ', acct.LAST_NAME) as Account_Name  
    ,concat(usr1.First_name,' ',usr1.LastName) as WV_Agent,concat(usr2.First_name,' ',usr2.LastName) as WV_Agent_Secondary  
    ,CASE WHEN usr1.Country = 'JP' then 'Japan' else usr1.Country end  as WV_Agent_Country  
    , prop.Name as Unit_Num, co.Reason_for_Purchase, IFNULL(SO.Proforma_Price,CO.Unit_Price)  AS Proforma_Price  
    , s.Net_Price  as  Purchase_Price ,s.CREDIT_TOWARDS_PURCHASE_PRICE_C  AS Concession  
    ,CASE WHEN co.Primary_Owner_Share IS NULL  
           THEN (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end)  
           ELSE (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end) * (co.Primary_Owner_Share / 100)  
    	   END  AS Net_Price  
    , case when s.Sort_Order = 3 then -1  
            when s.Sort_Order = 2 then 0 else 1 end *  
     CASE WHEN co.Primary_Owner_Share IS NULL THEN 1  
           ELSE co.Primary_Owner_Share / 100  
           END AS Net_Units  
    ,co.Contract_Signed_by_Seller,co.CANCELLED_DATE,co.TRANSFER_COMPLETE_DATE,co.TRANSFERRED_TO_UNIT,co.TRANSFERRED_FROM, co.ISWON  
    ,vds.WV_INTERNAL_AGENT, vds.WV_INTERNAL_AGENT_ALL, 1 as Level,co.CONTRACT_SIGNED_BY_SELLER as Contract_Signed_by_Seller_Original_Sale  
    from table(SEMANTIC.RPT_SF_CONDO_SALES(TO_Date('2013-01-01')::date,CURRENT_DATE::date)) s  
    inner join datalake_dev.semantic.DIM_SF_PROPERTY  cp on s.PROPERTY_ID = cp.PROPERTY_ID  
    inner join datalake_dev.semantic.DIM_SF_OPPORTUNITY   co on s.OPPORTUNITY_ID = co.OPPORTUNITY_ID  
    left join  datalake_dev.semantic.VIEW_FACT_CONDO_SALES_OPP  so on s.OPPORTUNITY_ID = so.OPPTY_ID  
    inner join  datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS   vds on co.OWNER_ID = vds.OWNER_ID  
    inner join  datalake_dev.semantic.DIM_SF_PROPSPACE   prop on co.SPACE_ID = prop.SPACE_ID  
    inner join datalake_dev.semantic.DIM_SF_ACCOUNT   acct on co.ACCOUNT_ID = acct.ACCOUNT_ID  
    inner join  datalake_dev.semantic.DIM_SF_USER  usr1 on co.OWNER_ID = usr1.USER_ID  
    left join datalake_dev.semantic.DIM_SF_USER   usr2 on co.SECONDARY_OWNER = usr2.USER_ID  
    where s.Sort_Order in (1,2,3)  
    and s.PROPERTY_ID is not null   
    UNION ALL  
    select ifnull(s.FULLDATE,co.Contract_Signed_by_Seller) as FULLDATE,s.PROPERTY_ID, case when s.Sort_Order = 1 then 'New Sales'   
    when s.Sort_Order = 2 then 'Transfer'   
    when s.Sort_Order = 3 then 'Cancellations' end as SalesType  
    ,co.Opportunity_ID,co.Building, concat(acct.FIRST_NAME, ' ', acct.LAST_NAME) as Account_Name  
    ,concat(usr1.First_name,' ',usr1.LastName) as WV_Agent,Null as WV_Agent_Secondary  
    ,CASE WHEN usr1.Country = 'JP' then 'Japan' else usr1.Country end  as WV_Agent_Country  
    , prop.Name as Unit_Num, co.Reason_for_Purchase, S.OPP_UNIT_PRICE_C AS Proforma_Price  
    ,s.NET_PRICE as  Purchase_Price,s.CREDIT_TOWARDS_PURCHASE_PRICE_C  AS Concession  
    ,CASE WHEN co.Secondary_Owner_Share IS NULL   
           THEN (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end)  
           ELSE (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end) * (co.Secondary_Owner_Share / 100)   
    	   END  AS Net_Price  
    , case when s.Sort_Order = 3 then -1   
            when s.Sort_Order = 2 then 0 else 1 end *  
     CASE WHEN co.Secondary_Owner_Share IS NULL THEN 1  
           ELSE co.Secondary_Owner_Share / 100  
           END AS Net_Units  
    ,co.Contract_Signed_by_Seller,co.CANCELLED_DATE,co.TRANSFER_COMPLETE_DATE,co.TRANSFERRED_TO_UNIT,co.TRANSFERRED_FROM, co.ISWON  
    ,vds.WV_INTERNAL_AGENT, vds.WV_INTERNAL_AGENT_ALL, 2 as Level,co.CONTRACT_SIGNED_BY_SELLER as Contract_Signed_by_Seller_Original_Sale  
    from table(SEMANTIC.RPT_SF_CONDO_SALES(TO_Date('2013-01-01')::date,CURRENT_DATE::date)) s  
    inner join datalake_dev.semantic.DIM_SF_PROPERTY  cp on s.PROPERTY_ID = cp.PROPERTY_ID  
    inner join datalake_dev.semantic.DIM_SF_OPPORTUNITY    co on s.OPPORTUNITY_ID = co.OPPORTUNITY_ID  
    inner join datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS    vds on co.OWNER_ID = vds.OWNER_ID  
    inner join datalake_dev.semantic.DIM_SF_PROPSPACE     prop on co.SPACE_ID = prop.SPACE_ID  
    inner join datalake_dev.semantic.DIM_SF_ACCOUNT   acct on co.ACCOUNT_ID = acct.ACCOUNT_ID  
    inner join datalake_dev.semantic.DIM_SF_USER   usr1 on co.SECONDARY_OWNER = usr1.USER_ID  
    where s.Sort_Order in (1,2,3)  
    and s.Opportunity_ID not in('0061Y00000qGcmqQAC','0061Y00000qGrI3QAK','a001Y00000p4WzaQAE')  
    and s.PROPERTY_ID is not null  
    and co.Secondary_Owner_Share > 0  
    UNION ALL  
    select ifnull(s.FULLDATE,co.Contract_Signed_by_Seller) as FULLDATE,s.PROPERTY_ID, case when s.Sort_Order = 1 then 'New Sales'   
    when s.Sort_Order = 2 then 'Transfer'   
    when s.Sort_Order = 3 then 'Cancellations' end as SalesType  
    ,co.Opportunity_ID,co.Building, concat(acct.FIRST_NAME, ' ', acct.LAST_NAME) as Account_Name  
    ,concat(usr1.First_name,' ',usr1.LastName) as WV_Agent,Null as WV_Agent_Secondary  
    ,CASE WHEN usr1.Country = 'JP' then 'Japan' else usr1.Country end  as WV_Agent_Country  
    , prop.Name as Unit_Num, co.Reason_for_Purchase, S.OPP_UNIT_PRICE_C AS Proforma_Price  
    ,s.NET_PRICE as  Purchase_Price,s.CREDIT_TOWARDS_PURCHASE_PRICE_C  AS Concession  
    ,CASE WHEN co.TERTIARY_OWNER_SHARE IS NULL   
           THEN (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end)  
           ELSE (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end) * (co.TERTIARY_OWNER_SHARE / 100)   
    	   END  AS Net_Price  
    , case when s.Sort_Order = 3 then -1   
            when s.Sort_Order = 2 then 0 else 1 end *  
     CASE WHEN co.TERTIARY_OWNER_SHARE IS NULL THEN 1  
           ELSE co.TERTIARY_OWNER_SHARE / 100  
           END AS Net_Units  
    ,co.Contract_Signed_by_Seller,co.CANCELLED_DATE,co.TRANSFER_COMPLETE_DATE,co.TRANSFERRED_TO_UNIT,co.TRANSFERRED_FROM, co.ISWON  
    ,vds.WV_INTERNAL_AGENT, vds.WV_INTERNAL_AGENT_ALL, 3 as Level,co.CONTRACT_SIGNED_BY_SELLER as Contract_Signed_by_Seller_Original_Sale  
    from table( SEMANTIC.RPT_SF_CONDO_SALES(TO_Date('2013-01-01')::date,CURRENT_DATE::date)) s  
    inner join  datalake_dev.semantic.DIM_SF_PROPERTY    cp on s.PROPERTY_ID = cp.PROPERTY_ID  
    inner join datalake_dev.semantic.DIM_SF_OPPORTUNITY    co on s.OPPORTUNITY_ID = co.OPPORTUNITY_ID  
    inner join datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS    vds on co.OWNER_ID = vds.OWNER_ID  
    inner join  datalake_dev.semantic.DIM_SF_PROPSPACE    prop on co.SPACE_ID = prop.SPACE_ID  
    inner join  datalake_dev.semantic.DIM_SF_ACCOUNT  acct on co.ACCOUNT_ID = acct.ACCOUNT_ID  
    inner join datalake_dev.semantic.DIM_SF_USER   usr1 on co.THIRD_OWNER = usr1.USER_ID  
    where s.Sort_Order in (1,2,3)  
    and s.PROPERTY_ID is not null   
    and co.TERTIARY_OWNER_SHARE > 0  
    UNION ALL  
    select ifnull(s.FULLDATE,co.Contract_Signed_by_Seller) as FULLDATE,s.PROPERTY_ID, case when s.Sort_Order = 1 then 'New Sales'   
    when s.Sort_Order = 2 then 'Transfer'   
    when s.Sort_Order = 3 then 'Cancellations' end as SalesType  
    ,co.Opportunity_ID,co.Building, concat(acct.FIRST_NAME, ' ', acct.LAST_NAME) as Account_Name  
    ,concat(usr1.First_name,' ',usr1.LastName) as WV_Agent,Null as WV_Agent_Secondary  
    ,CASE WHEN usr1.Country = 'JP' then 'Japan' else usr1.Country end  as WV_Agent_Country  
    , prop.Name as Unit_Num, co.Reason_for_Purchase, S.OPP_UNIT_PRICE_C AS Proforma_Price  
    ,s.NET_PRICE as  Purchase_Price  
    ,s.CREDIT_TOWARDS_PURCHASE_PRICE_C  AS Concession  
    ,CASE WHEN co.QUATERNARY_OWNER_SHARE IS NULL   
           THEN (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end)  
           ELSE (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end) * (co.QUATERNARY_OWNER_SHARE / 100)   
    	   END  AS Net_Price  
    , case when s.Sort_Order = 3 then -1   
            when s.Sort_Order = 2 then 0 else 1 end *  
     CASE WHEN co.QUATERNARY_OWNER_SHARE IS NULL THEN 1  
           ELSE co.QUATERNARY_OWNER_SHARE / 100  
           END AS Net_Units  
    ,co.Contract_Signed_by_Seller,co.CANCELLED_DATE,co.TRANSFER_COMPLETE_DATE,co.TRANSFERRED_TO_UNIT,co.TRANSFERRED_FROM, co.ISWON  
    ,vds.WV_INTERNAL_AGENT, vds.WV_INTERNAL_AGENT_ALL, 4 as Level,co.CONTRACT_SIGNED_BY_SELLER as Contract_Signed_by_Seller_Original_Sale  
       from table( SEMANTIC.RPT_SF_CONDO_SALES(TO_Date('2013-01-01')::date,CURRENT_DATE::date)) s  
    inner join  datalake_dev.semantic.DIM_SF_PROPERTY    cp on s.PROPERTY_ID = cp.PROPERTY_ID  
    inner join datalake_dev.semantic.DIM_SF_OPPORTUNITY    co on s.OPPORTUNITY_ID = co.OPPORTUNITY_ID  
    inner join datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS    vds on co.OWNER_ID = vds.OWNER_ID  
    inner join  datalake_dev.semantic.DIM_SF_PROPSPACE    prop on co.SPACE_ID = prop.SPACE_ID  
    inner join  datalake_dev.semantic.DIM_SF_ACCOUNT  acct on co.ACCOUNT_ID = acct.ACCOUNT_ID  
    inner join datalake_dev.semantic.DIM_SF_USER   usr1 on co.THIRD_OWNER = usr1.USER_ID  
    where s.Sort_Order in (1,2,3)  
    and s.PROPERTY_ID is not null   
    and co.QUATERNARY_OWNER_SHARE > 0  
    UNION ALL  
    select ifnull(s.FULLDATE,co.Contract_Signed_by_Seller) as FULLDATE,s.PROPERTY_ID, case when s.Sort_Order = 1 then 'New Sales'   
    when s.Sort_Order = 2 then 'Transfer'   
    when s.Sort_Order = 3 then 'Cancellations' end as SalesType  
    ,co.Opportunity_ID,co.Building, concat(acct.FIRST_NAME, ' ', acct.LAST_NAME) as Account_Name  
    ,concat(usr1.First_name,' ',usr1.LastName) as WV_Agent,Null as WV_Agent_Secondary  
    ,CASE WHEN usr1.Country = 'JP' then 'Japan' else usr1.Country end  as WV_Agent_Country  
    , prop.Name as Unit_Num, co.Reason_for_Purchase, S.OPP_UNIT_PRICE_C AS Proforma_Price  
    ,s.NET_PRICE as  Purchase_Price  
    ,s.CREDIT_TOWARDS_PURCHASE_PRICE_C  AS Concession  
    ,CASE WHEN co.QUINARY_OWNER_SHARE IS NULL   
           THEN (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end)  
           ELSE (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end) * (co.QUINARY_OWNER_SHARE / 100)   
    	   END  AS Net_Price  
    , case when s.Sort_Order = 3 then -1   
            when s.Sort_Order = 2 then 0 else 1 end *  
     CASE WHEN co.QUINARY_OWNER_SHARE IS NULL THEN 1  
           ELSE co.QUINARY_OWNER_SHARE / 100  
           END AS Net_Units  
    ,co.Contract_Signed_by_Seller,co.CANCELLED_DATE,co.TRANSFER_COMPLETE_DATE,co.TRANSFERRED_TO_UNIT,co.TRANSFERRED_FROM, co.ISWON  
    ,vds.WV_INTERNAL_AGENT, vds.WV_INTERNAL_AGENT_ALL, 5 as Level,co.CONTRACT_SIGNED_BY_SELLER as Contract_Signed_by_Seller_Original_Sale  
       from table( SEMANTIC.RPT_SF_CONDO_SALES(TO_Date('2013-01-01')::date,CURRENT_DATE::date)) s  
    inner join  datalake_dev.semantic.DIM_SF_PROPERTY    cp on s.PROPERTY_ID = cp.PROPERTY_ID  
    inner join datalake_dev.semantic.DIM_SF_OPPORTUNITY    co on s.OPPORTUNITY_ID = co.OPPORTUNITY_ID  
    inner join datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS    vds on co.OWNER_ID = vds.OWNER_ID  
    inner join  datalake_dev.semantic.DIM_SF_PROPSPACE    prop on co.SPACE_ID = prop.SPACE_ID  
    inner join  datalake_dev.semantic.DIM_SF_ACCOUNT  acct on co.ACCOUNT_ID = acct.ACCOUNT_ID  
    inner join datalake_dev.semantic.DIM_SF_USER   usr1 on co.THIRD_OWNER = usr1.USER_ID  
    where s.Sort_Order in (1,2,3)  
    and s.PROPERTY_ID is not null   
    and co.QUINARY_OWNER_SHARE > 0  
    UNION ALL  
    select ifnull(s.FULLDATE,co.Contract_Signed_by_Seller) as FULLDATE,s.PROPERTY_ID, case when s.Sort_Order = 1 then 'New Sales'   
    when s.Sort_Order = 2 then 'Transfer'   
    when s.Sort_Order = 3 then 'Cancellations' end as SalesType  
    ,co.Opportunity_ID,co.Building, concat(acct.FIRST_NAME, ' ', acct.LAST_NAME) as Account_Name  
    ,concat(usr1.First_name,' ',usr1.LastName) as WV_Agent,Null as WV_Agent_Secondary  
    ,CASE WHEN usr1.Country = 'JP' then 'Japan' else usr1.Country end  as WV_Agent_Country  
    , prop.Name as Unit_Num, co.Reason_for_Purchase, S.OPP_UNIT_PRICE_C AS Proforma_Price  
    ,s.NET_PRICE as  Purchase_Price  
    ,s.CREDIT_TOWARDS_PURCHASE_PRICE_C  AS Concession  
    ,CASE WHEN co.SENARY_OWNER_SHARE IS NULL   
           THEN (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end)  
           ELSE (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end) * (co.SENARY_OWNER_SHARE / 100)   
    	   END  AS Net_Price  
    , case when s.Sort_Order = 3 then -1   
            when s.Sort_Order = 2 then 0 else 1 end *  
     CASE WHEN co.SENARY_OWNER_SHARE IS NULL THEN 1  
           ELSE co.SENARY_OWNER_SHARE / 100  
           END AS Net_Units  
    ,co.Contract_Signed_by_Seller,co.CANCELLED_DATE,co.TRANSFER_COMPLETE_DATE,co.TRANSFERRED_TO_UNIT,co.TRANSFERRED_FROM, co.ISWON  
    ,vds.WV_INTERNAL_AGENT, vds.WV_INTERNAL_AGENT_ALL, 6 as Level,co.CONTRACT_SIGNED_BY_SELLER as Contract_Signed_by_Seller_Original_Sale  
      from table( SEMANTIC.RPT_SF_CONDO_SALES(TO_Date('2013-01-01')::date,CURRENT_DATE::date)) s  
    inner join  datalake_dev.semantic.DIM_SF_PROPERTY    cp on s.PROPERTY_ID = cp.PROPERTY_ID  
    inner join datalake_dev.semantic.DIM_SF_OPPORTUNITY    co on s.OPPORTUNITY_ID = co.OPPORTUNITY_ID  
    inner join datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS    vds on co.OWNER_ID = vds.OWNER_ID  
    inner join  datalake_dev.semantic.DIM_SF_PROPSPACE    prop on co.SPACE_ID = prop.SPACE_ID  
    inner join  datalake_dev.semantic.DIM_SF_ACCOUNT  acct on co.ACCOUNT_ID = acct.ACCOUNT_ID  
    inner join datalake_dev.semantic.DIM_SF_USER   usr1 on co.THIRD_OWNER = usr1.USER_ID  
    where s.Sort_Order in (1,2,3)  
    and s.PROPERTY_ID is not null   
    and co.SENARY_OWNER_SHARE > 0  
    UNION ALL  
    select ifnull(s.FULLDATE,co.Contract_Signed_by_Seller) as FULLDATE,s.PROPERTY_ID, case when s.Sort_Order = 1 then 'New Sales'   
    when s.Sort_Order = 2 then 'Transfer'   
    when s.Sort_Order = 3 then 'Cancellations' end as SalesType  
    ,co.Opportunity_ID,co.Building, concat(acct.FIRST_NAME, ' ', acct.LAST_NAME) as Account_Name  
    ,concat(usr1.First_name,' ',usr1.LastName) as WV_Agent,Null as WV_Agent_Secondary  
    ,CASE WHEN usr1.Country = 'JP' then 'Japan' else usr1.Country end  as WV_Agent_Country  
    , prop.Name as Unit_Num, co.Reason_for_Purchase, S.OPP_UNIT_PRICE_C AS Proforma_Price  
    ,s.NET_PRICE as  Purchase_Price  
    ,s.CREDIT_TOWARDS_PURCHASE_PRICE_C  AS Concession  
    ,CASE WHEN co.SEPTENARY_OWNER_SHARE IS NULL   
           THEN (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end)  
           ELSE (case when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price < 0 then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  * -1  
    when cp.Trade_Name in ('A''ali''i','Ae''o','Waiea','Victoria Place') and s.Net_Price >= 0  then (prop.SALE_PRICE - IFNULL(CO.Credit_Towards_Purchase_Price,0))  
    when cp.Trade_Name in ('Koula','Anaha') then s.Net_Price  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price < 0 then s.SPACE_UNIT_PRICE_C * -1  
    when cp.Trade_Name not in ('A''ali''i','Koula','Ae''o','Anaha','Waiea') and s.Net_Price >= 0 then s.SPACE_UNIT_PRICE_C  
     else s.SPACE_UNIT_PRICE_C end) * (co.SEPTENARY_OWNER_SHARE / 100)   
    	   END  AS Net_Price  
    , case when s.Sort_Order = 3 then -1   
            when s.Sort_Order = 2 then 0 else 1 end *  
     CASE WHEN co.SEPTENARY_OWNER_SHARE IS NULL THEN 1  
           ELSE co.SEPTENARY_OWNER_SHARE / 100  
           END AS Net_Units  
    ,co.Contract_Signed_by_Seller,co.CANCELLED_DATE,co.TRANSFER_COMPLETE_DATE,co.TRANSFERRED_TO_UNIT,co.TRANSFERRED_FROM, co.ISWON  
    ,vds.WV_INTERNAL_AGENT, vds.WV_INTERNAL_AGENT_ALL, 7 as Level,co.CONTRACT_SIGNED_BY_SELLER as Contract_Signed_by_Seller_Original_Sale  
       from table( SEMANTIC.RPT_SF_CONDO_SALES(TO_Date('2013-01-01')::date,CURRENT_DATE::date)) s  
    inner join  datalake_dev.semantic.DIM_SF_PROPERTY    cp on s.PROPERTY_ID = cp.PROPERTY_ID  
    inner join datalake_dev.semantic.DIM_SF_OPPORTUNITY    co on s.OPPORTUNITY_ID = co.OPPORTUNITY_ID  
    inner join datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS    vds on co.OWNER_ID = vds.OWNER_ID  
    inner join  datalake_dev.semantic.DIM_SF_PROPSPACE    prop on co.SPACE_ID = prop.SPACE_ID  
    inner join  datalake_dev.semantic.DIM_SF_ACCOUNT  acct on co.ACCOUNT_ID = acct.ACCOUNT_ID  
    inner join datalake_dev.semantic.DIM_SF_USER   usr1 on co.THIRD_OWNER = usr1.USER_ID  
    where s.Sort_Order in (1,2,3)  
    and s.PROPERTY_ID is not null   
    and co.SEPTENARY_OWNER_SHARE > 0
      );
    