
  create or replace  view datalake_dev.semantic.VIEW_CONDO_DAILY_SALES_AGENTS  as (
    
select USER_ID as OWNER_ID, CAST(WV_INTERNAL_AGENT AS INT)  as WV_INTERNAL_AGENT
,3 as WV_INTERNAL_AGENT_ALL
from datalake_dev.semantic.DIM_SF_USER    
where WV_INTERNAL_AGENT is not null
  );
