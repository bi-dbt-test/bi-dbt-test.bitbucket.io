
  create or replace  view datalake_dev.semantic.VIEW_CONTRACT_DURATION_SUMMARY_LEGAL_REVIEW  as (
    
WITH tempLegal_Duration(Legal_First_Name,Legal_Last_Name,Legal_Name,Legal_Team_Member_c,Number_of_Contracts,Legal_Duration) AS
(SELECT IFNULL(Legal_First_Name,'Unknown') as Legal_First_Name, IFNULL(Legal_Last_Name, 'Unknown') as Legal_Last_Name, CONCAT(Legal_First_Name,' ',Legal_Last_Name) AS Legal_Name, Legal_Team_Member_c, SUM(Number_of_Contracts) as Number_of_Contracts
, sum(Legal_Duration)/SUM(Number_of_Contracts) as Legal_Duration
FROM (
SELECT IFNULL(deu.First_Name,'Unknown') as Legal_First_Name, IFNULL(deu.LastName, 'Unknown') as Legal_Last_Name,Legal_Team_Member_c, 1 as Number_of_Contracts
,CASE WHEN cf.Contract_Status_c in ('S1: Decision Maker Approval','S1a: Risk Management Approval','S1b: Executive Approval','S2: CFO Approval','S3: General Counsel Approval') 
	  THEN 0
	  WHEN cf.Contract_Status_c in ('On Hold') 
	  AND cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is not null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
      WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
      WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null and cf.Date_Submitted_c is not null
	  THEN DATEDIFF(day, cf.Date_Submitted_c, getdate())
	  WHEN cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
	  WHEN cf.Date_Assigned_to_Legal_c is not null and cf.Submitted_to_Designee_c is not null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,getdate())
      ELSE DATEDIFF(day, cf.Date_Submitted_c, getdate ())
	  END as Legal_Duration
  FROM  datalake_dev.semantic.VIEW_CONTRACT_FORM  cf
LEFT JOIN datalake_dev.semantic.DIM_SF_USER    deu on cf.Legal_Team_Member_c = deu.User_ID
WHERE cf.Contract_Status_c NOT IN ('X: Dead','X: Terminated','S13: Dead','S12: Fully Executed','S0: Preparing Form','')
  UNION ALL
SELECT IFNULL(deu.First_Name,'Unknown') as Legal_First_Name, IFNULL(deu.LastName, 'Unknown') as Legal_Last_Name, Legal_Team_Member_c, 1 as Number_of_Contracts
,CASE WHEN cf.Submitted_to_Designee_c is not null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
      WHEN cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
      WHEN cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c, getdate())
	  WHEN cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null and cf.Date_Submitted_c is not null
	  THEN DATEDIFF(day, cf.Date_Submitted_c, getdate())
	  ELSE 0
	  END as Legal_Duration
  FROM datalake_dev.semantic.VIEW_CONTRACT_FORM   cf
LEFT JOIN datalake_dev.semantic.DIM_SF_USER    deu on cf.Legal_Team_Member_c = deu.User_ID
WHERE cf.Contract_Status_c IN ('S12: Fully Executed','X: Terminated')
) tab_a
group by Legal_First_Name, Legal_Last_Name,Legal_Team_Member_c
Order by Legal_First_Name, Legal_Last_Name)
,tempLegal_Duration6_Month (Legal_First_Name,Legal_Last_Name,Legal_Team_Member_c,Number_of_Contracts,Legal_Duration_6_Month) AS
(SELECT IFNULL(Legal_First_Name,'Unknown') as Legal_First_Name, IFNULL(Legal_Last_Name, 'Unknown') as Legal_Last_Name, Legal_Team_Member_c, SUM(Number_of_Contracts) as Number_of_Contracts
, sum(Legal_Duration)/SUM(Number_of_Contracts) as Legal_Duration_6_Month
FROM (
SELECT IFNULL(deu.First_Name,'Unknown') as Legal_First_Name, IFNULL(deu.LastName, 'Unknown') as Legal_Last_Name,Legal_Team_Member_c
, 1 as Number_of_Contracts
,CASE WHEN cf.Contract_Status_c in ('S1: Decision Maker Approval','S1a: Risk Management Approval','S1b: Executive Approval','S2: CFO Approval','S3: General Counsel Approval') 
	  THEN 0
	  WHEN cf.Contract_Status_c in ('On Hold') 
	  AND cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is not null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
      WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
      WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c, getdate())
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null and cf.Date_Submitted_c is not null
	  THEN DATEDIFF(day, cf.Date_Submitted_c, getdate())
	  WHEN cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
	  WHEN cf.Date_Assigned_to_Legal_c is not null and cf.Submitted_to_Designee_c is not null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
	  WHEN cf.Contract_Status_c in ('S4: Initial Contract Review','S7: Preparing Summary of Terms','S8: Awaiting Decision Maker Approval (CAM)','S8a: Awaiting Regional President Approval (CAM)','S9: Awaiting Construction Approval (CAM)', 'S11: Out for Signatures','On Hold') 
	  and cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,getdate())
      ELSE DATEDIFF(day, cf.Date_Submitted_c, getdate ())
	  END as Legal_Duration
FROM datalake_dev.semantic.VIEW_CONTRACT_FORM   cf
LEFT JOIN datalake_dev.semantic.DIM_SF_USER    deu on cf.Legal_Team_Member_c = deu.User_ID
WHERE 
cf.Contract_Status_c NOT IN ('X: Dead','X: Terminated','S13: Dead','S12: Fully Executed','S0: Preparing Form','')
AND cf.Date_Assigned_to_Legal_c >= TO_DATE(DATEADD(month, -6, CURRENT_DATE))
UNION ALL
SELECT IFNULL(deu.First_Name,'Unknown') as Legal_First_Name, IFNULL(deu.LastName, 'Unknown') as Legal_Last_Name, Legal_Team_Member_c, 1 as Number_of_Contracts
,CASE WHEN cf.Submitted_to_Designee_c is not null and cf.Date_Assigned_to_Legal_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Submitted_to_Designee_c)
      WHEN cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is not null and cf.Legal_Review_Date_c is not null
      THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c ,cf.Legal_Review_Date_c)
      WHEN cf.Submitted_to_Designee_c is null and cf.Date_Assigned_to_Legal_c is null
	  THEN DATEDIFF(day, cf.Date_Assigned_to_Legal_c, getdate())
	  WHEN cf.Date_Assigned_to_Legal_c is null and cf.Legal_Review_Date_c is null and cf.Submitted_to_Designee_c is null and cf.Date_Submitted_c is not null
	  THEN DATEDIFF(day, cf.Date_Submitted_c, getdate())
	  ELSE 0
	  END as Legal_Duration
FROM datalake_dev.semantic.VIEW_CONTRACT_FORM    cf
LEFT JOIN  datalake_dev.semantic.DIM_SF_USER    deu on cf.Legal_Team_Member_c = deu.User_ID
WHERE cf.Contract_Status_c IN ('S12: Fully Executed','X: Terminated')
AND cf.Date_Assigned_to_Legal_c >= TO_DATE(DATEADD(month, -6, CURRENT_DATE))
) tab_b
group by Legal_First_Name, Legal_Last_Name,CONCAT(Legal_First_Name,' ',Legal_Last_Name), Legal_Team_Member_c
Order by Legal_First_Name, Legal_Last_Name)

SELECT ld.Legal_First_Name,ld.Legal_Last_Name,ld.Legal_Team_Member_c,ld.Number_of_Contracts,ld.Legal_Duration, ld6.Legal_Duration_6_Month
FROM tempLegal_Duration  ld
LEFT JOIN tempLegal_Duration6_Month ld6 on ld.Legal_Team_Member_c = ld6.Legal_Team_Member_c
WHERE ld.Legal_Name NOT IN
(
SELECT NAMEEXCLUDED
FROM   datalake_dev.governed.STG_REPORT_EXCLUSION   -- datalake_dev.governed.STG_REPORT_EXCLUSION   
WHERE REPORTNAME='Contract Approval Duration Summary' AND REPORTSECTION='Legal Review'
)
  );
