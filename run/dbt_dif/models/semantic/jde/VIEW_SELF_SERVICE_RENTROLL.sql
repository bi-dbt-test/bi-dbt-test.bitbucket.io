
  create or replace  view datalake_dev.semantic.VIEW_SELF_SERVICE_RENTROLL  as (
    

with billcode_frequency
as
(
      select distinct BILLINGFREQUENCYCODE BILLING_FREQUENCY_CODE,DESCRIPTION 
      
      from   datalake_dev.governed.JDE_BILLING_FREQUENCY_MASTER  
)
select d.DAY,d.DAY_NAME,d.MONTH_START,d.MONTH_END,d.MONTH_NAME,d.YEAR_NUM ---date
--,bu.business_unit,COMPANY,COMPANY_RPT_BUSINESS_UNIT,BUSINESS_UNIT_TYPE,ADDRESS_NUMBER,COMPANY_DESC, bu.BU_CAT_CD28 Region , BU_CAT_CD18    Segment_cd , BU_CAT_CD12    Asset_type,
,bu.business_unit,COMPANY,COMPANY_RPT_BUSINESS_UNIT,BUSINESS_UNIT_TYPE,ADDRESS_NUMBER,COMPANY_DESC  , BU_CAT_CD12,bu.BU_CAT_CD12_DESC
  , bu.BU_CAT_CD24, bu.BU_CAT_CD24_DESC,BU_CAT_CD18 ,bu.BU_CAT_CD18_DESC
,bu.BU_CAT_CD28  , bu.BU_CAT_CD28_DESC       ,
--lease
LEASE_ID ,LEASE_VERSION_NBR,LEASE_BEGIN_DT,LEASE_END_DT,LEASE_DESC,LEASE_STATUS_DESC,LEASE_TYPE_CD,LEASE_TYPE_DESC,
RENT_COMMENCEMENT_DATE,MOVE_IN_DT,MOVE_OUT_DT,PLAN_OUT_DT,OPENING_DATE,COMMIT_DATE,SIC_CD,SIC_DESC
--unit
,u.unit_nbr unit, u.UNIT_BEGIN_EFF_DATE,u.UNIT_END_EFF_DATE,UNIT_TYPE_CD UNIT_TYPE,UNIT_TYPE_DESC,UNIT_DESCRIPTION,AREA_TYPE_CD,FLOOR_NBR,--SQR_FOOTAGE unit_sqft,
--tenant
t.TENANT_ID,t.TENANT_NAME
--fact table 
,f.SECURITY_DEPOSIT,TI,LLD_BROKER_LC,TNT_BROKER_LC,BILL_CODE,SCHEDULE_BEGIN_DATE,SCHEDULE_END_DATE,SUSPEND_DATE,SCHEDULE_AMOUNT,ANNUAL_SCHEDULE_AMOUNT,SCHEDULE_AMOUNT_PER_SQUARE
,SCHEDULE_SF,f.BILLING_FREQUENCY_CODE,billcode_frequency.DESCRIPTION BILLING_FREQUENCY_CODE_description
,PERCENTAGE_DUE_ON_SALES,LEASE_STATUS_KEY,OCCSTATUS_KEY,RATE_SQ_FT,UNIT_SQ_FT,
---fact calculation
LIST_CATEGORY,LIST_CATEGORY_CODE,CATEGORY,UNIT_TYPE_FLAG,RN_O_ALREADY_FLAG,rn_CATEGORY
--,COMPANY_TOTAL_SQFT,COMPANY_TOTAL_COUNT,COMPANY_LEASED_SQFT,COMPANY_LEASED_COUNT,COMPANY_UNLEASED_SQFT,COMPANY_UNLEASED_COUNT,COMPANY_VACANT_SQFT,COMPANY_VACANT_COUNT
---,COMPANY_OCCUPIED_SQFT,COMPANY_OCCUPIED_COUNT,COMPANY_LEASED_OCCUPIED_SQFT,COMPANY_LEASED_VACANT_SQFT,COMPANY_FUTURE_VACANT_SQFT,COMPANY_UNLEASED_VACANT_SQFT
--,COMPANY_UNIT_TYPE_TOTAL_SQFT,COMPANY_UNIT_TYPE_LEASED_SQFT,COMPANY_UNIT_TYPE_LEASED_COUNT,COMPANY_UNIT_TYPE_UNLEASED_SQFT,COMPANY_UNIT_TYPE_UNLEASED_COUNT,COMPANY_UNIT_TYPE_VACANT_SQFT
--,COMPANY_UNIT_TYPE_VACANT_COUNT,COMPANY_UNIT_TYPE_OCCUPIED_SQFT,COMPANY_UNIT_TYPE_OCCUPIED_COUNT,COMPANY_UNIT_TYPE_LEASED_OCCUPIED_SQFT,COMPANY_UNIT_TYPE_LEASED_VACCANT_SQFT,COMPANY_UNIT_TYPE_FUTURE_VACANT_SQFT
--,COMPANY_UNIT_TYPE_UNLEASED_VACANT_SQFT,COMPANY_CATEGORY_TOTAL_SQFT,COMPANY_UP_CATEGORY_TOTAL_SQFT,COMPANY_TOTAL_DEDUPLICATE_SQFT,COMPANY_DUPLICATE_SQFT,COMPANY_RENEWAL_SQFT
,row_number()over(partition by day, f.BU_KEY,u.unit_nbr,LEASE_ID,LEASE_VERSION_NBR order by case when LIST_CATEGORY_CODE  in ('O_P'
                                                                                                            ,'O_S'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP') then 1 else 2 end) deduplicate_lease_flag
,row_number()over(partition by day, f.BU_KEY,u.unit_nbr order by case when LIST_CATEGORY_CODE  in ('O_P'
                                                                                                            ,'O_S'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP') then 1 else 2 end ) deduplicate_unit_flag
from  datalake_dev.semantic.FACT_RENTROLL_V3  f
inner join  datalake_dev.semantic.DIM_DATE    d on f.date_key =d.DATE_ID 
inner join  datalake_dev.semantic.DIM_BUSINESS_UNIT  bu on f.bu_key=bu.business_unit
inner join datalake_dev.semantic.DIM_LEASE   l on f.lease_key =l.lease_key 
inner join datalake_dev.semantic.DIM_UNIT  u on f.unit_key =u.unit_key 
inner join datalake_dev.semantic.DIM_TENANT    t on f.tenant_key =t.tenant_key 
left join billcode_frequency on f.BILLING_FREQUENCY_CODE=billcode_frequency.BILLING_FREQUENCY_CODE
  );
