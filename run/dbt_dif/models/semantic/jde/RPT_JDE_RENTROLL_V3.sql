
  create or replace  view datalake_dev.semantic.RPT_JDE_RENTROLL_V3  as (
    
With base                                           as
      (
                SELECT
                          D.DAY
                        , bi.BILL_CODE_ALT_CATEGORY  as BILL_CODE_CATEGORY
                        , company
                        , COMPANY_DESC
                        , bu.COST_CENTER_TYPE        as bu_type
                        , bu.business_unit
                        , unit.unit_nbr
                        , l.lease_id
                        , l.LEASE_VERSION_NBR
                        , unit.UNIT_TYPE_CD
                        , unit.UNIT_TYPE_DESC
                        , l.tenant_id
                        , l.LEASE_DESC               as LEASE_DESCRIPTION
                        , l.OPENING_DATE
                        , unit.SQR_FOOTAGE           as rsf
                        , LEASE_STATUS_CD            as LSST
                        , l.LEASE_TYPE_CD            as LSTY
                        , SECURITY_DEPOSIT
                        , L.LEASE_BEGIN_DT
                        , L.LEASE_END_DT             as EXPIRE
                        , TI
                        , LLD_BROKER_LC
                        , TNT_BROKER_LC
                        , f.BILL_CODE                as BILLCODE
                        , SCHEDULE_BEGIN_DATE        as SCHEDULEBEGINDATE
                        , SCHEDULE_END_DATE          as SCHEDULEENDDATE
                        , SUSPEND_DATE               as SUSPENDDATE
                        , SCHEDULE_AMOUNT
                        , ANNUAL_SCHEDULE_AMOUNT
                        , SCHEDULE_AMOUNT_PER_SQUARE
                        , SCHEDULE_SF
                        , OCCSTATUS_KEY
                        , BILLING_FREQUENCY_CODE     as BILLINGFREQUENCYCODE
                        , Percentage_Due_On_Sales    as PercentageDueOnSales
                        , LIST_CATEGORY              as CATEGORY
                        , LIST_CATEGORY_CODE
                        , category                   as up_category
                        , COMPANY_CATEGORY_TOTAL_SQFT
                        , Company_total_Deduplicate_sqft
                        , Company_duplicate_sqft
                        , Company_Renewal_sqft
                        , case
                                    when LIST_CATEGORY_CODE='O_P'
                                              THEN '1'
                                    when LIST_CATEGORY_CODE='O_S'
                                              THEN '2'
                                    when LIST_CATEGORY_CODE='V_FP'
                                              THEN '3'
                                    when LIST_CATEGORY_CODE='V_FDP'
                                              THEN '4'
                                    when LIST_CATEGORY_CODE='V_SR'
                                              THEN '5'
                                    when LIST_CATEGORY_CODE='V_LT365'
                                              THEN '6'
                                    when LIST_CATEGORY_CODE='V_U'
                                              THEN '7'
                                    when LIST_CATEGORY_CODE='RE'
                                              THEN '8'
                          END                         as CATEGORY_SORT
                        , LEASE_STATUS_KEY
                FROM   datalake_dev.semantic.FACT_RENTROLL_V3 F                     
                          left join  datalake_dev.semantic.DIM_LEASE   l
                                    on
                                              f.lease_key=l.lease_key
                          left JOIN   datalake_dev.semantic.DIM_BUSINESS_UNIT  BU
                                    ON
                                              F.BU_KEY =BU.BUSINESS_UNIT
                          left join datalake_dev.semantic.DIM_BILL_CODE  bi
                                    on
                                              f.bill_code=bi.bill_code
                                             -- AND bi.BILL_CODE NOT IN ('COV1'
                                             --                        , 'COV2'
                                             --                        , 'COV3') ----------====NEED TO UPDATE LATER
                          left join  datalake_dev.semantic.DIM_UNIT  unit
                                    on
                                              f.unit_key =unit.unit_key
                          left JOIN  datalake_dev.semantic.DIM_DATE  D
                                    ON
                                              F.DATE_KEY =D.DATE_ID
      )
    , HEADER AS
      (
             SELECT   distinct
                    DAY
                  , company
                  , COMPANY_DESC
                  , bu_type
                  , business_unit
                  , unit_nbr
                  , TENANT_ID
                  , rsf
                  , lease_id
                  , LEASE_VERSION_NBR
                  , UNIT_TYPE_CD
                  , UNIT_TYPE_DESC
                  , LEASE_DESCRIPTION
                  , OPENING_DATE
                  , LSST
                  , LSTY
                  , SECURITY_DEPOSIT
                  , LEASE_BEGIN_DT
                  , EXPIRE
                  , TI
                  , LLD_BROKER_LC
                  , TNT_BROKER_LC
                  , CATEGORY
                  , up_category
                  , COMPANY_CATEGORY_TOTAL_SQFT
                  , Company_total_Deduplicate_sqft
                  , Company_duplicate_sqft
                  , Company_Renewal_sqft
                  , CATEGORY_SORT
             FROM
                    base
      )
    , BMR AS
      (
                   SELECT
                                DAY
                              , company
                              , COMPANY_DESC
                              , bu_type
                              , business_unit
                              , unit_nbr
                              , rsf
                              , tenant_id
                              , lease_id
                              , LEASE_VERSION_NBR
                              , UNIT_TYPE_CD
                              , base.UNIT_TYPE_DESC
                              , LEASE_DESCRIPTION
                              , OPENING_DATE
                              , LSST
                              , LSTY
                              , SECURITY_DEPOSIT
                              , LEASE_BEGIN_DT
                              , EXPIRE
                              , TI
                              , LLD_BROKER_LC
                              , TNT_BROKER_LC
                              , SCHEDULE_SF RENTABLE_SQFT
                              , BILLCODE
                              , SCHEDULE_AMOUNT
                              , SCHEDULE_AMOUNT_PER_SQUARE
                              , SCHEDULEBEGINDATE
                              , SCHEDULEENDDATE
                              , ANNUAL_SCHEDULE_AMOUNT
                              , BILLINGFREQUENCYCODE
                              , row_number()over(partition by BASE.DAY,BASE.company,BASE.lease_id,BASE.LEASE_VERSION_NBR,BASE.unit_nbr order by
                                                 BASE.unit_nbr,BILLCODE,BASE.SCHEDULEBEGINDATE ) rn
                              , CATEGORY
                              , up_category
                              , COMPANY_CATEGORY_TOTAL_SQFT
                              , Company_total_Deduplicate_sqft
                              , Company_duplicate_sqft
                              , Company_Renewal_sqft
                              , CATEGORY_SORT
                   FROM
                                base
                   where
                                BILL_CODE_CATEGORY in ('Base Minimum Rent' )
      )
    , additional_rents AS
      (
                   SELECT
                                BASE.DAY
                              , BASE.company
                              , BASE.COMPANY_DESC
                              , base.bu_type
                              , base.tenant_id
                              , BASE.business_unit
                              , BASE.unit_nbr
                              , base.rsf
                              , BASE.lease_id
                              , BASE.LEASE_VERSION_NBR
                              , BASE.UNIT_TYPE_CD
                              , base.UNIT_TYPE_DESC
                              , BASE.LEASE_DESCRIPTION
                              , BASE.OPENING_DATE
                              , BASE.LSST
                              , BASE.LSTY
                              , BASE.SECURITY_DEPOSIT
                              , BASE.LEASE_BEGIN_DT
                              , BASE.EXPIRE
                              , BASE.TI
                              , BASE.LLD_BROKER_LC
                              , BASE.TNT_BROKER_LC
                              , BASE.SCHEDULE_SF RENTABLE_SQFT
                              , BASE.BILLCODE
                              , BASE.SCHEDULE_AMOUNT
                              , BASE.SCHEDULE_AMOUNT_PER_SQUARE
                              , BASE.SCHEDULEBEGINDATE
                              , base.SCHEDULEENDDATE
                              , ANNUAL_SCHEDULE_AMOUNT
                              , base.BILLINGFREQUENCYCODE
                              , row_number()over(partition by BASE.DAY,BASE.company,BASE.lease_id,BASE.LEASE_VERSION_NBR,BASE.unit_nbr order by
                                                 BASE.unit_nbr,BILLCODE,BASE.SCHEDULEBEGINDATE ) rn
                              , base.CATEGORY
                              , base.up_category
                              , BASE.COMPANY_CATEGORY_TOTAL_SQFT
                              , BASE.Company_total_Deduplicate_sqft
                              , BASE.Company_duplicate_sqft
                              , BASE.Company_Renewal_sqft
                              , base.CATEGORY_SORT
                   FROM
                                base
                                left join
                                             HEADER b
                                             on
                                                          base.day                  =b.day
                                                          and base.unit_nbr         =b.unit_nbr
                                                          and base.lease_id         =b.lease_id
                                                          and base.lease_version_nbr=b.lease_version_nbr
                   where
                                BILL_CODE_CATEGORY in ('Additional Rent' )
      )
    , Overage as
      (
                   SELECT
                                BASE.DAY
                              , BASE.company
                              , BASE.COMPANY_DESC
                              , base.bu_type
                              , base.tenant_id
                              , BASE.business_unit
                              , BASE.unit_nbr
                              , base.rsf
                              , BASE.lease_id
                              , BASE.LEASE_VERSION_NBR
                              , BASE.UNIT_TYPE_CD
                              , base.UNIT_TYPE_DESC
                              , BASE.LEASE_DESCRIPTION
                              , BASE.OPENING_DATE
                              , BASE.LSST
                              , BASE.LSTY
                              , BASE.SECURITY_DEPOSIT
                              , BASE.LEASE_BEGIN_DT
                              , BASE.EXPIRE
                              , BASE.TI
                              , BASE.LLD_BROKER_LC
                              , BASE.TNT_BROKER_LC
                              , BASE.SCHEDULE_SF RENTABLE_SQFT
                              , BASE.BILLCODE
                              , BASE.SCHEDULE_AMOUNT
                              , BASE.SCHEDULE_AMOUNT_PER_SQUARE
                              , BASE.SCHEDULEBEGINDATE
                              , base.SCHEDULEENDDATE
                              , ANNUAL_SCHEDULE_AMOUNT
                              , base.BILLINGFREQUENCYCODE
                              , PercentageDueOnSales
                              , row_number()over(partition by BASE.DAY,BASE.company,BASE.lease_id,BASE.LEASE_VERSION_NBR,BASE.unit_nbr order by
                                                 BASE.unit_nbr,BILLCODE,BASE.SCHEDULEBEGINDATE ) rn
                              , base.CATEGORY
                              , base.up_category
                              , BASE.COMPANY_CATEGORY_TOTAL_SQFT
                              , BASE.Company_total_Deduplicate_sqft
                              , BASE.Company_duplicate_sqft
                              , BASE.Company_Renewal_sqft
                              , base.CATEGORY_SORT
                   FROM
                                base
                                left join
                                             HEADER b
                                             on
                                                          base.day                  =b.day
                                                          and base.unit_nbr         =b.unit_nbr
                                                          and base.lease_id         =b.lease_id
                                                          and base.lease_version_nbr=b.lease_version_nbr
                   where
                                BILL_CODE_CATEGORY in ('Overage_Rent' )
      )
    , add_BMR as
      (
                      select
                                      coalesce(b.CATEGORY ,BMR.CATEGORY )                                             CATEGORY
                                    , coalesce(b.up_category ,BMR.up_category )                                       up_category
                                    , coalesce(b.COMPANY_CATEGORY_TOTAL_SQFT ,BMR.COMPANY_CATEGORY_TOTAL_SQFT )       COMPANY_CATEGORY_TOTAL_SQFT
                                    , coalesce(b.Company_total_Deduplicate_sqft ,BMR.Company_total_Deduplicate_sqft ) Company_total_Deduplicate_sqft
                                    , coalesce(b.Company_duplicate_sqft ,BMR.Company_duplicate_sqft )                 Company_duplicate_sqft
                                    , coalesce(b.Company_Renewal_sqft ,BMR.Company_Renewal_sqft )                     Company_Renewal_sqft
                                    , coalesce(b.CATEGORY_SORT ,BMR.CATEGORY_SORT )                                   CATEGORY_SORT
                                    , coalesce(b.DAY ,BMR.DAY)                                                        DAY
                                    , coalesce(b.company ,BMR.company )                                               company
                                    , coalesce(b.bu_type ,BMR.bu_type )                                               bu_type
                                    , coalesce(b.tenant_id ,BMR.tenant_id)                                            tenant_id
                                    , coalesce(b.lease_id ,BMR.lease_id)                                              lease_id
                                    , coalesce(b.LEASE_DESCRIPTION ,BMR.LEASE_DESCRIPTION )                           LEASE_DESCRIPTION
                                    , coalesce(b.OPENING_DATE ,BMR.OPENING_DATE )                                     OPENING_DATE
                                    , coalesce(b.LSST ,BMR.LSST)                                                      LSST
                                    , coalesce(b.LSTY ,BMR.LSTY )                                                     LSTY
                                    , coalesce(b.UNIT_TYPE_CD ,BMR.UNIT_TYPE_CD )                                     UNIT_TYPE_CD
                                    , coalesce(b.UNIT_TYPE_DESC ,BMR.UNIT_TYPE_DESC)                                  UNIT_TYPE_DESC
                                    , coalesce(b.unit_nbr ,BMR.unit_nbr )                                             unit_nbr
                                    , coalesce(b.rsf ,BMR.rsf )                                                       rsf
                                    , coalesce(b.LEASE_BEGIN_DT ,BMR.LEASE_BEGIN_DT )                                 LEASE_BEGIN_DT
                                    , coalesce(b.EXPIRE ,BMR.EXPIRE )                                                 EXPIRE
                                    , coalesce(b.LEASE_VERSION_NBR ,BMR.LEASE_VERSION_NBR)                            LEASE_VERSION_NBR
                                    , BMR.SCHEDULEBEGINDATE                                                           BMR_SCHEDULEBEGINDATE
                                    , BMR.SCHEDULEENDDATE                                                             BMR_SCHEDULEENDDATE
                                    , BMR.BILLCODE                                                                    BMR_Billcode
                                    , BMR.SCHEDULE_AMOUNT                                                             BMR_SCHEDULE_AMOUNT
                                    , BMR.SCHEDULE_AMOUNT_PER_SQUARE                                                  BMR_PSF
                                    , BMR.ANNUAL_SCHEDULE_AMOUNT                                                      BMR_ANNUAL_SCHEDULE_AMOUNT
                                    , BMR.BILLINGFREQUENCYCODE                                                        BMR_BILLINGFREQUENCYCODE
                                    , ifnull(rn,1)                                                                    add_BMR_rn
                                    , coalesce(b.COMPANY_DESC ,BMR.COMPANY_DESC )                                     COMPANY_DESC
                                    , coalesce(b.business_unit ,BMR.business_unit )                                   business_unit
                                    , coalesce(b.SECURITY_DEPOSIT ,BMR.SECURITY_DEPOSIT )                             SECURITY_DEPOSIT
                                    , coalesce(b.TI ,BMR.TI)                                                          TI
                                    , coalesce(b.LLD_BROKER_LC ,BMR.LLD_BROKER_LC)                                    LLD_BROKER_LC
                                    , coalesce(b.TNT_BROKER_LC ,BMR.TNT_BROKER_LC)                                    TNT_BROKER_LC
                      from
                                      HEADER b
                                      full outer join
                                                      BMR
                                                      on
                                                                      b.DAY                  =BMR.DAY
                                                                      and b.lease_id         =BMR.lease_id
                                                                      and b.LEASE_VERSION_NBR=BMR.LEASE_VERSION_NBR
                                                                      and b.unit_nbr         =BMR.unit_nbr
      )
    , add_BMR_additional_rents as
      (
                      select
                                      coalesce(b.CATEGORY ,additional_rents.CATEGORY )                                             CATEGORY
                                    , coalesce(b.up_category ,additional_rents.up_category )                                       up_category
                                    , coalesce(b.COMPANY_CATEGORY_TOTAL_SQFT ,additional_rents.COMPANY_CATEGORY_TOTAL_SQFT )       COMPANY_CATEGORY_TOTAL_SQFT
                                    , coalesce(b.Company_total_Deduplicate_sqft ,additional_rents.Company_total_Deduplicate_sqft ) Company_total_Deduplicate_sqft
                                    , coalesce(b.Company_duplicate_sqft ,additional_rents.Company_duplicate_sqft )                 Company_duplicate_sqft
                                    , coalesce(b.Company_Renewal_sqft ,additional_rents.Company_Renewal_sqft )                     Company_Renewal_sqft
                                    , coalesce(b.CATEGORY_SORT ,additional_rents.CATEGORY_SORT )                                   CATEGORY_SORT
                                    , coalesce(b.DAY ,additional_rents.DAY)                                                        DAY
                                    , coalesce(b.company ,additional_rents.company )                                               company
                                    , coalesce(b.bu_type ,additional_rents.bu_type )                                               bu_type
                                    , coalesce(b.tenant_id ,additional_rents.tenant_id)                                            tenant_id
                                    , coalesce(b.lease_id ,additional_rents.lease_id)                                              lease_id
                                    , coalesce(b.LEASE_DESCRIPTION ,additional_rents.LEASE_DESCRIPTION )                           LEASE_DESCRIPTION
                                    , coalesce(b.OPENING_DATE ,additional_rents.OPENING_DATE )                                     OPENING_DATE
                                    , coalesce(b.LSST ,additional_rents.LSST)                                                      LSST
                                    , coalesce(b.LSTY ,additional_rents.LSTY )                                                     LSTY
                                    , coalesce(b.UNIT_TYPE_CD ,additional_rents.UNIT_TYPE_CD )                                     UNIT_TYPE_CD
                                    , coalesce(b.UNIT_TYPE_DESC ,additional_rents.UNIT_TYPE_DESC)                                  UNIT_TYPE_DESC
                                    , coalesce(b.unit_nbr ,additional_rents.unit_nbr )                                             unit_nbr
                                    , coalesce(b.rsf ,additional_rents.rsf )                                                       rsf
                                    , coalesce(b.LEASE_BEGIN_DT ,additional_rents.LEASE_BEGIN_DT )                                 LEASE_BEGIN_DT
                                    , coalesce(b.EXPIRE ,additional_rents.EXPIRE )                                                 EXPIRE
                                    , coalesce(b.LEASE_VERSION_NBR ,additional_rents.LEASE_VERSION_NBR)                            LEASE_VERSION_NBR
                                    , BMR_SCHEDULEBEGINDATE
                                    , BMR_SCHEDULEENDDATE
                                    , BMR_Billcode
                                    , BMR_SCHEDULE_AMOUNT
                                    , BMR_PSF
                                    , BMR_ANNUAL_SCHEDULE_AMOUNT
                                    , BMR_BILLINGFREQUENCYCODE
                                    , additional_rents.SCHEDULEBEGINDATE           additional_rents_SCHEDULEBEGINDATE
                                    , additional_rents.SCHEDULEENDDATE             additional_rents_SCHEDULEENDDATE
                                    , additional_rents.BILLCODE                    additional_rents_Billcode
                                    , additional_rents.SCHEDULE_AMOUNT_PER_SQUARE  additional_rents_PSF
                                    , additional_rents.SCHEDULE_AMOUNT             additional_rents_SCHEDULE_AMOUNT
                                    , additional_rents.BILLINGFREQUENCYCODE        additional_rents_BILLINGFREQUENCYCODE
                                    , additional_rents.ANNUAL_SCHEDULE_AMOUNT      additional_rents_ANNUAL_SCHEDULE_AMOUNT
                                    , coalesce(b.add_BMR_rn,additional_rents.rn,1) add_BMR_additional_rents_rn
                                      --===
                                    , coalesce(b.COMPANY_DESC ,additional_rents.COMPANY_DESC )         COMPANY_DESC
                                    , coalesce(b.business_unit ,additional_rents.business_unit )       business_unit
                                    , coalesce(b.SECURITY_DEPOSIT ,additional_rents.SECURITY_DEPOSIT ) SECURITY_DEPOSIT
                                    , coalesce(b.TI ,additional_rents.TI)                              TI
                                    , coalesce(b.LLD_BROKER_LC ,additional_rents.LLD_BROKER_LC)        LLD_BROKER_LC
                                    , coalesce(b.TNT_BROKER_LC ,additional_rents.TNT_BROKER_LC)        TNT_BROKER_LC
                      from
                                      add_BMR b
                                      full outer join
                                                      additional_rents
                                                      on
                                                                      b.DAY                  =additional_rents.DAY
                                                                      and b.lease_id         =additional_rents.lease_id
                                                                      and b.LEASE_VERSION_NBR=additional_rents.LEASE_VERSION_NBR
                                                                      and b.add_BMR_rn       =additional_rents.rn
                                                                      and b.unit_nbr         =additional_rents.unit_nbr
 
      )
    , add_BMR_additional_rents_Overage as
      (
                      select
                                      coalesce(b.CATEGORY ,Overage.CATEGORY )                                             CATEGORY
                                    , coalesce(b.up_category ,Overage.up_category )                                       up_category
                                    , coalesce(b.COMPANY_CATEGORY_TOTAL_SQFT ,Overage.COMPANY_CATEGORY_TOTAL_SQFT )       COMPANY_CATEGORY_TOTAL_SQFT
                                    , coalesce(b.Company_total_Deduplicate_sqft ,Overage.Company_total_Deduplicate_sqft ) Company_total_Deduplicate_sqft
                                    , coalesce(b.Company_duplicate_sqft ,Overage.Company_duplicate_sqft )                 Company_duplicate_sqft
                                    , coalesce(b.Company_Renewal_sqft ,Overage.Company_Renewal_sqft )                     Company_Renewal_sqft
                                    , coalesce(b.CATEGORY_SORT ,Overage.CATEGORY_SORT )                                   CATEGORY_SORT
                                    , coalesce(b.DAY ,Overage.DAY)                                                        DAY
                                    , coalesce(b.company ,Overage.company )                                               company
                                    , coalesce(b.bu_type ,Overage.bu_type )                                               bu_type
                                    , coalesce(b.tenant_id ,Overage.tenant_id)                                            tenant_id
                                    , coalesce(b.lease_id ,Overage.lease_id)                                              lease_id
                                    , coalesce(b.LEASE_DESCRIPTION ,Overage.LEASE_DESCRIPTION )                           LEASE_DESCRIPTION
                                    , coalesce(b.OPENING_DATE ,Overage.OPENING_DATE )                                     OPENING_DATE
                                    , coalesce(b.LSST ,Overage.LSST)                                                      LSST
                                    , coalesce(b.LSTY ,Overage.LSTY )                                                     LSTY
                                    , coalesce(b.UNIT_TYPE_CD ,Overage.UNIT_TYPE_CD )                                     UNIT_TYPE_CD
                                    , coalesce(b.UNIT_TYPE_DESC ,Overage.UNIT_TYPE_DESC)                                  UNIT_TYPE_DESC
                                    , coalesce(b.unit_nbr ,Overage.unit_nbr )                                             unit_nbr
                                    , coalesce(b.rsf ,Overage.rsf )                                                       rsf
                                    , coalesce(b.LEASE_BEGIN_DT ,Overage.LEASE_BEGIN_DT )                                 LEASE_BEGIN_DT
                                    , coalesce(b.EXPIRE ,Overage.EXPIRE )                                                 EXPIRE
                                    , coalesce(b.LEASE_VERSION_NBR ,Overage.LEASE_VERSION_NBR)                            LEASE_VERSION_NBR
                                    , BMR_SCHEDULEBEGINDATE
                                    , BMR_SCHEDULEENDDATE
                                    , BMR_Billcode
                                    , BMR_SCHEDULE_AMOUNT
                                    , BMR_PSF
                                    , BMR_ANNUAL_SCHEDULE_AMOUNT
                                    , BMR_BILLINGFREQUENCYCODE
                                    , additional_rents_SCHEDULEBEGINDATE
                                    , additional_rents_SCHEDULEENDDATE
                                    , additional_rents_Billcode
                                    , additional_rents_PSF
                                    , additional_rents_SCHEDULE_AMOUNT
                                    , additional_rents_BILLINGFREQUENCYCODE
                                    , additional_rents_ANNUAL_SCHEDULE_AMOUNT
                                    , Overage.SCHEDULEBEGINDATE Overage_SCHEDULEBEGINDATE
                                    , Overage.SCHEDULEENDDATE   Overage_SCHEDULEENDDATE
                                    , Overage.SCHEDULE_AMOUNT   Overage_BP
                                    , Overage.PercentageDueOnSales::text
                                                      ||'%'                                Overage_PercentageDueOnSales
                                    , coalesce(b.add_BMR_additional_rents_rn,Overage.rn,1) rnn
                                      --===
                                    , coalesce(b.COMPANY_DESC ,Overage.COMPANY_DESC )         COMPANY_DESC
                                    , coalesce(b.business_unit ,Overage.business_unit )       business_unit
                                    , coalesce(b.SECURITY_DEPOSIT ,Overage.SECURITY_DEPOSIT ) SECURITY_DEPOSIT
                                    , coalesce(b.TI ,Overage.TI)                              TI
                                    , coalesce(b.LLD_BROKER_LC ,Overage.LLD_BROKER_LC)        LLD_BROKER_LC
                                    , coalesce(b.TNT_BROKER_LC ,Overage.TNT_BROKER_LC)        TNT_BROKER_LC
                      from
                                      add_BMR_additional_rents b
                                      full outer join
                                                      Overage
                                                      on
                                                                      b.DAY                            =Overage.DAY
                                                                      and b.lease_id                   =Overage.lease_id
                                                                      and b.unit_nbr                   =Overage.unit_nbr 
                                                                      and b.LEASE_VERSION_NBR          =Overage.LEASE_VERSION_NBR
                                                                      and b.add_BMR_additional_rents_rn=Overage.rn
      )
    , BEFORE_TOTAL AS
      (
             select
                    MERGE. *
             from
                    add_BMR_additional_rents_Overage
             MERGE
                       left JOIN
                                 datalake_dev.semantic.DIM_LEASE LEASE
                                 ON
                                           MERGE.LEASE_ID              =LEASE.LEASE_ID
                                           and MERGE.LEASE_VERSION_NBR =LEASE.LEASE_VERSION_NBR
                                           and merge.unit_nbr          =LEASE.unit_nbr
      )
    , TOTAL AS
      (
                 select distinct
                            DATE.DAY
                          , bu.company
                          , bu.BU_CAT_CD28 Region
                          , BU_CAT_CD18    Segment_cd
                          , BU_CAT_CD12    Asset_type
                          , COMPANY_TOTAL_SQFT
                          , COMPANY_LEASED_SQFT
                          , COMPANY_UNLEASED_SQFT
                          , COMPANY_VACANT_SQFT
                          , COMPANY_OCCUPIED_SQFT
                          , COMPANY_LEASED_OCCUPIED_SQFT
                          , COMPANY_FUTURE_VACANT_SQFT
                          , COMPANY_UNLEASED_VACANT_SQFT
                          , UNIT_TYPE
                          , Company_UNIT_TYPE_Total_sqft
                          , Company_Total_count
                          , Company_leased_count
                          , Company_unleased_count
                          , Company_vacant_count
                          , Company_OCCUPIED_count
                          , COMPANY_UNIT_TYPE_LEASED_SQFT
                          , COMPANY_UNIT_TYPE_UNLEASED_SQFT
                          , COMPANY_UNIT_TYPE_VACANT_SQFT
                          , COMPANY_UNIT_TYPE_OCCUPIED_SQFT
                          , COMPANY_UNIT_TYPE_LEASED_OCCUPIED_SQFT
                          , COMPANY_UNIT_TYPE_LEASED_VACCANT_SQFT
                          , COMPANY_UNIT_TYPE_FUTURE_VACANT_SQFT
                          , COMPANY_UNIT_TYPE_UNLEASED_VACANT_SQFT
                 from
                           datalake_dev.semantic.FACT_RENTROLL_V3    f
                            inner join
                                    datalake_dev.semantic.DIM_BUSINESS_UNIT       bu
                                       on
                                                  f.bu_key=bu.BUSINESS_UNIT
                            INNER JOIN
                                     datalake_dev.semantic.DIM_DATE     DATE
                                       ON
                                                  F.DATE_KEY=DATE.DATE_ID
      )
    , result as
      (
                   SELECT
                                BT.*
                              , Region
                              , Segment_cd Segment
                              , Asset_type
                              , COMPANY_TOTAL_SQFT
                              , COMPANY_LEASED_SQFT
                              , COMPANY_UNLEASED_SQFT
                              , COMPANY_VACANT_SQFT
                              , COMPANY_OCCUPIED_SQFT
                              , COMPANY_LEASED_OCCUPIED_SQFT
                              , COMPANY_FUTURE_VACANT_SQFT
                              , COMPANY_UNLEASED_VACANT_SQFT
                              , UNIT_TYPE
                              , Company_UNIT_TYPE_Total_sqft
                              , COMPANY_UNIT_TYPE_LEASED_SQFT
                              , COMPANY_UNIT_TYPE_UNLEASED_SQFT
                              , COMPANY_UNIT_TYPE_VACANT_SQFT
                              , COMPANY_UNIT_TYPE_OCCUPIED_SQFT
                              , COMPANY_UNIT_TYPE_LEASED_OCCUPIED_SQFT
                              , COMPANY_UNIT_TYPE_LEASED_VACCANT_SQFT
                              , COMPANY_UNIT_TYPE_FUTURE_VACANT_SQFT
                              , COMPANY_UNIT_TYPE_UNLEASED_VACANT_SQFT
                              , Company_Total_count
                              , Company_leased_count
                              , Company_unleased_count
                              , Company_vacant_count
                              , Company_OCCUPIED_count
                              , row_number()over(partition by BT.DAY, BT.company, BT.lease_id, BT.LEASE_VERSION_NBR, BT.unit_nbr order by
                                                 rnn desc ) bold_used_rn
                   FROM
                                BEFORE_TOTAL BT
                                INNER JOIN
                                             TOTAL
                                             ON
                                                          BT.COMPANY         =TOTAL.COMPANY
                                                          AND BT.DAY         =TOTAL.DAY
                                                          and bt.UNIT_TYPE_CD=total.UNIT_TYPE
                                                          and bt.day         =total.day
      )
    , base_blank as
      (
             select distinct
                    CATEGORY
                  , up_category
                  , COMPANY_CATEGORY_TOTAL_SQFT
                  , Company_total_Deduplicate_sqft
                  , Company_duplicate_sqft
                  , Company_Renewal_sqft
                  , CATEGORY_SORT
                  , BU_TYPE
                  , DAY
                  , COMPANY
                  , TENANT_ID
                  , LEASE_ID
                  , LEASE_VERSION_NBR
                  , UNIT_NBR, 999 rnn
                  , region
                  , Segment
                  , Asset_type
                  , UNIT_TYPE
             from
                    result
      )
    , combine as
      (
             select *
             from
                    result
             union all
             select
                    CATEGORY
                  , up_category
                  , COMPANY_CATEGORY_TOTAL_SQFT
                  , Company_total_Deduplicate_sqft
                  , Company_duplicate_sqft
                  , Company_Renewal_sqft
                  , CATEGORY_SORT
                  , DAY
                  , COMPANY
                  , BU_TYPE
                  , TENANT_ID
                  , LEASE_ID
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , UNIT_NBR
                  , null
                  , null
                  , null
                  , LEASE_VERSION_NBR
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null, 999
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , region
                  , Segment
                  , Asset_type
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , UNIT_TYPE
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
                  , null
             from
                    base_blank
      )
 select *
 from
          combine
 order by
          day
        , lease_id
        , LEASE_VERSION_NBR
        , rnn
  );
