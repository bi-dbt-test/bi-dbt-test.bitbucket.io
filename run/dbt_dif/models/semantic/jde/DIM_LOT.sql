

      create or replace  table datalake_dev.semantic.DIM_LOT  as
      (SELECT concat(LM.COMMUNITY, '.', LM.HOMEBUILDERLOTNUMBER)                                      AS LOT_KEY,
       LM.COMMUNITY,
       LM.HOMEBUILDERLOTNUMBER,        
       LM.HOMEBUILDERLOTNUMBER                                                                 AS HOME_BUILDER_LOT_NUMBER,       
       LM.BUSINESSUNIT, 
       LM.BUSINESSUNIT                                                                         AS BUSINESS_UNIT,	       
       MBU.DESCRIPTION                                                                         AS BU_DESCRIPTION,
       LM.COMPANY,
       LM.LOTCATEGORY01                                                                        AS LCC01,
       C01.DESCRIPTION                                                                         AS LCC01_DESCRIPTION,
       LM.LOTCATEGORY02                                                                        AS LCC02,
       C02.DESCRIPTION                                                                         AS LCC02_DESCRIPTION,
       LM.LOTCATEGORY03                                                                        AS LCC03,
       C03.DESCRIPTION                                                                         AS LCC03_DESCRIPTION,
       LM.LMLCC04                                                                              AS LCC04,
       C04.DESCRIPTION                                                                         AS LCC04_DESCRIPTION,
       LM.LOTCATEGORY05                                                                        AS LCC05_LAND_TYPE,
       C05.DESCRIPTION                                                                         AS LCC05_DESCRIPTION,
       LM.LOTCATEGORY06                                                                        AS LCC06,
       C06.DESCRIPTION                                                                         AS LCC06_DESCRIPTION,
       LM.LMLCC07                                                                              AS LCC07_PRODUCT_CODE,
       C07.DESCRIPTION                                                                         AS LCC07_DESCRIPTION,
       LM.LMLCC08                                                                              AS LCC08_PRODUCT_CODE,
       C08.DESCRIPTION                                                                         AS LCC08_DESCRIPTION,
       LM.LMLCC09                                                                              AS LCC09_PRODUCT_CODE,
       C09.DESCRIPTION                                                                         AS LCC09_DESCRIPTION,
       LM.LMLCC10                                                                              AS LCC10_PRODUCT_CODE,
       C10.DESCRIPTION                                                                         AS LCC10_DESCRIPTION,
       LM.AMOUNTUSERDEFINEDAMOUNT02                                                            AS NET_ACREAGE,
       LM.AMOUNTUSERDEFINEDAMOUNT03                                                            AS GROSS_ACREAGE,
       LM.PLAN,
       LM.LEGALLOTNUMBER, 
       LM.LEGALLOTNUMBER                                                                       AS LEGAL_LOT_NUMBER, 	   
       LM.HOMEBUILDERBLOCK, 
       LM.HOMEBUILDERBLOCK                                                                     AS HOME_BUILDER_BLOCK, 	   
       LM.HOMEBUILDERTRACT, 
       LM.HOMEBUILDERTRACT                                                                     AS HOME_BUILDER_TRACT,	   
       LM.HOMEBUILDERAREA, 
       LM.HOMEBUILDERAREA                                                                      AS HOME_BUILDER_AREA,	   
       LM.PARCELNUMBER, 
       LM.PARCELNUMBER                                                                         AS PARCEL_NUMBER,  	   
       LM.AMOUNTBASEPRICE, 
       LM.AMOUNTBASEPRICE                                                                      AS AMOUNT_BASE_PRICE,  	   
       CAST (
       CASE
           WHEN LM.LOTSQUAREFOOTAGE <> 0 THEN LM.LOTSQUAREFOOTAGE
           ELSE LM.AMOUNTUSERDEFINEDAMOUNT02 / 0.00002295684 
		END AS DECIMAL(18,2) )                                                         AS NET_SQ_FEET,
       CAST (
	   CASE WHEN LM.AMOUNTUSERDEFINEDAMOUNT04 = 0 THEN 1 
	        ELSE LM.AMOUNTUSERDEFINEDAMOUNT04 
		END AS DECIMAL(18,2) )                                                         AS NO_OF_UNITS,
       CPM.PHASE,
       CPM.DESCRIPTION                                                                         AS PHASE_DESCRIPTION,
       PM.AMOUNTBASEPRICE                                                                      AS PLAN_BASE_PRICE
FROM datalake_dev.governed.JDE_LOT_MASTER    LM

         LEFT OUTER JOIN datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C04
                         ON LM.LMLCC04 = C04.USERDEFINEDCODE 
						AND C04.PRODUCTCODE = '44H2' 
						AND C04.USERDEFINEDCODES = '04'
         LEFT OUTER JOIN datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES    C03
                         ON LM.LOTCATEGORY03 = C03.USERDEFINEDCODE 
						AND C03.PRODUCTCODE = '44H2' 
						AND C03.USERDEFINEDCODES = '03'
         LEFT OUTER JOIN datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C08
                         ON LM.LMLCC08 = C08.USERDEFINEDCODE 
						AND C08.PRODUCTCODE = '44H2' 
						AND C08.USERDEFINEDCODES = '08'
         LEFT OUTER JOIN datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C09
                         ON LM.LMLCC09 = C09.USERDEFINEDCODE 
						AND C09.PRODUCTCODE = '44H2' 
						AND C09.USERDEFINEDCODES = '09'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES   C02
                         ON LM.LOTCATEGORY02 = C02.USERDEFINEDCODE 
						AND C02.PRODUCTCODE = '44H2' 
						AND C02.USERDEFINEDCODES = '02'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES    C05
                         ON LM.LOTCATEGORY05 = C05.USERDEFINEDCODE 
						AND C05.PRODUCTCODE = '44H2' 
						AND C05.USERDEFINEDCODES = '05'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C07
                         ON LM.LMLCC07 = C07.USERDEFINEDCODE 
						AND C07.PRODUCTCODE = '44H2' 
						AND C07.USERDEFINEDCODES = '07'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C10
                         ON LM.LMLCC10 = C10.USERDEFINEDCODE 
						AND C10.PRODUCTCODE = '44H2' 
						AND C10.USERDEFINEDCODES = '10'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C06
                         ON LM.LOTCATEGORY06 = C06.USERDEFINEDCODE 
						AND C06.PRODUCTCODE = '44H2' 
						AND C06.USERDEFINEDCODES = '06'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C01
                         ON LM.LOTCATEGORY01 = C01.USERDEFINEDCODE 
						AND C01.PRODUCTCODE = '44H2' 
						AND C01.USERDEFINEDCODES = '01'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_COMMUNITY_PHASE_MASTER       CPM
                         ON LM.COMMUNITY = CPM.COMMUNITY 
						AND LM.HOMEBUILDERAREA = CPM.HOMEBUILDERAREA 
						AND LM.PHASE = CPM.PHASE
         LEFT OUTER JOIN datalake_dev.governed.JDE_PLAN_MASTER    PM
                         ON LM.COMMUNITY = PM.COMMUNITY 
						AND LM.PHASE = PM.PHASE 
						AND LM.HOMEBUILDERAREA = PM.HOMEBUILDERAREA 
					    AND LM.PLAN = PM.PLAN
         LEFT OUTER JOIN  datalake_dev.semantic.DIM_BUSINESS_UNIT  MBU 
		                 ON LM.COMMUNITY = MBU.BUSINESS_UNIT
WHERE LM.COMPANY NOT LIKE ''
      );
    