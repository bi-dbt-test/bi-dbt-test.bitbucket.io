
  create or replace  view datalake_dev.semantic.VIEW_AR_DASHBOARD  as (
    

with base
as
(
SELECT 
  DD.MONTH_START, 
  DD.MONTH_END,
  REPLACE( DD.MONTH_END::TEXT,'-','')  MONTH_END_DATE_KEY ,
  BU.BUSINESS_UNIT,
  UNIT.UNIT_NBR UNIT,
  UNIT_SQFT,
  UNIT_STATUS_FLAG,
  UNIT_TYPE_CD UNIT_TYPE,
  AREA_TYPE_CD AREA_TYPE,
  LEASE.LEASE_ID,
  LEASE.LEASE_VERSION_NBR,
  LEASE.TENANT_ID,
  LEASE.LEASE_STATUS_CD,
  LEASE.LEASE_STATUS_DESC,
  LEASE.LEASE_TYPE_CD,
  LEASE.MOVE_IN_DT,
  LEASE.MOVE_OUT_DT,
  LEASE.PLAN_OUT_DT,
  LEASE.LEASE_BEGIN_DT,
  LEASE.LEASE_END_DT,
  TENANT.TENANT_NAME,
  BU.DESCRIPTION AS BU_DESC, 
  BU.COMPANY  , 
  BU.COMPANY_DESC, 
  BU.BU_CAT_CD28, -- REGION 
  BU.BU_CAT_CD28_DESC, 
  BU.BU_CAT_CD18, -- SEGMENT_DESC 
  BU.BU_CAT_CD18_DESC, 
  BU.BU_CAT_CD24 ,-- HFM_Bgt_Bk_Rollup 
  BU.BU_CAT_CD24_DESC, 
  BU.BU_CAT_CD11, -- OPER_PROP_CD_DESC 
  BU.BU_CAT_CD11_DESC, 
  BU.BU_CAT_CD12 ,-- ASSET_TYPE_DESC 
  BU.BU_CAT_CD12_DESC, 
  FAR.TRANSACTION_ORIGINATOR,  
  BU.BU_CAT_CD27 ,  
   CAST(IFNULL(FAR.AMOUNT_GROSS,0.00) AS                DECIMAL(18,2)) AS GROSS_AMT ,
  CAST(IFNULL(FAR.TOTAL_AR_BALANCE_COMMERCIAL,0.00) AS DECIMAL(18,2)) AS OPEN_AMT  , 
  CAST(FAR.AMOUNT_GROSS - FAR.TOTAL_AR_BALANCE_COMMERCIAL AS DECIMAL(18,2)) AS PAID_AMT,  
  FAR.ACCOUNT_ID , 
   FAR.DOCUMENT_VOUCHER AS DOCUMENT_NBR ,
   FAR.DOCUMENT_TYPE ,
   FAR.DOCUMENT_COMPANY ,
   FAR.BILL_CODE,
   FAR.NAME_REMARK AS REMARK   , 
   FAR.OBJECT_ACCOUNT  , 
   FAR.PAY_STATUS_CODE ,
   FAR.INVOICE_DATE            AS INVOICE_DATE ,
   FAR.DATE_INVOICE_CLOSED     AS INVOICE_DATE_CLOSED ,
   FAR.DATE_NET_DUE            AS DUE_DATE ,
   FAR.GL_DATE AS GL_DATE ,
   FAR.SUPPLIER_INVOICE_NUMBER AS INVOICE_NBR,  
   FAR.SECURITY_DEPOSIT_AMOUNT
FROM  datalake_dev.semantic.FACT_AR  FAR
LEFT JOIN  datalake_dev.semantic.DIM_DATE  DD ON FAR.DATE_KEY =DD.DATE_ID
LEFT JOIN datalake_dev.semantic.DIM_BUSINESS_UNIT    BU ON FAR.BU_KEY=BU.BUSINESS_UNIT
LEFT JOIN datalake_dev.semantic.DIM_LEASE   LEASE ON FAR.LEASE_KEY =LEASE.LEASE_KEY
LEFT JOIN datalake_dev.semantic.DIM_UNIT    UNIT ON FAR.UNIT_KEY=UNIT.UNIT_KEY
LEFT JOIN datalake_dev.semantic.DIM_TENANT  TENANT ON TENANT.TENANT_ID=LEASE.TENANT_ID)
,add_rn
as
(
select row_number()over(partition by MONTH_START,BUSINESS_UNIT,UNIT,LEASE_ID,LEASE_VERSION_NBR order by bill_code) rn_lease ,
  row_number()over(partition by MONTH_START,BUSINESS_UNIT,UNIT  order by bill_code) rn_unit ,
    row_number()over(partition by MONTH_START,BUSINESS_UNIT  order by bill_code) rn_bu ,
   * ,
  sum(GROSS_AMT)over(partition by MONTH_START,BUSINESS_UNIT) BU_GROSS_AMT
,  sum(case when BILL_CODE   in ('SECT','SECD','SECC') then GROSS_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  bu_GROSS_AMT_SEC
  ,  sum(case when BILL_CODE   in ('PRPD') then GROSS_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  bu_GROSS_AMT_PRPD
    ,  sum(case when BILL_CODE   in ('UC') then GROSS_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  bu_GROSS_AMT_UC 
    ,  sum(case when BILL_CODE   in ('HHB1','HHB3','HHC1','HHC2','HHC3','HHE1','HHM1','HHR1'
                                    ,'HHS1','HHS9','HHT1','HHT9','HHW1') then GROSS_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  bu_GROSS_AMT_HH 
  
  ,  sum(OPEN_AMT)over(partition by MONTH_START,BUSINESS_UNIT) BU_OPEN_AMT
,  sum(case when BILL_CODE   in ('SECT','SECD','SECC') then OPEN_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  BU_OPEN_AMT_sec 
  ,  sum(case when BILL_CODE   in ('PRPD') then OPEN_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  BU_OPEN_AMT_prpd 
    ,  sum(case when BILL_CODE   in ('UC') then OPEN_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  BU_OPEN_AMT_uc 
      ,  sum(case when BILL_CODE   in ('HHB1','HHB3','HHC1','HHC2','HHC3','HHE1','HHM1','HHR1'
                                    ,'HHS1','HHS9','HHT1','HHT9','HHW1') then OPEN_AMT else 0 end)over(partition by MONTH_START,BUSINESS_UNIT)  BU_OPEN_AMT_HH 

  from base order by MONTH_START,BUSINESS_UNIT,UNIT,LEASE_ID--bu_GROSS_AMT_excl_prpd_uc_sec,   bu_prpd_amt, bu_uc_amt,bu_sec_amt
)
select 
 *,case when rn_lease=1 then SECURITY_DEPOSIT_AMOUNT else 0 end SECURITY_DEPOSIT_AMOUNT_dashboard
 ,case when rn_unit=1 then UNIT_SQFT else 0 end UNIT_SQFT_dashboard
 ,sum(SECURITY_DEPOSIT_AMOUNT_dashboard)over(partition by MONTH_START,BUSINESS_UNIT) bu_SECURITY_DEPOSIT_AMOUNT_dashboard
  ,sum(UNIT_SQFT_dashboard)over(partition by MONTH_START,BUSINESS_UNIT) bu_UNIT_SQFT_dashboard
from add_rn
--WHERE FAR.DATE_KEY=20200731
  );
