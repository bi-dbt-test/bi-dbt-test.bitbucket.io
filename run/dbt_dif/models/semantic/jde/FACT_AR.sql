

      create or replace  table datalake_dev.semantic.FACT_AR  as
      (WITH DIMDATE AS
     (
            SELECT DAY
                 , DATE_ID DATEKEY
                 , MONTH_START
                 , MONTH_END
                 , MONTH_NUM MONTHOFYEAR
            FROM
                           datalake_dev.semantic.DIM_DATE
            WHERE
                   MONTH_FLAG='TRUE'
                   AND MONTH_START BETWEEN '01/01/2015' AND '12/01/2028'
     ),
      CURRENT_LEASE                                          AS 
     (SELECT * FROM (
                  SELECT
                                LEASE_ID
                             , SUITE_TYPE_CD_DESC
                             , LEASE_VERSION_NBR
                             , LEASE_STATUS_CD
                             , LEASE_STATUS_DESC
                             , LEASE_TYPE_CD
                             , LEASE_TYPE_DESC
                             , LEASE_KEY
                             , BUSINESS_UNIT
                             , UNIT_NBR
                             , ROW_NUMBER() OVER (PARTITION BY LEASE_ID , BUSINESS_UNIT, UNIT_NBR ORDER BY
                                                  CAST(LEASE_VERSION_NBR AS INT) DESC) RN
                  FROM
                               datalake_dev.semantic.DIM_LEASE
                  WHERE
                               LEASE_ID <> 0)
      WHERE RN =1 
     )  
   ,RENTABLE_UNIT_LIST
     AS
     (SELECT DISTINCT BUSINESS_UNIT,UNIT_NBR FROM  datalake_dev.semantic.DIM_UNIT   WHERE AREA_TYPE_CD        = 'REN'
     )
     , DIMUNIT as
     (
            select
                   BUSINESS_UNIT  
                 , UNIT_NBR       
                 , unit_key
                 , UNIT_BEGIN_EFF_DATE  
                 , UNIT_END_EFF_DATE    
                 , SQR_FOOTAGE         UNITSQFT,AREA_TYPE_CD
                 , UNIT_TYPE_CD
                 ,'RENTTABLE UNIT' FLAG
                 , UNIT_STATUS
            FROM
                   (
                                SELECT
                                             DIM_UNIT.BUSINESS_UNIT
                                           , UPPER(DIM_UNIT.UNIT_NBR) UNIT_NBR
                                           , DIM_UNIT.UNIT_KEY
                                           , DIM_UNIT.UNIT_BEGIN_EFF_DATE
                                           , ROW_NUMBER() OVER (PARTITION BY DIM_UNIT.BUSINESS_UNIT ,DIM_UNIT.UNIT_NBR ,IFNULL(DIM_UNIT.UNIT_BEGIN_EFF_DATE,'1990-01-01') ORDER BY
                                                                             
                                                                                          CASE
                                                                                                       WHEN AREA_TYPE_CD = 'REN'
                                                                                                                    THEN 1
                                                                                                       WHEN AREA_TYPE_CD = 'USE'
                                                                                                                    THEN 2
                                                                                                                    ELSE 3
                                                                                       END  DESC                                                     
                                                                ) rn
                                           , SQR_FOOTAGE
                                           , UNIT_END_EFF_DATE
                                           , UNIT_TYPE_CD,AREA_TYPE_CD
                                           , UNIT_STATUS    --CASE WHEN IFNULL(UNIT_STATUS,'D')<>'O'        
                                FROM
                                        datalake_dev.semantic.DIM_UNIT       DIM_UNIT
                                INNER JOIN RENTABLE_UNIT_LIST L ON DIM_UNIT.BUSINESS_UNIT =L.BUSINESS_UNIT AND DIM_UNIT.UNIT_NBR=L.UNIT_NBR
                                WHERE
                                                                           
                                              AREA_TYPE_CD        = 'REN'
 
                   )
                   as b
            WHERE
                   rn=1
       UNION 
                   select
                   BUSINESS_UNIT  
                 , UNIT_NBR       
                 , unit_key
                 , UNIT_BEGIN_EFF_DATE  
                 , UNIT_END_EFF_DATE    
                 , SQR_FOOTAGE         UNITSQFT
                 ,AREA_TYPE_CD
                 , UNIT_TYPE_CD,'NO RENTABLE UNIT' FLAG
                 , UNIT_STATUS
            FROM
                   (
                                SELECT
                                             DIM_UNIT.BUSINESS_UNIT
                                           , UPPER(DIM_UNIT.UNIT_NBR) UNIT_NBR
                                           , UNIT_KEY
                                           , UNIT_BEGIN_EFF_DATE
                                           , ROW_NUMBER() OVER (PARTITION BY DIM_UNIT.BUSINESS_UNIT ,DIM_UNIT.UNIT_NBR ,IFNULL(UNIT_BEGIN_EFF_DATE,'1990-01-01') ORDER BY
                                                                             
                                                                                          CASE
                                                                                                       WHEN AREA_TYPE_CD = 'REN'
                                                                                                                    THEN 1
                                                                                                       WHEN AREA_TYPE_CD = 'USE'
                                                                                                                    THEN 2
                                                                                                                    ELSE 3
                                                                                       END  DESC                                                     
                                                                ) rn
                                           , SQR_FOOTAGE
                                           , UNIT_END_EFF_DATE
                                           , UNIT_TYPE_CD,AREA_TYPE_CD,UNIT_STATUS
                                FROM
                                              datalake_dev.semantic.DIM_UNIT   DIM_UNIT
                                LEFT JOIN RENTABLE_UNIT_LIST L ON DIM_UNIT.BUSINESS_UNIT =L.BUSINESS_UNIT AND DIM_UNIT.UNIT_NBR=L.UNIT_NBR
                                WHERE
                                             L.BUSINESS_UNIT IS NULL
 
                   )
                   as b
            WHERE
                   rn=1
     )  -- SELECT * FROM DIMUNIT where  unit_nbr ='1600';
,latest_unit
as
(select * from (
select row_number()over(partition by BUSINESS_UNIT,UNIT_NBR order by ifnull(UNIT_END_EFF_DATE,'2075-01-01' )desc) rn,* from DIMUNIT
)m where rn=1
 )--   select * from latest_unit  where   unit_nbr='1600';

,BASE
AS
(
SELECT 
       CL.DOCUMENTVOUCHER       AS DOCUMENT_VOUCHER, 
       CL.DOCUMENTTYPE          AS DOCUMENT_TYPE, 
       CL.COMPANYKEY            AS DOCUMENT_COMPANY, 
       CL.DOCUMENTPAYITEM       AS DOCUMENT_PAY_ITEM, 
       CL.ADDRESSNUMBER         AS ADDRESS_NUMBER, 
       CL.DATEFORGLANDVOUCHER      AS GL_DATE, 
       DATEINVOICEJULIAN        AS INVOICE_DATE,
       CL.DATEINVOICEJULIAN     AS DATE_INVOICE,	   
       CL.CENTURY*100 + CL.FISCALYEAR   AS YEAR ,
       CL.CENTURY, 
       CL.FISCALYEAR            AS FISCAL_YEAR, 
       CL.ACCOUNTID             AS ACCOUNT_ID, 
       CL.CURRENCYCODEFROM      AS CURRENCY_CODE_FROM, 
       CL.BUSINESSUNIT          AS BUSINESS_UNIT, 
       CL.OBJECTACCOUNT         AS OBJECT_ACCOUNT, 
       CL.SUBSIDIARY, 
       CL.SUBLEDGERTYPE         AS SUBLEDGER_TYPE, 
       CL.SUBLEDGER, 
       CL.BATCHTYPE             AS BATCH_TYPE, 
       CL.BATCHNUMBER           AS BATCH_NUMBER, 
       CL.DATEBATCHJULIAN       AS DATE_BATCH_JULIAN, 
       CL.PERIODNUMBERGENERALLEDGER AS PERIOD_NUMBER_GENERAL_LEDGER, 
       CL.COMPANY, 
       CL.BILLCODE              AS BILL_CODE, 
       CL.ADDNOALTERNATEPAYEE   AS ADD_NO_ALTERNATE_PAYEE, 
       CL.GLPOSTEDCODE          AS GL_POSTED_CODE, 
       CL.PAYSTATUSCODE         AS PAY_STATUS_CODE, 
       CL.AMOUNTGROSS           AS AMOUNT_GROSS, 
       CL.AMOUNTOPEN            AS TOTAL_AR_BALANCE_COMMERCIAL, 
       CL.RPBCRC                AS BASE_CURRENCY_CODE,
       CL.TAXEXPLANATIONCODE1   AS TAX_EXPLANATION_CODE1, 
       CL.DATESERVICECURRENCY   AS DATE_SERVICE_CURRENCY, 
       CL.GLBANKACCOUNT         AS GL_BANK_ACCOUNT, 
       CL.DATENETDUE            AS DATE_NET_DUE, 
       CL.DATESTATEMENT         AS DATE_STATEMENT, 
       CL.SUPPLIERINVOICENUMBER AS SUPPLIER_INVOICE_NUMBER, 
       CL.PURCHASEORDER         AS PURCHASE_ORDER, 
       CL.ORDERTYPE             AS ORDER_TYPE, 
       CL.LINENUMBER            AS LINE_NUMBER, 
       CL.DATEVALUE             AS DATE_CLEARED_VALUE_JULIAN,
       CL.UNIT, 
       CL.BUSINESSUNIT2         AS BUSINESS_UNIT2, 
       CL.NAMEREMARK            AS NAME_REMARK, 
       CL.NAMEALPHA             AS NAME_ALPHA, 
       CL.UNITOFMEASURE         AS UNIT_OF_MEASURE, 
       CL.VOIDDATEFORGLN        AS VOID_DATE_FOR_GL_N, 
       CL.VOIDFLAG              AS VOID_FLAG, 
       CL.PAYMENTID             AS PAYMENT_ID, 
       CL.USERRESERVEDDATE      AS USER_RESERVED_DATE, 
       CL.USERRESERVEDAMOUNT    AS USER_RESERVED_AMOUNT, 
       CL.USERRESERVEDNUMBER    AS USER_RESERVED_NUMBER, 
       CL.USERRESERVEDREFERENCE AS USER_RESERVED_REFERENCE, 
       CL.TRANSACTIONORIGINATOR AS TRANSACTION_ORIGINATOR, 
       CL.DATEINVOICECLOSED     AS DATE_INVOICE_CLOSED, 
       CL.PROGRAMID             AS PROGRAM_ID, 
       CL.USERID                AS USER_ID,
       CL.DATEUPDATED           AS DATE_UPDATED, 
       CL.TIMELASTUPDATED       AS TIME_LAST_UPDATED, 
       CL.WORKSTATIONID         AS WORK_STATION_ID, 
       CL.DOCUMENTORDERINVOICE  AS DOCUMENT_ORDER_INVOICE  
    FROM datalake_dev.governed.JDE_CUSTOMER_LEDGER   CL
  )    --SELECT TOP 1000 BUSINESS_UNIT,BUSINESS_UNIT2,UNIT,GL_DATE,BILL_CODE,* FROM BASE where PURCHASE_ORDER like '%298923' AND GL_DATE  between '09/01/2020'::date and '09/30/2020'::date ;
     --select PURCHASEORDER,* from  GOVERNED.JDE_CUSTOMER_LEDGER CL where CL.PURCHASEORDER ='00298923' order by DATEFORGLANDVOUCHER desc
     ,SECURITY_DEPOSIT
     AS
     (
      select  ltrim(BASE.PURCHASE_ORDER , '0#') lease_id,  BUSINESS_UNIT2, UNIT ,sum(TOTAL_AR_BALANCE_COMMERCIAL) security_open
       ,SUM(AMOUNT_GROSS) security_gross from base where BILL_CODE like 'SE%'
       group by lease_id,  BUSINESS_UNIT2, UNIT
     ) --  SELECT * FROM SECURITY_DEPOSIT  ;--1522 WHERE  LEASE_ID ='298326' ;--SELECT DISTINCT DATEBEGINNINGEFFECTIVE FROM GOVERNED.JDE_SECURITY_DEPOSIT_MASTER
,BASE_UNIT_LIST
AS
(
SELECT DISTINCT  DIMDATE.MONTH_END,BUSINESS_UNIT2 BUSINESS_UNIT,UNIT 
  FROM DIMDATE
 LEFT JOIN  BASE ON  BASE.GL_DATE BETWEEN DIMDATE.MONTH_START AND DIMDATE.MONTH_END   
)    -- SELECT DISTINCT BUSINESS_UNIT,UNIT  FROM BASE_UNIT_LIST WHERE    unit ='1600' and MONTH_END='2020-09-30' ;and unit='3650'; ORDER BY MONTH_END;
,vacant_unit
as
(
SELECT  DIMDATE.DATEKEY  DATE_KEY,DIMUNIT.unit_key,DIMUNIT.BUSINESS_UNIT bu_key ,
  NULL LEASE_KEY,DIMUNIT.UNITSQFT UNIT_SQFT,DIMUNIT.BUSINESS_UNIT,DIMUNIT.UNIT_NBR unit,UNIT_STATUS
 FROM DIMDATE
INNER JOIN   DIMUNIT ON DIMDATE.MONTH_END BETWEEN IFNULL(UNIT_BEGIN_EFF_DATE,'1990-01-01')::DATE AND IFNULL(UNIT_END_EFF_DATE,'2075-01-01')::DATE
 LEFT JOIN  BASE_UNIT_LIST BUL ON DIMDATE.MONTH_END=BUL.MONTH_END AND DIMUNIT.BUSINESS_UNIT =BUL.BUSINESS_UNIT AND DIMUNIT.UNIT_NBR=BUL.UNIT
  where BUL.UNIT is null
) --  SELECT * FROM vacant_unit  WHERE   unit ='0800' ;and date_key ='20200930';
--SELECT DISTINCT BUSINESS_UNIT,UNIT FROM vacant_unit WHERE  MONTH_END='2020-08-31' ;AND BUSINESS_UNIT IS NOT NULL;
 ,RESULT
 AS
 (
SELECT
       DIMDATE.DATEKEY DATE_KEY,
       IFNULL(DIMUNIT.unit_key,latest_unit.unit_key) unit_key ,
       IFNULL(DIMUNIT.BUSINESS_UNIT,latest_unit.BUSINESS_UNIT)   bu_key,
       IFNULL(LEASE.LEASE_KEY,CURRENT_LEASE.LEASE_KEY) LEASE_KEY,
       IFNULL(DIMUNIT.UNITSQFT,latest_unit.UNITSQFT)  UNIT_SQFT,                                
       SD.security_open SECURITY_DEPOSIT_AMOUNT,
       BASE.DOCUMENT_VOUCHER       AS DOCUMENT_VOUCHER, 
       BASE.DOCUMENT_TYPE          AS DOCUMENT_TYPE, 
       BASE.DOCUMENT_COMPANY            AS DOCUMENT_COMPANY, 
       BASE.DOCUMENT_PAY_ITEM       AS DOCUMENT_PAY_ITEM, 
       BASE.ADDRESS_NUMBER         AS ADDRESS_NUMBER, 
       BASE.GL_DATE      AS GL_DATE, 
       BASE.INVOICE_DATE        AS INVOICE_DATE,   
       BASE.ACCOUNT_ID             AS ACCOUNT_ID, 
       BASE.CURRENCY_CODE_FROM      AS CURRENCY_CODE_FROM, 
       BASE.OBJECT_ACCOUNT         AS OBJECT_ACCOUNT, 
       BASE.SUBSIDIARY, 
       BASE.SUBLEDGER_TYPE         AS SUBLEDGER_TYPE, 
       BASE.SUBLEDGER, 
       BASE.BATCH_TYPE             AS BATCH_TYPE, 
       BASE.BATCH_NUMBER           AS BATCH_NUMBER, 
       BASE.DATE_BATCH_JULIAN       AS DATE_BATCH_JULIAN, 
       BASE.PERIOD_NUMBER_GENERAL_LEDGER AS PERIOD_NUMBER_GENERAL_LEDGER, 
       BASE.BILL_CODE              AS BILL_CODE, 
       BASE.ADD_NO_ALTERNATE_PAYEE   AS ADD_NO_ALTERNATE_PAYEE, 
       BASE.GL_POSTED_CODE          AS GL_POSTED_CODE, 
       BASE.PAY_STATUS_CODE         AS PAY_STATUS_CODE, 
       BASE.AMOUNT_GROSS           AS AMOUNT_GROSS, 
       BASE.TOTAL_AR_BALANCE_COMMERCIAL            AS TOTAL_AR_BALANCE_COMMERCIAL, 
       BASE.BASE_CURRENCY_CODE                AS BASE_CURRENCY_CODE,
       BASE.TAX_EXPLANATION_CODE1   AS TAX_EXPLANATION_CODE1, 
       BASE.DATE_SERVICE_CURRENCY   AS DATE_SERVICE_CURRENCY, 
       BASE.GL_BANK_ACCOUNT         AS GL_BANK_ACCOUNT, 
       BASE.DATE_NET_DUE            AS DATE_NET_DUE, 
       BASE.DATE_STATEMENT         AS DATE_STATEMENT, 
       BASE.SUPPLIER_INVOICE_NUMBER AS SUPPLIER_INVOICE_NUMBER, 
       BASE.PURCHASE_ORDER         AS PURCHASE_ORDER, 
       BASE.ORDER_TYPE             AS ORDER_TYPE, 
       BASE.LINE_NUMBER            AS LINE_NUMBER, 
       BASE.DATE_CLEARED_VALUE_JULIAN             AS DATE_CLEARED_VALUE_JULIAN,
       BASE.BUSINESS_UNIT2         AS BUSINESS_UNIT2, 
       BASE.NAME_REMARK            AS NAME_REMARK, 
       BASE.NAME_ALPHA             AS NAME_ALPHA, 
       BASE.UNIT_OF_MEASURE         AS UNIT_OF_MEASURE, 
       BASE.VOID_DATE_FOR_GL_N        AS VOID_DATE_FOR_GL_N, 
       BASE.VOID_FLAG              AS VOID_FLAG, 
       BASE.PAYMENT_ID             AS PAYMENT_ID, 
       BASE.USER_RESERVED_DATE      AS USER_RESERVED_DATE, 
       BASE.USER_RESERVED_AMOUNT    AS USER_RESERVED_AMOUNT, 
       BASE.USER_RESERVED_NUMBER    AS USER_RESERVED_NUMBER, 
       BASE.USER_RESERVED_REFERENCE AS USER_RESERVED_REFERENCE, 
       BASE.TRANSACTION_ORIGINATOR AS TRANSACTION_ORIGINATOR, 
       BASE.DATE_INVOICE_CLOSED     AS DATE_INVOICE_CLOSED, 
       BASE.PROGRAM_ID             AS PROGRAM_ID, 
       BASE.USER_ID                AS USER_ID,
       BASE.DATE_UPDATED           AS DATE_UPDATED, 
       BASE.TIME_LAST_UPDATED       AS TIME_LAST_UPDATED, 
       BASE.WORK_STATION_ID         AS WORK_STATION_ID, 
       BASE.DOCUMENT_ORDER_INVOICE  AS DOCUMENT_ORDER_INVOICE ,
       CASE WHEN coalesce(DIMUNIT.UNIT_STATUS,latest_unit.UNIT_STATUS,'D')='O' 
       OR   DIMDATE.MONTH_END >coalesce(DIMUNIT.UNIT_END_EFF_DATE,latest_unit.UNIT_END_EFF_DATE,'2075-01-01') ::DATE 
       OR DIMDATE.MONTH_END <coalesce(DIMUNIT.UNIT_BEGIN_EFF_DATE,latest_unit.UNIT_BEGIN_EFF_DATE,'1990-01-01') ::DATE  
       THEN 'INVALIDATE' ELSE 'ACTIVE' END UNIT_STATUS_FLAG
 FROM DIMDATE
 LEFT JOIN  BASE ON  BASE.GL_DATE BETWEEN DIMDATE.MONTH_START AND DIMDATE.MONTH_END   
  LEFT JOIN DIMUNIT ON DIMUNIT.BUSINESS_UNIT =BASE.BUSINESS_UNIT2 
                   AND DIMUNIT.UNIT_NBR=BASE.UNIT    
                  AND DIMDATE.MONTH_END BETWEEN IFNULL(UNIT_BEGIN_EFF_DATE,'1990-01-01')::DATE AND IFNULL(UNIT_END_EFF_DATE,'2075-01-01')::DATE          
 LEFT JOIN latest_unit ON  latest_unit.BUSINESS_UNIT =BASE.BUSINESS_UNIT2 
                   AND latest_unit.UNIT_NBR=BASE.UNIT       

   LEFT  JOIN   datalake_dev.semantic.DIM_BUSINESS_UNIT BU  ON  BU.BUSINESS_UNIT =  DIMUNIT.BUSINESS_UNIT
  LEFT  JOIN   datalake_dev.semantic.DIM_LEASE LEASE ON         cast( LEASE.LEASE_ID as nvarchar)=   ltrim(BASE.PURCHASE_ORDER , '0#')
                                                AND LEASE.BUSINESS_UNIT = BASE.BUSINESS_UNIT2
                                                AND LEASE.UNIT_NBR = BASE.UNIT  
                                               AND DIMDATE.MONTH_END BETWEEN 
                                               CASE WHEN IFNULL(LEASE_BEGIN_DT,'1990-01-01')::DATE< IFNULL(VERSION_BEGIN_DT,'1990-01-01')::DATE
                                               THEN  IFNULL(VERSION_BEGIN_DT,'1990-01-01')::DATE ELSE IFNULL(LEASE_BEGIN_DT,'1990-01-01')::DATE END AND
                                               CASE WHEN IFNULL(LEASE_END_DT,'2075-01-01')::DATE< IFNULL(VERSION_END_DT,'2075-01-01')::DATE
                                               THEN  IFNULL(LEASE_END_DT,'2075-01-01')::DATE ELSE IFNULL(VERSION_END_DT,'2075-01-01')::DATE END
  LEFT JOIN CURRENT_LEASE ON cast(CURRENT_LEASE.LEASE_ID as nvarchar)=   ltrim(BASE.PURCHASE_ORDER , '0#')
                                                AND CURRENT_LEASE.BUSINESS_UNIT = BASE.BUSINESS_UNIT2
                                                AND CURRENT_LEASE.UNIT_NBR = BASE.UNIT  
  LEFT JOIN SECURITY_DEPOSIT SD         ON
                                                SD.LEASE_ID =  ltrim(BASE.PURCHASE_ORDER , '0#')  
                                            AND SD.BUSINESS_UNIT2=BASE.BUSINESS_UNIT2
                                            AND SD.UNIT=BASE.UNIT
 
UNION ALL
SELECT DATE_KEY,unit_key,bu_key,LEASE_KEY,UNIT_SQFT,
NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL
,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,CASE WHEN IFNULL(UNIT_STATUS,'D')='O' 
       THEN 'INVALIDATE' ELSE 'ACTIVE' END UNIT_STATUS_FLAG
FROM vacant_unit
--WHERE  BUSINESS_UNIT IS NULL
   )
   
   SELECT * FROM RESULT -- WHERE lease_key is not null--DATE_KEY ='20200831' AND LEASE_KEY IS NULL AND UNIT_KEY IS NOT NULL ORDER BY UNIT_KEY
      );
    