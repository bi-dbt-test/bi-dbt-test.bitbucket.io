

      create or replace  table datalake_dev.semantic.BILLCODE_LIST  as
      (select   'ABMR' BILLCODE,'Abated Minimum Rent' BILLCODEDESCRIPTION,'Base Rent' BILLCODECATEGORY,'Base Minimum Rent' BILLCODESUBCATEGORY
,'TRUE' TSMINRENT ,'FALSE'  TSPERCENTRENT,'FALSE'  TSOPERATINGCOSTS,'TRUE'  TSOCCUPANCYCOSTS,'TRUE'  BILLCODECATEGORYSORT,'TRUE'  BILLCODESUBCATEGORYSORT   union all
select   'BMPR','Base Minimum Rent-Peripheral','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'BMRA','Base Minimum Rent-Additional','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'BMRD','Base Minimum Rent-Dept Store','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'BMRG','Base Minimum Rent-Gross','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BMRR','Base Minimum Rent','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BMRS','Base Min Rent-Spec Lease-12+','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'BMST','Base Minimum Rent-Stall','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'BPLD','Percent in Lieu-Dept Sto','Base Rent','% Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BPLG','Percent in Lieu-Gross','Base Rent','% Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BPLP','Percent in Lieu-Prior Yr','Base Rent','% Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BPLR','Percent in Lieu','Base Rent','% Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BPPR','Percent in Lieu-Peripheral','Base Rent','% Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CAMB','Occupancy Charge','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMD','CAM-Outdoor','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMF','CAM-Fixed','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMI','CAM-Insurance','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMN','CAM-Indoor','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMO','CAM','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMS','CAM-Sprinkler','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMT','CAMT-Total Operating Exp. Reim','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMU','Food Court Oper Exp Pymt','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CAMV','CAM-Other Recoverable','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CPIA','Base Minimum Rent-Escalation','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CPIM','MA Tenant Dues Escalation','Other','Marketing','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CPIT','Total Oper Exp Reim-Escalation','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'ELEC','Electric','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'ELLP','Tenant L&P Equipment','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'EPHB','HVAC Energy/Maint & Repair','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'EPHE','HVAC-Maint & Repair','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'FDCC','Food Court-CAM','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'FDFX','Food Court- Fixed CAM','Recoveries','CAM','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'GASC','Gas Charge','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'GNDR','Ground Rent','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'HVAC','HVAC-Maintenance','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'HVAF','HVAC-Fixed','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'HVAM','HVAC-Mall','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'HVCS','HVAC-Store','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'HVFT','HVAC-Fixed Tenant','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'HVRO','HVAC Equipment','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'INSR','Insurance Reimbursement','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'LAND','Landscaping','Recoveries','CAM','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'MADV','Merch Assoc Advertising','Other','Marketing','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'MEDC','Media Charge','Other','Marketing','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'MFFC','Marketing Fund-Food Court','Other','Marketing','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'MISC','Miscellaneous Tenant','Other','Other','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'MKFD','Marketing Fund','Other','Marketing','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'MRAD','Merchant Association Dues','Other','Marketing','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'PARK','Parking','Other','Parking','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'PART','Parking','Other','Parking','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'RETF','Reimbursed Charges-Fixed','Recoveries','CAM','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'RETX','Real Estate Tax','Recoveries','Tax','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'RSDX','Min Rent Spec Deal Xtra','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SEWE','Sewer','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPBM','Base Min Rent-Spec Lease-12+','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPCA','Spec Lease-CAM Reimburse','Recoveries','CAM','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPEL','Spec Lease-Electric','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPGS','Spec Lease-Inline Gas','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPIU','Spec Lease-TILS','Base Rent','Specialty Lease','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'SPIX','Spec Lease-TILS (1-30 days)','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPKS','Spec Lease-Kiosk','Base Rent','Specialty Lease','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'SPKW','Spec Lease-Kiosk-Wireless','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPKX','Spec Lease-Kiosk (1-30 days)','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPLA','Spec Lease-Late Fees','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPMI','Spec Lease-Miscellaneous','Other','Other','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPMK','Spec Lease-Marketing','Other','Marketing','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPOT','Spec Lease-Other Reimb','Other','Other','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPPL','Spec Lease-Percent in Lieu','Base Rent','% Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'SPRI','Sprinkler','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPRM','Spec Lease-RMU Income','Base Rent','Specialty Lease','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'SPRW','Spec Lease-RMU-Wireless','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPRX','Spec Lease-RMU (1-30 days)','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPST','Specialty Lease - Storage','Base Rent','Specialty Lease','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPUT','Spec Lease-Utilities','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'STOR','Storage Charge','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'TARC','Tenant Allowance Rent Credit','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'TARD','Development TA Rent Credit','Base Rent','Base Minimum Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'TENT','Tenant Trash Removal','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'TNTL','Tenant Telephone','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'TRAS','Trash','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'UTIL','Utilities','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'WATR','Water','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'WTSR','Water & Sewer','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'SPPO','Spec Lease-Overage Rent Other','Base Rent','% Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'SPPP','Percentage Rent','Base Rent','% Rent','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'GAAP','Spec Lease-Miscellaneous      ','Unknown','Unknown','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'HOLD','Holdover Rent                 ','Unknown','Unknown','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'AR  ','Abated Rent                   ','Unknown','Unknown','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'BRAO','Rent Abatement - Office','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BRAR','Rent Abatement - Retail','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BRGO','Base Rent-Gross - Office','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BRGR','Base Rent-Gross - Retail','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BROF','Base rent - office','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'BRRE','Base rent - retail','Base Rent','Base Minimum Rent','TRUE','FALSE','FALSE','TRUE','TRUE','TRUE'   union all
select   'CRFO','CAM Recovery - fixed - office','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CRFR','CAM Recovery - fixed - retail','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CARO','CAM Recovery - office','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CARR','CAM Recovery - retail','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CRPO','CAM Recovery - PY office','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CRPR','CAM Recovery - PY retail','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CREO','CAM Rec - Electric - Office','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CRER','CAM Rec - Electric - Retail','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CEOP','CAM Rec - Elect-Office-PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CERP','CAM Rec - Elect -Retail-PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CRGO','CAM Recovery - Gas - Office','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CRGR','CAM Recovery - Gas - Retail','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CGOP','CAM Rec - Gas - Office - PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CGRP','CAM Rec - Gas - Retail - PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CRHO','CAM Recovery - HVAC - Office','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CWRP','CAM Rec-Water/Sewer- Retail-PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CWSO','CAM Rec - Water/Sewer- Office','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CWSR','CAM Rec - Water/Sewer- Retail','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'ASFS','Assignment Fees','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'AUDC','CAM-Prior Year Audit','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'AUDT','Real Estate Tax-Pr Year Audit','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'BMRP','Base Minimum Rent-Prior Yr','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CAMP','CAM-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CNTR','Contractor Rent','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'DEFR','Deferred Rent','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'ENVI','Environmental Services','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'FACL','FASB 13 - Accrual','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'FADJ','FASB 13 - Adjustment','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'FCNV','FASB 13 - Conversion','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'FDEF','FASB 13 - Deferral','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'GASP','Gas Charge-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'GROA','Grand Opening Assessment','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'HVAP','HVAC-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'IMKA','Initial Mkg Assessment','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'INNT','Interest Notes Receivable','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'INTT','Interest-Tenants','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'KEYM','Key Money','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'LATE','Late Charges','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'LMIS','Misc Revenue-Land','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'LSCF','Lease Cancellation Fees','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'MAWO','Maintenance Work Order','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'NRCT','Notes Receivable-Tenant','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'NSFI','NSF','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PBRT','Prepaid Base Rent','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PCSA','Overage Rent-Sale Audit','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PCTR','Overage Rent','Not on Rent Roll','Not on Rent Roll','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'RETP','Real Estate Tax-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SECC','Security Deposit-Construction','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SELF','License Fee Deposit','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SLTX','Sales Tax Payable-Tenant','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPAD','Advertising Venues & Sales','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPAF','Installed Fixture Advertising','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPCF','Spec Leas-Termination Income','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPLO','Spec Lease-Local & Reg Spons.','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPMP','Spec Lease-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPPK','Spec Lease-Overage Rent Kiosk','Not on Rent Roll','Not on Rent Roll','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'SPPR','Spec Lease-Overage Rent Inline','Not on Rent Roll','Not on Rent Roll','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'SPPU','Spec Lease-Overage Rnt-RMU','Not on Rent Roll','Not on Rent Roll','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'SPSA','National Sampling & Tours','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPSH','Shows & Displays','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPVD','Spec Lease - Vending','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'TNCR','Tenant Const Reimbursement','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'TNSC','Tenant Service Charge','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'WTSP','Water & Sewer-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'LPRM','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'ELEP','Electric-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'TXCC','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PROF','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SECT','Security Deposit-Temporary','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'MRNT','Misc Rent','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'COS1','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PCCP','Overage Rent-Pr Yr-Cash Basis','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'DEVS','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SECD','Security Deposit-Permanent','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'ADIN','Insufficient Advertising','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'LAOP','Late Opening Penalty','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'HVCP','HVAC-Store Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'SPPP','Spec Lease-Overage Rent Pr Yr','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PCCB','Overage Rent-Cash Basis','Not on Rent Roll','Not on Rent Roll','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'SPPE','Spec Lease-Pepsi','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'LGLF','Legal Fees','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'MOIA','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'INSP','Insurance Reimb-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PRNT','Prepaid Rent-Auto Cash','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'RFND','Tenant Refund','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'BPP','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PCTA','Overage Rent-Adjustment','Not on Rent Roll','Not on Rent Roll','TRUE','FALSE','FALSE','TRUE','FALSE','FALSE'   union all
select   'UC','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'ADVF','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'REMD','Local Real Estate Tax Bill Adv','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'PCPP','Overage Rent-Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'FDCP','Food Court-CAM Prior Year','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'DEPD','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'STFE','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'C890','Unknown','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'OPEP','Projected Op Exp-To Be Billed','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'OPER','Rebilled Project OPE','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CRHR','CAM Recovery - HVAC - Retail','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CHOP','CAM Rec - HVAC - Office -PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CHRP','CAM Rec - HVAC - Retail -PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'CHOF','CAM Rec - HVAC - Office-Fixed','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'CHRF','CAM Rec - HVAC - Retail -Fixed','Other','Utilities','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'INRO','Ins Recovery-Office','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'INRR','Ins Recovery-Retail','Recoveries','CAM','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'TXFO','Tax Recovery - fixed - office','Recoveries','CAM','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'TXFR','Tax Recovery - fixed - retail','Recoveries','CAM','FALSE','FALSE','FALSE','FALSE','TRUE','TRUE'   union all
select   'TRPO','Tax Recovery - PY office','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'TRPR','Tax Recovery - PY retail','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'   union all
select   'TXRO','Tax Recovery - office','Recoveries','Tax','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'TXRR','Tax Recovery - retail','Recoveries','Tax','FALSE','FALSE','TRUE','TRUE','TRUE','TRUE'   union all
select   'CWOP','CAM Rec-Water/Sewer- Office-PY','Not on Rent Roll','Not on Rent Roll','FALSE','FALSE','FALSE','FALSE','FALSE','FALSE'
      );
    