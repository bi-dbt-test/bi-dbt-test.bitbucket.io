

      create or replace  table datalake_dev.semantic.FACT_FINANCE  as
      (
with 
 DIMDATE                                      AS
     (
            SELECT
                   DATE_ID DATEKEY
                 , MONTH_START
                 , MONTH_END
                 , MONTH_NUM MONTHOFYEAR
            FROM
                 datalake_dev.semantic.DIM_DATE  
            WHERE
                    MONTH_START BETWEEN DATEADD(YEAR,-10,CURRENT_DATE) AND DATEADD(YEAR,10,CURRENT_DATE)
     )
,base
as
(
select  ACCOUNTID,COMPANY,BUSINESSUNIT,OBJECTACCOUNT,SUBSIDIARY,LEDGERTYPE,CENTURY,FISCALYEAR,
sum(AMTBEGINNINGBALANCEPY) Beginning_Bal_Amt,sum(AMOUNTNETPOSTING001) jan_amt,sum(AMOUNTNETPOSTING002)  Feb_Amt
,sum(AMOUNTNETPOSTING003) mar_amt,sum(AMOUNTNETPOSTING004) apr_amt,sum(AMOUNTNETPOSTING005) may_amt
,sum(AMOUNTNETPOSTING006) jun_amt,sum(AMOUNTNETPOSTING007) jul_amt,sum(AMOUNTNETPOSTING008) aug_amt
,sum(AMOUNTNETPOSTING009) sep_amt,sum(AMOUNTNETPOSTING010) oct_amt,sum(AMOUNTNETPOSTING011) nov_amt,sum(AMOUNTNETPOSTING012) dec_amt
,sum(AMTORIGINALBEGINBUD) Original_Amt
 from  datalake_dev.governed.JDE_ACCOUNT_BALANCES     AL
		where ledgertype in ('AA', 'E1', 'AJ', 'AU', 'BA', '1F', '2F', '3F', '4F', '1B', '2B', '3B', '4B', 'JA', 'PA', 'HA', 'JY', 'EB', 'F1', 'F2', 'F3', 'F4', 'B1', 'B2', 'B3', 'B4')
        --and OBJECTACCOUNT='141310' and businessunit ='32005'
   --     AND OBJECTACCOUNT='202020' AND SUBSIDIARY='WFB'
 group by  ACCOUNTID,COMPANY,BUSINESSUNIT,OBJECTACCOUNT,SUBSIDIARY,LEDGERTYPE,CENTURY,FISCALYEAR
)

,unpivot_base
as
(
select  accountid,company,businessunit,objectaccount, SUBSIDIARY,cast(cast(century as varchar)||cast(fiscalyear as varchar) as smallint) cal_year, 
									CAST(CASE WHEN MNTH = 'JAN_AMT' THEN 1
									WHEN MNTH = 'FEB_AMT' THEN 2
									WHEN MNTH = 'MAR_AMT' THEN 3
									WHEN MNTH = 'APR_AMT' THEN 4
									WHEN MNTH = 'MAY_AMT' THEN 5
									WHEN MNTH = 'JUN_AMT' THEN 6
									WHEN MNTH = 'JUL_AMT' THEN 7
									WHEN MNTH = 'AUG_AMT' THEN 8
									WHEN MNTH = 'SEP_AMT' THEN 9
									WHEN MNTH = 'OCT_AMT' THEN 10
									WHEN MNTH = 'NOV_AMT' THEN 11
									WHEN MNTH = 'DEC_AMT' THEN 12
									else null 
									end as tinyint) Mnth_nbr,  


   sum(case when ledgertype = 'AA' then Amount else 0 end)  as AA_Amt,

   sum(case when ledgertype = 'E1' then Amount else 0 end)  as E1_Amt,

   sum(case when ledgertype = 'AJ' then Amount else 0 end)  as AJ_Amt,

   sum(case when ledgertype = 'AU' then Amount else 0 end)  as AU_Amt,

   sum(case when ledgertype = 'BA' then Amount else 0 end)  as BA_Amt,

   sum(case when ledgertype = '1F' then Amount else 0 end)  as _1F_Amt,

   sum(case when ledgertype = '2F' then Amount else 0 end)  as _2F_Amt,

   sum(case when ledgertype = '3F' then Amount else 0 end)  as _3F_Amt,

   sum(case when ledgertype = '4F' then Amount else 0 end)  as _4F_Amt,

   sum(case when ledgertype = '1B' then Amount else 0 end)  as _1B_Amt,

   sum(case when ledgertype = '2B' then Amount else 0 end)  as _2B_Amt,

   sum(case when ledgertype = '3B' then Amount else 0 end)  as _3B_Amt,

   sum(case when ledgertype = '4B' then Amount else 0 end)  as _4B_Amt,

   sum(case when ledgertype = 'JA' then Amount else 0 end)  as JA_Amt,

   sum(case when ledgertype = 'PA' then Amount else 0 end)  as PA_Amt,

   sum(case when ledgertype = 'HA' then Amount else 0 end)  as HA_Amt,

   sum(case when ledgertype = 'JY' then Amount else 0 end)  as JY_Amt,

   sum(case when ledgertype = 'EB' then Amount else 0 end)  as EB_Amt,

   sum(case when ledgertype = 'F1' then Amount else 0 end)  as F1_Amt,

   sum(case when ledgertype = 'F2' then Amount else 0 end)  as F2_Amt,

   sum(case when ledgertype = 'F3' then Amount else 0 end)  as F3_Amt,

   sum(case when ledgertype = 'F4' then Amount else 0 end)  as F4_Amt,

   sum(case when ledgertype = 'B1' then Amount else 0 end)  as B1_Amt,

   sum(case when ledgertype = 'B2' then Amount else 0 end)  as B2_Amt,

   sum(case when ledgertype = 'B3' then Amount else 0 end)  as B3_Amt,

   sum(case when ledgertype = 'B4' then Amount else 0 end)  as B4_Amt,



      sum(case when ledgertype = 'AA' then Beginning_Bal_Amt else 0 end)  as AA_Beg_Bal,
      sum(case when ledgertype = 'AA' then Original_Amt else 0 end)  as AA_Orig_Amt
,

      sum(case when ledgertype = 'E1' then Beginning_Bal_Amt else 0 end)  as E1_Beg_Bal,
      sum(case when ledgertype = 'E1' then Original_Amt else 0 end)  as E1_Orig_Amt
,

      sum(case when ledgertype = 'AJ' then Beginning_Bal_Amt else 0 end)  as AJ_Beg_Bal,
      sum(case when ledgertype = 'AJ' then Original_Amt else 0 end)  as AJ_Orig_Amt
,

      sum(case when ledgertype = 'AU' then Beginning_Bal_Amt else 0 end)  as AU_Beg_Bal,
      sum(case when ledgertype = 'AU' then Original_Amt else 0 end)  as AU_Orig_Amt
,

      sum(case when ledgertype = 'BA' then Beginning_Bal_Amt else 0 end)  as BA_Beg_Bal,
      sum(case when ledgertype = 'BA' then Original_Amt else 0 end)  as BA_Orig_Amt
,

      sum(case when ledgertype = '1F' then Beginning_Bal_Amt else 0 end)  as _1F_Beg_Bal,
      sum(case when ledgertype = '1F' then Original_Amt else 0 end)  as _1F_Orig_Amt
,

      sum(case when ledgertype = '2F' then Beginning_Bal_Amt else 0 end)  as _2F_Beg_Bal,
      sum(case when ledgertype = '2F' then Original_Amt else 0 end)  as _2F_Orig_Amt
,

      sum(case when ledgertype = '3F' then Beginning_Bal_Amt else 0 end)  as _3F_Beg_Bal,
      sum(case when ledgertype = '3F' then Original_Amt else 0 end)  as _3F_Orig_Amt
,

      sum(case when ledgertype = '4F' then Beginning_Bal_Amt else 0 end)  as _4F_Beg_Bal,
      sum(case when ledgertype = '4F' then Original_Amt else 0 end)  as _4F_Orig_Amt
,

      sum(case when ledgertype = '1B' then Beginning_Bal_Amt else 0 end)  as _1B_Beg_Bal,
      sum(case when ledgertype = '1B' then Original_Amt else 0 end)  as _1B_Orig_Amt
,

      sum(case when ledgertype = '2B' then Beginning_Bal_Amt else 0 end)  as _2B_Beg_Bal,
      sum(case when ledgertype = '2B' then Original_Amt else 0 end)  as _2B_Orig_Amt
,

      sum(case when ledgertype = '3B' then Beginning_Bal_Amt else 0 end)  as _3B_Beg_Bal,
      sum(case when ledgertype = '3B' then Original_Amt else 0 end)  as _3B_Orig_Amt
,

      sum(case when ledgertype = '4B' then Beginning_Bal_Amt else 0 end)  as _4B_Beg_Bal,
      sum(case when ledgertype = '4B' then Original_Amt else 0 end)  as _4B_Orig_Amt
,

      sum(case when ledgertype = 'JA' then Beginning_Bal_Amt else 0 end)  as JA_Beg_Bal,
      sum(case when ledgertype = 'JA' then Original_Amt else 0 end)  as JA_Orig_Amt
,

      sum(case when ledgertype = 'PA' then Beginning_Bal_Amt else 0 end)  as PA_Beg_Bal,
      sum(case when ledgertype = 'PA' then Original_Amt else 0 end)  as PA_Orig_Amt
,

      sum(case when ledgertype = 'HA' then Beginning_Bal_Amt else 0 end)  as HA_Beg_Bal,
      sum(case when ledgertype = 'HA' then Original_Amt else 0 end)  as HA_Orig_Amt
,

      sum(case when ledgertype = 'JY' then Beginning_Bal_Amt else 0 end)  as JY_Beg_Bal,
      sum(case when ledgertype = 'JY' then Original_Amt else 0 end)  as JY_Orig_Amt
,

      sum(case when ledgertype = 'EB' then Beginning_Bal_Amt else 0 end)  as EB_Beg_Bal,
      sum(case when ledgertype = 'EB' then Original_Amt else 0 end)  as EB_Orig_Amt
,

      sum(case when ledgertype = 'F1' then Beginning_Bal_Amt else 0 end)  as F1_Beg_Bal,
      sum(case when ledgertype = 'F1' then Original_Amt else 0 end)  as F1_Orig_Amt
,

      sum(case when ledgertype = 'F2' then Beginning_Bal_Amt else 0 end)  as F2_Beg_Bal,
      sum(case when ledgertype = 'F2' then Original_Amt else 0 end)  as F2_Orig_Amt
,

      sum(case when ledgertype = 'F3' then Beginning_Bal_Amt else 0 end)  as F3_Beg_Bal,
      sum(case when ledgertype = 'F3' then Original_Amt else 0 end)  as F3_Orig_Amt
,

      sum(case when ledgertype = 'F4' then Beginning_Bal_Amt else 0 end)  as F4_Beg_Bal,
      sum(case when ledgertype = 'F4' then Original_Amt else 0 end)  as F4_Orig_Amt
,

      sum(case when ledgertype = 'B1' then Beginning_Bal_Amt else 0 end)  as B1_Beg_Bal,
      sum(case when ledgertype = 'B1' then Original_Amt else 0 end)  as B1_Orig_Amt
,

      sum(case when ledgertype = 'B2' then Beginning_Bal_Amt else 0 end)  as B2_Beg_Bal,
      sum(case when ledgertype = 'B2' then Original_Amt else 0 end)  as B2_Orig_Amt
,

      sum(case when ledgertype = 'B3' then Beginning_Bal_Amt else 0 end)  as B3_Beg_Bal,
      sum(case when ledgertype = 'B3' then Original_Amt else 0 end)  as B3_Orig_Amt
,

      sum(case when ledgertype = 'B4' then Beginning_Bal_Amt else 0 end)  as B4_Beg_Bal,
      sum(case when ledgertype = 'B4' then Original_Amt else 0 end)  as B4_Orig_Amt


  from 
  base
  unpivot 
        (Amount for Mnth in 
        	(jan_amt,feb_amt,mar_amt,apr_amt,May_amt,Jun_amt,Jul_amt, Aug_amt, Sep_amt, Oct_amt,Nov_amt, Dec_amt) 
        ) as unpvt_amt
  group by  accountid,company,businessunit,objectaccount, SUBSIDIARY,cast(cast(century as varchar)||cast(fiscalyear as varchar) as smallint)  ,MNTH
)--select * from unpivot_base
,result_non_format
as
(
select accountid,company,businessunit,objectaccount, SUBSIDIARY,cal_year,Mnth_nbr,

AA_Amt MTD_AA
,sum(AA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_AA
,sum(AA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_AA
 ,sum(AA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_AA
 ,AA_Beg_Bal
 ,AA_Orig_Amt
,

E1_Amt MTD_E1
,sum(E1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_E1
,sum(E1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_E1
 ,sum(E1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_E1
 ,E1_Beg_Bal
 ,E1_Orig_Amt
,

AJ_Amt MTD_AJ
,sum(AJ_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_AJ
,sum(AJ_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_AJ
 ,sum(AJ_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_AJ
 ,AJ_Beg_Bal
 ,AJ_Orig_Amt
,

AU_Amt MTD_AU
,sum(AU_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_AU
,sum(AU_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_AU
 ,sum(AU_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_AU
 ,AU_Beg_Bal
 ,AU_Orig_Amt
,

BA_Amt MTD_BA
,sum(BA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_BA
,sum(BA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_BA
 ,sum(BA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_BA
 ,BA_Beg_Bal
 ,BA_Orig_Amt
,

_1F_Amt MTD_1F
,sum(_1F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_1F
,sum(_1F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_1F
 ,sum(_1F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_1F
 ,1F_Beg_Bal
 ,1F_Orig_Amt
,

_2F_Amt MTD_2F
,sum(_2F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_2F
,sum(_2F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_2F
 ,sum(_2F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_2F
 ,2F_Beg_Bal
 ,2F_Orig_Amt
,

_3F_Amt MTD_3F
,sum(_3F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_3F
,sum(_3F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_3F
 ,sum(_3F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_3F
 ,3F_Beg_Bal
 ,3F_Orig_Amt
,

_4F_Amt MTD_4F
,sum(_4F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_4F
,sum(_4F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_4F
 ,sum(_4F_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_4F
 ,4F_Beg_Bal
 ,4F_Orig_Amt
,

_1B_Amt MTD_1B
,sum(_1B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_1B
,sum(_1B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_1B
 ,sum(_1B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_1B
 ,1B_Beg_Bal
 ,1B_Orig_Amt
,

_2B_Amt MTD_2B
,sum(_2B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_2B
,sum(_2B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_2B
 ,sum(_2B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_2B
 ,2B_Beg_Bal
 ,2B_Orig_Amt
,

_3B_Amt MTD_3B
,sum(_3B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_3B
,sum(_3B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_3B
 ,sum(_3B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_3B
 ,3B_Beg_Bal
 ,3B_Orig_Amt
,

_4B_Amt MTD_4B
,sum(_4B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_4B
,sum(_4B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_4B
 ,sum(_4B_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_4B
 ,4B_Beg_Bal
 ,4B_Orig_Amt
,

JA_Amt MTD_JA
,sum(JA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_JA
,sum(JA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_JA
 ,sum(JA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_JA
 ,JA_Beg_Bal
 ,JA_Orig_Amt
,

PA_Amt MTD_PA
,sum(PA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_PA
,sum(PA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_PA
 ,sum(PA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_PA
 ,PA_Beg_Bal
 ,PA_Orig_Amt
,

HA_Amt MTD_HA
,sum(HA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_HA
,sum(HA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_HA
 ,sum(HA_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_HA
 ,HA_Beg_Bal
 ,HA_Orig_Amt
,

JY_Amt MTD_JY
,sum(JY_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_JY
,sum(JY_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_JY
 ,sum(JY_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_JY
 ,JY_Beg_Bal
 ,JY_Orig_Amt
,

EB_Amt MTD_EB
,sum(EB_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_EB
,sum(EB_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_EB
 ,sum(EB_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_EB
 ,EB_Beg_Bal
 ,EB_Orig_Amt
,

F1_Amt MTD_F1
,sum(F1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_F1
,sum(F1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_F1
 ,sum(F1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_F1
 ,F1_Beg_Bal
 ,F1_Orig_Amt
,

F2_Amt MTD_F2
,sum(F2_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_F2
,sum(F2_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_F2
 ,sum(F2_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_F2
 ,F2_Beg_Bal
 ,F2_Orig_Amt
,

F3_Amt MTD_F3
,sum(F3_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_F3
,sum(F3_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_F3
 ,sum(F3_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_F3
 ,F3_Beg_Bal
 ,F3_Orig_Amt
,

F4_Amt MTD_F4
,sum(F4_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_F4
,sum(F4_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_F4
 ,sum(F4_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_F4
 ,F4_Beg_Bal
 ,F4_Orig_Amt
,

B1_Amt MTD_B1
,sum(B1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_B1
,sum(B1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_B1
 ,sum(B1_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_B1
 ,B1_Beg_Bal
 ,B1_Orig_Amt
,

B2_Amt MTD_B2
,sum(B2_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_B2
,sum(B2_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_B2
 ,sum(B2_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_B2
 ,B2_Beg_Bal
 ,B2_Orig_Amt
,

B3_Amt MTD_B3
,sum(B3_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_B3
,sum(B3_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_B3
 ,sum(B3_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_B3
 ,B3_Beg_Bal
 ,B3_Orig_Amt
,

B4_Amt MTD_B4
,sum(B4_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year,cast(CEIL(mnth_nbr/3.0) as int) order by cal_year,mnth_nbr) QTD_B4
,sum(B4_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year order by cal_year,mnth_nbr) YTD_B4
 ,sum(B4_Amt) over (partition by businessunit,objectaccount,SUBSIDIARY,cal_year) Annual_B4
 ,B4_Beg_Bal
 ,B4_Orig_Amt


from unpivot_base
)
,AB_RESULT
 AS
 (select  REPLACE(MONTH_END::TEXT,'-','') DATE_KEY  , ACCOUNTID ACCOUNT_KEY,BUSINESSUNIT BU_KEY--,CAL_YEAR::text||right('00'||MNTH_NBR::text,2)||'01' D
 --ACCOUNTID ACCOUNT_ID,BUSINESSUNIT BUSINESS_UNIT,OBJECTACCOUNT,SUBSIDIARY sub_account
 ,'AB' DATA_TYPE,--,ledger_type  

MTD_AA,
QTD_AA,
YTD_AA,
Annual_AA
,

MTD_E1,
QTD_E1,
YTD_E1,
Annual_E1
,

MTD_AJ,
QTD_AJ,
YTD_AJ,
Annual_AJ
,

MTD_AU,
QTD_AU,
YTD_AU,
Annual_AU
,

MTD_BA,
QTD_BA,
YTD_BA,
Annual_BA
,

MTD_1F,
QTD_1F,
YTD_1F,
Annual_1F
,

MTD_2F,
QTD_2F,
YTD_2F,
Annual_2F
,

MTD_3F,
QTD_3F,
YTD_3F,
Annual_3F
,

MTD_4F,
QTD_4F,
YTD_4F,
Annual_4F
,

MTD_1B,
QTD_1B,
YTD_1B,
Annual_1B
,

MTD_2B,
QTD_2B,
YTD_2B,
Annual_2B
,

MTD_3B,
QTD_3B,
YTD_3B,
Annual_3B
,

MTD_4B,
QTD_4B,
YTD_4B,
Annual_4B
,

MTD_JA,
QTD_JA,
YTD_JA,
Annual_JA
,

MTD_PA,
QTD_PA,
YTD_PA,
Annual_PA
,

MTD_HA,
QTD_HA,
YTD_HA,
Annual_HA
,

MTD_JY,
QTD_JY,
YTD_JY,
Annual_JY
,

MTD_EB,
QTD_EB,
YTD_EB,
Annual_EB
,

MTD_F1,
QTD_F1,
YTD_F1,
Annual_F1
,

MTD_F2,
QTD_F2,
YTD_F2,
Annual_F2
,

MTD_F3,
QTD_F3,
YTD_F3,
Annual_F3
,

MTD_F4,
QTD_F4,
YTD_F4,
Annual_F4
,

MTD_B1,
QTD_B1,
YTD_B1,
Annual_B1
,

MTD_B2,
QTD_B2,
YTD_B2,
Annual_B2
,

MTD_B3,
QTD_B3,
YTD_B3,
Annual_B3
,

MTD_B4,
QTD_B4,
YTD_B4,
Annual_B4


  from result_non_format r
INNER join dimdate d on CAL_YEAR::text||right('00'||MNTH_NBR::text,2)||'01'=d.datekey
  )
,AL
AS
(
SELECT 
         DM.DATE_ID                   AS PERIOD_DATE_ID,  
       AL.COMPANYKEY                AS COMPANY_KEY, 
       AL.DOCUMENTTYPE              AS DOCUMENT_TYPE, 
       AL.DOCUMENTVOUCHER           AS DOCUMENT_VOUCHER, 
       DGV.DATE_ID                  AS GL_DATE_ID, 
       AL.GLEXTL                    AS EXTENSION_CODE, 
       AL.JOURNALENTRYLINENO        AS JOURNAL_ENTRY_LINE_NO, 
       AL.LEDGERTYPE                AS LEDGER_TYPE, 
       AL.ACCOUNTID                 AS ACCOUNT_ID, 
       AL.BUSINESSUNIT              AS BUSINESS_UNIT, 
      -- AL.SUBSIDIARY                AS SUB_ACCOUNT, 
     --  AL.OBJECTACCOUNT             AS OBJECT_ACCOUNT, 
       AL.COMPANY, 
       AL.ADDRESSNUMBER             AS ADDRESS_NUMBER, 
       AL.DATEFORGLANDVOUCHER       AS DATE_FOR_GL_AND_VOUCHER, 
       AL.GLPOSTEDCODE              AS GL_POSTED_CODE, 
       AL.BATCHNUMBER               AS BATCH_NUMBER, 
       AL.BATCHTYPE                 AS BATCH_TYPE, 
       AL.DATEBATCHJULIAN           AS DATE_BATCH_JULIAN, 
       AL.ACCTNOINPUTMODE           AS ACCT_NO_INPUT_MODE, 
       AL.SUBLEDGER, 
       AL.SUBLEDGERTYPE             AS SUBLEDGER_TYPE, 
      
       AL.PERIODNUMBERGENERALLEDGER AS PERIOD_NUMBER_GENERAL_LEDGER, 
       AL.CENTURY, 
       AL.FISCALYEAR                AS FISCAL_YEAR, 
       DY.DATE_ID                   AS YEAR_DATE_ID, 
       AL.CURRENCYCODEFROM          AS CURRENCY_CODE_FROM, 
       AL.AMOUNTFIELD               AS AMOUNT_FIELD, 
       AL.UNITOFMEASURE             AS UNIT_OF_MEASURE, 
       AL.BILLCODE                  AS BILL_CODE, 
       AL.NAMEALPHAEXPLANATION      AS NAME_ALPHA_EXPLANATION, 
       AL.NAMEREMARKEXPLANATION     AS NAME_REMARK_EXPLANATION, 
       AL.DOCUMENTPAYITEM           AS DOCUMENT_PAY_ITEM, 
       AL.CHECKNUMBER               AS PAYMENT_NUMBER, 
       AL.DATECHECKJ                AS DATE_CHECK_JULIAN, 
       AL.DATECHECKCLEARED          AS DATE_CHECK_CLEARED, 
       AL.SUPPLIERINVOICENUMBER     AS SUPPLIER_INVOICE_NUMBER, 
       AL.DATEINVOICEN              AS DATE_INVOICE_JULIAN, 
       AL.PURCHASEORDER             AS PURCHASE_ORDER, 
       AL.ORDERTYPE                 AS ORDER_TYPE, 
       AL.LINENUMBER                AS LINE_NUMBER, 
       AL.DATESERVICECURRENCY       AS DATE_SERVICE_CURRENCY, 
       AL.TRANSACTIONORIGINATOR     AS TRANSACTION_ORIGINATOR 
    FROM  datalake_dev.governed.JDE_ACCOUNT_LEDGER     AL

    JOIN datalake_dev.semantic.DIM_DATE    DGV
      ON AL.DATEFORGLANDVOUCHER = DGV.DAY   
    JOIN  datalake_dev.semantic.DIM_DATE   DY
      ON AL.CENTURY*100+AL.FISCALYEAR = DY.YEAR_NUM
        AND DY.YEAR_FLAG = 'TRUE'
    JOIN  datalake_dev.semantic.DIM_DATE   DM
      ON AL.CENTURY*100+AL.FISCALYEAR = DM.YEAR_NUM
        AND AL.PERIODNUMBERGENERALLEDGER = DM.MONTH_NUM
        AND DM.MONTH_FLAG = 'TRUE'
  ) ,AL_RESULT
  AS
  (SELECT  
       PERIOD_DATE_ID  DATE_KEY,
       ACCOUNT_ID  ACCOUNT_KEY, 
	   BUSINESS_UNIT  BU_KEY, 
       'AL' DATA_TYPE,
       LEDGER_TYPE,       
       SUBLEDGER, 
	   SUBLEDGER_TYPE, 
	   DOCUMENT_TYPE, 
	   DOCUMENT_VOUCHER, 
	   GL_DATE_ID,
	   DATE_FOR_GL_AND_VOUCHER, 
	   GL_POSTED_CODE, 
	   BATCH_NUMBER, 
	   BATCH_TYPE, 
	   DATE_BATCH_JULIAN, 	     
	   AMOUNT_FIELD,  
	   BILL_CODE, 
	   NAME_ALPHA_EXPLANATION, 
	   NAME_REMARK_EXPLANATION, 	  
	   DATE_CHECK_JULIAN, 
	   DATE_CHECK_CLEARED, 
	   SUPPLIER_INVOICE_NUMBER, 
	   DATE_INVOICE_JULIAN, 
	   PURCHASE_ORDER, 
	   ORDER_TYPE, 
	   LINE_NUMBER, 
	   DATE_SERVICE_CURRENCY, 
	   TRANSACTION_ORIGINATOR  
 FROM  AL
) 
select   DATE_KEY  ,  ACCOUNT_KEY, BU_KEY 
 ,  DATA_TYPE,--,ledger_type  
  NULL  LEDGER_TYPE, NULL  SUBLEDGER,NULL  SUBLEDGER_TYPE,NULL  DOCUMENT_TYPE,NULL DOCUMENT_VOUCHER, 

MTD_AA,
QTD_AA,
YTD_AA,
Annual_AA,

MTD_E1,
QTD_E1,
YTD_E1,
Annual_E1,

MTD_AJ,
QTD_AJ,
YTD_AJ,
Annual_AJ,

MTD_AU,
QTD_AU,
YTD_AU,
Annual_AU,

MTD_BA,
QTD_BA,
YTD_BA,
Annual_BA,

MTD_1F,
QTD_1F,
YTD_1F,
Annual_1F,

MTD_2F,
QTD_2F,
YTD_2F,
Annual_2F,

MTD_3F,
QTD_3F,
YTD_3F,
Annual_3F,

MTD_4F,
QTD_4F,
YTD_4F,
Annual_4F,

MTD_1B,
QTD_1B,
YTD_1B,
Annual_1B,

MTD_2B,
QTD_2B,
YTD_2B,
Annual_2B,

MTD_3B,
QTD_3B,
YTD_3B,
Annual_3B,

MTD_4B,
QTD_4B,
YTD_4B,
Annual_4B,

MTD_JA,
QTD_JA,
YTD_JA,
Annual_JA,

MTD_PA,
QTD_PA,
YTD_PA,
Annual_PA,

MTD_HA,
QTD_HA,
YTD_HA,
Annual_HA,

MTD_JY,
QTD_JY,
YTD_JY,
Annual_JY,

MTD_EB,
QTD_EB,
YTD_EB,
Annual_EB,

MTD_F1,
QTD_F1,
YTD_F1,
Annual_F1,

MTD_F2,
QTD_F2,
YTD_F2,
Annual_F2,

MTD_F3,
QTD_F3,
YTD_F3,
Annual_F3,

MTD_F4,
QTD_F4,
YTD_F4,
Annual_F4,

MTD_B1,
QTD_B1,
YTD_B1,
Annual_B1,

MTD_B2,
QTD_B2,
YTD_B2,
Annual_B2,

MTD_B3,
QTD_B3,
YTD_B3,
Annual_B3,

MTD_B4,
QTD_B4,
YTD_B4,
Annual_B4,

 NULL GL_DATE_ID,NULL  DATE_FOR_GL_AND_VOUCHER,NULL  GL_POSTED_CODE,NULL  BATCH_NUMBER, NULL  BATCH_TYPE,NULL  DATE_BATCH_JULIAN,NULL  AMOUNT_FIELD,NULL  BILL_CODE
from AB_RESULT
union all 
select CAST(DATE_KEY AS INT) DATE_KEY,ACCOUNT_KEY,BU_KEY
,DATA_TYPE,
 LEDGER_TYPE,  SUBLEDGER, SUBLEDGER_TYPE, DOCUMENT_TYPE,DOCUMENT_VOUCHER,

CAST(0 AS DECIMAL(18,2)) as MTD_AA,
CAST(0 AS DECIMAL(18,2)) as QTD_AA,
CAST(0 AS DECIMAL(18,2)) as YTD_AA,
CAST(0 AS DECIMAL(18,2)) as Annual_AA,

CAST(0 AS DECIMAL(18,2)) as MTD_E1,
CAST(0 AS DECIMAL(18,2)) as QTD_E1,
CAST(0 AS DECIMAL(18,2)) as YTD_E1,
CAST(0 AS DECIMAL(18,2)) as Annual_E1,

CAST(0 AS DECIMAL(18,2)) as MTD_AJ,
CAST(0 AS DECIMAL(18,2)) as QTD_AJ,
CAST(0 AS DECIMAL(18,2)) as YTD_AJ,
CAST(0 AS DECIMAL(18,2)) as Annual_AJ,

CAST(0 AS DECIMAL(18,2)) as MTD_AU,
CAST(0 AS DECIMAL(18,2)) as QTD_AU,
CAST(0 AS DECIMAL(18,2)) as YTD_AU,
CAST(0 AS DECIMAL(18,2)) as Annual_AU,

CAST(0 AS DECIMAL(18,2)) as MTD_BA,
CAST(0 AS DECIMAL(18,2)) as QTD_BA,
CAST(0 AS DECIMAL(18,2)) as YTD_BA,
CAST(0 AS DECIMAL(18,2)) as Annual_BA,

CAST(0 AS DECIMAL(18,2)) as MTD_1F,
CAST(0 AS DECIMAL(18,2)) as QTD_1F,
CAST(0 AS DECIMAL(18,2)) as YTD_1F,
CAST(0 AS DECIMAL(18,2)) as Annual_1F,

CAST(0 AS DECIMAL(18,2)) as MTD_2F,
CAST(0 AS DECIMAL(18,2)) as QTD_2F,
CAST(0 AS DECIMAL(18,2)) as YTD_2F,
CAST(0 AS DECIMAL(18,2)) as Annual_2F,

CAST(0 AS DECIMAL(18,2)) as MTD_3F,
CAST(0 AS DECIMAL(18,2)) as QTD_3F,
CAST(0 AS DECIMAL(18,2)) as YTD_3F,
CAST(0 AS DECIMAL(18,2)) as Annual_3F,

CAST(0 AS DECIMAL(18,2)) as MTD_4F,
CAST(0 AS DECIMAL(18,2)) as QTD_4F,
CAST(0 AS DECIMAL(18,2)) as YTD_4F,
CAST(0 AS DECIMAL(18,2)) as Annual_4F,

CAST(0 AS DECIMAL(18,2)) as MTD_1B,
CAST(0 AS DECIMAL(18,2)) as QTD_1B,
CAST(0 AS DECIMAL(18,2)) as YTD_1B,
CAST(0 AS DECIMAL(18,2)) as Annual_1B,

CAST(0 AS DECIMAL(18,2)) as MTD_2B,
CAST(0 AS DECIMAL(18,2)) as QTD_2B,
CAST(0 AS DECIMAL(18,2)) as YTD_2B,
CAST(0 AS DECIMAL(18,2)) as Annual_2B,

CAST(0 AS DECIMAL(18,2)) as MTD_3B,
CAST(0 AS DECIMAL(18,2)) as QTD_3B,
CAST(0 AS DECIMAL(18,2)) as YTD_3B,
CAST(0 AS DECIMAL(18,2)) as Annual_3B,

CAST(0 AS DECIMAL(18,2)) as MTD_4B,
CAST(0 AS DECIMAL(18,2)) as QTD_4B,
CAST(0 AS DECIMAL(18,2)) as YTD_4B,
CAST(0 AS DECIMAL(18,2)) as Annual_4B,

CAST(0 AS DECIMAL(18,2)) as MTD_JA,
CAST(0 AS DECIMAL(18,2)) as QTD_JA,
CAST(0 AS DECIMAL(18,2)) as YTD_JA,
CAST(0 AS DECIMAL(18,2)) as Annual_JA,

CAST(0 AS DECIMAL(18,2)) as MTD_PA,
CAST(0 AS DECIMAL(18,2)) as QTD_PA,
CAST(0 AS DECIMAL(18,2)) as YTD_PA,
CAST(0 AS DECIMAL(18,2)) as Annual_PA,

CAST(0 AS DECIMAL(18,2)) as MTD_HA,
CAST(0 AS DECIMAL(18,2)) as QTD_HA,
CAST(0 AS DECIMAL(18,2)) as YTD_HA,
CAST(0 AS DECIMAL(18,2)) as Annual_HA,

CAST(0 AS DECIMAL(18,2)) as MTD_JY,
CAST(0 AS DECIMAL(18,2)) as QTD_JY,
CAST(0 AS DECIMAL(18,2)) as YTD_JY,
CAST(0 AS DECIMAL(18,2)) as Annual_JY,

CAST(0 AS DECIMAL(18,2)) as MTD_EB,
CAST(0 AS DECIMAL(18,2)) as QTD_EB,
CAST(0 AS DECIMAL(18,2)) as YTD_EB,
CAST(0 AS DECIMAL(18,2)) as Annual_EB,

CAST(0 AS DECIMAL(18,2)) as MTD_F1,
CAST(0 AS DECIMAL(18,2)) as QTD_F1,
CAST(0 AS DECIMAL(18,2)) as YTD_F1,
CAST(0 AS DECIMAL(18,2)) as Annual_F1,

CAST(0 AS DECIMAL(18,2)) as MTD_F2,
CAST(0 AS DECIMAL(18,2)) as QTD_F2,
CAST(0 AS DECIMAL(18,2)) as YTD_F2,
CAST(0 AS DECIMAL(18,2)) as Annual_F2,

CAST(0 AS DECIMAL(18,2)) as MTD_F3,
CAST(0 AS DECIMAL(18,2)) as QTD_F3,
CAST(0 AS DECIMAL(18,2)) as YTD_F3,
CAST(0 AS DECIMAL(18,2)) as Annual_F3,

CAST(0 AS DECIMAL(18,2)) as MTD_F4,
CAST(0 AS DECIMAL(18,2)) as QTD_F4,
CAST(0 AS DECIMAL(18,2)) as YTD_F4,
CAST(0 AS DECIMAL(18,2)) as Annual_F4,

CAST(0 AS DECIMAL(18,2)) as MTD_B1,
CAST(0 AS DECIMAL(18,2)) as QTD_B1,
CAST(0 AS DECIMAL(18,2)) as YTD_B1,
CAST(0 AS DECIMAL(18,2)) as Annual_B1,

CAST(0 AS DECIMAL(18,2)) as MTD_B2,
CAST(0 AS DECIMAL(18,2)) as QTD_B2,
CAST(0 AS DECIMAL(18,2)) as YTD_B2,
CAST(0 AS DECIMAL(18,2)) as Annual_B2,

CAST(0 AS DECIMAL(18,2)) as MTD_B3,
CAST(0 AS DECIMAL(18,2)) as QTD_B3,
CAST(0 AS DECIMAL(18,2)) as YTD_B3,
CAST(0 AS DECIMAL(18,2)) as Annual_B3,

CAST(0 AS DECIMAL(18,2)) as MTD_B4,
CAST(0 AS DECIMAL(18,2)) as QTD_B4,
CAST(0 AS DECIMAL(18,2)) as YTD_B4,
CAST(0 AS DECIMAL(18,2)) as Annual_B4,

GL_DATE_ID,DATE_FOR_GL_AND_VOUCHER,GL_POSTED_CODE, BATCH_NUMBER,  BATCH_TYPE, DATE_BATCH_JULIAN, AMOUNT_FIELD, BILL_CODE
 from AL_RESULT
      );
    