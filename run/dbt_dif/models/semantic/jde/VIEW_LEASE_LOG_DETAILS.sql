
  create or replace  view datalake_dev.semantic.VIEW_LEASE_LOG_DETAILS  as (
    

SELECT distinct
	LDM.LogLevel, 
	LDM.lognumber AS LogNbr, 
	LDM.documentorderinvoice AS LeaseID,
	LDM.LeaseVersion, 
	ifnull(cast(MOS.generictextblobbuffer as varchar(16777216)),LDM.nameremarkexplanation) AS Narrative,
	BusinessUnit, 
	unit Unit_Nbr, 
	PropertyLogClass, 
	LDM.Description LogDescription, 
	LDM.nameremarkexplanation RemarkExpl, 
	datebeginningeffective EffBeginDt, 
	expireddate ExpiredDt, 
    tickledate AS CriticalDt, 
	LDM.amenityidentification AmenityIdentification, 
	A.AmenityDesc AS AmenityDesc,
	propmanunits001/100 AS Units, 
	UnitofMeasure, 
	addressnumber AS TenantID
FROM   datalake_dev.governed.JDE_LOG_DETAIL_MASTER      LDM
LEFT JOIN 
(SELECT userdefinedcode AS AmenityIdentification, description AS AmenityDesc
    FROM   datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES    WHERE productcode = '15'AND USERDEFINEDCODES = 'AM') A
    ON A.AmenityIdentification = LDM.amenityidentification 
LEFT JOIN   datalake_dev.governed.JDE_MEDIA_OBJECTS_STORAGE   MOS 
    ON CAST (LDM.lognumber AS VARCHAR) = MOS.GenericTextKey and MOS.objectname = 'GT1521'
INNER JOIN (SELECT documentorderinvoice, MAX(LeaseVersion) AS LeaseVersion 
FROM  datalake_dev.governed.JDE_LOG_DETAIL_MASTER      GROUP BY documentorderinvoice) M2 
    ON M2.documentorderinvoice = LDM.documentorderinvoice AND M2.LeaseVersion = LDM.LeaseVersion
  );
