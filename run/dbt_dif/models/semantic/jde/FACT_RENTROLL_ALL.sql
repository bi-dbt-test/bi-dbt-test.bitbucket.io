

      create or replace  table datalake_dev.semantic.FACT_RENTROLL_ALL  as
      (with DIMDATE                                      AS
     (
            SELECT
                   DATE_ID DATEKEY
                 , MONTH_START
                 , MONTH_END
                 , MONTH_NUM MONTHOFYEAR
            FROM
                  datalake_dev.semantic.DIM_DATE  
            WHERE
                   MONTH_FLAG='TRUE'
                   AND MONTH_START BETWEEN '01/01/2015' AND '12/01/2028'
     )
     ,RENTABLE_UNIT_LIST
     AS
     (SELECT DISTINCT BUSINESS_UNIT,UNIT_NBR FROM  datalake_dev.semantic.DIM_UNIT    WHERE AREA_TYPE_CD        = 'REN'
     )
     , DIM_HHC_UNIT as
     (
             select
                   BUSINESS_UNIT   BUSINESSUNITNBR
                 , UNIT_NBR        UNITNBR
                 , unit_key
                 , UNIT_BEGIN_EFF_DATE   UNITEFFBEGINDT
                 , UNIT_END_EFF_DATE     UNITEFFENDDT
                 , SQR_FOOTAGE         UNITSQFT,AREA_TYPE_CD
                 , UNIT_TYPE_CD
                 ,'RENTTABLE UNIT' FLAG
                 , UNIT_STATUS
            FROM
                   (
                                SELECT
                                             DIM_UNIT.BUSINESS_UNIT
                                           , UPPER(DIM_UNIT.UNIT_NBR) UNIT_NBR
                                           , DIM_UNIT.UNIT_KEY
                                           , DIM_UNIT.UNIT_BEGIN_EFF_DATE
                                           , ROW_NUMBER() OVER (PARTITION BY DIM_UNIT.BUSINESS_UNIT ,DIM_UNIT.UNIT_NBR ,IFNULL(DIM_UNIT.UNIT_BEGIN_EFF_DATE,'1990-01-01') ORDER BY
                                                                             
                                                                                          CASE
                                                                                                       WHEN AREA_TYPE_CD = 'REN'
                                                                                                                    THEN 1
                                                                                                       WHEN AREA_TYPE_CD = 'USE'
                                                                                                                    THEN 2
                                                                                                                    ELSE 3
                                                                                       END  DESC                                                     
                                                                ) rn
                                           , SQR_FOOTAGE
                                           , UNIT_END_EFF_DATE
                                           , UNIT_TYPE_CD,AREA_TYPE_CD
                                           , UNIT_STATUS    --CASE WHEN IFNULL(UNIT_STATUS,'D')<>'O'        
                                FROM
                                             datalake_dev.semantic.DIM_UNIT   DIM_UNIT
                                INNER JOIN RENTABLE_UNIT_LIST L ON DIM_UNIT.BUSINESS_UNIT =L.BUSINESS_UNIT AND DIM_UNIT.UNIT_NBR=L.UNIT_NBR
                                WHERE
                                                                           
                                              AREA_TYPE_CD        = 'REN'
 
                   )
                   as b
            WHERE
                   rn=1
       UNION 
                   select
                   BUSINESS_UNIT  
                 , UNIT_NBR       
                 , unit_key
                 , UNIT_BEGIN_EFF_DATE  
                 , UNIT_END_EFF_DATE    
                 , SQR_FOOTAGE         UNITSQFT
                 ,AREA_TYPE_CD
                 , UNIT_TYPE_CD,'NO RENTABLE UNIT' FLAG
                 , UNIT_STATUS
            FROM
                   (
                                SELECT
                                             DIM_UNIT.BUSINESS_UNIT
                                           , UPPER(DIM_UNIT.UNIT_NBR) UNIT_NBR
                                           , UNIT_KEY
                                           , UNIT_BEGIN_EFF_DATE
                                           , ROW_NUMBER() OVER (PARTITION BY DIM_UNIT.BUSINESS_UNIT ,DIM_UNIT.UNIT_NBR ,IFNULL(UNIT_BEGIN_EFF_DATE,'1990-01-01') ORDER BY
                                                                             
                                                                                          CASE
                                                                                                       WHEN AREA_TYPE_CD = 'REN'
                                                                                                                    THEN 1
                                                                                                       WHEN AREA_TYPE_CD = 'USE'
                                                                                                                    THEN 2
                                                                                                                    ELSE 3
                                                                                       END  DESC                                                     
                                                                ) rn
                                           , SQR_FOOTAGE
                                           , UNIT_END_EFF_DATE
                                           , UNIT_TYPE_CD,AREA_TYPE_CD,UNIT_STATUS
                                FROM
                                              datalake_dev.semantic.DIM_UNIT  DIM_UNIT
                                LEFT JOIN RENTABLE_UNIT_LIST L ON DIM_UNIT.BUSINESS_UNIT =L.BUSINESS_UNIT AND DIM_UNIT.UNIT_NBR=L.UNIT_NBR
                                WHERE
                                             L.BUSINESS_UNIT IS NULL
 
                   )
                   as b
            WHERE
                   rn=1
     )  --  select distinct BUSINESSUNITNBR,UNITNBR  from DIM_HHC_UNIT ;5321order by  FLAG, BUSINESS_UNIT,UNIT_NBR,UNIT_BEGIN_EFF_DATE; where businessunitnbr ='12028001' and unitnbr ='DH';
   ---select distinct BUSINESS_UNIT,UNIT_NBR  from semantic.dim_unit
   , PRE_BASE AS
     (
                  SELECT
                               rbm.documentOrderInvoice leaseid
                             , rbm.LeaseVersion         LsV
                             , rbm.BusinessUnit
                             , rbm.Unit
                             , rbm.BillCode
                             , rbm.DATEBEGINNINGEFFECTIVE ScheduleBeginDate
                             , CASE
                                             WHEN SusPendCode = 'Y' and rbm.DateSuspended  <rbm.DATEENDINGEFFECTIVE 
                                                         THEN rbm.DateSuspended       
                                                         ELSE rbm.DATEENDINGEFFECTIVE 
                               END ScheduleEndDate
                             , 
                               SusPendCode
                             , CASE
                                            WHEN SusPendCode = 'Y'
                                                         THEN rbm.DateSuspended 
                                                         ELSE NULL
                               END                      as SuspendDate
                             , rbm.AmountGross             Schedule_Amount
                             , rbm.AnnualRatePerSquare     Schedule_Amount_per_Square
                             , rbm.UnitRentableAreaSqFt    schedule_sf
                             , RIGHT(month_col,2)::int *
                               CASE
                                            WHEN Billable = 'X'
                                                         THEN 1
                                                         ELSE NULL
                               END billable_months
                             , month_col
                             , BC.BILL_CODE_ALT_CATEGORY CATEGORY
                             , BILLINGFREQUENCYCODE
                             , rbm.AnnualRatePerSquare*rbm.UnitRentableAreaSqFt   Annual_schedule_amount 
                  FROM
                              datalake_dev.governed.JDE_RECURRING_BILLING_MASTER      rbm
                               -- Unpivots these columns into a single column called month
                               UNPIVOT(Billable for month_col in (BILLINGPERIOD01
                                                                , BILLINGPERIOD02
                                                                , BILLINGPERIOD03
                                                                , BILLINGPERIOD04
                                                                , BILLINGPERIOD05
                                                                , BILLINGPERIOD06
                                                                , BILLINGPERIOD07
                                                                , BILLINGPERIOD08
                                                                , BILLINGPERIOD09
                                                                , BILLINGPERIOD10
                                                                , BILLINGPERIOD11
                                                                , BILLINGPERIOD12))
                               INNER JOIN
                                          datalake_dev.semantic.DIM_BILL_CODE   BC
                                            ON
                                                         rbm.BillCode=BC.BILL_CODE 
                  WHERE
                               billable_months IS not NULL
                              -- AND BILL_CODE NOT IN ('COV1'
                              --                     , 'COV2'
                              --                     , 'COV3')
                               AND
                               (
                                            COALESCE(rbm.DateSuspended,'2100-01-01') >=rbm.DATEBEGINNINGEFFECTIVE 
                                            OR rbm.SuspendCode                       != 'Y'
                               )
     
     )--select * from BASE where leaseid='184571';
     ,BASE
     as
	 (
	 select lmd.DOCUMENTORDERINVOICE leaseid,lmd.LEASEVERSION lsv,lmd.BUSINESSUNIT,lmd.UNIT ,
base.BillCode,base.ScheduleBeginDate,base.ScheduleEndDate,base.SuspendCode,base.SuspendDate
,Schedule_Amount,Schedule_Amount_per_Square,schedule_sf,billable_months,month_col,CATEGORY,BILLINGFREQUENCYCODE,Annual_schedule_amount
from      datalake_dev.governed.JDE_LEASE_MASTER_DETAIL   lmd
left join pre_BASE base on base.Unit=lmd.UNIT 
              and base.leaseid=lmd.DOCUMENTORDERINVOICE
              and base.BusinessUnit=lmd.BUSINESSUNIT
             and base.LsV=lmd.LEASEVERSION
--where lmd.DOCUMENTORDERINVOICE='298625'
	 ) 
   , SUM_CATEGORY AS
     (
              SELECT
                       leaseid
                     , LsV
                     , BusinessUnit
                     , Unit
                     , CATEGORY
                     , ScheduleBeginDate
                     , ScheduleEndDate
                     , billCode
                     , SuspendCode
                     , SuspendDate
                     , SCHEDULE_AMOUNT
                     , CASE
                                WHEN Schedule_Amount_per_Square=0
                                         AND schedule_sf      <>0
                                         then CAST(SCHEDULE_AMOUNT*
                                         case
                                                  when BILLINGFREQUENCYCODE='M'
                                                           THEN 12
                                                  WHEN BILLINGFREQUENCYCODE IN ('1')
                                                           THEN 4
                                                  WHEN BILLINGFREQUENCYCODE IN ('S'
                                                                              ,'T')
                                                           THEN 2
                                                  WHEN BILLINGFREQUENCYCODE IN ('A'
                                                                              ,'B'
                                                                              ,'K'
                                                                              ,'J')
                                                           THEN 1
                                         END /schedule_sf AS DECIMAL(10,2))
                                         else Schedule_Amount_per_Square
                       end Schedule_Amount_per_Square
                     , schedule_sf
                     , Annual_schedule_amount
                     , Array_agg(DISTINCT billable_Months) WITHIN GROUP (order by billable_Months) schedule_Months
                     , BILLINGFREQUENCYCODE
              FROM
                       BASE
              GROUP BY
                       leaseid
                     , LsV
                     , BusinessUnit
                     , Unit
                     , CATEGORY
                     , ScheduleBeginDate
                     , ScheduleEndDate
                     , billCode
                     , SuspendCode
                     , SuspendDate
                     , SCHEDULE_AMOUNT
                     , Schedule_Amount_per_Square
                     , BILLINGFREQUENCYCODE
                     , Annual_schedule_amount
                     , schedule_sf
                     , BILLINGFREQUENCYCODE
     ) 
   , CATEGORY_add_Overage as
     (
            select *
                 , NULL PercentageDueOnSales
            from
                   SUM_CATEGORY
            union all
            SELECT
                   psm.documentOrderInvoice lease_id
                 , psm.LeaseVersion         LsV
                 , psm.BusinessUnit
                 , psm.Unit
                 , 'Overage_Rent'                CATEGORY
                 , psm.DATEBEGINNINGEFFECTIVE as ScheduleBeginDate
                 , CASE
                          WHEN psm.SusPendCode = 'Y'
                                 THEN psm.DateSuspended
                                 ELSE psm.DATEENDINGEFFECTIVE
                   END ScheduleEndDate
                 , 
                   'BKPT' BillCode
                 , psm.SuspendCode
                 , CASE
                          WHEN psm.SusPendCode = 'Y'
                                 THEN psm.DateSuspended
                                 ELSE NULL
                   END                      as SuspendDate
                 , psm.ProductDollarBreakPt    Schedule_Amount
                 , 0                           schedule_amount_per_square , 0 schedule_sf ---
                 , 0                           Annual_schedule_amount
                 , null                        schedule_months
                 , null                        BILLINGFREQUENCYCODE
                 , psm.PercentageDueOnSales
            FROM
                     datalake_dev.governed.JDE_PRODUCT_SCALES_MASTER    psm 
            WHERE
                   (
                          COALESCE(psm.DateSuspended,'2100-01-01') >psm.DATEBEGINNINGEFFECTIVE --  removes a fully removed schedule
                          OR psm.SuspendCode                      != 'Y'
                   )
     )
   , SECURITY_DEPOSIT as
     (
                select
                           DATE.DATEKEY
                         , sum(AMOUNTGROSS)       AMOUNTGROSS
                         , f.DOCUMENTORDERINVOICE lease_id
                         , LEASEVERSION
                         , unit
                from
                            datalake_dev.governed.JDE_SECURITY_DEPOSIT_MASTER    f
                           INNER JOIN
                                      DIMDATE DATE
                                      on
                                                 DATE.MONTH_END BETWEEN f.DATEBEGINNINGEFFECTIVE AND
                                                 CASE
                                                            WHEN SusPendCode               = 'Y'
                                                                       and f.DATESUSPENDED < f.DATEENDINGEFFECTIVE
                                                                       THEN f.DATESUSPENDED
                                                                       ELSE f.DATEENDINGEFFECTIVE
                                                 end
                group by
                           DATE.DATEKEY
                         , unit
                         , f.DOCUMENTORDERINVOICE
                         , LEASEVERSION
     ) 
   , TI_ls_br AS
     (
            select
                   DOCUMENTORDERINVOICE LEASE_ID
                 , LEASEVERSION
                 , BUSINESSUNIT
                 , UNIT
                 ,"'CD10'" TI
                 ,"'CDLS'" LLD_Broker_LC
                 ,"'CDBR'" TNT_Broker_LC
            from
                   (
                          select
                                 DOCUMENTORDERINVOICE
                               , LEASEVERSION
                               , BUSINESSUNIT
                               , UNIT
                               , AMENITYIDENTIFICATION
                               , PROPMANUNITS001 PROPMANUNITS001
                          from
                                     datalake_dev.governed.JDE_LOG_DETAIL_MASTER   
                          where
                                 AMENITYIDENTIFICATION IN ('CD10'
                                                         ,'CDLS'
                                                         ,'CDBR')
                   )
                   f pivot(sum(PROPMANUNITS001 ) for AMENITYIDENTIFICATION in ('CD10'
                                                                             ,'CDLS'
                                                                             ,'CDBR') )
     )
   , d_lease as
     (
                  select
                               base.*
                             , UNITSUSABLEAREASQFT                                                          ratesqft
                             , UNITRENTABLEAREASQFT                                                         unitsqft
                             , min(ifnull(LEASE_BEGIN_DT,'1990-01-01'))over(partition by LEASE_ID,UNIT_NBR) term_begin_date
                             , max(ifnull(LEASE_end_DT,'2050-01-01'))over(partition by LEASE_ID,UNIT_NBR)   term_end_date
                  from
                               datalake_dev.semantic.DIM_LEASE  base
                               inner join
                                               datalake_dev.governed.JDE_LEASE_MASTER_DETAIL    dlease
                                            on
                                                         dlease.DOCUMENTORDERINVOICE=base.LEASE_ID --select * from  "DATALAKE_DEV"."SEMANTIC"."DIM_LEASE" WHERE  BUSINESS_UNIT='13071' AND UNIT_NBR='E500'
                                                         and dlease.LEASEVERSION    =base.LEASE_VERSION_NBR
                                                         and dlease. BUSINESSUNIT   =base.BUSINESS_UNIT
                                                         and dlease.Unit            = base.UNIT_NBR
     )
   , COMBINE_LEASE AS
     (
               SELECT
                         LEASE.UNIT_NBR      UNIT
                       , LEASE.BUSINESS_UNIT businessunit
                       , LEASE.LEASE_ID
                       , IFNULL(LEASE.LEASE_KEY,0)   LEASE_KEY
                       , IFNULL(tenant.TENANT_KEY,0) TENANT_KEY
                       , BASE.BillCode
                       , BASE.ScheduleBeginDate
                       , BASE.ScheduleEndDate
                       , BASE.SuspendDate
                       , BASE.Schedule_Amount
                       , Annual_schedule_amount
                       , base.Schedule_Amount_per_Square
                       , base.schedule_sf
                       , base.CATEGORY
                       , LEASE.LEASE_VERSION_NBR
                       , OPENING_DATE
                       , RENT_COMMENCEMENT_DATE RentStartDate
                       , LEASE.VERSION_BEGIN_DT
                       , VERSION_END_DT
                       , LEASE_BEGIN_DT
                       , LEASE_END_DT
                       , LEASE.ratesqft
                       , LEASE.unitsqft
                       , COMMIT_DATE CommitBeginDate
                       , MOVE_OUT_DT
                       , PLAN_OUT_DT
                       , move_in_dt    moveindate
                       , LEASE_TYPE_CD LEASE_TYPE
                       , BILLINGFREQUENCYCODE
                       , PercentageDueOnSales
                       , case
                                   when datediff(day,term_begin_date,term_end_date)>=365
                                             then '>1_year'
                                             else '<1_year'
                         end term_flag
               FROM
                         d_lease LEASE
                         left JOIN
                                   CATEGORY_add_Overage BASE
                                   on
                                             base.leaseid          =LEASE.LEASE_ID 
                                             and base.lsv          =lease.LEASE_VERSION_NBR
                                             and base. businessunit=lease.BUSINESS_UNIT
                                             and base.Unit         = lease.UNIT_NBR
                         left join
                                     datalake_dev.semantic.DIM_TENANT   tenant
                                   on
                                             lease.TENANT_ID=tenant.TENANT_ID
     )
   , RESULT_occupied_future AS
     (
                select
                           DIMDATE.DATEKEY DATEKEY
                         , unit.unit_key
                         , LEASE_KEY
                         , TENANT_KEY
                         , COMBINE_LEASE.BUSINESSUNIT
                         , COMBINE_LEASE.UNIT
                         , SECURITY_DEPOSIT.AMOUNTGROSS SECURITY_DEPOSIT
                         , TI_ls_br.TI
                         , TI_ls_br.LLD_Broker_LC
                         , TI_ls_br.TNT_Broker_LC
                         , COMBINE_LEASE.BILLCODE
                         , COMBINE_LEASE.SCHEDULEBEGINDATE
                         , COMBINE_LEASE.SCHEDULEENDDATE
                         , COMBINE_LEASE.SUSPENDDATE
                         , COMBINE_LEASE.SCHEDULE_AMOUNT
                         , Annual_schedule_amount
                         , COMBINE_LEASE.SCHEDULE_AMOUNT_PER_SQUARE
                         , COMBINE_LEASE.SCHEDULE_SF
                         , case
                                      when DIMDATE.MONTH_END    >moveindate
                                                 and LEASE_TYPE<>'SR'
                                                 and term_flag  = '>1_year'
                                                 then 'O'
                                                 ELSE 'V'
                           end occupancystatus
                         , case
                                      when dimdate.month_end >CommitBeginDate
                                                 then 'L'
                                      WHEN DIMDATE.MONTH_END< CommitBeginDate
                                                 THEN 'F'
                                                 ELSE 'U'
                           end LeaseStage
                         , CASE
                                      when LEASE_TYPE='SR'
                                                 then 'License Occupants ( Lease Type = SR )'
                                      WHEN CommitBeginDate             <=DIMDATE.MONTH_END
                                                 AND DIMDATE.MONTH_END <=RentStartDate
                                                 AND DIMDATE.MONTH_END <=IFNULL(moveindate,DIMDATE.MONTH_END)
                                                 THEN 'Vacant - Leased, doesn''t pay rent'                   -- 'Future Lease/Vacant doesn''t pay rent'
                                      WHEN CommitBeginDate            <=DIMDATE.MONTH_END
                                                 AND DIMDATE.MONTH_END >RentStartDate
                                                 AND DIMDATE.MONTH_END<=IFNULL(moveindate,DIMDATE.MONTH_END) 
                                                 THEN 'Vacant - Leased, pay rent'                            -- 'Future Lease/Vacant Pays Rent'
                                      when term_flag= '<1_year'
                                                 THEN 'Licensed Occupants Less than 365 days'
                                      WHEN occupancystatus='V'
                                                 and LeaseStage in ('U')
                                                 THEN 'Vacant Units' --'Units without a Signed Document'
                                      WHEN occupancystatus                             ='O'
                                                 AND LENGTH(COMBINE_LEASE.BUSINESSUNIT)=5
                                                 THEN 'Permanent Tenants'--'Perm Occupied'
                                      WHEN occupancystatus                             ='O'
                                                 AND LENGTH(COMBINE_LEASE.BUSINESSUNIT)>5
                                                 THEN 'Licensed Occupants' --'Speciality Occupied'
                                                 else 'Renewal'
                           END LIST_CATEGORY
                         , CASE
                                      when LEASE_TYPE='SR'
                                                 then 'V_SR'
                                      WHEN CommitBeginDate             <=DIMDATE.MONTH_END
                                                 AND DIMDATE.MONTH_END <=RentStartDate
                                                 AND DIMDATE.MONTH_END <=IFNULL(moveindate,DIMDATE.MONTH_END)
                                                 THEN 'V_FDP'
                                      WHEN CommitBeginDate            <=DIMDATE.MONTH_END
                                                 AND DIMDATE.MONTH_END >RentStartDate
                                                 AND DIMDATE.MONTH_END<=IFNULL(moveindate,DIMDATE.MONTH_END) 
                                                 THEN 'V_FP'
                                      when term_flag= '<1_year'
                                                 THEN 'V_LT365'
                                      WHEN occupancystatus='V'
                                                 and LeaseStage in ('U')
                                                 or term_flag= '<1_year'
                                                 THEN 'V_U'
                                      WHEN occupancystatus                             ='O'
                                                 AND LENGTH(COMBINE_LEASE.BUSINESSUNIT)=5
                                                 THEN 'O_P'
                                      WHEN occupancystatus                             ='O'
                                                 AND LENGTH(COMBINE_LEASE.BUSINESSUNIT)>5
                                                 THEN 'O_S'
                                                 else 'RE'
                           END LIST_CATEGORY_code
                         , CASE
                                      WHEN LIST_CATEGORY_code IN ('O_P'
                                                                ,'O_S')
                                                 THEN 'OCCUPIED UNITS'--'Occupied'
                                      WHEN LIST_CATEGORY_code IN ('V_SR'
                                                                ,'V_FDP'
                                                                ,'V_FP'
                                                                ,'V_U'
                                                                ,'V_LT365')
                                                 THEN 'VACANT UNITS'  --'Vacant'
                                                 ELSE 'RENEWED UNITS'---'Renewal'
                           END CATEGORY
                         , COMBINE_LEASE.ratesqft
                         , unit.unitsqft     unitsqft
                         , unit.UNIT_TYPE_CD UNIT_TYPE
                         , BILLINGFREQUENCYCODE
                         , PercentageDueOnSales
                from
                           DIMDATE
                           inner JOIN
                                      COMBINE_LEASE
                                      ON
                                                 (
                                                            DIMDATE.MONTH_END BETWEEN ifnull(SCHEDULEBEGINDATE,DIMDATE.MONTH_END) AND ifnull(SCHEDULEENDDATE,DIMDATE.MONTH_END)
                                                            OR
                                                            (
                                                                       ifnull(SCHEDULEBEGINDATE,DIMDATE.MONTH_START)>=DIMDATE.MONTH_START
                                                            )
                                                 )
                                                 AND
                                                 (
                                                            DIMDATE.MONTH_END >
                                                            case
                                                                       when COMMITBEGINDATE < RentStartDate
                                                                                  then CommitBeginDate
                                                                                  else rentstartdate
                                                            end
                                                            or DIMDATE.MONTH_END<VERSION_BEGIN_DT
                                                 )
                                                 AND DIMDATE.MONTH_END<=
                                                 case
                                                            when VERSION_END_DT < IFNULL(MOVE_OUT_DT,DIMDATE.MONTH_END)
                                                                       then VERSION_END_DT
                                                                       else IFNULL(MOVE_OUT_DT,DIMDATE.MONTH_END)
                                                 end
                           inner join
                                      DIM_HHC_UNIT unit
                                      on
                                                 unit.UNITNBR            =COMBINE_LEASE.Unit
                                                 and unit.BUSINESSUNITNBR=COMBINE_LEASE.businessunit
                                                 and dimdate.MONTH_END between ifnull(unit. uniteffbegindt,'1990-01-01') and ifnull(unit.UNITEFFENDDT,'2099-01-01')
                           LEFT JOIN
                                      SECURITY_DEPOSIT
                                      ON
                                                 SECURITY_DEPOSIT.lease_id        =COMBINE_LEASE.LEASE_ID
                                                 AND SECURITY_DEPOSIT.LEASEVERSION=COMBINE_LEASE.LEASE_VERSION_NBR
                                                 AND SECURITY_DEPOSIT.unit        =COMBINE_LEASE.UNIT
                                                 AND SECURITY_DEPOSIT.DATEKEY     =DIMDATE.DATEKEY
                           LEFT JOIN
                                      TI_ls_br
                                      ON
                                                 TI_ls_br.lease_id        =COMBINE_LEASE.LEASE_ID
                                                 AND TI_ls_br.LEASEVERSION=COMBINE_LEASE.LEASE_VERSION_NBR
                                                 AND TI_ls_br.UNIT        =COMBINE_LEASE.UNIT
                                                 AND TI_ls_br.BUSINESSUNIT=COMBINE_LEASE.BUSINESSUNIT
                where
                           ifnull(dimdate.MONTH_END,'1990-01-01') <=ifnull(LEASE_END_DT,'2099-01-01')
                           AND dimdate.MONTH_END                  <= IFNULL(COMBINE_LEASE.PLAN_OUT_DT, dimdate.MONTH_END)
                           AND dimdate.MONTH_END                  <= IFNULL( COMBINE_LEASE.PLAN_OUT_DT, dimdate.MONTH_END)
     )
   , pre_FactHHCAMBaseRentRoll as
     (
                select
                           a.DATEKEY
                         , B.unit_key , 0 LEASE_KEY , 0 TENANT_KEY
                         , b.businessunitnbr
                         , b.unitnbr
                         , NULL          SECURITY_DEPOSIT
                         , NULL          TI
                         , NULL          LLD_Broker_LC
                         , NULL          TNT_Broker_LC
                         , null          BILLCODE
                         , null          SCHEDULEBEGINDATE
                         , null          SCHEDULEENDDATE
                         , null          SUSPENDDATE
                         , null          SCHEDULE_AMOUNT
                         , NULL          Annual_schedule_amount
                         , null          SCHEDULE_AMOUNT_PER_SQUARE
                         , null          SCHEDULE_SF
                         ,'V'            occupancystatus
                         ,'U'            LEASESTAGE
                         ,'Vacant Units' LIST_CATEGORY
                         ,'V_U'          LIST_CATEGORY_CODE
                         ,'VACANT UNITS' category , 0 ratesqft
                         , b.unitsqft
                         , b.UNIT_TYPE_CD UNIT_TYPE
                         , NULL           BILLINGFREQUENCYCODE
                         , null           PercentageDueOnSales
                from
                           DIMDATE a
                           inner join
                                      DIM_HHC_UNIT b
                                      on
                                                 a.MONTH_END between ifnull(uniteffbegindt,a.MONTH_END) and ifnull(UNITEFFENDDT,a.MONTH_END)
                                                 and not exists
                                                 (
                                                        select
                                                               1
                                                        from
                                                               RESULT_occupied_future c
                                                        where
                                                               a.DATEKEY             = c.DATEKEY
                                                               and b.businessunitnbr = c.BUSINESSUNIT
                                                               and b.unitnbr         = c.UNIT
                                                 )
                UNION ALL
                SELECT *
                FROM
                       RESULT_occupied_future
     )
   , add_rn as
     (
                  SELECT
                               row_number()over(partition by DATEKEY,BUSINESSUNITNBR, unit_key order by
                                                case
                                                             when category='OCCUPIED UNITS'
                                                                          THEN 1
                                                             when LIST_CATEGORY='Renewal'
                                                                          then 3
                                                                          ELSE 2
                                                END , case
                                                             when LEASESTAGE='L'
                                                                          THEN 1
                                                                          ELSE 2
                                                END )rn
                             , row_number()over(partition by DATEKEY,BUSINESSUNITNBR, unit_key,LIST_CATEGORY order by
                                                LIST_CATEGORY)rn_CATEGORY
                             , dense_rank()over(partition by DATEKEY,BUSINESSUNITNBR, unit_key order by
                                                case
                                                             when category='OCCUPIED UNITS'
                                                                          THEN 1
                                                             when LIST_CATEGORY='Renewal'
                                                                          then 3
                                                                          ELSE 2
                                                END , case
                                                             when LEASESTAGE='L'
                                                                          THEN 1
                                                                          ELSE 2
                                                END) RN_O_ALREADY_FLAG
                             , DATEKEY
                             , BUSINESSUNITNBR bu_key
                             , unit_key
                             , LEASE_KEY
                             , TENANT_KEY
                             , SECURITY_DEPOSIT
                             , TI
                             , LLD_Broker_LC
                             , TNT_Broker_LC
                             , BILLCODE
                             , SCHEDULEBEGINDATE
                             , SCHEDULEENDDATE
                             , SUSPENDDATE
                             , SCHEDULE_AMOUNT
                             , Annual_schedule_amount
                             , SCHEDULE_AMOUNT_PER_SQUARE
                             , SCHEDULE_SF
                             , LEASESTAGE      LEASE_STATUS_KEY
                             , occupancystatus OCCSTATUS_KEY
                             , LIST_CATEGORY
                             , LIST_CATEGORY_CODE
                             , CATEGORY
                             , ratesqft
                             , unitsqft
                             , UNIT_TYPE
                             , BILLINGFREQUENCYCODE
                             , PercentageDueOnSales
                  FROM
                               pre_FactHHCAMBaseRentRoll BASE
     ) --select * from add_rn where DATEKEY='20200831' and LEASE_KEY='1044';
   , add_total as
     (
                  SELECT
                               DATEKEY DATE_KEY
                             , bu_key
                             , unit_key
                             , LEASE_KEY
                             , TENANT_KEY
                             , SECURITY_DEPOSIT
                             , TI
                             , LLD_Broker_LC
                             , TNT_Broker_LC
                             , BILLCODE          BILL_CODE
                             , SCHEDULEBEGINDATE SCHEDULE_BEGIN_DATE
                             , SCHEDULEENDDATE   SCHEDULE_END_DATE
                             , SUSPENDDATE       SUSPEND_DATE
                             , SCHEDULE_AMOUNT
                             , Annual_schedule_amount
                             , SCHEDULE_AMOUNT_PER_SQUARE
                             , SCHEDULE_SF
                             , BILLINGFREQUENCYCODE BILLING_FREQUENCY_CODE
                             , PercentageDueOnSales Percentage_Due_On_Sales
                             , LEASE_STATUS_KEY
                             , OCCSTATUS_KEY
                             , ratesqft Rate_SQ_FT
                             , unitsqft Unit_SQ_FT
                             , UNIT_TYPE
                             , case
                                            when UNIT_TYPE in ('ATM'
                                                             ,'CAM'
                                                             ,'OTH'
                                                             ,'RMU'
                                                             ,'ROF'
                                                             ,'STR')
                                                         then 'Other'
                                                         else 'Normal'
                               end unit_type_flag
                             , sum
                                            (
                                                         case
                                                                      when rn                             =1
                                                                                   and unit_type_flag     ='Normal'
                                                                                   and LIST_CATEGORY_CODE<>'RE'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_Total_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn                             =1
                                                                                   and unit_type_flag     ='Normal'
                                                                                   and LIST_CATEGORY_CODE<>'RE'
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_Total_count
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY               =1
                                                                                   and unit_type_flag='Normal'
                                                                                   and LIST_CATEGORY_CODE in ('O_P'
                                                                                                            ,'O_S'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP')
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_leased_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY               =1
                                                                                   and unit_type_flag='Normal'
                                                                                   and LIST_CATEGORY_CODE in ('O_P'
                                                                                                            ,'O_S'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP')
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_leased_count
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY               =1
                                                                                   and unit_type_flag='Normal'
                                                                                   and LIST_CATEGORY_CODE IN ('V_SR'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_unleased_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY               =1
                                                                                   and unit_type_flag='Normal'
                                                                                   and LIST_CATEGORY_CODE IN ('V_SR'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_unleased_count
                               --CJ
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('V_SR'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   and unit_type_flag='Normal'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_vacant_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('V_SR'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   and unit_type_flag='Normal'
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_vacant_count
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE IN ('O_P'
                                                                                                            ,'O_S')
                                                                                   and unit_type_flag='Normal'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_OCCUPIED_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE IN ('O_P'
                                                                                                            ,'O_S')
                                                                                   and unit_type_flag='Normal'
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_OCCUPIED_count
                             , sum
                                            (
                                                         case
                                                                      when OCCSTATUS_KEY                ='O'
                                                                                   and LEASE_STATUS_KEY ='L'
                                                                                   and rn               =1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_Leased_Occupied_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when OCCSTATUS_KEY                ='V'
                                                                                   and LEASE_STATUS_KEY ='L'
                                                                                   and rn               =1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_Leased_Vacant_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when OCCSTATUS_KEY                ='V'
                                                                                   and LEASE_STATUS_KEY ='F'
                                                                                   and rn               =1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_FUTURE_VACANT_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when OCCSTATUS_KEY                ='V'
                                                                                   and LEASE_STATUS_KEY ='U'
                                                                                   and rn               =1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_Unleased_VACANT_sqft --new category
                               --unit_type
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY                    =1
                                                                                   and LIST_CATEGORY_CODE<>'RE'
                                                                                   and RN_O_ALREADY_FLAG  =1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_Total_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('O_P'
                                                                                                            ,'O_S'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP')
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_leased_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('O_P'
                                                                                                            ,'O_S'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP')
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_leased_count
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE IN ('V_SR'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_unleased_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE IN ('V_SR'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_unleased_count
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('V_SR'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   and RN_O_ALREADY_FLAG=1then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_vacant_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('V_SR'
                                                                                                            ,'V_FDP'
                                                                                                            ,'V_FP'
                                                                                                            ,'V_U'
                                                                                                            ,'V_LT365')
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_vacant_count
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('O_P'
                                                                                                            ,'O_S')
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_OCCUPIED_sqft
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('O_P'
                                                                                                            ,'O_S')
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   then 1
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_OCCUPIED_count
                             , sum
                                            (
                                                         case
                                                                      when OCCSTATUS_KEY                ='O'
                                                                                   and LEASE_STATUS_KEY ='L'
                                                                                   and rn               =1
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_Leased_Occupied_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY=1
                                                                                   and LIST_CATEGORY_CODE in ('V_FP'
                                                                                                            ,'V_FDP')
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_Leased_Vaccant_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when OCCSTATUS_KEY                ='V'
                                                                                   and LEASE_STATUS_KEY ='F'
                                                                                   and rn               =1
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_FUTURE_VACANT_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when OCCSTATUS_KEY                ='V'
                                                                                   and LEASE_STATUS_KEY ='U'
                                                                                   and rn               =1
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),UNIT_TYPE) Company_UNIT_TYPE_Unleased_VACANT_sqft --new category
                             , LIST_CATEGORY
                             , CATEGORY
                             , LIST_CATEGORY_code
                             , rn
                             , rn_CATEGORY
                             , RN_O_ALREADY_FLAG
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY               =1
                                                                                   and unit_type_flag='Normal'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),LIST_CATEGORY) Company_CATEGORY_total_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY               =1
                                                                                   and unit_type_flag='Normal'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5),CATEGORY) Company_up_CATEGORY_total_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY                  =1
                                                                                   and RN_O_ALREADY_FLAG=1
                                                                                   and unit_type_flag   ='Normal'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_total_Deduplicate_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY                  =1
                                                                                   and RN_O_ALREADY_FLAG=2
                                                                                   and unit_type_flag   ='Normal'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_duplicate_sqft --new category
                             , sum
                                            (
                                                         case
                                                                      when rn_CATEGORY                   =1
                                                                                   and unit_type_flag    ='Normal'
                                                                                   and LIST_CATEGORY_CODE='RE'
                                                                                   then UNITSQFT
                                                                                   else 0
                                                         end
                                            )
                               over(partition by DATEKEY,LEFT(bu_key,5)) Company_Renewal_sqft --new category
                  FROM
                               add_rn
     )
SELECT *
FROM
         add_total
--where         DATE_KEY='20200831' and LEASE_KEY='1044'
order by
         LEFT(bu_key,5)
       , LIST_CATEGORY
       , RN_O_ALREADY_FLAG DESC
      );
    