

      create or replace  table datalake_dev.semantic.DIM_TENANT  as
      (SELECT   CAST(row_number()over(order by ADDRESSNUMBER)  as nvarchar)  TENANT_KEY
,  CAST(ADDRESSNUMBER as nvarchar)  AS TENANT_ID
        ,NAMEALPHA     AS TENANT_NAME 
        ,CURRENT_TIMESTAMP() ::DATETIME  CREATED_DT
 FROM  datalake_dev.governed.JDE_ADDRESS_BOOK_MASTER   
 union all
SELECT  '0','0' TENANT_ID
        ,'Unknown'
        ,CURRENT_TIMESTAMP() ::DATETIME
      );
    