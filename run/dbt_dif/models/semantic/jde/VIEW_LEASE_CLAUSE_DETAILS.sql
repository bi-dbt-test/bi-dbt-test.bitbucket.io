
  create or replace  view datalake_dev.semantic.VIEW_LEASE_CLAUSE_DETAILS  as (
    


SELECT 
	LC.DocumentOrderInvoice LeaseID, 
	LC.LegalClauseNumber LegalClauseNbr, 
	LC.LeaseVersion, 
	LO.LeaseType,
	LCA.AmountGross GrossAmt,
	LCA.UnitOfMeasure,
	LCA.AmountType AmtType,
	LCA.DateBeginningEffective AS AmtEffBeginDt,
	LCA.DateEndingEffective AS AmtEffEndDt,
	LCN.Narrative,
	LC.LeaseOptionType, 
	LC.optiontypedetail OptionTypeDtl,
	LO.OptionDetailDescription OptDtlDesc, 
	LO.StandardOption StdOption,
	LO.StandardOptionLevel StdOptionLvl,
	LC.DateBeginningEffective AS LegalClauseEffBeginDt, 
	LC.DateEndingEffective AS LegalClauseEffEndDt, 
	LC.NoticeDate NoticeDt, 
	LC.NoticeResponsibility NoticeResp,
	LC.TickleDate CriticalDt, 
	LC.LeaseDocumentReference LeaseDocRef,
	LC.OptionStatus OptionStatusCD,
	UDC.UDC_Desc OptionStatusCDDesc
FROM datalake_dev.governed.JDE_LEGAL_CLAUSES     LC
JOIN (SELECT userdefinedcode UDC_KY,Description udc_desc 
FROM datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     WHERE ProductCode ='15' and userdefinedcodes = 'OS') UDC ON LC.OptionStatus = udc.UDC_KY
JOIN datalake_dev.semantic.VIEW_LEASE_CLAUSE_NARRATIVE  LCN ON TRIM(LC.DocumentOrderInvoice) = LCN.LeaseId AND TRIM(LC.LegalClauseNumber) = LCN.ClauseNumber
JOIN (SELECT DocumentOrderInvoice LeaseId, MAX(LeaseVersion) AS LeaseVersion
 FROM datalake_dev.governed.JDE_LEGAL_CLAUSES     GROUP BY DocumentOrderInvoice) M2 ON M2.LeaseID = LC.DocumentOrderInvoice AND M2.LeaseVersion = LC.LeaseVersion
LEFT JOIN datalake_dev.governed.JDE_LEASE_OPTION_TYPE_DETAIL      LO ON LO.LeaseOptionType = LC.LeaseOptionType AND LO.OptionTypeDetail = LC.optiontypedetail
LEFT JOIN datalake_dev.governed.JDE_LEGAL_CLAUSE_AMOUNTS       LCA ON LCA.DocumentOrderInvoice = LC.DocumentOrderInvoice AND LCA.LegalClauseNumber = LC.LegalClauseNumber AND LCA.LeaseVersion = LC.LeaseVersion
  );
