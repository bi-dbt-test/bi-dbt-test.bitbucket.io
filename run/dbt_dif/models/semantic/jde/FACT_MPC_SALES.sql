

      create or replace  table datalake_dev.semantic.FACT_MPC_SALES  as
      (with _STG_JDE_MPC_ACCOUNTS 
AS
(
SELECT DISTINCT DESCRIPTION                     AS ACCOUNT_KEY,
                SPLIT_PART(DESCRIPTION, '.', 1) AS OBJECTACCOUNT,
                SPLIT_PART(DESCRIPTION, '.', 1) AS OBJECT_ACCOUNT,				
                SPLIT_PART(DESCRIPTION, '.', 2) AS SUBACCOUNT,
                SPLIT_PART(DESCRIPTION, '.', 2) AS SUB_ACCOUNT,				
                SPECIALHANDLINGCODE,
                SPECIALHANDLINGCODE             AS SPECIAL_HANDLING_CODE,				
                CASE
                    WHEN SPECIALHANDLINGCODE = '1' THEN 'Gross Sales'
                    WHEN SPECIALHANDLINGCODE = '2' THEN 'SID Assumption'
                    WHEN SPECIALHANDLINGCODE = '4' THEN 'Cost of Sales'
                 END                            AS SPECIAL_HANDLING_CASE
FROM  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES   
WHERE PRODUCTCODE = '55'
  AND USERDEFINEDCODES = 'H3'
  AND SPECIALHANDLINGCODE IN ('1', '2', '4')
UNION 
SELECT '110130.90950165' AS ACCOUNT_KEY,
	   '11030'           AS OBJECTACCOUNT,
	   '11030'           AS OBJECT_ACCOUNT,	   
	   '90950110'        AS SUBACCOUNT, 
	   '90950110'        AS SUB_ACCOUNT, 	   
	   '2'               AS SPECIALHANDLINGCODE, 
	   '2'               AS SPECIAL_HANDLING_CODE, 	   
	   'SID Assumption'  AS SPECIAL_HANDLING_CASE
UNION 
SELECT '400160'         AS ACCOUNT_KEY,
	   '11030'          AS OBJECTACCOUNT,
	   '11030'          AS OBJECT_ACCOUNT,	   
	   '90950110'       AS SUBACCOUNT,
	   '90950110'       AS SUB_ACCOUNT,	   
	   '2'              AS SPECIALHANDLINGCODE, 
	   '2'              AS SPECIAL_HANDLING_CODE, 	   
	   'SID Assumption' AS SPECIAL_HANDLING_CASE
    
)
,_STG_JDE_DIM_MPC_ACCOUNT_BALANCES
as
(
 SELECT CASE
               WHEN LENGTH(SUBSIDIARY) = 0 THEN OBJECTACCOUNT
               ELSE CONCAT(OBJECTACCOUNT, '.', SUBSIDIARY)
            END                                        AS ACCOUNT_KEY,
           BUSINESSUNIT,
           BUSINESSUNIT                                AS BUSINESS_UNIT,		   
           CONCAT(CENTURY, FISCALYEAR)                 AS YEAR,
           -1 * SUM(AMTBEGINNINGBALANCEPY + AMOUNTWTD) AS BALANCE
    FROM   datalake_dev.governed.JDE_ACCOUNT_BALANCES     AB
    WHERE LEDGERTYPE = 'AA'
      AND ACCOUNT_KEY IN (SELECT ACCOUNT_KEY FROM _STG_JDE_MPC_ACCOUNTS)
    GROUP BY ACCOUNT_KEY, BUSINESSUNIT, YEAR

)
,_STG_JDE_DIM_MPC_BALANCE
as
(
    SELECT DISTINCT AB.BUSINESSUNIT, AB.YEAR, AB.BALANCE, DA.SPECIAL_HANDLING_CASE
    FROM  _STG_JDE_DIM_MPC_ACCOUNT_BALANCES AB
         LEFT JOIN  _STG_JDE_MPC_ACCOUNTS DA
                   ON DA.ACCOUNT_KEY = AB.ACCOUNT_KEY
)
,MPC_TEMP_FACT_BALANCE
as
(

   SELECT *
           FROM _STG_JDE_DIM_MPC_BALANCE
               PIVOT (SUM(BALANCE) FOR SPECIAL_HANDLING_CASE IN ('Gross Sales', 'SID Assumption', 'Cost of Sales')) 
)
,_STG_JDE_MPC_BALANCE
as
(
SELECT X.BUSINESSUNIT,
       X.YEAR,
       X."'Gross Sales'"    AS GROSS_SALES_PRICE,
       X."'Cost of Sales'"  AS COST_OF_SALES,
       X."'SID Assumption'" AS SID_ASSUMPTION
FROM  MPC_TEMP_FACT_BALANCE X

)
SELECT CONCAT(COMMUNITY, '.', HOMEBUILDERLOTNUMBER)        AS LOT_KEY,
       C4.DESCRIPTION                                      AS MPC_AREA,
       SM.BUSINESSUNIT,
       SM.BUSINESSUNIT                                     AS BUSINESS_UNIT,	   
       SM.COMMUNITY,
       SM.BUYERNUMBERA                                     AS BUYER_NUMBER,
       SM.HOMEBUILDERLOTNUMBER                             AS LOT_NUMBER,
       SM.SALESACTIVITYCODELAST,
       SM.SALESACTIVITYCODELAST                            AS SALES_ACTIVITY_CODE_LAST,	 	   
       C2.DESCRIPTION                                      AS SALES_ACTIVITY_LAST,
       SM.SALESACTIVITYCODENEXT,
       SM.SALESACTIVITYCODENEXT                            AS SALES_ACTIVITY_CODE_NEXT,	   
       C3.DESCRIPTION                                      AS SALES_ACTIVITY_NEXT,
       SM.LOANNUMBER                                       AS SALES_CONTRACT_NUMBER,
       SM.SALESCONTRACTSTATUS,
       SM.SALESCONTRACTSTATUS                              AS SALES_CONTRACT_STATUS_CODE,	   
       C1.DESCRIPTION                                      AS SALES_CONTRACT_STATUS,
       SM.SALESDATE                                        AS SALES_DATE,
       SM.CLOSEDATE                                        AS CLOSE_DATE,
       SM.USERDATE4                                        AS TAKE_DOWN_DATE,
       SM.BASEHOUSESALESPRICE                              AS BASE_PRICE,
       SM.TOTALSALESPRICE                                  AS TOTAL_SALES_PRICE,
       SM.MORTGAGEAPPROVALDATE                             AS MORTGAGE_APPROVAL_DATE,
       SM.NETBASEPRICE, 
       SM.NETBASEPRICE                                     AS NET_BASE_PRICE,	   
       SM.AMOUNTUSERDEFINEDAMOUNT03                        AS PRORATA_ITEMS_DUE_TO_SELLER,
       SM.AMOUNTUSERDEFINEDAMOUNT04                        AS NUMBER_OF_LOTS,
       SM.AMOUNTUSERDEFINEDAMOUNT08                        AS NET_PROCEEDS,
       SM.AMOUNTUSERDEFINEDAMOUNT09                        AS DEPOSIT_AMOUNT_RECEIVED,
       SM.AMOUNTUSERDEFINEDAMOUNT10, 
       SM.AMOUNTUSERDEFINEDAMOUNT10                        AS AMOUNT_USER_DEFINED_AMOUNT10,	   
       SM.PHASE,
       SM.HOMEBUILDERAREA,
       SM.HOMEBUILDERAREA                                  AS HOME_BUILDER_AREA,	   
       SM.SMLOTPREM                                        AS LOT_PREMIUM,
       YEAR,
       CASE
           WHEN F.GROSS_SALES_PRICE IS NOT NULL THEN (GROSS_SALES_PRICE)
           WHEN GROSS_SALES_PRICE IS NULL THEN
                (SM.NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08) 
        END                                                AS GROSS_SALES_PRICE,
	   CAST(		
       CASE
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) <> 0
               THEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND SID_ASSUMPTION IS NOT NULL
               THEN SID_ASSUMPTION 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 THEN 0
           WHEN (SID_ASSUMPTION) IS NULL THEN 0
        END AS DECIMAL(18,2))                               AS SID_ASSUMPTION,		
       CASE
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) <> 0 AND GROSS_SALES_PRICE IS NOT NULL
               THEN (GROSS_SALES_PRICE + SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) <> 0 AND GROSS_SALES_PRICE IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 +
                     SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND SID_ASSUMPTION IS NOT NULL AND
                GROSS_SALES_PRICE IS NOT NULL
               THEN (GROSS_SALES_PRICE + SID_ASSUMPTION) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND SID_ASSUMPTION IS NOT NULL AND
                GROSS_SALES_PRICE IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 + SID_ASSUMPTION) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND GROSS_SALES_PRICE IS NOT NULL
               THEN GROSS_SALES_PRICE 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND GROSS_SALES_PRICE IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08) 
           WHEN (SID_ASSUMPTION) IS NULL AND GROSS_SALES_PRICE IS NOT NULL THEN GROSS_SALES_PRICE 
           WHEN (SID_ASSUMPTION) IS NULL AND GROSS_SALES_PRICE IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08) 
        END                                                  AS NET_REVENUE,	
       CASE
           WHEN COST_OF_SALES IS NOT NULL THEN COST_OF_SALES 
           WHEN COST_OF_SALES IS NULL THEN AMOUNTUSERDEFINEDAMOUNT10 
        END                                                  AS COST_OF_SALES, 	
       CASE
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) <> 0 AND GROSS_SALES_PRICE IS NOT NULL AND
                COST_OF_SALES IS NOT NULL
               THEN (GROSS_SALES_PRICE + SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04 - COST_OF_SALES) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) <> 0 AND GROSS_SALES_PRICE IS NULL AND
                COST_OF_SALES IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 +
                     SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04 - SM.AMOUNTUSERDEFINEDAMOUNT10) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) <> 0 AND GROSS_SALES_PRICE IS NOT NULL AND
                COST_OF_SALES IS NOT NULL
               THEN (GROSS_SALES_PRICE + SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04 - COST_OF_SALES) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) <> 0 AND GROSS_SALES_PRICE IS NULL AND
                COST_OF_SALES IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 +
                     SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04 - SM.AMOUNTUSERDEFINEDAMOUNT10) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND SID_ASSUMPTION IS NOT NULL AND
                GROSS_SALES_PRICE IS NOT NULL AND COST_OF_SALES IS NOT NULL
               THEN (GROSS_SALES_PRICE + SID_ASSUMPTION - COST_OF_SALES) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND SID_ASSUMPTION IS NOT NULL AND
                GROSS_SALES_PRICE IS NOT NULL AND COST_OF_SALES IS NULL
               THEN (GROSS_SALES_PRICE + SID_ASSUMPTION - SM.AMOUNTUSERDEFINEDAMOUNT10) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND SID_ASSUMPTION IS NOT NULL AND
                GROSS_SALES_PRICE IS NULL AND COST_OF_SALES IS NOT NULL THEN
                   (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 + SID_ASSUMPTION -
                    COST_OF_SALES) 
           WHEN (SM.AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND SID_ASSUMPTION IS NOT NULL AND
                GROSS_SALES_PRICE IS NULL AND COST_OF_SALES IS NULL THEN
                   (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 + SID_ASSUMPTION -
                    SM.AMOUNTUSERDEFINEDAMOUNT10) 
           WHEN (AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND GROSS_SALES_PRICE IS NOT NULL AND
                COST_OF_SALES IS NOT NULL
               THEN (GROSS_SALES_PRICE - COST_OF_SALES) 
           WHEN (AMOUNTUSERDEFINEDAMOUNT09 + SM.AMOUNTUSERDEFINEDAMOUNT04) = 0 AND GROSS_SALES_PRICE IS NULL AND
                COST_OF_SALES IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 -
                     SM.AMOUNTUSERDEFINEDAMOUNT10) 
           WHEN (SID_ASSUMPTION) IS NULL AND GROSS_SALES_PRICE IS NOT NULL AND COST_OF_SALES IS NOT NULL
               THEN (GROSS_SALES_PRICE - COST_OF_SALES) 
           WHEN (SID_ASSUMPTION) IS NULL AND GROSS_SALES_PRICE IS NULL AND COST_OF_SALES IS NULL
               THEN (NETBASEPRICE + SM.AMOUNTUSERDEFINEDAMOUNT03 + SM.AMOUNTUSERDEFINEDAMOUNT08 -
                     SM.AMOUNTUSERDEFINEDAMOUNT10) 
        END                                                 AS GROSS_MARGIN
FROM   datalake_dev.governed.JDE_SALES_MASTER_TABLE   SM
         LEFT OUTER JOIN GOVERNED.JDE_USER_DEFINED_CODE_VALUES C1
                         ON SM.SALESCONTRACTSTATUS = C1.USERDEFINEDCODE 
						AND C1.PRODUCTCODE = '44H5' 
						AND C1.USERDEFINEDCODES = 'ST'
         LEFT OUTER JOIN   datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES    C2
                         ON SM.SALESACTIVITYCODELAST = C2.USERDEFINEDCODE 
						AND C2.PRODUCTCODE = '44H0' 
						AND C2.USERDEFINEDCODES = 'RL'
         LEFT OUTER JOIN  datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES      C3
                         ON SM.SALESACTIVITYCODENEXT = C3.USERDEFINEDCODE 
						AND C3.PRODUCTCODE = '44H0' 
						AND C3.USERDEFINEDCODES = 'RL'
         LEFT OUTER JOIN datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES     C4
                         ON SM.HOMEBUILDERAREA = C4.USERDEFINEDCODE 
						AND C4.PRODUCTCODE = '00' 
						AND C4.USERDEFINEDCODES = '05'
         LEFT OUTER JOIN _STG_JDE_MPC_BALANCE F
                         ON SM.BUSINESSUNIT = F.BUSINESSUNIT  AND F.YEAR = YEAR( SEMANTIC.JDE_JULIAN_TO_DATE(SM.CLOSEDATE)  )
         LEFT OUTER JOIN datalake_dev.semantic.DIM_ADDRESS_BOOK    T
                         ON SM.BUYERNUMBERA = T.ADDRESS_NUMBER 
						AND T.ADDRESS_TYPE1 LIKE 'BD'
WHERE SM.BUSINESSUNIT    NOT LIKE ''
  AND SM.HOMEBUILDERAREA NOT LIKE ''
      );
    