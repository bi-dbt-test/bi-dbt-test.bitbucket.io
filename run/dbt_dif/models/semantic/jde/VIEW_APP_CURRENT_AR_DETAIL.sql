
  create or replace  view datalake_dev.semantic.VIEW_APP_CURRENT_AR_DETAIL  as (
    
 WITH LEASE_INFO
AS
( 
SELECT A.LEASEID, A.BUSINESSUNITNBR,UNIT_NBR, A.GUARANTOR ,MOVE_IN_DT,MOVE_OUT_DT,PLAN_OUT_DT,LEASE_BEGIN_DT
		          FROM (
						select LEASE_ID LEASEID,BUSINESS_UNIT BUSINESSUNITNBR,UNIT_NBR,GUARANTOR
                              ,ROW_NUMBER()OVER(PARTITION BY LEASE_ID ORDER BY LEASE_VERSION_NBR desc) RN
                    ,MOVE_IN_DT,MOVE_OUT_DT,PLAN_OUT_DT,LEASE_BEGIN_DT
                          from datalake_dev.semantic.DIM_LEASE  ---select  * from semantic.dim_lease
						) A WHERE RN=1
)
,tenant
as
(
 Select distinct Tenant_ID, Tenant_Name from datalake_dev.semantic.DIM_TENANT 
)

SELECT BU_NBR bunbr,vc.UNIT_NBR,l.LEASEID,OPER_PROP_CD OperPropCd,OPER_PROP_CD_DESC OperPropCDDesc,vc.TENANT_ID TenantId
  , T.Tenant_Name,
			case when AGING <= 30 then AR_AMT end as AR1,
			case when AGING > 30 and AGING <= 60  then AR_AMT  end as AR31,
			case when AGING > 60 and AGING <= 90  then AR_AMT  end as AR61,
			case when AGING > 90 and AGING <= 120  then AR_AMT  end as AR91,
			case when AGING > 120  then AR_AMT else 0  end as AR121,
			AR_AMT ARTotal,
			sum(AR_AMT) Over(Partition By OperPropCd, TenantId Order by OperPropCd, TenantId) OverallTotal,
			LEASE_STATUS_CD LeaseStatusCD,
			LEASE_STATUS_DESC LeaseStatusDesc, MOVE_IN_DT,MOVE_OUT_DT,PLAN_OUT_DT,LEASE_BEGIN_DT,
			Case when G.Tenant_Name = 'Unknown' then null else G.Tenant_Name end Guarantor
FROM  datalake_dev.semantic.VIEW_AR_CURRENT  vc
LEFT JOIN LEASE_INFO l on  l.LEASEID=LTRIM(vc.LEASE_ID,'0')
LEFT JOIN tenant T on vc.Tenant_ID=t.Tenant_ID
LEFT JOIN tenant g on l.GUARANTOR=g.Tenant_ID
where BILLCODE not in ('SECT','SECD','SECC','PRPD','HHB1','HHB2','HHB3','HHB4','HHC1','HHC2','HHC3','HHC4','HHT1','HHT2','HHT9','HHM1','HHE1')
		  and Aging > 0
  );
