

      create or replace  table datalake_dev.semantic.DIM_UNIT  as
      (WITH BASEUNIT 
AS 
( 
SELECT    
      MUNIT.BUSINESSUNIT, 
      MUNIT.UNIT                  AS UNITNBR, 
      MUNIT.PROPMANFLOOR          AS UNITFLOOR, 
      MUNIT.DESCRIPTION           AS UNITDESC, 
      MUNIT.PROPMANUNITTYPE       AS UNITTYPECD, 
      UDCUNITTYPEDESC.DESCRIPTION AS UNITTYPEDESC, 
      UNIT.AREATYPE               AS AREATYPECD, 
      UNIT.DATEBEGINNINGEFFECTIVE AS UNIT_BEGIN_EFF_DATE, 
      UNIT.DATEENDINGEFFECTIVE    AS UNIT_END_EFF_DATE, 	 
      UNIT.PROPMANUNITS001        AS UNITSQFT, 
      ROW_NUMBER() OVER (PARTITION BY MUNIT.BUSINESSUNIT, MUNIT.UNIT, UNIT.DATEBEGINNINGEFFECTIVE 
	                         ORDER BY (CASE WHEN  UNIT.AREATYPE  = 'REN' THEN 1 
						WHEN  UNIT.AREATYPE  = 'USE' THEN 2 
						ELSE 3 
					    END) ,IFNULL(UNIT.DATEENDINGEFFECTIVE, '12/31/2099') DESC) AS RN,  			
      MUNIT.STATUSUNIT            AS UNITSTATUS
         FROM  datalake_dev.governed.JDE_UNIT_MASTER    MUNIT 
    LEFT JOIN  datalake_dev.governed.JDE_AREA_MASTER        UNIT 
	       ON UNIT.BUSINESSUNIT = MUNIT.BUSINESSUNIT 
		  AND UNIT.UNIT = MUNIT.UNIT
    LEFT JOIN datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES      UDCUNITTYPEDESC 
           ON UDCUNITTYPEDESC.PRODUCTCODE = '15' 
          AND UDCUNITTYPEDESC.USERDEFINEDCODES = 'UT' 
          AND UDCUNITTYPEDESC.USERDEFINEDCODE = MUNIT.PROPMANUNITTYPE  
 ) 
   SELECT 
            0                             AS UNIT_KEY,
            'UKN'                         AS BUSINESS_UNIT, 
            '0'                             AS UNIT_NBR, 
            '2000-01-01'::DATE            AS UNIT_BEGIN_EFF_DATE, 
            '2100-01-01'::DATE            AS UNIT_END_EFF_DATE, 
            '1'                             AS ACTIVE_FLAG, 
            'UKN'                         AS UNIT_TYPE_CD, 
            'UNKNOWN'                     AS UNIT_TYPE_DESC, 
            NULL                          AS UNIT_DESCRIPTION, 
            NULL                          AS UNIT_STATUS, 
            'UKN'                         AS AREA_TYPE_CD, 
            '0'                             AS FLOOR_NBR, 
            '0'                             AS SQR_FOOTAGE, 
            CURRENT_TIMESTAMP()::DATETIME AS CREATE_DT
            union all 
SELECT  row_number()over(order by BUSINESSUNIT ,UNITNBR)  UNIT_KEY,
      BUSINESSUNIT,   
      UNITNBR, 
      UNIT_BEGIN_EFF_DATE, 
      UNIT_END_EFF_DATE, 
      CASE WHEN CURRENT_DATE BETWEEN UNIT_BEGIN_EFF_DATE AND UNIT_END_EFF_DATE 
           THEN 1 ELSE 0 
       END AS ACTIVE_FLAG, 
      UNITTYPECD, 
      UNITTYPEDESC, 
      UNITDESC, 
      UNITSTATUS, 
      AREATYPECD, 
      UNITFLOOR, 
      UNITSQFT, 
      CURRENT_DATE() 
 FROM BASEUNIT 
WHERE RN = 1   
 
ORDER BY UNIT_KEY
      );
    