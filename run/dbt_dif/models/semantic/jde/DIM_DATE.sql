

      create or replace  table datalake_dev.semantic.DIM_DATE  as
      (with base
AS
(



/*
call as follows:

date_spine(
    "day",
    "to_date('01/01/2016', 'mm/dd/yyyy')",
    "dateadd(week, 1, current_date)"
)

*/

with rawdata as (

    

    

    with p as (
        select 0 as generated_number union all select 1
    ), unioned as (

    select

    
    p0.generated_number * pow(2, 0)
     + 
    
    p1.generated_number * pow(2, 1)
     + 
    
    p2.generated_number * pow(2, 2)
     + 
    
    p3.generated_number * pow(2, 3)
     + 
    
    p4.generated_number * pow(2, 4)
     + 
    
    p5.generated_number * pow(2, 5)
     + 
    
    p6.generated_number * pow(2, 6)
     + 
    
    p7.generated_number * pow(2, 7)
     + 
    
    p8.generated_number * pow(2, 8)
     + 
    
    p9.generated_number * pow(2, 9)
     + 
    
    p10.generated_number * pow(2, 10)
     + 
    
    p11.generated_number * pow(2, 11)
     + 
    
    p12.generated_number * pow(2, 12)
     + 
    
    p13.generated_number * pow(2, 13)
     + 
    
    p14.generated_number * pow(2, 14)
    
    
    + 1
    as generated_number

    from

    
    p as p0
     cross join 
    
    p as p1
     cross join 
    
    p as p2
     cross join 
    
    p as p3
     cross join 
    
    p as p4
     cross join 
    
    p as p5
     cross join 
    
    p as p6
     cross join 
    
    p as p7
     cross join 
    
    p as p8
     cross join 
    
    p as p9
     cross join 
    
    p as p10
     cross join 
    
    p as p11
     cross join 
    
    p as p12
     cross join 
    
    p as p13
     cross join 
    
    p as p14
    
    

    )

    select *
    from unioned
    where generated_number <= 26067
    order by generated_number



),

all_periods as (

    select (
        

    dateadd(
        day,
        row_number() over (order by 1) - 1,
        cast('1970-01-01' as date)
        )


    ) as date_day
    from rawdata

),

filtered as (

    select *
    from all_periods
    where date_day <= dateadd(day,7299,current_date)

)

select * from filtered

  
)
,DAY_LIST
as
(
        SELECT '2099-12-31'::DATE AS CALC_DATE
        UNION
select  *
from base
)
, 
UNKNOWN_DATE AS 
   (SELECT '2000-01-01'::DATE AS CALC_DATE 
    ), 
HIGH_DATE AS 
   (SELECT '2100-01-01'::DATE AS CALC_DATE 
    )
SELECT CAST(TO_VARCHAR(CALC_DATE, 'YYYYMMDD') AS INT) AS DATE_ID,
       CALC_DATE AS DAY,
       TO_VARCHAR(CALC_DATE, 'MM/DD/YYYY') AS DATE_NAME,
       (YEAR(CALC_DATE)-1900)*1000 + DAYOFYEAR(CALC_DATE) AS JDE_JULIAN_DATE,
       DAYOFYEAR(CALC_DATE) AS DAY_OF_YEAR,
       DAYNAME(CALC_DATE) AS DAY_NAME_ABR,
       DECODE( EXTRACT ('dayofweek_iso',CALC_DATE),
                       1, 'Monday',
                       2, 'Tuesday',
                       3, 'Wednesday',
                       4, 'Thursday',
                       5, 'Friday',
                       6, 'Saturday',
                       7, 'Sunday'
             ) AS DAY_NAME,
       CASE WHEN EXTRACT ('dayofweek_iso',CALC_DATE) IN (6, 7) THEN 'WEEKEND' ELSE 'WEEKDAY' END AS WEEKDAY_WEEKEND,   
       WEEK(CALC_DATE) AS WEEK_NUM,
       DATE_TRUNC(WEEK,CALC_DATE) AS WEEK_START,
       DATEADD(DAY,-1,DATEADD(WEEK,1,DATE_TRUNC(WEEK,CALC_DATE))) AS WEEK_END,
       EXTRACT ('dayofweek_iso',CALC_DATE) AS DAY_OF_WEEK, 
       MONTH(CALC_DATE) AS MONTH_NUM,
       DATE_TRUNC(MONTH,CALC_DATE) AS MONTH_START,
       DATEADD(DAY,-1,DATEADD(MONTH,1,DATE_TRUNC(MONTH,CALC_DATE))) AS MONTH_END,
       MONTHNAME(CALC_DATE) AS MONTH_NAME_ABR, 
       DECODE(MONTH(CALC_DATE),   
                       1, 'January',
                       2, 'February',
                       3, 'March',
                       4, 'April',
                       5, 'May',
                       6, 'June',
                       7, 'July',
                       8, 'August',
                       9, 'September',
                      10, 'October',
                      11, 'November',
                      12, 'December' 
             ) AS MONTH_NAME, 
       DAYOFMONTH(CALC_DATE) AS DAY_IN_MONTH,
       cast( CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(MONTH,1,DATE_TRUNC(MONTH,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END  as boolean) AS IS_LAST_DAY_OF_MONTH,
       QUARTER(CALC_DATE) AS QTR_NUM,
       'Q' || (QUARTER(CALC_DATE))::TEXT AS QUARTER_TXT,
       DATE_TRUNC(QUARTER,CALC_DATE) AS QUARTER_START,
       DATEADD(DAY,-1,DATEADD(QUARTER,1,DATE_TRUNC(QUARTER,CALC_DATE))) QUARTER_END,
       YEAR(CALC_DATE) AS YEAR_NUM,
       DATE_TRUNC(YEAR,CALC_DATE) AS YEAR_START,
       DATEADD(DAY,-1,DATEADD(YEAR,1,DATE_TRUNC(YEAR,CALC_DATE))) YEAR_END,
       CAST(TO_VARCHAR(CALC_DATE, 'YYYYMM') AS INT) AS YEAR_MONTH, 
       TO_VARCHAR(CALC_DATE, 'YYYYQ')||(QUARTER(CALC_DATE))::TEXT AS YEAR_QUARTER, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(WEEK,1,DATE_TRUNC(WEEK,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE'END  as boolean)  AS WEEK_FLAG, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(MONTH,1,DATE_TRUNC(MONTH,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END  as boolean)  AS MONTH_FLAG, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(QUARTER,1,DATE_TRUNC(QUARTER,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END  as boolean)  AS QUARTER_FLAG, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(YEAR,1,DATE_TRUNC(YEAR,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END  as boolean)  AS YEAR_FLAG, 
       cast(CURRENT_TIMESTAMP() as date) AS CREATED_DT
    FROM DAY_LIST 
        UNION
SELECT 21000101 AS DATE_ID,
       CALC_DATE AS DAY,
       TO_VARCHAR(CALC_DATE, 'MM/DD/YYYY') AS DATE_NAME,
       9999999 AS JDE_JULIAN_DATE,
       0 AS DAY_OF_YEAR,
       'Unk date' AS DAY_NAME_ABR,
       'Unk date' AS DAY_NAME,
       'Unk date' AS WEEKDAY_WEEKEND,   
       0 AS WEEK_NUM,
       DATE_TRUNC(WEEK,CALC_DATE) AS WEEK_START,
       DATEADD(DAY,-1,DATEADD(WEEK,1,DATE_TRUNC(WEEK,CALC_DATE))) AS WEEK_END,
       0 AS DAY_OF_WEEK, 
       0 AS MONTH_NUM,
       DATE_TRUNC(MONTH,CALC_DATE) AS MONTH_START,
       DATEADD(DAY,-1,DATEADD(MONTH,1,DATE_TRUNC(MONTH,CALC_DATE))) AS MONTH_END,
       'Unk date' AS MONTH_NAME_ABR, 
       'Unk date' AS MONTH_NAME, 
       0 AS DAY_IN_MONTH,
        cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(MONTH,1,DATE_TRUNC(MONTH,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END  as boolean) AS IS_LAST_DAY_OF_MONTH,
       0 AS QTR_NUM,
       'Unk date' AS QUARTER_TXT,
       DATE_TRUNC(QUARTER,CALC_DATE) AS QUARTER_START,
       DATEADD(DAY,-1,DATEADD(QUARTER,1,DATE_TRUNC(QUARTER,CALC_DATE))) QUARTER_END,
       0 AS YEAR_NUM,
       DATE_TRUNC(YEAR,CALC_DATE) AS YEAR_START,
       DATEADD(DAY,-1,DATEADD(YEAR,1,DATE_TRUNC(YEAR,CALC_DATE))) YEAR_END,
       0 AS YEAR_MONTH, 
       'Unk date' AS YEAR_QUARTER, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(WEEK,1,DATE_TRUNC(WEEK,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE'END as boolean)  AS WEEK_FLAG, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(MONTH,1,DATE_TRUNC(MONTH,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END as boolean)  AS MONTH_FLAG, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(QUARTER,1,DATE_TRUNC(QUARTER,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END as boolean)  AS QUARTER_FLAG, 
       cast(CASE WHEN CALC_DATE = DATEADD(DAY,-1,DATEADD(YEAR,1,DATE_TRUNC(YEAR,CALC_DATE))) THEN 'TRUE' ELSE 'FALSE' END as boolean)  AS YEAR_FLAG, 
        cast(CURRENT_TIMESTAMP() as date) AS CREATED_DT
    FROM HIGH_DATE
      );
    