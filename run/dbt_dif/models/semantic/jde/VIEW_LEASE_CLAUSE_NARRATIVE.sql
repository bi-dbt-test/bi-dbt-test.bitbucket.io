
  create or replace  view datalake_dev.semantic.VIEW_LEASE_CLAUSE_NARRATIVE  as (
    
SELECT 
	LEFT(GenericTextKey,charindex('|',GenericTextKey)-1) AS LeaseId,
	RIGHT(GenericTextKey,(charindex('|',REVERSE(GenericTextKey))-1)::INT) AS ClauseNumber,
	mediaobjectsequencenumber AS LeaseVersion,
	generictextmediaobjecttype ObjectType,
	generictextblobbuffer Narrative
FROM  datalake_dev.governed.JDE_MEDIA_OBJECTS_STORAGE       
WHERE objectname = 'GT1570'
AND IFNULL(RIGHT(GenericTextKey,(charindex('|',REVERSE(GenericTextKey))-1)::INT), '') <> ''
  );
