

        insert into datalake_dev.semantic_snapshot.DIM_BILL_CODE ("BILL_CODE", "BILL_CODE_DESC", "BILL_CODE_CATEGORY", "BILL_CODE_ALT_CATEGORY", "GROSS_FLAG", "BASE_FLAG", "ALL_CHARGES_FLAG", "CREATE_DT", "FLAG", "INGESTION_TIME")
        (
            select "BILL_CODE", "BILL_CODE_DESC", "BILL_CODE_CATEGORY", "BILL_CODE_ALT_CATEGORY", "GROSS_FLAG", "BASE_FLAG", "ALL_CHARGES_FLAG", "CREATE_DT", "FLAG", "INGESTION_TIME"
            from datalake_dev.semantic_snapshot.DIM_BILL_CODE__dbt_tmp
        );