

        insert into datalake_dev.semantic_snapshot.BRG_SF_EVENT_SPACE ("ID", "EVENT_ID", "SPACE_ID", "CREATEDBY_ID", "CREATED_DATE", "IS_DELETED", "LAST_MODIFIED_BY_ID", "LAST_MODIFIED_DATE", "NAME", "OWNER_ID", "SYSTEM_MOD_STAMP", "FLAG", "INGESTION_TIME")
        (
            select "ID", "EVENT_ID", "SPACE_ID", "CREATEDBY_ID", "CREATED_DATE", "IS_DELETED", "LAST_MODIFIED_BY_ID", "LAST_MODIFIED_DATE", "NAME", "OWNER_ID", "SYSTEM_MOD_STAMP", "FLAG", "INGESTION_TIME"
            from datalake_dev.semantic_snapshot.BRG_SF_EVENT_SPACE__dbt_tmp
        );