
  create or replace  view datalake_dev.governed.APPIAN_DRM_APPLICATIONAPPROVAL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_drm_applicationapproval_2359)
SELECT applicationApprovalId applicationApprovalId
                ,applicationLookupId applicationLookupId
                ,personId personId
                ,stepOrderId stepOrderId
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_drm_applicationapproval_2359 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
