
  create or replace  view datalake_dev.governed.APPIAN_HOME_CCHAMP  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_home_cchamp_2337)
SELECT CChampId CChampId
                ,CChampName CChampName
                ,CChampOffice CChampOffice
                ,CChampGroup CChampGroup
                ,CChampEmail CChampEmail
                ,CChampPhonenumber CChampPhonenumber
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isActive isActive
                
FROM datalake_dev.pre_governed.appian_home_cchamp_2337 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
