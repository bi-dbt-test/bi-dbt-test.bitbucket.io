
  create or replace  view datalake_dev.governed.APPIAN_HHC_SHAREPOINTFOLDER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_sharepointfolder_2352)
SELECT id id
                ,requestId requestId
                ,applicationType applicationType
                ,folderPartialURL folderPartialURL
                
FROM datalake_dev.pre_governed.appian_hhc_sharepointfolder_2352 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
