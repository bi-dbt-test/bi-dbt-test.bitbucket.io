
  create or replace  view datalake_dev.governed.APPIAN_HHC_LOOKUP  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_lookup_2365)
SELECT lookupId lookupId
                ,lookupCategoryId lookupCategoryId
                ,shortDescription shortDescription
                ,longDescription longDescription
                ,parentLookupId parentLookupId
                ,sortOrder sortOrder
                ,value value
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_hhc_lookup_2365 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
