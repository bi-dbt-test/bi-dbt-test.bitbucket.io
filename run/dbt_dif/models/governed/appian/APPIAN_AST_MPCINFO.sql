
  create or replace  view datalake_dev.governed.APPIAN_AST_MPCINFO  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_mpcinfo_2390)
SELECT MPCInfoId MPCInfoId
                ,catCode24 catCode24
                ,yearSalesBegan yearSalesBegan
                ,lotsAtCompletion lotsAtCompletion
                ,lotsSold lotsSold
                ,lotsRemaining lotsRemaining
                ,estimatedSelloutDateResidential estimatedSelloutDateResidential
                ,estimatedSelloutDateCommercial estimatedSelloutDateCommercial
                
FROM datalake_dev.pre_governed.appian_ast_mpcinfo_2390 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
