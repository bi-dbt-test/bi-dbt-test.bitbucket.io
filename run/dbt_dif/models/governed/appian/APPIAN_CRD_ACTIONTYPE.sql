
  create or replace  view datalake_dev.governed.APPIAN_CRD_ACTIONTYPE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_actiontype_2339)
SELECT actionTypeId actionTypeId
                ,name name
                ,description description
                ,priorityLookupId priorityLookupId
                ,taskNameLookupId taskNameLookupId
                ,stopNotificationsUponCompletion stopNotificationsUponCompletion
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                ,taskDue taskDue
                
FROM datalake_dev.pre_governed.appian_crd_actiontype_2339 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
