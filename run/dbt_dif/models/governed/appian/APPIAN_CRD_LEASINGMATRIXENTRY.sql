
  create or replace  view datalake_dev.governed.APPIAN_CRD_LEASINGMATRIXENTRY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_leasingmatrixentry_2384)
SELECT leasingMatrixEntryId leasingMatrixEntryId
                ,regionCode regionCode
                ,buNumber buNumber
                ,leaseTypeLookupId leaseTypeLookupId
                ,appianUsername appianUsername
                ,appianGroupId appianGroupId
                ,isPropertyManager isPropertyManager
                ,isLeasingRecipient isLeasingRecipient
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                ,isThirdPartyLeasingAgent isThirdPartyLeasingAgent
                ,externalEmailAddress externalEmailAddress
                
FROM datalake_dev.pre_governed.appian_crd_leasingmatrixentry_2384 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
