
  create or replace  view datalake_dev.governed.APPIAN_AST_QUICKLINK  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_quicklink_2377)
SELECT quicklinkId quicklinkId
                ,name name
                ,url url
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,catCode24 catCode24
                ,linkTypeLookupId linkTypeLookupId
                
FROM datalake_dev.pre_governed.appian_ast_quicklink_2377 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
