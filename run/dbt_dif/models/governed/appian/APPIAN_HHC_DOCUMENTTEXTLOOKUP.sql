
  create or replace  view datalake_dev.governed.APPIAN_HHC_DOCUMENTTEXTLOOKUP  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_documenttextlookup_2363)
SELECT documentTextLookupId documentTextLookupId
                ,documentText documentText
                ,name name
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_hhc_documenttextlookup_2363 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
