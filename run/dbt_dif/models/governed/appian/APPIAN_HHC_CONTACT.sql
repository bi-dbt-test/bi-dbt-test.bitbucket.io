
  create or replace  view datalake_dev.governed.APPIAN_HHC_CONTACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_contact_2334)
SELECT contactId contactId
                ,contactTypeLookupId contactTypeLookupId
                ,entityTypeLookupId entityTypeLookupId
                ,entityId entityId
                ,oktaUsername oktaUsername
                ,appianUsername appianUsername
                ,firstName firstName
                ,middleName middleName
                ,lastName lastName
                ,phoneNumber phoneNumber
                ,faxNumber faxNumber
                ,email email
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_hhc_contact_2334 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
