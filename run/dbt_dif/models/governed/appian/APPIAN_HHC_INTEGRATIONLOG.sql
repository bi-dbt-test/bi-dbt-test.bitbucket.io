
  create or replace  view datalake_dev.governed.APPIAN_HHC_INTEGRATIONLOG  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_integrationlog_2350)
SELECT id id
                ,statusLine statusLine
                ,statusCode statusCode
                ,headers headers
                ,contentType contentType
                ,body body
                ,context context
                ,createdDate createdDate
                
FROM datalake_dev.pre_governed.appian_hhc_integrationlog_2350 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
