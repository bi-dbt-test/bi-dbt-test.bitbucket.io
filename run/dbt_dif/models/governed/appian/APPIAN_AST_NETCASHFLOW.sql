
  create or replace  view datalake_dev.governed.APPIAN_AST_NETCASHFLOW  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_netcashflow_2374)
SELECT netCashFlowId netCashFlowId
                ,catCode24 catCode24
                ,catCode19 catCode19
                ,ledger ledger
                ,year year
                ,month month
                ,amount amount
                
FROM datalake_dev.pre_governed.appian_ast_netcashflow_2374 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
