
  create or replace  view datalake_dev.governed.APPIAN_AST_CONDOSALESPERFORMANCE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_condosalesperformance_2370)
SELECT salesPerformanceId salesPerformanceId
                ,assetName assetName
                ,catCode24 catCode24
                ,unitNumber unitNumber
                ,unitSquareFoot unitSquareFoot
                ,actualNetSales actualNetSales
                ,proFormaNetSales proFormaNetSales
                ,actualPrice actualPrice
                ,proFormaPrice proFormaPrice
                ,unsoldPrice unsoldPrice
                ,proFormaUnsoldPrice proFormaUnsoldPrice
                
FROM datalake_dev.pre_governed.appian_ast_condosalesperformance_2370 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
