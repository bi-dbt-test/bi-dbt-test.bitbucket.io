
  create or replace  view datalake_dev.governed.APPIAN_WSP_PROJECT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_wsp_project_2423)
SELECT projectId projectId
                ,name name
                ,purpose purpose
                ,plan plan
                ,startDate startDate
                ,originalEndDate originalEndDate
                ,currentEndDate currentEndDate
                ,priorityLookupId priorityLookupId
                ,cost cost
                ,statusLookupId statusLookupId
                ,businessUnitString businessUnitString
                ,createdDate createdDate
                ,createdBy createdBy
                ,lastUpdatedDate lastUpdatedDate
                ,lastUpdatedBy lastUpdatedBy
                ,isActive isActive
                ,SysStartTime SysStartTime
                ,SysEndTime SysEndTime
                ,parentProjectId parentProjectId
                
FROM datalake_dev.pre_governed.appian_wsp_project_2423 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
