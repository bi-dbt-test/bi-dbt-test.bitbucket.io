
  create or replace  view datalake_dev.governed.APPIAN_HHC_WORKFLOW  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_workflow_2355)
SELECT id id
                ,typeId typeId
                ,statusId statusId
                ,entityTypeId entityTypeId
                ,entityId entityId
                ,metadata metadata
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_hhc_workflow_2355 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
