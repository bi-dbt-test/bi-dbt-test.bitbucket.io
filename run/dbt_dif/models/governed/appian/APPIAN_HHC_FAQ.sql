
  create or replace  view datalake_dev.governed.APPIAN_HHC_FAQ  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_faq_2364)
SELECT faqId faqId
                ,faqType faqType
                ,question question
                ,answer answer
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isActive isActive
                
FROM datalake_dev.pre_governed.appian_hhc_faq_2364 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
