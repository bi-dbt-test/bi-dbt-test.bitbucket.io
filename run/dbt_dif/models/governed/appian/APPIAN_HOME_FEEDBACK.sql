
  create or replace  view datalake_dev.governed.APPIAN_HOME_FEEDBACK  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_home_feedback_2338)
SELECT feedbackId feedbackId
                ,starRating starRating
                ,description description
                ,feedbackTypeLookupId feedbackTypeLookupId
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isActive isActive
                
FROM datalake_dev.pre_governed.appian_home_feedback_2338 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
