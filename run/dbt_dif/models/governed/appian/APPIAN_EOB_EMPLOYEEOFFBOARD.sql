
  create or replace  view datalake_dev.governed.APPIAN_EOB_EMPLOYEEOFFBOARD  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_eob_employeeoffboard_2427)
SELECT employeeOffboardId employeeOffboardId
                ,isVoluntary isVoluntary
                ,terminationDate terminationDate
                ,statusLookupId statusLookupId
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isActive isActive
                ,appianUsername appianUsername
                ,additionalComments additionalComments
                
FROM datalake_dev.pre_governed.appian_eob_employeeoffboard_2427 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
