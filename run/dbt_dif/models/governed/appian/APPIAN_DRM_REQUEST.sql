
  create or replace  view datalake_dev.governed.APPIAN_DRM_REQUEST  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_drm_request_2385)
SELECT requestId requestId
                ,requestTypeLookupId requestTypeLookupId
                ,requestStatusLookupId requestStatusLookupId
                ,requestOutcomeLookupId requestOutcomeLookupId
                ,description description
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isParent isParent
                ,parentRequestId parentRequestId
                
FROM datalake_dev.pre_governed.appian_drm_request_2385 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
