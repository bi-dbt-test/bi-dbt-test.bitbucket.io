
  create or replace  view datalake_dev.governed.APPIAN_HHC_PERSON  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_person_2368)
SELECT id id
                ,appianUsername appianUsername
                ,oktaId oktaId
                ,oktaProfileEmail oktaProfileEmail
                ,oktaProfileLogin oktaProfileLogin
                ,oktaProfileFirstName oktaProfileFirstName
                ,oktaProfileLastName oktaProfileLastName
                ,oktaProfileManagerId oktaProfileManagerId
                ,oktaProfileManager oktaProfileManager
                ,oktaProfileTitle oktaProfileTitle
                ,oktaStatus oktaStatus
                ,oktaProfileZipCode oktaProfileZipCode
                ,oktaProfileCompany oktaProfileCompany
                ,oktaProfileADupn oktaProfileADupn
                ,oktaProfileCity oktaProfileCity
                ,oktaProfileLocationName oktaProfileLocationName
                ,oktaProfileEmployeeNumber oktaProfileEmployeeNumber
                ,oktaProfileO365username oktaProfileO365username
                ,oktaProfileCountryCode oktaProfileCountryCode
                ,oktaProfileHireDate oktaProfileHireDate
                ,oktaProfileState oktaProfileState
                ,oktaProfileDepartment oktaProfileDepartment
                ,oktaProfileBirthday_without_year oktaProfileBirthday_without_year
                ,oktaProfileSecondEmail oktaProfileSecondEmail
                ,oktaProfileNameidentifier oktaProfileNameidentifier
                ,oktaProfileAddress_Line2 oktaProfileAddress_Line2
                ,oktaProfilePrimaryPhone oktaProfilePrimaryPhone
                ,oktaProfileMobilePhone oktaProfileMobilePhone
                ,oktaProfileStreetAddress oktaProfileStreetAddress
                ,oktaProfileName oktaProfileName
                ,lastUpdated lastUpdated
                ,oktaProfileImmutableID oktaProfileImmutableID
                ,oktaProfilePC_Id oktaProfilePC_Id
                ,oktaProfileWorkdayType oktaProfileWorkdayType
                ,oktaProfileManagment_Level oktaProfileManagment_Level
                ,oktaProfileDisplayName oktaProfileDisplayName
                ,oktaProfileRegion oktaProfileRegion
                ,regionId regionId
                ,oktaProfileEmployeeType oktaProfileEmployeeType
                ,oktaProfileCompanyCode oktaProfileCompanyCode
                ,oktaProfileWorkspace oktaProfileWorkspace
                ,oktaProfileJDEBU oktaProfileJDEBU
                ,oktaProfileEmployeeWID oktaProfileEmployeeWID
                ,oktaProfileManagerWID oktaProfileManagerWID
                ,nexusUserProfileId nexusUserProfileId
                ,nexusRoleId nexusRoleId
                ,nexusRoleName nexusRoleName
                ,nexusUsername nexusUsername
                ,vorexContactId vorexContactId
                ,vorexLocationId vorexLocationId
                ,oktaProfileCostCenter oktaProfileCostCenter
                
FROM datalake_dev.pre_governed.appian_hhc_person_2368 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
