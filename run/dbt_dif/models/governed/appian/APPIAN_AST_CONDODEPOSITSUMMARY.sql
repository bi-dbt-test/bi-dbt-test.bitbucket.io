
  create or replace  view datalake_dev.governed.APPIAN_AST_CONDODEPOSITSUMMARY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_condodepositsummary_2369)
SELECT depositSummaryId depositSummaryId
                ,catCode24 catCode24
                ,totalDepositsRecieved totalDepositsRecieved
                ,totalRefundableDeposits totalRefundableDeposits
                ,totalNonrefundableDeposits totalNonrefundableDeposits
                ,totalUsed totalUsed
                ,totalInEscrow totalInEscrow
                
FROM datalake_dev.pre_governed.appian_ast_condodepositsummary_2369 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
