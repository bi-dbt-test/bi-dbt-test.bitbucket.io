
  create or replace  view datalake_dev.governed.APPIAN_CRD_DATETYPECUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_datetypecustom_2341)
SELECT dateTypeCustomId dateTypeCustomId
                ,frequencyId frequencyId
                ,categoryId categoryId
                ,catCode24 catCode24
                ,startDate startDate
                ,endDate endDate
                ,isActive isActive
                ,createdBy createdBy
                ,createdDateTime createdDateTime
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdateDateTime lastUpdateDateTime
                ,weekDayId weekDayId
                ,monthDay monthDay
                ,quarterMonth quarterMonth
                ,month month
                ,isPrivate isPrivate
                ,deletionComment deletionComment
                
FROM datalake_dev.pre_governed.appian_crd_datetypecustom_2341 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
