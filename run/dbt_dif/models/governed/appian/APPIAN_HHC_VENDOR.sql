
  create or replace  view datalake_dev.governed.APPIAN_HHC_VENDOR  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_vendor_2345)
SELECT vendorMasterId vendorMasterId
                ,vendorName vendorName
                ,paymentTerms paymentTerms
                ,currencyCode currencyCode
                ,addressId addressId
                ,lastUpdatedOn lastUpdatedOn
                ,taxId taxId
                ,entityType entityType
                ,paymentInstrument paymentInstrument
                
FROM datalake_dev.pre_governed.appian_hhc_vendor_2345 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
