
  create or replace  view datalake_dev.governed.APPIAN_CRD_DATETYPENOTIFICATIONASSIGNEE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_datetypenotificationassignee_2382)
SELECT dateTypeNotificationAssigneeId dateTypeNotificationAssigneeId
                ,dateTypeNotificationId dateTypeNotificationId
                ,actionTypeId actionTypeId
                ,appianGroupUuid appianGroupUuid
                ,appianUsername appianUsername
                ,useCustomAssignment useCustomAssignment
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                
FROM datalake_dev.pre_governed.appian_crd_datetypenotificationassignee_2382 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
