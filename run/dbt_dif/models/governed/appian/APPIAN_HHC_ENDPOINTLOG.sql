
  create or replace  view datalake_dev.governed.APPIAN_HHC_ENDPOINTLOG  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_endpointlog_2919)
SELECT id id
                ,body body
                ,formdata formdata
                ,headers headers
                ,pathsegments pathsegments
                ,queryparameters queryparameters
                ,requesturl requesturl
                ,createdDate createdDate
                ,responseStatusCode responseStatusCode
                ,responseHeaders responseHeaders
                ,responseBody responseBody
                ,documentId documentId
                
FROM datalake_dev.pre_governed.appian_hhc_endpointlog_2919 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
