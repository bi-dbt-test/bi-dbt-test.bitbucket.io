
  create or replace  view datalake_dev.governed.APPIAN_CRD_EXTERNALENTITYTYPE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_externalentitytype_2526)
SELECT externalEntityTypeId externalEntityTypeId
                ,name name
                ,description description
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                ,sourceSystemLookupId sourceSystemLookupId
                ,categoryId categoryId
                
FROM datalake_dev.pre_governed.appian_crd_externalentitytype_2526 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
