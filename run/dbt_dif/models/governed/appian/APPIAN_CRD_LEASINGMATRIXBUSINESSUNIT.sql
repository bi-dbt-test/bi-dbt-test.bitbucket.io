
  create or replace  view datalake_dev.governed.APPIAN_CRD_LEASINGMATRIXBUSINESSUNIT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_leasingmatrixbusinessunit_2358)
SELECT leasingMatrixBusinessUnitId leasingMatrixBusinessUnitId
                ,buNumber buNumber
                ,isDisplayable isDisplayable
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                
FROM datalake_dev.pre_governed.appian_crd_leasingmatrixbusinessunit_2358 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
