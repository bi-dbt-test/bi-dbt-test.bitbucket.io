
  create or replace  view datalake_dev.governed.APPIAN_HHC_DOCUMENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_document_4204)
SELECT documentId documentId
                ,documentTypeLookupId documentTypeLookupId
                ,entityTypeLookupId entityTypeLookupId
                ,entityId entityId
                ,appianDocumentId appianDocumentId
                ,documentName documentName
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,sharepointUrl sharepointUrl
                ,description description
                ,appianDocSize appianDocSize
                ,extension extension
                
FROM datalake_dev.pre_governed.appian_hhc_document_4204 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
