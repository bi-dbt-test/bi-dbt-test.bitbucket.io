
  create or replace  view datalake_dev.governed.APPIAN_CRD_CATEGORYSUBSCRIPTION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_categorysubscription_2379)
SELECT categorySubscriptionId categorySubscriptionId
                ,categoryId categoryId
                ,appianUsername appianUsername
                ,isSubscribed isSubscribed
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                
FROM datalake_dev.pre_governed.appian_crd_categorysubscription_2379 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
