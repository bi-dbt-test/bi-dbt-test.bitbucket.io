
  create or replace  view datalake_dev.governed.APPIAN_HHC_COMMENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_comment_2333)
SELECT commentId commentId
                ,entityTypeLookupId entityTypeLookupId
                ,entityId entityId
                ,comment comment
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isEveryoneTagged isEveryoneTagged
                ,isEdited isEdited
                ,SysStartTime SysStartTime
                ,SysEndTime SysEndTime
                
FROM datalake_dev.pre_governed.appian_hhc_comment_2333 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
