
  create or replace  view datalake_dev.governed.APPIAN_AST_DETAILS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_details_2388)
SELECT catCode24 catCode24
                ,addressLine1 addressLine1
                ,addressLine2 addressLine2
                ,city city
                ,state state
                ,postalCode postalCode
                ,region region
                ,segment segment
                ,hide hide
                ,assetTypeLookupId assetTypeLookupId
                ,imageAppianDocumentId imageAppianDocumentId
                ,createdOn createdOn
                
FROM datalake_dev.pre_governed.appian_ast_details_2388 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
