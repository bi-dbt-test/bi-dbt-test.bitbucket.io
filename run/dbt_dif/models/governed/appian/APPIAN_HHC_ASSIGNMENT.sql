
  create or replace  view datalake_dev.governed.APPIAN_HHC_ASSIGNMENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_assignment_2391)
SELECT assignmentId assignmentId
                ,appianUsername appianUsername
                ,appianGroupId appianGroupId
                ,entityTypeLookupId entityTypeLookupId
                ,entityId entityId
                ,attributeLookupId attributeLookupId
                ,createdDate createdDate
                ,createdBy createdBy
                ,lastUpdatedDate lastUpdatedDate
                ,lastUpdatedBy lastUpdatedBy
                ,isActive isActive
                ,SysStartTime SysStartTime
                ,SysEndTime SysEndTime
                ,customBool customBool
                
FROM datalake_dev.pre_governed.appian_hhc_assignment_2391 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
