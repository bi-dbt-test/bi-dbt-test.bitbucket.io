
  create or replace  view datalake_dev.governed.APPIAN_DRM_REQUESTFIELDMAPPING  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_drm_requestfieldmapping_2811)
SELECT requestFieldMappingId requestFieldMappingId
                ,requestTypeLookupId requestTypeLookupId
                ,fieldLookupId fieldLookupId
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,sectionLookupId sectionLookupId
                ,sortOrder sortOrder
                ,isRequired isRequired
                ,isEmailcontent isEmailcontent
                ,emailSortOrder emailSortOrder
                ,emailFieldLabel emailFieldLabel
                
FROM datalake_dev.pre_governed.appian_drm_requestfieldmapping_2811 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
