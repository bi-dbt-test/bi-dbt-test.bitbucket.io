
  create or replace  view datalake_dev.governed.APPIAN_HHC_ASSET  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_asset_2343)
SELECT F0006_MCMCU F0006_MCMCU
                ,F0006_MCDC F0006_MCDC
                ,F0006_MCCO F0006_MCCO
                ,F0010_CCNAME F0010_CCNAME
                ,F0006_MCRP01 F0006_MCRP01
                ,F0006_MCRP02 F0006_MCRP02
                ,F0006_MCRP03 F0006_MCRP03
                ,F0006_MCRP04 F0006_MCRP04
                ,F0006_MCRP05 F0006_MCRP05
                ,F0006_MCRP06 F0006_MCRP06
                ,F0006_MCRP07 F0006_MCRP07
                ,F0006_MCRP08 F0006_MCRP08
                ,F0006_MCRP09 F0006_MCRP09
                ,F0006_MCRP10 F0006_MCRP10
                ,F0006_MCRP11 F0006_MCRP11
                ,F0006_MCRP12 F0006_MCRP12
                ,F0006_MCRP13 F0006_MCRP13
                ,F0006_MCRP14 F0006_MCRP14
                ,F0006_MCRP15 F0006_MCRP15
                ,F0006_MCRP16 F0006_MCRP16
                ,F0006_MCRP17 F0006_MCRP17
                ,F0006_MCRP18 F0006_MCRP18
                ,F0006_MCRP19 F0006_MCRP19
                ,F0006_MCRP20 F0006_MCRP20
                ,F0006_MCRP21 F0006_MCRP21
                ,F0006_MCRP22 F0006_MCRP22
                ,F0006_MCRP23 F0006_MCRP23
                ,F0006_MCRP24 F0006_MCRP24
                ,F0006_MCRP25 F0006_MCRP25
                ,F0006_MCRP26 F0006_MCRP26
                ,F0006_MCRP27 F0006_MCRP27
                ,F0006_MCRP28 F0006_MCRP28
                ,F0006_MCRP29 F0006_MCRP29
                ,F0006_MCRP30 F0006_MCRP30
                ,lastUpdatedOn lastUpdatedOn
                
FROM datalake_dev.pre_governed.appian_hhc_asset_2343 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
