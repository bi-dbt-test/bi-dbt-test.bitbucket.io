
  create or replace  view datalake_dev.governed.APPIAN_HHC_TASKHISTORY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_taskhistory_2354)
SELECT taskHistoryId taskHistoryId
                ,entityTypeLookupId entityTypeLookupId
                ,entityId entityId
                ,taskTypeLookupId taskTypeLookupId
                ,taskStatusLookupId taskStatusLookupId
                ,approvalDecisionLookupId approvalDecisionLookupId
                ,taskName taskName
                ,startTime startTime
                ,endTime endTime
                ,assignees assignees
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,taskNameLookupId taskNameLookupId
                ,approvalTypeId approvalTypeId
                ,processModelId processModelId
                ,taskId taskId
                ,workflowId workflowId
                ,dueDate dueDate
                ,priorityId priorityId
                ,description description
                
FROM datalake_dev.pre_governed.appian_hhc_taskhistory_2354 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
