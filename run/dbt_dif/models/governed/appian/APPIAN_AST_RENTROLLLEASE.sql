
  create or replace  view datalake_dev.governed.APPIAN_AST_RENTROLLLEASE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_rentrolllease_2375)
SELECT id id
                ,catCode24 catCode24
                ,catCode24Description catCode24Description
                ,catCode12 catCode12
                ,catCode12Description catCode12Description
                ,catCode20 catCode20
                ,catCode20Description catCode20Description
                ,buNumber buNumber
                ,propertyCodeDescription propertyCodeDescription
                ,company company
                ,leaseId leaseId
                ,tenantTypeCode tenantTypeCode
                ,suiteTypeCode suiteTypeCode
                ,suiteTypeCodeDescription suiteTypeCodeDescription
                ,leaseTypeCode leaseTypeCode
                ,unitNumber unitNumber
                ,unitTypeCode unitTypeCode
                ,tenantName tenantName
                ,occupancyStatusDescription occupancyStatusDescription
                ,maxLeaseEndDate maxLeaseEndDate
                ,unitSf unitSf
                ,useableSf useableSf
                ,rentableSf rentableSf
                ,dateKey dateKey
                
FROM datalake_dev.pre_governed.appian_ast_rentrolllease_2375 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
