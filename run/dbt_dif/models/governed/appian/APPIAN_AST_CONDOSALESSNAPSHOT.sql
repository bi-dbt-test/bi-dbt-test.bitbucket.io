
  create or replace  view datalake_dev.governed.APPIAN_AST_CONDOSALESSNAPSHOT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_condosalessnapshot_2371)
SELECT salesSnapshotId salesSnapshotId
                ,propertyId propertyId
                ,frequency frequency
                ,assetName assetName
                ,catCode24 catCode24
                ,unitsSold unitsSold
                ,unitsUnsold unitsUnsold
                ,unitsInRescission unitsInRescission
                ,unitsTotal unitsTotal
                ,unitsPercentSold unitsPercentSold
                ,revenueSold revenueSold
                ,revenueUnsold revenueUnsold
                ,revenueTotal revenueTotal
                ,revenuePercentSold revenuePercentSold
                ,squareFootSold squareFootSold
                ,squareFootUnsold squareFootUnsold
                ,squareFootTotal squareFootTotal
                ,squareFootPercentSold squareFootPercentSold
                ,asOfDate asOfDate
                
FROM datalake_dev.pre_governed.appian_ast_condosalessnapshot_2371 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
