
  create or replace  view datalake_dev.governed.APPIAN_AST_LEASINGPIPELINE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_leasingpipeline_2373)
SELECT leasingPipelineId leasingPipelineId
                ,type type
                ,catCode24 catCode24
                ,unitNumber unitNumber
                ,tenantName tenantName
                ,rentableSf rentableSf
                ,stage stage
                ,stageDescription stageDescription
                ,rentCommencementDate rentCommencementDate
                ,executedDate executedDate
                
FROM datalake_dev.pre_governed.appian_ast_leasingpipeline_2373 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
