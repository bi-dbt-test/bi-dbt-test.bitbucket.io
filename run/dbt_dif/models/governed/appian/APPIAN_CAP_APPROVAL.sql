
  create or replace  view datalake_dev.governed.APPIAN_CAP_APPROVAL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_cap_approval_2422)
SELECT approvalId approvalId
                ,transactionId transactionId
                ,coreSystem coreSystem
                ,coreSystemUrl coreSystemUrl
                ,approver approver
                ,coreSystemWorkflowId coreSystemWorkflowId
                ,coreSystemTaskId coreSystemTaskId
                ,approvalOptions approvalOptions
                ,approvalTaskType approvalTaskType
                ,taskDetailsUrl taskDetailsUrl
                ,approvalDescription approvalDescription
                ,metadata metadata
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,approvalTypeId approvalTypeId
                ,coreSystemId coreSystemId
                
FROM datalake_dev.pre_governed.appian_cap_approval_2422 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
