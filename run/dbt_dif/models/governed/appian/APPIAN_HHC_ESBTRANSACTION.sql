
  create or replace  view datalake_dev.governed.APPIAN_HHC_ESBTRANSACTION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_esbtransaction_2349)
SELECT transactionId transactionId
                ,correlationId correlationId
                ,createdTimestamp createdTimestamp
                ,lastUpdatedTimestamp lastUpdatedTimestamp
                ,callbackReceived callbackReceived
                ,callbackPayload callbackPayload
                ,transactionTypeLookupId transactionTypeLookupId
                ,sourceRecordDetails sourceRecordDetails
                
FROM datalake_dev.pre_governed.appian_hhc_esbtransaction_2349 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
