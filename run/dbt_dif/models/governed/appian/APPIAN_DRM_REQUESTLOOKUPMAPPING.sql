
  create or replace  view datalake_dev.governed.APPIAN_DRM_REQUESTLOOKUPMAPPING  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_drm_requestlookupmapping_2362)
SELECT requestLookupMappingId requestLookupMappingId
                ,requestTypeId requestTypeId
                ,parentLookupId parentLookupId
                ,childLookupId childLookupId
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_drm_requestlookupmapping_2362 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
