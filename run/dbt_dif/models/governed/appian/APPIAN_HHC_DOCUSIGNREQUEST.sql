
  create or replace  view datalake_dev.governed.APPIAN_HHC_DOCUSIGNREQUEST  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_docusignrequest_2347)
SELECT docusignRequestId docusignRequestId
                ,requestId requestId
                ,application application
                ,docusignDocumentId docusignDocumentId
                ,envelopeId envelopeId
                ,statusDateTime statusDateTime
                ,status status
                
FROM datalake_dev.pre_governed.appian_hhc_docusignrequest_2347 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
