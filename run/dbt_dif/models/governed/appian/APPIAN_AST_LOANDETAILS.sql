
  create or replace  view datalake_dev.governed.APPIAN_AST_LOANDETAILS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_loandetails_2389)
SELECT loanDetailsId loanDetailsId
                ,primaryLender primaryLender
                ,guarantor guarantor
                ,reportingEntity reportingEntity
                ,totalCommitment totalCommitment
                ,currentPrincipalBalance currentPrincipalBalance
                ,contractualMaturityDate contractualMaturityDate
                ,fullyExtendedMaturityDate fullyExtendedMaturityDate
                ,companyId companyId
                ,catCode24 catCode24
                ,createdOn createdOn
                ,allocationPercent allocationPercent
                
FROM datalake_dev.pre_governed.appian_ast_loandetails_2389 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
