
  create or replace  view datalake_dev.governed.APPIAN_HHC_ADDRESS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_address_2309)
SELECT addressId addressId
                ,addressTypeLookupId addressTypeLookupId
                ,entityTypeLookupId entityTypeLookupId
                ,entityId entityId
                ,addressLineOne addressLineOne
                ,addressLineTwo addressLineTwo
                ,city city
                ,stateLookupId stateLookupId
                ,zipCode zipCode
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,countryLookupId countryLookupId
                ,stateName stateName
                
FROM datalake_dev.pre_governed.appian_hhc_address_2309 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
