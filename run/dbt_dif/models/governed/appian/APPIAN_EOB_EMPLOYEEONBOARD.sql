
  create or replace  view datalake_dev.governed.APPIAN_EOB_EMPLOYEEONBOARD  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_eob_employeeonboard_2426)
SELECT employeeOnboardId employeeOnboardId
                ,employeeNumber employeeNumber
                ,statusLookupId statusLookupId
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isActive isActive
                ,capEmailSent capEmailSent
                ,itEmailSent itEmailSent
                ,errorMessage errorMessage
                ,regionId regionId
                ,questionnaireJson questionnaireJson
                ,region region
                
FROM datalake_dev.pre_governed.appian_eob_employeeonboard_2426 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
