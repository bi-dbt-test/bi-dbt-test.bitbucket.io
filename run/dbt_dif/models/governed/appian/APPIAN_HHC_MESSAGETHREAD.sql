
  create or replace  view datalake_dev.governed.APPIAN_HHC_MESSAGETHREAD  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_messagethread_2425)
SELECT messageThreadId messageThreadId
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,isActive isActive
                
FROM datalake_dev.pre_governed.appian_hhc_messagethread_2425 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
