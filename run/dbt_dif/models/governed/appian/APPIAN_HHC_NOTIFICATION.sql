
  create or replace  view datalake_dev.governed.APPIAN_HHC_NOTIFICATION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_notification_2351)
SELECT notificationId notificationId
                ,entityTypeId entityTypeId
                ,entityId entityId
                ,parentEntityTypeId parentEntityTypeId
                ,parentEntityId parentEntityId
                ,actionId actionId
                ,categoryId categoryId
                ,priorityId priorityId
                ,primaryInfo primaryInfo
                ,secondaryInfo secondaryInfo
                ,dueDate dueDate
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                ,notificationSubcategoryId notificationSubcategoryId
                ,taskId taskId
                
FROM datalake_dev.pre_governed.appian_hhc_notification_2351 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
