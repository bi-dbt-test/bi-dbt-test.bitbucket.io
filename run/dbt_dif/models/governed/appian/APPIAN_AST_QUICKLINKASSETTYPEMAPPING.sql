
  create or replace  view datalake_dev.governed.APPIAN_AST_QUICKLINKASSETTYPEMAPPING  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_quicklinkassettypemapping_2378)
SELECT quicklinkAssetTypeMappingId quicklinkAssetTypeMappingId
                ,quicklinkId quicklinkId
                ,assetTypeLookupId assetTypeLookupId
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_ast_quicklinkassettypemapping_2378 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
