
  create or replace  view datalake_dev.governed.APPIAN_HHC_STOCKINFO  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_stockinfo_2353)
SELECT symbol symbol
                ,open open
                ,high high
                ,low low
                ,price price
                ,volume volume
                ,latestTradingDay latestTradingDay
                ,previousClose previousClose
                ,change change
                ,changePercent changePercent
                
FROM datalake_dev.pre_governed.appian_hhc_stockinfo_2353 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
