
  create or replace  view datalake_dev.governed.APPIAN_CRD_DATETYPESUBSCRIPTION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_datetypesubscription_2383)
SELECT dateTypeSubscriptionId dateTypeSubscriptionId
                ,dateTypeId dateTypeId
                ,appianGroupId appianGroupId
                ,appianUsername appianUsername
                ,isActive isActive
                
FROM datalake_dev.pre_governed.appian_crd_datetypesubscription_2383 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
