
  create or replace  view datalake_dev.governed.APPIAN_CRD_DATESUBSCRIPTION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_datesubscription_2380)
SELECT dateSubscriptionId dateSubscriptionId
                ,criticalDateId criticalDateId
                ,appianUsername appianUsername
                ,isSubscribed isSubscribed
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                
FROM datalake_dev.pre_governed.appian_crd_datesubscription_2380 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
