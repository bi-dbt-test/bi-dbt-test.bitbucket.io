
  create or replace  view datalake_dev.governed.APPIAN_CRD_DATETYPE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_datetype_2340)
SELECT dateTypeId dateTypeId
                ,externalEntityTypeId externalEntityTypeId
                ,name name
                ,description description
                ,isSubscribedByDefault isSubscribedByDefault
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                ,integrationKey integrationKey
                
FROM datalake_dev.pre_governed.appian_crd_datetype_2340 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
