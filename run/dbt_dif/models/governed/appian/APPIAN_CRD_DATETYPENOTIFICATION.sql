
  create or replace  view datalake_dev.governed.APPIAN_CRD_DATETYPENOTIFICATION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_datetypenotification_2381)
SELECT dateTypeNotificationId dateTypeNotificationId
                ,dateTypeId dateTypeId
                ,daysPrior daysPrior
                ,timeOfDay timeOfDay
                ,description description
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                ,isCustomReminder isCustomReminder
                
FROM datalake_dev.pre_governed.appian_crd_datetypenotification_2381 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
