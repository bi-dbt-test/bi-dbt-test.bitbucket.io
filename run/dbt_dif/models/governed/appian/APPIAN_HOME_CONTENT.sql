
  create or replace  view datalake_dev.governed.APPIAN_HOME_CONTENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_home_content_2424)
SELECT contentId contentId
                ,title title
                ,summary summary
                ,expirationDate expirationDate
                ,contentTypeLookupId contentTypeLookupId
                ,bannerId bannerId
                ,richText richText
                ,rawText rawText
                ,postedBy postedBy
                ,postedDate postedDate
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,requiresAcknowledgement requiresAcknowledgement
                ,isCompanyNews isCompanyNews
                ,isIndustryNews isIndustryNews
                ,isActive isActive
                ,isDraft isDraft
                ,appianFolderId appianFolderId
                ,contentStatusLookupId contentStatusLookupId
                ,SysStartTime SysStartTime
                ,SysEndTime SysEndTime
                ,newsTypeLookupId newsTypeLookupId
                
FROM datalake_dev.pre_governed.appian_home_content_2424 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
