
  create or replace  view datalake_dev.governed.APPIAN_HHC_JDE_UDC  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_hhc_jde_udc_2344)
SELECT udfKey udfKey
                ,DRSY DRSY
                ,DRRT DRRT
                ,DRKY DRKY
                ,DRDL01 DRDL01
                ,DRDL02 DRDL02
                ,DRSPHD DRSPHD
                ,DRUDCO DRUDCO
                ,DRHRDC DRHRDC
                ,DRUSER DRUSER
                ,DRPID DRPID
                ,DRJOBN DRJOBN
                ,lastUpdatedOn lastUpdatedOn
                
FROM datalake_dev.pre_governed.appian_hhc_jde_udc_2344 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
