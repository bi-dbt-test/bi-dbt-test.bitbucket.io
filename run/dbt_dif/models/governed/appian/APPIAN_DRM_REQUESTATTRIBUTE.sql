
  create or replace  view datalake_dev.governed.APPIAN_DRM_REQUESTATTRIBUTE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_drm_requestattribute_2386)
SELECT requestAttributeId requestAttributeId
                ,requestId requestId
                ,fieldLookupId fieldLookupId
                ,fieldValue fieldValue
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                ,occurenceIndex occurenceIndex
                
FROM datalake_dev.pre_governed.appian_drm_requestattribute_2386 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
