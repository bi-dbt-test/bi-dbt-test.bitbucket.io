
  create or replace  view datalake_dev.governed.APPIAN_DRM_FIELDLOOKUP  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_drm_fieldlookup_2360)
SELECT fieldLookupId fieldLookupId
                ,fieldName fieldName
                ,fieldTypeLookupId fieldTypeLookupId
                ,fieldLabel fieldLabel
                ,isRequired isRequired
                ,isEditable isEditable
                ,lookupCategoryId lookupCategoryId
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_drm_fieldlookup_2360 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
