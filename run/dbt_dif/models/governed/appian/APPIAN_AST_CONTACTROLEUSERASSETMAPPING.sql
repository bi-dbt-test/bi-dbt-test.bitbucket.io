
  create or replace  view datalake_dev.governed.APPIAN_AST_CONTACTROLEUSERASSETMAPPING  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_contactroleuserassetmapping_2376)
SELECT contactRoleUserAssetMappingId contactRoleUserAssetMappingId
                ,contactRoleId contactRoleId
                ,appianUsername appianUsername
                ,catCode24 catCode24
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_ast_contactroleuserassetmapping_2376 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
