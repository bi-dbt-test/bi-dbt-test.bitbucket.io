
  create or replace  view datalake_dev.governed.APPIAN_AST_CONTACTROLE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_contactrole_2387)
SELECT contactRoleId contactRoleId
                ,name name
                ,sortOrder sortOrder
                ,isActive isActive
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastUpdatedBy lastUpdatedBy
                ,lastUpdatedDate lastUpdatedDate
                
FROM datalake_dev.pre_governed.appian_ast_contactrole_2387 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
