
  create or replace  view datalake_dev.governed.APPIAN_CRD_CRITICALDATE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_criticaldate_2564)
SELECT criticalDateId criticalDateId
                ,dateTypeId dateTypeId
                ,entityId entityId
                ,criticalDate criticalDate
                ,context context
                ,titleOverride titleOverride
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                ,catCode24 catCode24
                ,tenant tenant
                
FROM datalake_dev.pre_governed.appian_crd_criticaldate_2564 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
