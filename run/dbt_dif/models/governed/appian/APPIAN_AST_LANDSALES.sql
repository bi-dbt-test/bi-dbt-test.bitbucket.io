
  create or replace  view datalake_dev.governed.APPIAN_AST_LANDSALES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_ast_landsales_2372)
SELECT landSalesId landSalesId
                ,catCode24 catCode24
                ,netAcreage netAcreage
                ,netSquareFeet netSquareFeet
                ,grossAcreage grossAcreage
                ,numberOfUnits numberOfUnits
                ,contractDate contractDate
                ,closeDate closeDate
                ,contractSignedDate contractSignedDate
                ,basePrice basePrice
                ,totalSalesPrice totalSalesPrice
                ,grossSalesPrice grossSalesPrice
                ,netRevenue netRevenue
                ,pricePerAcre pricePerAcre
                ,pricePerSquareFoot pricePerSquareFoot
                
FROM datalake_dev.pre_governed.appian_ast_landsales_2372 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
