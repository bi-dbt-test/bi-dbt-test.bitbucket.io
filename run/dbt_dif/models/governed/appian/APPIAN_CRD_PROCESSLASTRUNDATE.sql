
  create or replace  view datalake_dev.governed.APPIAN_CRD_PROCESSLASTRUNDATE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.appian_crd_processlastrundate_2346)
SELECT processModelId processModelId
                ,lastRunDate lastRunDate
                ,createdBy createdBy
                ,createdDate createdDate
                ,lastModifiedBy lastModifiedBy
                ,lastModifiedDate lastModifiedDate
                ,isActive isActive
                
FROM datalake_dev.pre_governed.appian_crd_processlastrundate_2346 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
