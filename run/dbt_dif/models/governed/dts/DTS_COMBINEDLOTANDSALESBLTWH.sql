
  create or replace  view datalake_dev.governed.DTS_COMBINEDLOTANDSALESBLTWH  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.dts_combinedlotandsalesbltwh_4076)
SELECT lotid lotid
                ,BuilderID BuilderID
                ,BuilderName BuilderName
                ,CreationDate CreationDate
                ,Lot Lot
                ,Block Block
                ,SectionNumber SectionNumber
                ,SectionID SectionID
                ,Address Address
                ,LotContractDate LotContractDate
                ,TakeDownDate TakeDownDate
                ,BaseLotPrice BaseLotPrice
                ,LotPremium LotPremium
                ,MarketingFee MarketingFee
                ,InterestCarry InterestCarry
                ,TotalLotRevenue TotalLotRevenue
                ,SpecStartDate SpecStartDate
                ,CuldesacLot CuldesacLot
                ,OversizeLot OversizeLot
                ,StandardLot StandardLot
                ,AmenityLot AmenityLot
                ,LotType LotType
                ,LotWidth LotWidth
                ,LotSqFt LotSqFt
                ,DisclosureNotice DisclosureNotice
                ,DrillSiteNotice DrillSiteNotice
                ,FloodNotice FloodNotice
                ,PickUpDate PickUpDate
                ,Note Note
                ,Status Status
                ,SaleID SaleID
                ,ContractDate ContractDate
                ,ClosingDate ClosingDate
                ,Buyer Buyer
                ,BuyerEmail BuyerEmail
                ,Builder Builder
                ,Section Section
                ,BuilderAgent BuilderAgent
                ,SaleType SaleType
                ,OfficeName OfficeName
                ,RealtorName RealtorName
                ,ReatorEmail ReatorEmail
                ,EstimatedClosingDate EstimatedClosingDate
                ,ContractPrice ContractPrice
                ,FinalContractPrice FinalContractPrice
                ,SaleSquareFootage SaleSquareFootage
                ,CreateDate CreateDate
                ,StartType StartType
                ,DTS DTS
                
FROM datalake_dev.pre_governed.dts_combinedlotandsalesbltwh_4076 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
