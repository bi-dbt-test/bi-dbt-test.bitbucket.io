
  create or replace  view datalake_dev.governed.KYRIBA_THHC_NC_EXPORT_CASH_BI_TRANSACTIONS_CF_EXPORT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.kyriba_thhc_nc_export_cash_bi_transactions_cf_export_2516)
SELECT Account Account
                ,Flow_Amount Flow_Amount
                ,Transaction_Date Transaction_Date
                ,Accounting_Date Accounting_Date
                ,Flow_Code Flow_Code
                ,Budget_Code Budget_Code
                ,Currency Currency
                ,Flow_Status Flow_Status
                ,Description Description
                ,Reference Reference
                ,Update_Date Update_Date
                ,Company Company
                ,Bank Bank
                ,Report_Name Report_Name
                ,File_Export_Date File_Export_Date
                ,DEBIT_CREDIT DEBIT_CREDIT
                
FROM datalake_dev.pre_governed.kyriba_thhc_nc_export_cash_bi_transactions_cf_export_2516 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
