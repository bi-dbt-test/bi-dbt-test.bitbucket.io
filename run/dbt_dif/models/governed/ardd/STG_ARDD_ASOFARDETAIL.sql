
  create or replace  view datalake_dev.governed.STG_ARDD_ASOFARDETAIL  as (
    SELECT BU_NBR, BU_NBR2, BU_DESC, COMPANY, COMPANY_DESC, SEGMENT_CD, SEGMENT_CD_DESC, REGION_CD, REGION_CD_DESC, OPER_PROP_CD, 
            OPER_PROP_CD_DESC, DOCUMENT_NBR, DOCUMENT_TYPE, DOCUMENT_COMPANY, TENANT_ID, TENANT_NAME, LEASE_ID, LEASE_TYPE, UNIT_NBR, BILLCODE, REMARK, OBJECTACCOUNT, 
            GROSS_AMT, OPEN_AMT, INVOICE_DATE, INVOICE_DATE_CLOSED, DUE_DATE, GL_DATE, INVOICE_NBR, AR_AMT_BASED_DATE, AGING_BASED_DATE, LEASE_STATUS_CD, LEASE_STATUS_DESC 
            FROM  datalake_dev.semantic.VIEW_APP_ASOF_AR_DETAIL
  );
