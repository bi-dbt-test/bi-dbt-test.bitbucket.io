
  create or replace  view datalake_dev.governed.E_BUILDER_PLAN_CASHFLOW_SCENARIOCATEGORY_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_plan_cashflow_scenariocategory_fact_1830)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,id id
                ,category_name category_name
                ,scenariocategory_month scenariocategory_month
                ,amount_thisperiod amount_thisperiod
                ,actuals_plusprojections actuals_plusprojections
                ,scenario_name scenario_name
                ,scenario_description scenario_description
                ,scenario_area scenario_area
                ,scenario_createddate scenario_createddate
                ,scenario_createdby scenario_createdby
                ,scenario_lastmodifieddate scenario_lastmodifieddate
                ,scenario_lastmodifiedby scenario_lastmodifiedby
                ,scenario_status scenario_status
                ,published_scenario_id published_scenario_id
                ,scenario_startdate scenario_startdate
                ,scenario_finishdate scenario_finishdate
                ,total_amount total_amount
                ,displayorder displayorder
                ,segment_lookup_id segment_lookup_id
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_plan_cashflow_scenariocategory_fact_1830 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
