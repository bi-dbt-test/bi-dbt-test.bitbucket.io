
  create or replace  view datalake_dev.governed.E_BUILDER_TASKACTIVITY_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_taskactivity_fact_1835)
SELECT task_id task_id
                ,account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,task_name task_name
                ,task_description task_description
                ,task_sequencenumber task_sequencenumber
                ,task_outlinenumber task_outlinenumber
                ,task_outlinelevel task_outlinelevel
                ,task_createdby task_createdby
                ,task_createddate task_createddate
                ,master_tasknumber master_tasknumber
                ,master_taskname master_taskname
                ,resource_role resource_role
                ,resource_name resource_name
                ,manager_role manager_role
                ,manager_name manager_name
                ,task_constraint_type task_constraint_type
                ,task_constraintdate task_constraintdate
                ,task_startdate task_startdate
                ,task_finishdate task_finishdate
                ,task_earlystartdate task_earlystartdate
                ,task_earlyfinishdate task_earlyfinishdate
                ,task_latestartdate task_latestartdate
                ,task_latefinishdate task_latefinishdate
                ,task_durationfactor task_durationfactor
                ,task_actualstartdate task_actualstartdate
                ,task_actualfinishdate task_actualfinishdate
                ,task_actualdurationfactor task_actualdurationfactor
                ,task_baselinestartdate task_baselinestartdate
                ,task_baselinefinishdate task_baselinefinishdate
                ,task_baselinedurationfactor task_baselinedurationfactor
                ,baseline_percentcomplete baseline_percentcomplete
                ,baseline_savedby baseline_savedby
                ,master_activity_id master_activity_id
                ,task_suggestedactualstartdate task_suggestedactualstartdate
                ,task_suggestedactualfinishdate task_suggestedactualfinishdate
                ,task_suggesteddurationfactor task_suggesteddurationfactor
                ,task_suggested_percentcomplete task_suggested_percentcomplete
                ,task_suggestedconstraintdate task_suggestedconstraintdate
                ,task_suggestedonedate task_suggestedonedate
                ,task_suggestedby task_suggestedby
                ,event_id event_id
                ,predecessors predecessors
                ,actual_duration actual_duration
                ,baseline_duration baseline_duration
                ,duration duration
                ,suggested_duration suggested_duration
                ,finish_variance finish_variance
                ,start_variance start_variance
                ,freeslack freeslack
                ,total_slack total_slack
                ,approvalrequired_forcompletion approvalrequired_forcompletion
                ,approvalrequired_forchanges approvalrequired_forchanges
                ,iscritical iscritical
                ,isdraft isdraft
                ,ismilestone ismilestone
                ,isrollup isrollup
                ,ismanuallyscheduled ismanuallyscheduled
                ,isawaitingapproval isawaitingapproval
                ,process_name process_name
                ,process_number process_number
                ,resource_lastlogindate resource_lastlogindate
                ,taskmanager_lastlogindate taskmanager_lastlogindate
                ,task_mostrecentnote task_mostrecentnote
                ,percent_complete percent_complete
                ,baselineassigned_resources baselineassigned_resources
                ,scheduling_mode scheduling_mode
                ,baseline_saveddate baseline_saveddate
                ,work work
                ,actual_work actual_work
                ,remaining_work remaining_work
                ,baseline_work baseline_work
                ,work_variance work_variance
                ,cost cost
                ,actual_cost actual_cost
                ,remaining_cost remaining_cost
                ,baseline_cost baseline_cost
                ,cost_variance cost_variance
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_taskactivity_fact_1835 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
