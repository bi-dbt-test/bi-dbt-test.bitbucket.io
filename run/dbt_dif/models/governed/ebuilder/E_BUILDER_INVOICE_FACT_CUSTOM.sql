
  create or replace  view datalake_dev.governed.E_BUILDER_INVOICE_FACT_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_invoice_fact_custom_1857)
SELECT account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,id id
                ,invoice_id invoice_id
                ,customfieldvariablename customfieldvariablename
                ,customfieldname customfieldname
                ,customfieldvalue customfieldvalue
                ,loadgroup loadgroup
                ,partkey partkey
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_invoice_fact_custom_1857 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
