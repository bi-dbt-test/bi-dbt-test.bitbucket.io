
  create or replace  view datalake_dev.governed.E_BUILDER_EVENTMEETINGMINUTES_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_eventmeetingminutes_fact_1824)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,events_id events_id
                ,meetingminute_id meetingminute_id
                ,meetingitem_id meetingitem_id
                ,meetingitem_discussion_id meetingitem_discussion_id
                ,meeting_number meeting_number
                ,startdate startdate
                ,enddate enddate
                ,meetingitem_discussion meetingitem_discussion
                ,discussion_enteredby discussion_enteredby
                ,meetingitem_itemnumber meetingitem_itemnumber
                ,meetingitem_status meetingitem_status
                ,meetingitem_topic meetingitem_topic
                ,actionitem_forms actionitem_forms
                ,discussion_entereddate discussion_entereddate
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_eventmeetingminutes_fact_1824 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
