
  create or replace  view datalake_dev.governed.E_BUILDER_PORTAL_DIM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_portal_dim_1826)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,accountisactive accountisactive
                ,portalactive portalactive
                ,portaldeleted portaldeleted
                ,portal_description portal_description
                ,portal_street portal_street
                ,portal_city portal_city
                ,portal_state portal_state
                ,portal_zip portal_zip
                ,portal_country portal_country
                ,enable_medassets_integration enable_medassets_integration
                ,webcam_url webcam_url
                ,portal_startdate portal_startdate
                ,portal_targetdate portal_targetdate
                ,project_status project_status
                ,project_statusdescription project_statusdescription
                ,ismasterproject ismasterproject
                ,site_admin site_admin
                ,mostrecent_projectnote mostrecent_projectnote
                ,portal_datecreated portal_datecreated
                ,portal_lastmodifiedby portal_lastmodifiedby
                ,portal_lastmodifieddate portal_lastmodifieddate
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_portal_dim_1826 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
