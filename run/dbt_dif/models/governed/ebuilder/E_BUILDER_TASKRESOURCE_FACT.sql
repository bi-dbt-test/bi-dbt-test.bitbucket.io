
  create or replace  view datalake_dev.governed.E_BUILDER_TASKRESOURCE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_taskresource_fact_1834)
SELECT task_id task_id
                ,account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,task_name task_name
                ,task_description task_description
                ,task_sequencenumber task_sequencenumber
                ,task_outlinenumber task_outlinenumber
                ,task_outlinelevel task_outlinelevel
                ,task_createdby task_createdby
                ,task_createddate task_createddate
                ,task_type task_type
                ,iscritical iscritical
                ,isdraft isdraft
                ,ismilestone ismilestone
                ,isrollup isrollup
                ,ismanuallyscheduled ismanuallyscheduled
                ,task_constraint_type task_constraint_type
                ,task_constraintdate task_constraintdate
                ,task_startdate task_startdate
                ,task_finishdate task_finishdate
                ,task_earlystartdate task_earlystartdate
                ,task_earlyfinishdate task_earlyfinishdate
                ,task_latestartdate task_latestartdate
                ,task_latefinishdate task_latefinishdate
                ,task_durationfactor task_durationfactor
                ,task_actualstartdate task_actualstartdate
                ,task_actualfinishdate task_actualfinishdate
                ,baseline_saveddate baseline_saveddate
                ,task_actualdurationfactor task_actualdurationfactor
                ,task_baselinestartdate task_baselinestartdate
                ,task_baselinefinishdate task_baselinefinishdate
                ,task_baselinedurationfactor task_baselinedurationfactor
                ,baseline_percentcomplete baseline_percentcomplete
                ,baseline_savedby baseline_savedby
                ,master_activity_id master_activity_id
                ,duration_factor duration_factor
                ,actual_duration_factor actual_duration_factor
                ,baseline_duration_factor baseline_duration_factor
                ,actual_duration actual_duration
                ,baseline_duration baseline_duration
                ,duration duration
                ,finish_variance finish_variance
                ,start_variance start_variance
                ,freeslack freeslack
                ,total_slack total_slack
                ,master_tasknumber master_tasknumber
                ,master_taskname master_taskname
                ,baselineassigned_resources baselineassigned_resources
                ,predecessors predecessors
                ,process_name process_name
                ,process_number process_number
                ,task_mostrecentnote task_mostrecentnote
                ,event_id event_id
                ,scheduling_mode scheduling_mode
                ,work work
                ,actualwork actualwork
                ,remaining_work remaining_work
                ,baseline_work baseline_work
                ,work_variance work_variance
                ,cost cost
                ,actual_cost actual_cost
                ,remaining_cost remaining_cost
                ,baseline_cost baseline_cost
                ,cost_variance cost_variance
                ,approvedinvoice_cost approvedinvoice_cost
                ,remaininginvoice_cost remaininginvoice_cost
                ,planned_cost planned_cost
                ,planned_work planned_work
                ,approvedinvoice_work approvedinvoice_work
                ,remaininginvoice_work remaininginvoice_work
                ,estwork_atcompletion estwork_atcompletion
                ,estcost_atcompletion estcost_atcompletion
                ,assigned_resources assigned_resources
                ,budgetlineitem_id budgetlineitem_id
                ,percent_complete percent_complete
                ,calendar calendar
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_taskresource_fact_1834 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
