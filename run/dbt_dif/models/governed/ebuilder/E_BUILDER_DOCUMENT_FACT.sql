
  create or replace  view datalake_dev.governed.E_BUILDER_DOCUMENT_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_document_fact_1858)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,file_id file_id
                ,file_path file_path
                ,document_file_name document_file_name
                ,redline_count redline_count
                ,version_number version_number
                ,file_type file_type
                ,file_description file_description
                ,document_lastmodifiedby document_lastmodifiedby
                ,document_lastmodifieddate document_lastmodifieddate
                ,document_uploadeddate document_uploadeddate
                ,object_id object_id
                ,is_tagged is_tagged
                ,folder_name folder_name
                ,file_size file_size
                ,document_type document_type
                ,file_uploadedby file_uploadedby
                ,company_name company_name
                ,file_lastmodifiedby file_lastmodifiedby
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_document_fact_1858 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
