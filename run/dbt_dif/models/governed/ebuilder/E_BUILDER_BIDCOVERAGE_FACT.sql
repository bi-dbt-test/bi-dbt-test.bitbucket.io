
  create or replace  view datalake_dev.governed.E_BUILDER_BIDCOVERAGE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_bidcoverage_fact_1862)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,bid_invitation_id bid_invitation_id
                ,bidpackage_id bidpackage_id
                ,not_invited not_invited
                ,not_accepted not_accepted
                ,not_declined not_declined
                ,not_pending not_pending
                ,not_awarded not_awarded
                ,no_bids no_bids
                ,bidpackage_name bidpackage_name
                ,bidpackage_description bidpackage_description
                ,bidpackage_createdby bidpackage_createdby
                ,bidpackage_createddate bidpackage_createddate
                ,bidpackage_lastmodifiedby bidpackage_lastmodifiedby
                ,bidpackage_lastmodifieddate bidpackage_lastmodifieddate
                ,construction_code construction_code
                ,construction_code_description construction_code_description
                ,division division
                ,division_description division_description
                ,codes_bidding_status codes_bidding_status
                ,bidpackage_responsedate bidpackage_responsedate
                ,bidpackage_bidduedate bidpackage_bidduedate
                ,bidder_invited_date bidder_invited_date
                ,bidpackage_startdate bidpackage_startdate
                ,bidpackage_tentative_awarddate bidpackage_tentative_awarddate
                ,bidpackage_tentative_workfinsihdate bidpackage_tentative_workfinsihdate
                ,bidpackage_hardbid_cutoffdate bidpackage_hardbid_cutoffdate
                ,bidpackage_actualbid_opendate bidpackage_actualbid_opendate
                ,invitation_contact invitation_contact
                ,bid_invitationby bid_invitationby
                ,reopen_requests reopen_requests
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_bidcoverage_fact_1862 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
