
  create or replace  view datalake_dev.governed.E_BUILDER_COMMITMENT_FACT_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_commitment_fact_custom_1845)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,commitment_id commitment_id
                ,customfieldvariablename customfieldvariablename
                ,customfieldname customfieldname
                ,customfieldvaluekey customfieldvaluekey
                ,customfieldvalue customfieldvalue
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_commitment_fact_custom_1845 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
