
  create or replace  view datalake_dev.governed.E_BUILDER_COMMITMENTITEM_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_commitmentitem_fact_1846)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,commitmentitem_id commitmentitem_id
                ,account_code account_code
                ,accountcode_withdescription accountcode_withdescription
                ,segment1_code segment1_code
                ,segment1_description segment1_description
                ,segment2_code segment2_code
                ,segment2_description segment2_description
                ,segment3_code segment3_code
                ,segment3_description segment3_description
                ,segment4_code segment4_code
                ,segment4_description segment4_description
                ,segment5_code segment5_code
                ,segment5_description segment5_description
                ,segment6_code segment6_code
                ,segment6_description segment6_description
                ,commitment_id commitment_id
                ,commitment_number commitment_number
                ,commitment_status commitment_status
                ,company_name company_name
                ,company_number company_number
                ,commitment_createdby commitment_createdby
                ,commitment_createddate commitment_createddate
                ,commitment_approveby commitment_approveby
                ,commitment_approvedbydate commitment_approvedbydate
                ,commitment_lastmodifiedby commitment_lastmodifiedby
                ,commitment_lastmodifieddate commitment_lastmodifieddate
                ,commitment_voidedby commitment_voidedby
                ,commitment_voideddate commitment_voideddate
                ,commitment_submittedforapprovalby commitment_submittedforapprovalby
                ,commitment_submittedforapprovaldate commitment_submittedforapprovaldate
                ,commitment_itemnumber commitment_itemnumber
                ,commitment_itemdescription commitment_itemdescription
                ,amount amount
                ,original_contractvalue original_contractvalue
                ,approved_changes approved_changes
                ,current_contractvalue current_contractvalue
                ,pending_changes pending_changes
                ,projected_changes projected_changes
                ,projected_contractvalue projected_contractvalue
                ,actuals_received actuals_received
                ,actuals_approved actuals_approved
                ,actuals_paid actuals_paid
                ,netactuals_approved netactuals_approved
                ,netactuals_paid netactuals_paid
                ,actuals_approvedqty actuals_approvedqty
                ,actuals_paidqty actuals_paidqty
                ,actuals_receivedqty actuals_receivedqty
                ,approved_changesqty approved_changesqty
                ,amount_retained amount_retained
                ,amountretained_backup amountretained_backup
                ,amountretained_paid amountretained_paid
                ,commitmentitem_createdate commitmentitem_createdate
                ,commitment_changeitemid commitment_changeitemid
                ,commitment_change commitment_change
                ,commitment_changedate commitment_changedate
                ,mastercommitment_changeitemid mastercommitment_changeitemid
                ,budget_id budget_id
                ,budget_amount budget_amount
                ,user_givenname user_givenname
                ,user_lastname user_lastname
                ,user_jobtitle user_jobtitle
                ,business_type business_type
                ,user_number user_number
                ,funding_rule funding_rule
                ,commitmentcontrol commitmentcontrol
                ,originalcommitment_qty originalcommitment_qty
                ,pendingchanges_qty pendingchanges_qty
                ,projectedchanges_qty projectedchanges_qty
                ,projectedcommitment_qty projectedcommitment_qty
                ,quantity quantity
                ,retainagepercent retainagepercent
                ,retainagereleased retainagereleased
                ,isallowance isallowance
                ,unitcost unitcost
                ,unitofmeasure unitofmeasure
                ,currentcontractqty currentcontractqty
                ,currentretainageheld currentretainageheld
                ,approvedstoredmaterials approvedstoredmaterials
                ,retainagereleased_paid retainagereleased_paid
                ,retainageheld_paid retainageheld_paid
                ,equipment_itemname equipment_itemname
                ,equipment_itemnumber equipment_itemnumber
                ,equipment_itemcost equipment_itemcost
                ,equipment_itemqty equipment_itemqty
                ,equipment_item_subtotal equipment_item_subtotal
                ,equipment_item_tax equipment_item_tax
                ,equipment_freight_subtotal equipment_freight_subtotal
                ,equipment_item_freighttax equipment_item_freighttax
                ,equipment_item_install_subtotal equipment_item_install_subtotal
                ,equipment_item_extendedcost equipment_item_extendedcost
                ,equipment_item_totaltax equipment_item_totaltax
                ,equipment_item_totalconfig equipment_item_totalconfig
                ,equipment_item_vendor equipment_item_vendor
                ,mastercommitmentitemnumber mastercommitmentitemnumber
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_commitmentitem_fact_1846 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
