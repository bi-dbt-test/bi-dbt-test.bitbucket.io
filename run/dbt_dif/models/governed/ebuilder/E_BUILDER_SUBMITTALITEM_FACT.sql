
  create or replace  view datalake_dev.governed.E_BUILDER_SUBMITTALITEM_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_submittalitem_fact_1839)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,submittal_item_id submittal_item_id
                ,submittal_package_id submittal_package_id
                ,is_submittal_package is_submittal_package
                ,submittal_package_name submittal_package_name
                ,submittal_package_description submittal_package_description
                ,submittal_package_number submittal_package_number
                ,submittal_package_revision_number submittal_package_revision_number
                ,submittal_package_is_mostrecentrevision submittal_package_is_mostrecentrevision
                ,submittal_package_trade submittal_package_trade
                ,submittal_package_status submittal_package_status
                ,submittal_package_createddate submittal_package_createddate
                ,submittal_package_lastmodifieddate submittal_package_lastmodifieddate
                ,submittal_package_closeddate submittal_package_closeddate
                ,csicode csicode
                ,parent_submittal_package_id parent_submittal_package_id
                ,mostrecent_submittal_package_id mostrecent_submittal_package_id
                ,submittal_package_createdby submittal_package_createdby
                ,submittal_package_lastmodifiedby submittal_package_lastmodifiedby
                ,submittal_package_coordinator submittal_package_coordinator
                ,submittal_package_heldby submittal_package_heldby
                ,submittal_package_most_recentcomment submittal_package_most_recentcomment
                ,submittal_item_number submittal_item_number
                ,submittal_item_specsection submittal_item_specsection
                ,submittal_item_subsection submittal_item_subsection
                ,submittal_item_revisionnumber submittal_item_revisionnumber
                ,submittal_item_title submittal_item_title
                ,submittal_item_description submittal_item_description
                ,submittal_item_status submittal_item_status
                ,submittal_status submittal_status
                ,status_prefix status_prefix
                ,submittal_item_priority submittal_item_priority
                ,submittal_category submittal_category
                ,responsible_subcontractor responsible_subcontractor
                ,submittal_item_createddate submittal_item_createddate
                ,submittal_item_lastmodifieddate submittal_item_lastmodifieddate
                ,submittal_item_duedate submittal_item_duedate
                ,submittal_item_receiveddate submittal_item_receiveddate
                ,submit_forreview_duedate submit_forreview_duedate
                ,forreview_submitteddate forreview_submitteddate
                ,review_complete_duedate review_complete_duedate
                ,review_completeddate review_completeddate
                ,consultant_respondbydate consultant_respondbydate
                ,consultant_review_completeddate consultant_review_completeddate
                ,return_duedate return_duedate
                ,returned_date returned_date
                ,delivery_duedate delivery_duedate
                ,delivered_date delivered_date
                ,closeddate closeddate
                ,forwardedtoconsultant_date forwardedtoconsultant_date
                ,gc_reviewcomplete_duedate gc_reviewcomplete_duedate
                ,senttosubcontractor_date senttosubcontractor_date
                ,senttogc_date senttogc_date
                ,submittal_item_createdby submittal_item_createdby
                ,submittal_item_lastmodifiedby submittal_item_lastmodifiedby
                ,submittal_item_forreviewby submittal_item_forreviewby
                ,submittal_item_forwardedby submittal_item_forwardedby
                ,submittal_item_heldby submittal_item_heldby
                ,commitment_id commitment_id
                ,parent_submittal_item_id parent_submittal_item_id
                ,is_mostrecentrevision is_mostrecentrevision
                ,commitment_number commitment_number
                ,submittal_item_accepted submittal_item_accepted
                ,submittal_item_declined submittal_item_declined
                ,mostrecent_statuschangeby mostrecent_statuschangeby
                ,mostrecent_statuschangedate mostrecent_statuschangedate
                ,submittal_item_mostrecent_comment submittal_item_mostrecent_comment
                ,submittal_item_coordinator submittal_item_coordinator
                ,reviewcount reviewcount
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_submittalitem_fact_1839 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
