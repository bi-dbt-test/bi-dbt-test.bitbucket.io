
  create or replace  view datalake_dev.governed.E_BUILDER_SCHEDULE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_schedule_fact_1817)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,schedule_pk schedule_pk
                ,schedule_startdate schedule_startdate
                ,schedule_finishdate schedule_finishdate
                ,hoursperday hoursperday
                ,hoursperweek hoursperweek
                ,dayspermonth dayspermonth
                ,schedule_datecreated schedule_datecreated
                ,schedule_lastmodifieddate schedule_lastmodifieddate
                ,baseline_startdate baseline_startdate
                ,baseline_finishdate baseline_finishdate
                ,actual_startdate actual_startdate
                ,actual_finishdate actual_finishdate
                ,baseline_duration baseline_duration
                ,baseline_durationuom baseline_durationuom
                ,baseline_durationunits baseline_durationunits
                ,actual_duration actual_duration
                ,actual_durationuom actual_durationuom
                ,actual_durationunits actual_durationunits
                ,duration duration
                ,durationuom durationuom
                ,durationunits durationunits
                ,finish_variance finish_variance
                ,finish_varianceeuom finish_varianceeuom
                ,finish_varianceunits finish_varianceunits
                ,start_variance start_variance
                ,start_varianceuom start_varianceuom
                ,start_varianceunits start_varianceunits
                ,work work
                ,baseline_work baseline_work
                ,work_variance work_variance
                ,cost cost
                ,baseline_cost baseline_cost
                ,cost_variance cost_variance
                ,assigned_resources assigned_resources
                ,baseline_assignedresources baseline_assignedresources
                ,approved_invoicecost approved_invoicecost
                ,remaining_invoicecost remaining_invoicecost
                ,numberofactivities_notstarted numberofactivities_notstarted
                ,numberofactivities_inprogress numberofactivities_inprogress
                ,numberofactivities_completed numberofactivities_completed
                ,schedule_manager schedule_manager
                ,schedule_managerrole schedule_managerrole
                ,schedule_mostrecentnote schedule_mostrecentnote
                ,calculate_percentcomplete calculate_percentcomplete
                ,calendar calendar
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_schedule_fact_1817 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
