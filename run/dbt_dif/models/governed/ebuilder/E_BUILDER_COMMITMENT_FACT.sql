
  create or replace  view datalake_dev.governed.E_BUILDER_COMMITMENT_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_commitment_fact_1847)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,commitment_id commitment_id
                ,commitment_number commitment_number
                ,company_name company_name
                ,company_number company_number
                ,mastercommitment_id mastercommitment_id
                ,actuals_approved actuals_approved
                ,actuals_paid actuals_paid
                ,actuals_received actuals_received
                ,approved_changes approved_changes
                ,amount_retained amount_retained
                ,commitment_date commitment_date
                ,commitment_description commitment_description
                ,retainage_percent retainage_percent
                ,retainage_released retainage_released
                ,commitment_contact commitment_contact
                ,current_commitmentvalue current_commitmentvalue
                ,current_retainageheld current_retainageheld
                ,noticetoproceed_date noticetoproceed_date
                ,original_commitmentvalue original_commitmentvalue
                ,pending_changes pending_changes
                ,projected_changes projected_changes
                ,projected_commitmentvalue projected_commitmentvalue
                ,scopeofwork scopeofwork
                ,unitcostoption unitcostoption
                ,netactuals_approved netactuals_approved
                ,netactuals_paid netactuals_paid
                ,commitment_status commitment_status
                ,costcontroltolerance_percent costcontroltolerance_percent
                ,commitment_control commitment_control
                ,commitment_createdby commitment_createdby
                ,commitment_createddate commitment_createddate
                ,commitment_approveby commitment_approveby
                ,commitment_approvedbydate commitment_approvedbydate
                ,commitment_lastmodifieddate commitment_lastmodifieddate
                ,commitment_lastmodifiedby commitment_lastmodifiedby
                ,commitment_voideddate commitment_voideddate
                ,commitment_voidedby commitment_voidedby
                ,commitment_submittedforapprovaldate commitment_submittedforapprovaldate
                ,commitment_submittedforapprovalby commitment_submittedforapprovalby
                ,commitment_type commitment_type
                ,applicationforpayment_flag applicationforpayment_flag
                ,controllingprocess controllingprocess
                ,remainingtobepaid remainingtobepaid
                ,mastercommitment_number mastercommitment_number
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_commitment_fact_1847 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
