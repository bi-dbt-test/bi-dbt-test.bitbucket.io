
  create or replace  view datalake_dev.governed.E_BUILDER_BIDINVITATION_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_bidinvitation_fact_1863)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,bid_invitation_id bid_invitation_id
                ,bidpackage_id bidpackage_id
                ,response_status response_status
                ,bid_status bid_status
                ,codes_bidding_status codes_bidding_status
                ,bidpackage_name bidpackage_name
                ,bidpackage_responsedate bidpackage_responsedate
                ,bidpackage_bidduedate bidpackage_bidduedate
                ,bidder_invited_date bidder_invited_date
                ,invitation_contact invitation_contact
                ,bid_invitationby bid_invitationby
                ,bid_reopen_request bid_reopen_request
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_bidinvitation_fact_1863 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
