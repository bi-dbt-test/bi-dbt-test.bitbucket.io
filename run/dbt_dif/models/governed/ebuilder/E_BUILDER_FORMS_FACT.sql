
  create or replace  view datalake_dev.governed.E_BUILDER_FORMS_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_forms_fact_1821)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,form_id form_id
                ,form_type form_type
                ,isworkflow isworkflow
                ,interpretas interpretas
                ,form_counter form_counter
                ,formcounter_prefix formcounter_prefix
                ,form_subject form_subject
                ,workdayend workdayend
                ,workdays workdays
                ,workdaystart workdaystart
                ,form_duedate form_duedate
                ,form_closeddate form_closeddate
                ,form_createddate form_createddate
                ,form_author form_author
                ,form_heldby form_heldby
                ,form_owner form_owner
                ,isdraft isdraft
                ,author_company author_company
                ,heldby_company heldby_company
                ,priority priority
                ,exceptions exceptions
                ,exceptioncounts exceptioncounts
                ,completion_variance completion_variance
                ,ccompletion_variance_rounded ccompletion_variance_rounded
                ,form_statusdays form_statusdays
                ,form_status form_status
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_forms_fact_1821 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
