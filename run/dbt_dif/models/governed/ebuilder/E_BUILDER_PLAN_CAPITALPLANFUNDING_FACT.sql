
  create or replace  view datalake_dev.governed.E_BUILDER_PLAN_CAPITALPLANFUNDING_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_plan_capitalplanfunding_fact_1829)
SELECT account_id account_id
                ,account_name account_name
                ,id id
                ,capitalplan_month capitalplan_month
                ,amount_thisperiod amount_thisperiod
                ,actuals_plusprojections actuals_plusprojections
                ,display_order display_order
                ,funding_sourcename funding_sourcename
                ,capitalplan_funding_createddate capitalplan_funding_createddate
                ,capitalplan_funding_createdby capitalplan_funding_createdby
                ,capitalplan_funding_lastmodifieddate capitalplan_funding_lastmodifieddate
                ,capitalplan_funding_lastmodifiedby capitalplan_funding_lastmodifiedby
                ,capitalplan_name capitalplan_name
                ,capitalplan_description capitalplan_description
                ,capitalplan_status capitalplan_status
                ,capitalplan_createddate capitalplan_createddate
                ,capitalplan_createdby capitalplan_createdby
                ,capitalplan_lastmodifieddate capitalplan_lastmodifieddate
                ,capitalplan_lastmodifiedby capitalplan_lastmodifiedby
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_plan_capitalplanfunding_fact_1829 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
