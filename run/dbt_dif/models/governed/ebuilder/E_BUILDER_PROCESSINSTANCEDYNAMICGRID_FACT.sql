
  create or replace  view datalake_dev.governed.E_BUILDER_PROCESSINSTANCEDYNAMICGRID_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_processinstancedynamicgrid_fact_1851)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,dynamicgrid_name dynamicgrid_name
                ,dynamicgrid_description dynamicgrid_description
                ,dynamicgrid_active dynamicgrid_active
                ,dynamicgrid_createdby dynamicgrid_createdby
                ,dynamicgrid_createddate dynamicgrid_createddate
                ,dynamicgrid_lastmodifiedby dynamicgrid_lastmodifiedby
                ,dynamicgrid_lastmodifieddate dynamicgrid_lastmodifieddate
                ,dynamicgrid_variablename dynamicgrid_variablename
                ,entitytype entitytype
                ,processinstance_id processinstance_id
                ,process_name process_name
                ,process_prefix process_prefix
                ,counterprefix counterprefix
                ,instancecounter instancecounter
                ,customfield_name customfield_name
                ,customfieldvalue customfieldvalue
                ,displayorder displayorder
                ,rownumber rownumber
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_processinstancedynamicgrid_fact_1851 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
