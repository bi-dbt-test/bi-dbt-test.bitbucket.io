
  create or replace  view datalake_dev.governed.E_BUILDER_PROCESS_DATAHISTORY_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_process_datahistory_fact_1848)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,datafieldhistory_id datafieldhistory_id
                ,processinstance_id processinstance_id
                ,process_name process_name
                ,process_prefix process_prefix
                ,counterprefix counterprefix
                ,step_name step_name
                ,processinstance_status processinstance_status
                ,process_counter process_counter
                ,process_status process_status
                ,instancestepid instancestepid
                ,isactivestep isactivestep
                ,ispendingprocessing ispendingprocessing
                ,oldvalue oldvalue
                ,newvalue newvalue
                ,datafield_name datafield_name
                ,history_createddate history_createddate
                ,history_createdby history_createdby
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_process_datahistory_fact_1848 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
