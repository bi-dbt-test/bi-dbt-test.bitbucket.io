
  create or replace  view datalake_dev.governed.E_BUILDER_SUBMITTALITEM_REVIEW_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_submittalitem_review_fact_1840)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,submittal_item_id submittal_item_id
                ,reviewer reviewer
                ,review_requestedby review_requestedby
                ,review_requesteddate review_requesteddate
                ,review_duedate review_duedate
                ,review_completeddate review_completeddate
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_submittalitem_review_fact_1840 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
