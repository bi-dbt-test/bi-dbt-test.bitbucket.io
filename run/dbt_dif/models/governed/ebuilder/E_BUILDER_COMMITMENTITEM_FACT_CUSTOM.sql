
  create or replace  view datalake_dev.governed.E_BUILDER_COMMITMENTITEM_FACT_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_commitmentitem_fact_custom_1844)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,commitmentitem_id commitmentitem_id
                ,customfieldvariablename customfieldvariablename
                ,customfieldname customfieldname
                ,customfieldvaluekey customfieldvaluekey
                ,customfieldvalue customfieldvalue
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_commitmentitem_fact_custom_1844 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
