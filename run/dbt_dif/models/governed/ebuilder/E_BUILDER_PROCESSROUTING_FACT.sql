
  create or replace  view datalake_dev.governed.E_BUILDER_PROCESSROUTING_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_processrouting_fact_1819)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,instancestep_id instancestep_id
                ,processinstance_id processinstance_id
                ,process_name process_name
                ,process_prefix process_prefix
                ,counter_prefix counter_prefix
                ,processinstance_company processinstance_company
                ,current_step current_step
                ,sequencenumber sequencenumber
                ,responsible_actors responsible_actors
                ,step_name step_name
                ,step_description step_description
                ,instancestep_createdby instancestep_createdby
                ,instancestep_exitby instancestep_exitby
                ,instancestep_datecreated instancestep_datecreated
                ,instancestep_dateexit instancestep_dateexit
                ,instancestep_datedue instancestep_datedue
                ,step_variance step_variance
                ,step_age step_age
                ,step_type step_type
                ,created_actiontype created_actiontype
                ,exit_actiontype exit_actiontype
                ,stepworkday_datedue stepworkday_datedue
                ,process_status process_status
                ,processinstance_status processinstance_status
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_processrouting_fact_1819 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
