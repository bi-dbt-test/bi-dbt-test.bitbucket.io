
  create or replace  view datalake_dev.governed.E_BUILDER_FORMS_FACT_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_forms_fact_custom_1820)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,form_id form_id
                ,form_customdata_lastmodifieddate form_customdata_lastmodifieddate
                ,form_customdata_lastmodifiedby form_customdata_lastmodifiedby
                ,form_type form_type
                ,formfield_customfieldname formfield_customfieldname
                ,form_customdata form_customdata
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_forms_fact_custom_1820 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
