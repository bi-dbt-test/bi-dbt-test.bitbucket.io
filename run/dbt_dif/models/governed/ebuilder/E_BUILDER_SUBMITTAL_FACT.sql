
  create or replace  view datalake_dev.governed.E_BUILDER_SUBMITTAL_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_submittal_fact_1838)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,submittal_package_id submittal_package_id
                ,submittal_package_name submittal_package_name
                ,submittal_package_description submittal_package_description
                ,submittal_package_number submittal_package_number
                ,submittal_package_revision_number submittal_package_revision_number
                ,is_mostrecentrevision is_mostrecentrevision
                ,submittal_package_trade submittal_package_trade
                ,submittal_package_status submittal_package_status
                ,submittal_package_createddate submittal_package_createddate
                ,submittal_package_lastmodifieddate submittal_package_lastmodifieddate
                ,submittal_package_closeddate submittal_package_closeddate
                ,csicode csicode
                ,parentsubmittalpackageid parentsubmittalpackageid
                ,submittal_package_createdby submittal_package_createdby
                ,submittal_package_lastmodifiedby submittal_package_lastmodifiedby
                ,submittal_package_coordinator submittal_package_coordinator
                ,submittal_package_heldby submittal_package_heldby
                ,most_recentcomment most_recentcomment
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_submittal_fact_1838 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
