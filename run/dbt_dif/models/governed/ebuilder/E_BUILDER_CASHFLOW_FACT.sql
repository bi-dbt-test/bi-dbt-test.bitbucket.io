
  create or replace  view datalake_dev.governed.E_BUILDER_CASHFLOW_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_cashflow_fact_1849)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,cashflow_pk cashflow_pk
                ,cashflow_description cashflow_description
                ,formonth formonth
                ,isbaseline isbaseline
                ,cashflow_createddate cashflow_createddate
                ,cashflow_lastmodifieddate cashflow_lastmodifieddate
                ,cashflow_createdby cashflow_createdby
                ,cashflow_lastmodifiedby cashflow_lastmodifiedby
                ,ismostrecent ismostrecent
                ,cashflow_month cashflow_month
                ,projectedbudget projectedbudget
                ,amountthisperiod amountthisperiod
                ,actualsplusprojections actualsplusprojections
                ,projectedcommitments projectedcommitments
                ,estimateatcompletion estimateatcompletion
                ,budgetlineitem_id budgetlineitem_id
                ,account_code account_code
                ,accountcode_withdescription accountcode_withdescription
                ,segment1_code segment1_code
                ,segment1_description segment1_description
                ,segment2_code segment2_code
                ,segment2_description segment2_description
                ,segment3_code segment3_code
                ,segment3_description segment3_description
                ,segment4_code segment4_code
                ,segment4_description segment4_description
                ,segment5_code segment5_code
                ,segment5_description segment5_description
                ,segment6_code segment6_code
                ,segment6_description segment6_description
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_cashflow_fact_1849 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
