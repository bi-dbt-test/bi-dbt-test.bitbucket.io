
  create or replace  view datalake_dev.governed.E_BUILDER_INVOICEITEM_FACT_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_invoiceitem_fact_custom_1854)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,id id
                ,invoiceitemid invoiceitemid
                ,customfieldvariablename customfieldvariablename
                ,customfieldname customfieldname
                ,customfieldvalue customfieldvalue
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_invoiceitem_fact_custom_1854 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
