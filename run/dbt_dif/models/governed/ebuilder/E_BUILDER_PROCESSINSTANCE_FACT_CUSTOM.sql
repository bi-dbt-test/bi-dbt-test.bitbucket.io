
  create or replace  view datalake_dev.governed.E_BUILDER_PROCESSINSTANCE_FACT_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_processinstance_fact_custom_1852)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,processinstance_id processinstance_id
                ,process_id process_id
                ,process_name process_name
                ,process_prefix process_prefix
                ,counterprefix counterprefix
                ,processinstance_status processinstance_status
                ,datafield_id datafield_id
                ,datafield_name datafield_name
                ,datafield_value datafield_value
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_processinstance_fact_custom_1852 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
