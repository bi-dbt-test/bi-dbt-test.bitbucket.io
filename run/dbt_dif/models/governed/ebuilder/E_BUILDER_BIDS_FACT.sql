
  create or replace  view datalake_dev.governed.E_BUILDER_BIDS_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_bids_fact_1864)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,bid_detail_id bid_detail_id
                ,bid_id bid_id
                ,bidpackage_id bidpackage_id
                ,bidpackage_name bidpackage_name
                ,bidpackage_description bidpackage_description
                ,bidpackage_bidduedate bidpackage_bidduedate
                ,bidpackage_responsedate bidpackage_responsedate
                ,bid_timezone bid_timezone
                ,bid_documentfolder_name bid_documentfolder_name
                ,bid_documentfolder_description bid_documentfolder_description
                ,bid_instructions_file bid_instructions_file
                ,response_folder_name response_folder_name
                ,response_folder_description response_folder_description
                ,status status
                ,bidpackage_owner bidpackage_owner
                ,is_public is_public
                ,allow_bids_after_responsedate allow_bids_after_responsedate
                ,bidpackage_startdate bidpackage_startdate
                ,bidpackage_prebidmeetingdate bidpackage_prebidmeetingdate
                ,bidpackage_tentative_awarddate bidpackage_tentative_awarddate
                ,bidpackage_tentative_workfinsihdate bidpackage_tentative_workfinsihdate
                ,bidpackage_hardbid_cutoffdate bidpackage_hardbid_cutoffdate
                ,bidpackage_actualbid_opendate bidpackage_actualbid_opendate
                ,bidsqft bidsqft
                ,allow_adjustments allow_adjustments
                ,is_prebid_meetingrequired is_prebid_meetingrequired
                ,is_documentation_required is_documentation_required
                ,is_noticeofopportunity is_noticeofopportunity
                ,enableplanroom enableplanroom
                ,allow_electronicbids allow_electronicbids
                ,bidpackage_createdby bidpackage_createdby
                ,bidpackage_createddate bidpackage_createddate
                ,bidpackage_lastmodifiedby bidpackage_lastmodifiedby
                ,bidpackage_lastmodifieddate bidpackage_lastmodifieddate
                ,bid_qualifications bid_qualifications
                ,bid_status bid_status
                ,bid_createdby bid_createdby
                ,bid_lastmodifiedby bid_lastmodifiedby
                ,bid_submittedby bid_submittedby
                ,bid_invitationby bid_invitationby
                ,bid_createddate bid_createddate
                ,bid_lastmodifieddate bid_lastmodifieddate
                ,bid_submitteddate bid_submitteddate
                ,bid_invitation_date bid_invitation_date
                ,bid_item_id bid_item_id
                ,original_unit_cost original_unit_cost
                ,original_total_cost original_total_cost
                ,adjusted_unit_cost adjusted_unit_cost
                ,adjusted_total_cost adjusted_total_cost
                ,customfield1_value customfield1_value
                ,customfield2_value customfield2_value
                ,customfield3_value customfield3_value
                ,bid_section_name bid_section_name
                ,bid_section_type bid_section_type
                ,bid_section_totalcost bid_section_totalcost
                ,bid_item_number bid_item_number
                ,display_order display_order
                ,bid_item_description bid_item_description
                ,bid_item_longdescription bid_item_longdescription
                ,bid_item_spec_reference bid_item_spec_reference
                ,bid_item_partnumber bid_item_partnumber
                ,bid_item_quantity bid_item_quantity
                ,bid_item_uom bid_item_uom
                ,bid_item_estimated_unit_cost bid_item_estimated_unit_cost
                ,bid_item_estimated_total_cost bid_item_estimated_total_cost
                ,biditem_createdby biditem_createdby
                ,biditem_datecreated biditem_datecreated
                ,biditem_lastmodifiedby biditem_lastmodifiedby
                ,biditem_lastmodifieddate biditem_lastmodifieddate
                ,budgetlineitem_id budgetlineitem_id
                ,construction_code construction_code
                ,invitation_response invitation_response
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_bids_fact_1864 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
