
  create or replace  view datalake_dev.governed.E_BUILDER_PROCESSINSTANCE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_processinstance_fact_1853)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,processinstance_id processinstance_id
                ,process_name process_name
                ,process_prefix process_prefix
                ,counterprefix counterprefix
                ,processinstance_company processinstance_company
                ,current_step current_step
                ,processinstance_status processinstance_status
                ,process_counter process_counter
                ,process_status process_status
                ,process_subject process_subject
                ,createdby_company createdby_company
                ,responsible_actors responsible_actors
                ,processinstance_createddate processinstance_createddate
                ,processinstance_duedate processinstance_duedate
                ,processinstance_finisheddate processinstance_finisheddate
                ,processinstance_step_duedate processinstance_step_duedate
                ,process_variance process_variance
                ,step_variance step_variance
                ,process_age process_age
                ,step_age step_age
                ,process_document process_document
                ,processinstance_createdby processinstance_createdby
                ,processinstance_acceptedby processinstance_acceptedby
                ,processinstance_updatedby processinstance_updatedby
                ,costintegration_type costintegration_type
                ,dynamicitem_commitment_id dynamicitem_commitment_id
                ,processinstance_submitteddate processinstance_submitteddate
                ,step_createddate step_createddate
                ,processinstance_submittedby processinstance_submittedby
                ,submittedby_company submittedby_company
                ,workday_duedate workday_duedate
                ,stepworkday_duedate stepworkday_duedate
                ,processinstance_lastmodifieddate processinstance_lastmodifieddate
                ,instancestepid instancestepid
                ,isactivestep isactivestep
                ,ispendingprocessing ispendingprocessing
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_processinstance_fact_1853 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
