
  create or replace  view datalake_dev.governed.E_BUILDER_INVOICEITEM_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_invoiceitem_fact_1856)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,invoice_type invoice_type
                ,id id
                ,invoice_id invoice_id
                ,invoiceitem_id invoiceitem_id
                ,invoice_itemnumber invoice_itemnumber
                ,invoice_description invoice_description
                ,invoice_amount invoice_amount
                ,amount_retained amount_retained
                ,amount_lessretainage amount_lessretainage
                ,amount_thisperiod amount_thisperiod
                ,invoice_createddate invoice_createddate
                ,invoice_lastmodifieddate invoice_lastmodifieddate
                ,frompreviousapplication frompreviousapplication
                ,materialspresentlystored materialspresentlystored
                ,netretainage_amount netretainage_amount
                ,retainage_percent retainage_percent
                ,retainage_released retainage_released
                ,quantity quantity
                ,quantitythisperiod quantitythisperiod
                ,commitmentitem_scheduledvalue commitmentitem_scheduledvalue
                ,funding_rule funding_rule
                ,invoice_status invoice_status
                ,invoice_createdby invoice_createdby
                ,invoice_lastmodifiedby invoice_lastmodifiedby
                ,invoice_approveddate invoice_approveddate
                ,invoice_approvedby invoice_approvedby
                ,invoice_voidedby invoice_voidedby
                ,invoice_voideddate invoice_voideddate
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_invoiceitem_fact_1856 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
