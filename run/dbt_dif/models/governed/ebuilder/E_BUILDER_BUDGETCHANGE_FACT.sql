
  create or replace  view datalake_dev.governed.E_BUILDER_BUDGETCHANGE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_budgetchange_fact_1842)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,budget_id budget_id
                ,budgetchange_reasoncode budgetchange_reasoncode
                ,reasoncode_appliesto reasoncode_appliesto
                ,reasoncode_description reasoncode_description
                ,budgetchange_number budgetchange_number
                ,budgetchange_description budgetchange_description
                ,statusvalue statusvalue
                ,amount amount
                ,dateofchange dateofchange
                ,budgetchange_datecreated budgetchange_datecreated
                ,budgetchange_submittedfor_approvaldate budgetchange_submittedfor_approvaldate
                ,budgetchange_approvedbydate budgetchange_approvedbydate
                ,budgetchange_datevoided budgetchange_datevoided
                ,budgetchange_lastmodifieddate budgetchange_lastmodifieddate
                ,submittedfor_approvalby submittedfor_approvalby
                ,budgetchange_createdby budgetchange_createdby
                ,budgetchange_approvedby budgetchange_approvedby
                ,budgetchange_lastmodifiedby budgetchange_lastmodifiedby
                ,budgetchange_voidedby budgetchange_voidedby
                ,controllingprocess controllingprocess
                ,adjustment_amount adjustment_amount
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_budgetchange_fact_1842 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
