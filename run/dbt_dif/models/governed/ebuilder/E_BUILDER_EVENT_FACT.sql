
  create or replace  view datalake_dev.governed.E_BUILDER_EVENT_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_event_fact_1822)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,events_id events_id
                ,event_subject event_subject
                ,event_location event_location
                ,event_description event_description
                ,event_priority event_priority
                ,is_public is_public
                ,is_recurrence is_recurrence
                ,recurrence_rule recurrence_rule
                ,reminder_offset reminder_offset
                ,event_startdate event_startdate
                ,event_enddate event_enddate
                ,event_createdby event_createdby
                ,event_createddate event_createddate
                ,event_lastmodifieddate event_lastmodifieddate
                ,is_alldayevent is_alldayevent
                ,is_deleted is_deleted
                ,event_sequencenumber event_sequencenumber
                ,event_recur_enddate event_recur_enddate
                ,meeting_type meeting_type
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_event_fact_1822 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
