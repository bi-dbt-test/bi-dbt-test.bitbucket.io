
  create or replace  view datalake_dev.governed.E_BUILDER_INVOICE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_invoice_fact_1855)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,invoice_type invoice_type
                ,id id
                ,masterinvoice_number masterinvoice_number
                ,commitment_id commitment_id
                ,company_name company_name
                ,invoice_description invoice_description
                ,invoice_amount invoice_amount
                ,invoice_number invoice_number
                ,amount_retained amount_retained
                ,invoice_approvedbydate invoice_approvedbydate
                ,invoice_createddate invoice_createddate
                ,invoice_duedate invoice_duedate
                ,invoice_paiddate invoice_paiddate
                ,invoice_receiveddate invoice_receiveddate
                ,invoice_lastmodifieddate invoice_lastmodifieddate
                ,netretainage_amount netretainage_amount
                ,retainage_released retainage_released
                ,amount_lessretainage amount_lessretainage
                ,scopeofwork scopeofwork
                ,invoice_status invoice_status
                ,invoice_submittedforapprovaldate invoice_submittedforapprovaldate
                ,invoice_voideddate invoice_voideddate
                ,invoice_submittedforapprovalby invoice_submittedforapprovalby
                ,invoice_lastmodifiedby invoice_lastmodifiedby
                ,invoice_createdby invoice_createdby
                ,invoice_approvedby invoice_approvedby
                ,invoice_voidedby invoice_voidedby
                ,controllingprocess controllingprocess
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_invoice_fact_1855 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
