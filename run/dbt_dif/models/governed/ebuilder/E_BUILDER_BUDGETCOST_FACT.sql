
  create or replace  view datalake_dev.governed.E_BUILDER_BUDGETCOST_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_budgetcost_fact_1815)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,budgetlineitem_id budgetlineitem_id
                ,budget_id budget_id
                ,accountcode accountcode
                ,segment1_code segment1_code
                ,segment1_description segment1_description
                ,segment2_code segment2_code
                ,segment2_description segment2_description
                ,segment3_code segment3_code
                ,segment3_description segment3_description
                ,segment4_code segment4_code
                ,segment4_description segment4_description
                ,segment5_code segment5_code
                ,segment5_description segment5_description
                ,segment6_code segment6_code
                ,segment6_description segment6_description
                ,budgetline_description budgetline_description
                ,budgetline_createdby budgetline_createdby
                ,budgetline_datecreated budgetline_datecreated
                ,budgetline_lastmodifiedby budgetline_lastmodifiedby
                ,budgetline_lastmodifieddate budgetline_lastmodifieddate
                ,budget_createdby budget_createdby
                ,budget_datecreated budget_datecreated
                ,budget_lastmodifiedby budget_lastmodifiedby
                ,budget_lastmodifieddate budget_lastmodifieddate
                ,budget_approvedby budget_approvedby
                ,budget_approvedbydate budget_approvedbydate
                ,budgetstatusvalue budgetstatusvalue
                ,budgetstatus budgetstatus
                ,originalbudget originalbudget
                ,approvedbudgetchanges approvedbudgetchanges
                ,currentbudget currentbudget
                ,pendingbudgetchanges pendingbudgetchanges
                ,projectedbudgetchanges projectedbudgetchanges
                ,projectedbudget projectedbudget
                ,originalcommitments originalcommitments
                ,pendingcommitments pendingcommitments
                ,approvedcommitmentchanges approvedcommitmentchanges
                ,noncommitmentcosts noncommitmentcosts
                ,currentcommitments currentcommitments
                ,pendingcommitmentchanges pendingcommitmentchanges
                ,projectedcommitmentchanges projectedcommitmentchanges
                ,projectedcommitments projectedcommitments
                ,actualsreceived actualsreceived
                ,actualsapproved actualsapproved
                ,actualspaid actualspaid
                ,actualcosttocomplete actualcosttocomplete
                ,percentcostcomplete percentcostcomplete
                ,currentoverunder currentoverunder
                ,projectedoverunder projectedoverunder
                ,allocatedforecast allocatedforecast
                ,unallocatedforecast unallocatedforecast
                ,estimateatcompletion estimateatcompletion
                ,forecastedoverunder forecastedoverunder
                ,amountretained amountretained
                ,retainagereleased retainagereleased
                ,currentretainageheld currentretainageheld
                ,netactualspaid netactualspaid
                ,netactualsapproved netactualsapproved
                ,totalresourcecosts totalresourcecosts
                ,remainingresourcecosts remainingresourcecosts
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_budgetcost_fact_1815 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
