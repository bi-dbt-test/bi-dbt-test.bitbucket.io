
  create or replace  view datalake_dev.governed.E_BUILDER_BUDGETCOST_FACT_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_budgetcost_fact_custom_1816)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,budgetlineitem_id budgetlineitem_id
                ,budget_id budget_id
                ,accountcode accountcode
                ,customvalue customvalue
                ,displayname displayname
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_budgetcost_fact_custom_1816 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
