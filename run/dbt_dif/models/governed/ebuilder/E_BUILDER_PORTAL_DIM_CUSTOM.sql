
  create or replace  view datalake_dev.governed.E_BUILDER_PORTAL_DIM_CUSTOM  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_portal_dim_custom_1825)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,customfieldvariablename customfieldvariablename
                ,customfieldname customfieldname
                ,customfieldvalue customfieldvalue
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_portal_dim_custom_1825 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
