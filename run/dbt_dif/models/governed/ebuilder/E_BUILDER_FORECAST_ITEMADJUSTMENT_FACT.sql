
  create or replace  view datalake_dev.governed.E_BUILDER_FORECAST_ITEMADJUSTMENT_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_forecast_itemadjustment_fact_1832)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,forecast_item_id forecast_item_id
                ,forecast_name forecast_name
                ,status status
                ,forecast_createddate forecast_createddate
                ,forecast_createdby forecast_createdby
                ,forecast_updateddate forecast_updateddate
                ,forecast_updatedby forecast_updatedby
                ,budgetlineitem_id budgetlineitem_id
                ,account_code account_code
                ,segment1_code segment1_code
                ,segment1_description segment1_description
                ,segment2_code segment2_code
                ,segment2_description segment2_description
                ,segment3_code segment3_code
                ,segment3_description segment3_description
                ,segment4_code segment4_code
                ,segment4_description segment4_description
                ,segment5_code segment5_code
                ,segment5_description segment5_description
                ,segment6_code segment6_code
                ,segment6_description segment6_description
                ,budgetline_description budgetline_description
                ,budgetline_datecreated budgetline_datecreated
                ,adjustment_description adjustment_description
                ,adjustment_assumption adjustment_assumption
                ,adjustment_amount adjustment_amount
                ,useunallocatedforecast useunallocatedforecast
                ,forecast_adjustment_datecreated forecast_adjustment_datecreated
                ,forecast_adjustment_createdby forecast_adjustment_createdby
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_forecast_itemadjustment_fact_1832 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
