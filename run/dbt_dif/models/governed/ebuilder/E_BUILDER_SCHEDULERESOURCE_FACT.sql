
  create or replace  view datalake_dev.governed.E_BUILDER_SCHEDULERESOURCE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_scheduleresource_fact_1818)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,assignment_id assignment_id
                ,task_id task_id
                ,task_name task_name
                ,task_description task_description
                ,task_sequencenumber task_sequencenumber
                ,resource_type resource_type
                ,resource_name resource_name
                ,resource_title resource_title
                ,resource_phone resource_phone
                ,resource_email resource_email
                ,resource_maxallocation resource_maxallocation
                ,resource_active resource_active
                ,resource_createddate resource_createddate
                ,resource_createdby resource_createdby
                ,resource_lastmodifiedby resource_lastmodifiedby
                ,resource_lastmodifieddate resource_lastmodifieddate
                ,resource_assignedunits resource_assignedunits
                ,resource_assignedcost resource_assignedcost
                ,resource_cost_variance resource_cost_variance
                ,resource_baseline_cost resource_baseline_cost
                ,resource_assigned_work resource_assigned_work
                ,resource_assigned_workuom resource_assigned_workuom
                ,resource_assigned_workunits resource_assigned_workunits
                ,resource_baseline_work resource_baseline_work
                ,resource_baseline_workuom resource_baseline_workuom
                ,resource_baseline_workunits resource_baseline_workunits
                ,resource_work_variance resource_work_variance
                ,resource_work_varianceuom resource_work_varianceuom
                ,resource_work_varianceunits resource_work_varianceunits
                ,resource_role_name resource_role_name
                ,approved_invoice_cost approved_invoice_cost
                ,remaining_invoice_cost remaining_invoice_cost
                ,budgetlineitem_id budgetlineitem_id
                ,planned_cost planned_cost
                ,planned_work planned_work
                ,approved_invoice_work approved_invoice_work
                ,remaining_invoice_work remaining_invoice_work
                ,estwork_atcompletion estwork_atcompletion
                ,estcost_atcompletion estcost_atcompletion
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_scheduleresource_fact_1818 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
