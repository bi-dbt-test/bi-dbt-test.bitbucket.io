
  create or replace  view datalake_dev.governed.E_BUILDER_COMMITMENTCHANGE_FACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.e_builder_commitmentchange_fact_1861)
SELECT account_id account_id
                ,account_name account_name
                ,portal_id portal_id
                ,portal_name portal_name
                ,commitmentchange_id commitmentchange_id
                ,commitment_id commitment_id
                ,commitmentchange_changenumber commitmentchange_changenumber
                ,commitmentchange_description commitmentchange_description
                ,commitmentchange_status commitmentchange_status
                ,commitmentchange_approvedbydate commitmentchange_approvedbydate
                ,mastercommitmentnumber mastercommitmentnumber
                ,dateofchange dateofchange
                ,commitmentchange_amount commitmentchange_amount
                ,changereason changereason
                ,allowance_reasoncode allowance_reasoncode
                ,commitmentchange_submittedforapprovaldate commitmentchange_submittedforapprovaldate
                ,commitmentchange_createddate commitmentchange_createddate
                ,commitmentchange_lastmodifieddate commitmentchange_lastmodifieddate
                ,commitmentchange_voideddate commitmentchange_voideddate
                ,commitmentchange__submittedforapprovalby commitmentchange__submittedforapprovalby
                ,commitmentchange_createdby commitmentchange_createdby
                ,commitmentchange_approvedby commitmentchange_approvedby
                ,commitmentchange_lastmodifiedby commitmentchange_lastmodifiedby
                ,commitmentchange_voidedby commitmentchange_voidedby
                ,controllingprocess controllingprocess
                ,adjustment_amount adjustment_amount
                ,loaddate loaddate
                
FROM datalake_dev.pre_governed.e_builder_commitmentchange_fact_1861 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
