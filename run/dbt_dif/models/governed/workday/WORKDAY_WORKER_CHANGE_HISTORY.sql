
  create or replace  view datalake_dev.governed.WORKDAY_WORKER_CHANGE_HISTORY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.workday_worker_change_history_3128)
SELECT Name Name
                ,Employee_ID Employee_ID
                ,Job_Title Job_Title
                ,Hire_Date Hire_Date
                ,Budget Budget
                ,Operating_Unit Operating_Unit
                ,Line_of_Business Line_of_Business
                ,Cost_Center Cost_Center
                ,Supervisory_Organization Supervisory_Organization
                ,GL_String GL_String
                ,Effective_Date Effective_Date
                ,Termination_Date Termination_Date
                
FROM datalake_dev.pre_governed.workday_worker_change_history_3128 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
