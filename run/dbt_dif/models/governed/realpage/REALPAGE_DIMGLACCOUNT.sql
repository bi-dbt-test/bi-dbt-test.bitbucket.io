
  create or replace  view datalake_dev.governed.REALPAGE_DIMGLACCOUNT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimglaccount_2276)
SELECT GLAccountKey GLAccountKey
                ,OrganizationKey OrganizationKey
                ,osl_PMCID osl_PMCID
                ,GLAccountNumber GLAccountNumber
                ,GLAccountName GLAccountName
                ,ChartOfAccountsID ChartOfAccountsID
                ,CategoryID CategoryID
                ,ChartOfAccounts ChartOfAccounts
                ,NormalBalance NormalBalance
                ,AccountType AccountType
                ,ClosingType ClosingType
                ,ClosesToAccount ClosesToAccount
                ,StatisticalAccount StatisticalAccount
                ,SummaryClass SummaryClass
                ,SourceRecordID SourceRecordID
                ,IsDeleted IsDeleted
                ,CreateDate CreateDate
                
FROM datalake_dev.pre_governed.realpage_dimglaccount_2276 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
