
  create or replace  view datalake_dev.governed.REALPAGE_DIMTASKCATEGORY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimtaskcategory_1716)
SELECT TaskCategoryKey TaskCategoryKey
                ,TaskCategoryID TaskCategoryID
                ,TaskCategory TaskCategory
                ,TaskPriorityID TaskPriorityID
                ,TaskPriority TaskPriority
                ,CDSPMCID CDSPMCID
                
FROM datalake_dev.pre_governed.realpage_dimtaskcategory_1716 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
