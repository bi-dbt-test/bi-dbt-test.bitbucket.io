
  create or replace  view datalake_dev.governed.REALPAGE_DIMBUILDING  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimbuilding_1701)
SELECT BuildingKey BuildingKey
                ,PropertyKey PropertyKey
                ,BuildingName BuildingName
                ,BuildingNumber BuildingNumber
                ,NumberOfFloors NumberOfFloors
                ,Address1 Address1
                ,Address2 Address2
                ,Address3 Address3
                ,City City
                ,State State
                ,Zip Zip
                ,County County
                ,Province Province
                ,BlockNumber BlockNumber
                ,BuildingType BuildingType
                ,ConstructionStartDate ConstructionStartDate
                ,ConstructionEndDate ConstructionEndDate
                ,FirstOccupiedDate FirstOccupiedDate
                ,FloorplanCount FloorplanCount
                ,GrossSquareFootage GrossSquareFootage
                ,Latitude Latitude
                ,Longitude Longitude
                ,RentableSquareFootage RentableSquareFootage
                ,MSAFIPS MSAFIPS
                ,AccessibleFlag AccessibleFlag
                ,DisplayFlag DisplayFlag
                ,Description Description
                ,osl_PropertyID osl_PropertyID
                ,osl_BuildingID osl_BuildingID
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsDeleted IsDeleted
                ,IsLastRow IsLastRow
                ,osl_CDSPMCID osl_CDSPMCID
                
FROM datalake_dev.pre_governed.realpage_dimbuilding_1701 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
