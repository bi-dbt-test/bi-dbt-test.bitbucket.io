
  create or replace  view datalake_dev.governed.REALPAGE_DIMFLOORPLAN  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimfloorplan_2582)
SELECT FloorPlanKey FloorPlanKey
                ,PropertyKey PropertyKey
                ,FloorPlanCode FloorPlanCode
                ,FloorPlanName FloorPlanName
                ,FloorPlanBrochureName FloorPlanBrochureName
                ,NumberofBedrooms NumberofBedrooms
                ,NumberofBathrooms NumberofBathrooms
                ,GrossSquareFeet GrossSquareFeet
                ,RentSquareFeet RentSquareFeet
                ,MaximumOccupants MaximumOccupants
                ,FloorPlanGroupName FloorPlanGroupName
                ,FloorPlanGroupDescription FloorPlanGroupDescription
                ,FloorPlanGroupNumberofBedrooms FloorPlanGroupNumberofBedrooms
                ,FloorPlanGroupNumberofBathrooms FloorPlanGroupNumberofBathrooms
                ,FloorPlanOnlineDisplayBit FloorPlanOnlineDisplayBit
                ,CommissionAmount CommissionAmount
                ,HighPriceRangeAmount HighPriceRangeAmount
                ,LowPriceRangeAmount LowPriceRangeAmount
                ,ReserveFeeAmount ReserveFeeAmount
                ,SubsidyRentAmount SubsidyRentAmount
                ,MaximumSqFt MaximumSqFt
                ,MinimumSqFt MinimumSqFt
                ,RentType RentType
                ,AvailableFlag AvailableFlag
                ,DisplayFlag DisplayFlag
                ,ReportUnitOccupancyFlag ReportUnitOccupancyFlag
                ,Status Status
                ,CommissionPercentage CommissionPercentage
                ,SubsidizedPercentage SubsidizedPercentage
                ,osl_FloorPlanGroupID osl_FloorPlanGroupID
                ,osl_PropertyID osl_PropertyID
                ,osl_FloorPlanID osl_FloorPlanID
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsDeleted IsDeleted
                ,IsLastRow IsLastRow
                ,osl_PMCID osl_PMCID
                ,ToProcess ToProcess
                ,OriginalBaseRent OriginalBaseRent
                ,StartDate StartDate
                ,EndDate EndDate
                ,Refunddepositamount Refunddepositamount
                
FROM datalake_dev.pre_governed.realpage_dimfloorplan_2582 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
