
  create or replace  view datalake_dev.governed.REALPAGE_DIMRESIDENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimresident_3241)
SELECT ResidentKey ResidentKey
                ,ResidentUniqueIdentifier ResidentUniqueIdentifier
                ,ResidentFullName ResidentFullName
                ,ResidentMailingName ResidentMailingName
                ,IsConventional IsConventional
                ,IsFinalPaymentSettled IsFinalPaymentSettled
                ,IsInCollection IsInCollection
                ,IsMiscellaneousAccount IsMiscellaneousAccount
                ,DoNotAcceptCheck DoNotAcceptCheck
                ,DoNotAcceptMoneyOrder DoNotAcceptMoneyOrder
                ,NoOfLatePayments NoOfLatePayments
                ,NoOfNSFChecks NoOfNSFChecks
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsDeleted IsDeleted
                ,IsLastRow IsLastRow
                ,osl_CDSPMCID osl_CDSPMCID
                ,osl_PropertyID osl_PropertyID
                ,osl_reshID osl_reshID
                ,PropertyKey PropertyKey
                ,LastLatePeriodID LastLatePeriodID
                ,osl_SiteGuestCard osl_SiteGuestCard
                ,StatusCode StatusCode
                ,Status Status
                ,AllowPaymentForFormerResidentFlag AllowPaymentForFormerResidentFlag
                
FROM datalake_dev.pre_governed.realpage_dimresident_3241 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
