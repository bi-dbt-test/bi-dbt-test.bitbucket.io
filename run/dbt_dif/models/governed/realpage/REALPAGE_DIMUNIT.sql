
  create or replace  view datalake_dev.governed.REALPAGE_DIMUNIT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimunit_4417)
SELECT UnitKey UnitKey
                ,PropertyKey PropertyKey
                ,FloorPlanKey FloorPlanKey
                ,UnitNumber UnitNumber
                ,BuildingNumber BuildingNumber
                ,FloorNumber FloorNumber
                ,GrossSquareFeet GrossSquareFeet
                ,RentableSquareFeet RentableSquareFeet
                ,DepositAmount DepositAmount
                ,NonRefundableFee NonRefundableFee
                ,ComplianceExemptCode ComplianceExemptCode
                ,Hearing Hearing
                ,Mobility Mobility
                ,Vision Vision
                ,AvailableForOccupancyFlag AvailableForOccupancyFlag
                ,UnitDesignation UnitDesignation
                ,UnitDesignationGroup UnitDesignationGroup
                ,MadeReadyDate MadeReadyDate
                ,MadeReadyBit MadeReadyBit
                ,UnavailableCode UnavailableCode
                ,BuilderWarrantyExpirationDate BuilderWarrantyExpirationDate
                ,MilitarySubdivision MilitarySubdivision
                ,SubPropertyName SubPropertyName
                ,SubPropertyNumber SubPropertyNumber
                ,osl_SubPropertyID osl_SubPropertyID
                ,osl_FloorPlanID osl_FloorPlanID
                ,osl_PropertyID osl_PropertyID
                ,osl_UnitID osl_UnitID
                ,osl_PMCID osl_PMCID
                ,BuildingKey BuildingKey
                ,BuildingName BuildingName
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsDeleted IsDeleted
                ,IsLastRow IsLastRow
                ,ApartmentID ApartmentID
                ,unitDisplayBit unitDisplayBit
                ,CensusPercentage CensusPercentage
                ,UnitStartDate UnitStartDate
                ,UnitEndDate UnitEndDate
                ,UnitOnlineDisplayBit UnitOnlineDisplayBit
                ,UnitEndDateBit UnitEndDateBit
                ,unitUniqueIdentity unitUniqueIdentity
                ,fpDisplayBit fpDisplayBit
                ,ToProcess ToProcess
                ,osl_UnitDesignationID osl_UnitDesignationID
                ,osl_UnitDesignationGroupID osl_UnitDesignationGroupID
                ,Address1 Address1
                ,Address2 Address2
                ,City City
                ,State State
                ,Zip Zip
                ,County County
                ,HUDExemptFlag HUDExemptFlag
                ,HoldFlag HoldFlag
                ,NonRevenueFlag NonRevenueFlag
                ,NoteDescription NoteDescription
                ,ExpectedTurnDate ExpectedTurnDate
                ,HoldUntilDate HoldUntilDate
                ,RHSExemptFlag RHSExemptFlag
                ,TCExemptFlag TCExemptFlag
                ,UnitDescription UnitDescription
                
FROM datalake_dev.pre_governed.realpage_dimunit_4417 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
