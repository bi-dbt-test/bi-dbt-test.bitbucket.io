
  create or replace  view datalake_dev.governed.REALPAGE_DIMPROPERTY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimproperty_2857)
SELECT PropertyKey PropertyKey
                ,OrganizationKey OrganizationKey
                ,PropertyName PropertyName
                ,PropertyNumber PropertyNumber
                ,PropertyAddress1 PropertyAddress1
                ,PropertyAddress2 PropertyAddress2
                ,PropertyAddress3 PropertyAddress3
                ,PropertyCity PropertyCity
                ,PropertyStateProvinceCode PropertyStateProvinceCode
                ,PropertyPostalCode PropertyPostalCode
                ,PropertyCountryCode PropertyCountryCode
                ,ClaimedUnitCount ClaimedUnitCount
                ,PropertyStatus PropertyStatus
                ,osl_PropertyID osl_PropertyID
                ,osl_PMCID osl_PMCID
                ,IsDeleted IsDeleted
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsLastRow IsLastRow
                ,CDSExtractDate CDSExtractDate
                ,ModifyDate ModifyDate
                ,PropertySourceCode PropertySourceCode
                ,AccountingPropertyID AccountingPropertyID
                ,ExternalPropertyIdentifier ExternalPropertyIdentifier
                ,Phone Phone
                ,Phone2 Phone2
                ,FaxPhone FaxPhone
                ,EmailAddress EmailAddress
                ,WebAddress WebAddress
                
FROM datalake_dev.pre_governed.realpage_dimproperty_2857 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
