
  create or replace  view datalake_dev.governed.REALPAGE_FACTPROPERTYSTATISTICSONDATE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_factpropertystatisticsondate_1723)
SELECT OrganizationKey OrganizationKey
                ,PropertyKey PropertyKey
                ,PostPropertyDateKey PostPropertyDateKey
                ,PostDate PostDate
                ,RenewalSignedSquareFeet RenewalSignedSquareFeet
                ,RenewalSignedUnitCount RenewalSignedUnitCount
                ,RenewalSignedScheduledEffectiveRevenue RenewalSignedScheduledEffectiveRevenue
                ,RenewalSignedScheduledConcessions RenewalSignedScheduledConcessions
                ,RenewalSignedScheduledRent RenewalSignedScheduledRent
                ,RenewalSignedTotalConcessions RenewalSignedTotalConcessions
                ,RenewalSignedTotalLeaseTerm RenewalSignedTotalLeaseTerm
                ,RenewalSignedEffectiveRent RenewalSignedEffectiveRent
                ,RenewalSignedEffectiveRentTradeOut RenewalSignedEffectiveRentTradeOut
                ,NewLeaseSignedSquareFeet NewLeaseSignedSquareFeet
                ,NewLeaseSignedUnitCount NewLeaseSignedUnitCount
                ,NewLeaseSignedScheduledEffectiveRevenue NewLeaseSignedScheduledEffectiveRevenue
                ,NewLeaseSignedScheduledConcessions NewLeaseSignedScheduledConcessions
                ,NewLeaseSignedScheduledRent NewLeaseSignedScheduledRent
                ,NewLeaseSignedTotalConcessions NewLeaseSignedTotalConcessions
                ,NewLeaseSignedTotalLeaseTerm NewLeaseSignedTotalLeaseTerm
                ,NewLeaseSignedEffectiveRent NewLeaseSignedEffectiveRent
                ,NewLeaseSignedEffectiveRentTradeOut NewLeaseSignedEffectiveRentTradeOut
                ,RenewalStartSquareFeet RenewalStartSquareFeet
                ,RenewalStartUnitCount RenewalStartUnitCount
                ,RenewalStartScheduledEffectiveRevenue RenewalStartScheduledEffectiveRevenue
                ,RenewalStartScheduledConcessions RenewalStartScheduledConcessions
                ,RenewalStartScheduledRent RenewalStartScheduledRent
                ,RenewalStartTotalConcessions RenewalStartTotalConcessions
                ,RenewalStartTotalLeaseTerm RenewalStartTotalLeaseTerm
                ,RenewalStartEffectiveRent RenewalStartEffectiveRent
                ,RenewalStartEffectiveRentTradeOut RenewalStartEffectiveRentTradeOut
                ,LeaseApplicationCount LeaseApplicationCount
                ,LeaseApplicationCancelledCount LeaseApplicationCancelledCount
                ,LeaseApplicationDeniedCount LeaseApplicationDeniedCount
                ,ExpiringLeaseCount ExpiringLeaseCount
                ,ScheduledExpiringLeaseCount ScheduledExpiringLeaseCount
                ,MoveInLeaseCount MoveInLeaseCount
                ,MoveOutLeaseCount MoveOutLeaseCount
                ,AffordableRenewalSignedScheduledConcessions AffordableRenewalSignedScheduledConcessions
                ,AffordableRenewalSignedTotalConcessions AffordableRenewalSignedTotalConcessions
                ,AffordableNewLeaseSignedScheduledConcessions AffordableNewLeaseSignedScheduledConcessions
                ,AffordableNewLeaseSignedTotalConcessions AffordableNewLeaseSignedTotalConcessions
                ,AffordableRenewalStartScheduledConcessions AffordableRenewalStartScheduledConcessions
                ,AffordableRenewalStartTotalConcessions AffordableRenewalStartTotalConcessions
                ,TransferMoveInLeaseCount TransferMoveInLeaseCount
                ,TransferMoveOutLeaseCount TransferMoveOutLeaseCount
                
FROM datalake_dev.pre_governed.realpage_factpropertystatisticsondate_1723 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
