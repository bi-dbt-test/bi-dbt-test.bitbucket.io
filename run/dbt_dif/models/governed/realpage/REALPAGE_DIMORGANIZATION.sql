
  create or replace  view datalake_dev.governed.REALPAGE_DIMORGANIZATION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimorganization_2010)
SELECT OrganizationKey OrganizationKey
                ,OrganizationName OrganizationName
                ,OrganizationType OrganizationType
                ,CompanyID CompanyID
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                
FROM datalake_dev.pre_governed.realpage_dimorganization_2010 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
