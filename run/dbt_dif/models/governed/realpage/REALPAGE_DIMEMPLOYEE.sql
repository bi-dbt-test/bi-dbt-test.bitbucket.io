
  create or replace  view datalake_dev.governed.REALPAGE_DIMEMPLOYEE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimemployee_1992)
SELECT EmployeeKey EmployeeKey
                ,EmployeeName EmployeeName
                ,EmployeeLogin EmployeeLogin
                ,EmployeeCreateDate EmployeeCreateDate
                ,osl_EmployeeNumber osl_EmployeeNumber
                ,USER_ThirdPartyReferenceNumber USER_ThirdPartyReferenceNumber
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsDeleted IsDeleted
                ,IsLastRow IsLastRow
                ,CMPNY_ID CMPNY_ID
                ,CURRENT_PRPTY_ID CURRENT_PRPTY_ID
                ,osl_CDSPropertyID osl_CDSPropertyID
                ,osl_CDSPMCID osl_CDSPMCID
                ,UserDisableBit UserDisableBit
                
FROM datalake_dev.pre_governed.realpage_dimemployee_1992 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
