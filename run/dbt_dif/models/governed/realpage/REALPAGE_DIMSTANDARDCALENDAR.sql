
  create or replace  view datalake_dev.governed.REALPAGE_DIMSTANDARDCALENDAR  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimstandardcalendar_1714)
SELECT DateKey DateKey
                ,CalendarDate CalendarDate
                ,CalendarYear CalendarYear
                ,CalendarQuarterName CalendarQuarterName
                ,CalendarQuarterNumber CalendarQuarterNumber
                ,CalendarMonthName CalendarMonthName
                ,CalendarMonthNameLong CalendarMonthNameLong
                ,CalendarMonthNumber CalendarMonthNumber
                ,WeekdayName WeekdayName
                ,WeekdayNameLong WeekdayNameLong
                ,CalendarStartofWeekDate CalendarStartofWeekDate
                ,CalendarEndofWeekDate CalendarEndofWeekDate
                ,CalendarStartOfMonthDate CalendarStartOfMonthDate
                ,CalendarEndOfMonthDate CalendarEndOfMonthDate
                ,CalendarStartOfQuarterDate CalendarStartOfQuarterDate
                ,CalendarEndOfQuarterDate CalendarEndOfQuarterDate
                ,CalendarStartOfYearDate CalendarStartOfYearDate
                ,CalendarEndOfYearDate CalendarEndOfYearDate
                ,IsPublicHoliday IsPublicHoliday
                ,IsWeekday IsWeekday
                ,IsWeekend IsWeekend
                
FROM datalake_dev.pre_governed.realpage_dimstandardcalendar_1714 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
