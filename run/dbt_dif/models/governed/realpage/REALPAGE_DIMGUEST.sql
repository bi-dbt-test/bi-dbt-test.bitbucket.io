
  create or replace  view datalake_dev.governed.REALPAGE_DIMGUEST  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimguest_3581)
SELECT GuestKey GuestKey
                ,GuestUniqueIdentifier GuestUniqueIdentifier
                ,GuestCreateDate GuestCreateDate
                ,GuestDesiredMoveInDate GuestDesiredMoveInDate
                ,GuestStatus GuestStatus
                ,PrimaryMarketingChannel PrimaryMarketingChannel
                ,PrimaryMarketingCategory PrimaryMarketingCategory
                ,SecondaryMarketingChannel SecondaryMarketingChannel
                ,SecondaryMarketingCategory SecondaryMarketingCategory
                ,OccupantCount OccupantCount
                ,PreferredLeaseTerm PreferredLeaseTerm
                ,PreferredFloor PreferredFloor
                ,FloorLevelId FloorLevelId
                ,PreferredFloorPlanGroup PreferredFloorPlanGroup
                ,PreferredPriceRangeLow PreferredPriceRangeLow
                ,PreferredPriceRangeHigh PreferredPriceRangeHigh
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsDeleted IsDeleted
                ,IsLastRow IsLastRow
                ,osl_gcardId osl_gcardId
                ,osl_ProspectCode osl_ProspectCode
                ,osl_PMCID osl_PMCID
                ,osl_PropertyID osl_PropertyID
                ,PreferredLanguage PreferredLanguage
                ,osl_PriceRangeID osl_PriceRangeID
                ,osl_PetWeightRangeID osl_PetWeightRangeID
                ,osl_ReasonForMovingID osl_ReasonForMovingID
                ,PrimaryMarketingChannelID PrimaryMarketingChannelID
                ,SecondaryMarketingChannelID SecondaryMarketingChannelID
                
FROM datalake_dev.pre_governed.realpage_dimguest_3581 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
