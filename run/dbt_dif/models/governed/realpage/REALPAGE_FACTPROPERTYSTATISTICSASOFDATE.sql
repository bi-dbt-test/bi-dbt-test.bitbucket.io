
  create or replace  view datalake_dev.governed.REALPAGE_FACTPROPERTYSTATISTICSASOFDATE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_factpropertystatisticsasofdate_2720)
SELECT OrganizationKey OrganizationKey
                ,PropertyKey PropertyKey
                ,PostPropertyDateKey PostPropertyDateKey
                ,PostDate PostDate
                ,TotalGrossSquareFeet TotalGrossSquareFeet
                ,TotalRentSquareFeet TotalRentSquareFeet
                ,TotalUnitCount TotalUnitCount
                ,DownSquareFeet DownSquareFeet
                ,DownUnitCount DownUnitCount
                ,ModelSquareFeet ModelSquareFeet
                ,ModelUnitCount ModelUnitCount
                ,OtherUnavailableSquareFeet OtherUnavailableSquareFeet
                ,OtherUnavailableUnitCount OtherUnavailableUnitCount
                ,OccupiedSquareFeet OccupiedSquareFeet
                ,OccupiedUnitCount OccupiedUnitCount
                ,VacantUnitCount VacantUnitCount
                ,OccupiedScheduledEffectiveRevenue OccupiedScheduledEffectiveRevenue
                ,OccupiedScheduledConcessions OccupiedScheduledConcessions
                ,OccupiedScheduledRent OccupiedScheduledRent
                ,OccupiedEffectiveRent OccupiedEffectiveRent
                ,MarketRent MarketRent
                ,BudgetRent BudgetRent
                ,TotalAmenities TotalAmenities
                ,AverageAmenities AverageAmenities
                ,OnNoticeUnitCount OnNoticeUnitCount
                ,OnNoticeCurrentMonthUnitCount OnNoticeCurrentMonthUnitCount
                ,OnNoticeFutureMonthUnitCount OnNoticeFutureMonthUnitCount
                ,OnNoticeFutureMonth2UnitCount OnNoticeFutureMonth2UnitCount
                ,OnNoticeFutureMonth3UnitCount OnNoticeFutureMonth3UnitCount
                ,OnNoticePreleasedUnitCount OnNoticePreleasedUnitCount
                ,VacantPreleasedUnitCount VacantPreleasedUnitCount
                ,OnNoticeExposedCurrentMonthUnitCount OnNoticeExposedCurrentMonthUnitCount
                ,OnNoticeExposedFutureMonthUnitCount OnNoticeExposedFutureMonthUnitCount
                ,OnNoticeExposedFutureMonth2UnitCount OnNoticeExposedFutureMonth2UnitCount
                ,OnNoticeExposedFutureMonth3UnitCount OnNoticeExposedFutureMonth3UnitCount
                ,ExposedUnitCount ExposedUnitCount
                ,RenewalOccupiedSquareFeet RenewalOccupiedSquareFeet
                ,RenewalOccupiedUnitCount RenewalOccupiedUnitCount
                ,RenewalOccupiedScheduledEffectiveRevenue RenewalOccupiedScheduledEffectiveRevenue
                ,RenewalOccupiedScheduledConcessions RenewalOccupiedScheduledConcessions
                ,RenewalOccupiedScheduledRent RenewalOccupiedScheduledRent
                ,RenewalOccupiedTotalConcessions RenewalOccupiedTotalConcessions
                ,RenewalOccupiedTotalLeaseTerm RenewalOccupiedTotalLeaseTerm
                ,RenewalOccupiedEffectiveRent RenewalOccupiedEffectiveRent
                ,RenewalOccupiedEffectiveRentTradeOut RenewalOccupiedEffectiveRentTradeOut
                ,NewLeaseOccupiedSquareFeet NewLeaseOccupiedSquareFeet
                ,NewLeaseOccupiedUnitCount NewLeaseOccupiedUnitCount
                ,NewLeaseOccupiedScheduledEffectiveRevenue NewLeaseOccupiedScheduledEffectiveRevenue
                ,NewLeaseOccupiedScheduledConcessions NewLeaseOccupiedScheduledConcessions
                ,NewLeaseOccupiedScheduledRent NewLeaseOccupiedScheduledRent
                ,NewLeaseOccupiedTotalConcessions NewLeaseOccupiedTotalConcessions
                ,NewLeaseOccupiedTotalLeaseTerm NewLeaseOccupiedTotalLeaseTerm
                ,NewLeaseOccupiedEffectiveRent NewLeaseOccupiedEffectiveRent
                ,NewLeaseOccupiedEffectiveRentTradeOut NewLeaseOccupiedEffectiveRentTradeOut
                ,MonthToMonthLeaseCount MonthToMonthLeaseCount
                ,MonthToMonthRenewalOccupiedLeaseCount MonthToMonthRenewalOccupiedLeaseCount
                ,MonthToMonthNewLeaseOccupiedLeaseCount MonthToMonthNewLeaseOccupiedLeaseCount
                ,VacantMarketReadyUnitCount VacantMarketReadyUnitCount
                ,AffordableOccupiedScheduledConcessions AffordableOccupiedScheduledConcessions
                ,AffordableRenewalOccupiedScheduledConcessions AffordableRenewalOccupiedScheduledConcessions
                ,AffordableRenewalOccupiedTotalConcessions AffordableRenewalOccupiedTotalConcessions
                ,AffordableNewLeaseOccupiedScheduledConcessions AffordableNewLeaseOccupiedScheduledConcessions
                ,AffordableNewLeaseOccupiedTotalConcessions AffordableNewLeaseOccupiedTotalConcessions
                ,OccupiedScheduledRent_ResSubj OccupiedScheduledRent_ResSubj
                ,OccupiedEffectiveRent_ResSubj OccupiedEffectiveRent_ResSubj
                ,OccupiedEffectiveRent_Aff OccupiedEffectiveRent_Aff
                ,OnNoticeSquareFeet OnNoticeSquareFeet
                ,OnNoticePreleasedSquareFeet OnNoticePreleasedSquareFeet
                ,VacantSquareFeet VacantSquareFeet
                ,VacantPreleasedSquareFeet VacantPreleasedSquareFeet
                
FROM datalake_dev.pre_governed.realpage_factpropertystatisticsasofdate_2720 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
