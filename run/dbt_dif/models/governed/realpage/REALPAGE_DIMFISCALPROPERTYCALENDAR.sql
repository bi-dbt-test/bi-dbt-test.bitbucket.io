
  create or replace  view datalake_dev.governed.REALPAGE_DIMFISCALPROPERTYCALENDAR  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimfiscalpropertycalendar_1704)
SELECT DateKey DateKey
                ,PropertyDateKey PropertyDateKey
                ,PropertyKey PropertyKey
                ,osl_PropertyID osl_PropertyID
                ,osl_PMCID osl_PMCID
                ,osl_FiscalPeriodID osl_FiscalPeriodID
                ,osl_SetupId osl_SetupId
                ,ClosedFlag ClosedFlag
                ,PropertyDate PropertyDate
                ,PropertyFiscalPeriod PropertyFiscalPeriod
                ,PropertyFiscalQuarter PropertyFiscalQuarter
                ,PropertyFiscalYear PropertyFiscalYear
                ,IsPropertyHoliday IsPropertyHoliday
                ,IsPropertyBusinessDay IsPropertyBusinessDay
                ,PropertyYearWeek PropertyYearWeek
                ,PropertyStartOfPeriodDate PropertyStartOfPeriodDate
                ,PropertyEndOfPeriodDate PropertyEndOfPeriodDate
                ,PropertyStartOfQuarterDate PropertyStartOfQuarterDate
                ,PropertyEndOfQuarterDate PropertyEndOfQuarterDate
                ,PropertyStartOfYearDate PropertyStartOfYearDate
                ,PropertyEndOfYearDate PropertyEndOfYearDate
                ,PropertyFiscalQuarterIndex PropertyFiscalQuarterIndex
                ,IsDeleted IsDeleted
                
FROM datalake_dev.pre_governed.realpage_dimfiscalpropertycalendar_1704 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
