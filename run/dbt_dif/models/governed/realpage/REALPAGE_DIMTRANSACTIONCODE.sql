
  create or replace  view datalake_dev.governed.REALPAGE_DIMTRANSACTIONCODE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimtransactioncode_1717)
SELECT TransactionCodeKey TransactionCodeKey
                ,TransactionCode TransactionCode
                ,TransactionCodeDescription TransactionCodeDescription
                ,TransactionCategoryCode TransactionCategoryCode
                ,TransactionCategoryDescription TransactionCategoryDescription
                ,TransactionMajorTypeCode TransactionMajorTypeCode
                ,TransactionMinorTypeCode TransactionMinorTypeCode
                ,DefaultAmount DefaultAmount
                ,WarningAmount WarningAmount
                ,ManageFeeMaxAmount ManageFeeMaxAmount
                ,ManageFeeMinAmount ManageFeeMinAmount
                ,OfferAmount OfferAmount
                ,ManageFeePercent ManageFeePercent
                ,AccountingMethod AccountingMethod
                ,EditableFlag EditableFlag
                ,AllowBillingOverlapFlag AllowBillingOverlapFlag
                ,CashFlag CashFlag
                ,ManageFeeFlag ManageFeeFlag
                ,RevocableFlag RevocableFlag
                ,OfferFlag OfferFlag
                ,DoNotProrateFlag DoNotProrateFlag
                ,InterimDepositRefundFlag InterimDepositRefundFlag
                ,TriggerRevocationFlag TriggerRevocationFlag
                ,ProrateMidMonthFlag ProrateMidMonthFlag
                ,InactiveDate InactiveDate
                ,OffSettingMultiplier OffSettingMultiplier
                ,Priority Priority
                ,osl_PropertyID osl_PropertyID
                ,osl_PMCID osl_PMCID
                ,IsDeleted IsDeleted
                ,CDSExtractDate CDSExtractDate
                ,trancID trancID
                ,GroupForRent GroupForRent
                ,ActiveBit ActiveBit
                ,CreditglID CreditglID
                ,DebitglID DebitglID
                ,osl_TransactionCategoryID osl_TransactionCategoryID
                ,ModeofPayment ModeofPayment
                ,OffSettingID OffSettingID
                ,OffSettingNumber OffSettingNumber
                
FROM datalake_dev.pre_governed.realpage_dimtransactioncode_1717 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
