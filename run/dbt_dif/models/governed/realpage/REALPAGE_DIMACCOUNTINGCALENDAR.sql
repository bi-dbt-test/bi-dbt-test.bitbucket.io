
  create or replace  view datalake_dev.governed.REALPAGE_DIMACCOUNTINGCALENDAR  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimaccountingcalendar_1699)
SELECT ChartofAccountsID ChartofAccountsID
                ,SubPropertyKey SubPropertyKey
                ,TimePeriod TimePeriod
                ,FiscalPeriod FiscalPeriod
                ,FiscalYear FiscalYear
                ,PropertyKey PropertyKey
                ,CalendarYear CalendarYear
                ,StartDate StartDate
                ,EndDate EndDate
                ,Tablekey Tablekey
                ,CreateDate CreateDate
                
FROM datalake_dev.pre_governed.realpage_dimaccountingcalendar_1699 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
