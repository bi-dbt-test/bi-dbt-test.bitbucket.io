
  create or replace  view datalake_dev.governed.REALPAGE_FACTLEASE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_factlease_3242)
SELECT LeaseTerm LeaseTerm
                ,CalculatedLeaseTerm CalculatedLeaseTerm
                ,TermRent TermRent
                ,MTMRent MTMRent
                ,TotalTermConcessions TotalTermConcessions
                ,AmortizedTermConcessions AmortizedTermConcessions
                ,MTMConcessions MTMConcessions
                ,LeaseType LeaseType
                ,DepositAmount DepositAmount
                ,DueDepositAmount DueDepositAmount
                ,PetDepositAmount PetDepositAmount
                ,UtilityCount UtilityCount
                ,RequiredDaysOfNoticeCount RequiredDaysOfNoticeCount
                ,osl_LeaseID osl_LeaseID
                ,PropertyKey PropertyKey
                ,UnitKey UnitKey
                ,ResidentKey ResidentKey
                ,GuestKey GuestKey
                ,LeaseAttributesKey LeaseAttributesKey
                ,PriorLeaseAttributesKey PriorLeaseAttributesKey
                ,NextLeaseAttributesKey NextLeaseAttributesKey
                ,PriorUnitLeaseAttributesKey PriorUnitLeaseAttributesKey
                ,QuoteAttributesKey QuoteAttributesKey
                ,LeasingEmployeeKey LeasingEmployeeKey
                ,MarketingChannelKey MarketingChannelKey
                ,LateMethodKey LateMethodKey
                ,LoadTimestamp LoadTimestamp
                ,osl_PMCID osl_PMCID
                ,osl_PropertyID osl_PropertyID
                ,IsDeleted IsDeleted
                ,osl_UnitID osl_UnitID
                ,osl_LateMethodID osl_LateMethodID
                ,residentHouseHoldID residentHouseHoldID
                ,TenantID TenantID
                ,AffordableTotalTermConcessions AffordableTotalTermConcessions
                ,AffordableAmortizedTermConcessions AffordableAmortizedTermConcessions
                ,AffordableMTMConcessions AffordableMTMConcessions
                ,PriorLeaseTermRent PriorLeaseTermRent
                ,PriorLeaseTotalTermConcessions PriorLeaseTotalTermConcessions
                ,PriorLeaseAmortizedTermConcessions PriorLeaseAmortizedTermConcessions
                ,PriorLeaseAffordableTermConcessions PriorLeaseAffordableTermConcessions
                ,PriorLeaseAffordableAmortizedTermConcession PriorLeaseAffordableAmortizedTermConcession
                ,PriorLeaseCalculatedLeaseTerm PriorLeaseCalculatedLeaseTerm
                ,DaysVacant DaysVacant
                ,TradeoutAmount TradeoutAmount
                ,TradeOutPercentage TradeOutPercentage
                ,NextLeaseTermRent NextLeaseTermRent
                ,NextLeaseTotalTermConcessions NextLeaseTotalTermConcessions
                ,NextLeaseAmortizedTermConcessions NextLeaseAmortizedTermConcessions
                ,NextLeaseAffordableTermConcessions NextLeaseAffordableTermConcessions
                ,NextLeaseAffordableAmortizedTermConcession NextLeaseAffordableAmortizedTermConcession
                ,NextLeaseCalculatedLeaseTerm NextLeaseCalculatedLeaseTerm
                ,TotalContractValue TotalContractValue
                ,AcademicYearKey AcademicYearKey
                ,BookingStatus BookingStatus
                ,UnitUniqueIdentity UnitUniqueIdentity
                ,SubsidyTermRent SubsidyTermRent
                ,ResidentTermRent ResidentTermRent
                ,SubsidyMTMRent SubsidyMTMRent
                ,ResidentMTMRent ResidentMTMRent
                ,TotalMTMConcessions TotalMTMConcessions
                ,AffordableTotalMTMConcessions AffordableTotalMTMConcessions
                ,TotalSubsidyConcessions TotalSubsidyConcessions
                ,AffordableTotalSubsidyConcessions AffordableTotalSubsidyConcessions
                ,AmortizedConcession_Subsidy AmortizedConcession_Subsidy
                ,AmortizedConcessions_ResSubj AmortizedConcessions_ResSubj
                
FROM datalake_dev.pre_governed.realpage_factlease_3242 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
