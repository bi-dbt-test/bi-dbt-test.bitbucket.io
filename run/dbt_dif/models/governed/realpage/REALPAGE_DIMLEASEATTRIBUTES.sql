
  create or replace  view datalake_dev.governed.REALPAGE_DIMLEASEATTRIBUTES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimleaseattributes_1708)
SELECT LeaseAttributesKey LeaseAttributesKey
                ,LeaseUniqueIdentifier LeaseUniqueIdentifier
                ,LeaseTermDescription LeaseTermDescription
                ,LeaseTermInMonths LeaseTermInMonths
                ,LeaseType LeaseType
                ,LeaseTypeID LeaseTypeID
                ,LeaseStatus LeaseStatus
                ,ReasonForLeasing ReasonForLeasing
                ,osl_ReasonForLeasingID osl_ReasonForLeasingID
                ,MoveOutReason MoveOutReason
                ,MoveOutReasonGroup MoveOutReasonGroup
                ,CancelReason CancelReason
                ,CancelReasonGroup CancelReasonGroup
                ,LateMethod LateMethod
                ,IsRenewal IsRenewal
                ,IsTransfer IsTransfer
                ,IsCancelled IsCancelled
                ,IsBulkMoveIn IsBulkMoveIn
                ,IsBulkMoveOut IsBulkMoveOut
                ,IsDenyRenewal IsDenyRenewal
                ,IsEvictionProceedingsStarted IsEvictionProceedingsStarted
                ,IsNonResidentAccount IsNonResidentAccount
                ,IsAccountClosed IsAccountClosed
                ,LeaseApplicationDate LeaseApplicationDate
                ,LeaseApprovedDate LeaseApprovedDate
                ,LeaseRejectedDate LeaseRejectedDate
                ,LeaseBeginDate LeaseBeginDate
                ,LeaseEndDate LeaseEndDate
                ,LeaseSignedDate LeaseSignedDate
                ,LeaseActiveDate LeaseActiveDate
                ,ScheduledMoveInDate ScheduledMoveInDate
                ,ActualMoveInDate ActualMoveInDate
                ,NoticeOnDate NoticeOnDate
                ,NoticeForDate NoticeForDate
                ,ScheduledMoveOutDate ScheduledMoveOutDate
                ,ActualMoveOutDate ActualMoveOutDate
                ,LeaseInactiveDate LeaseInactiveDate
                ,CreditApprovedDate CreditApprovedDate
                ,ModifyDate ModifyDate
                ,osl_LeaseID osl_LeaseID
                ,osl_PMCID osl_PMCID
                ,osl_PropertyID osl_PropertyID
                ,IsDeleted IsDeleted
                ,CDSExtractDate CDSExtractDate
                ,IsLastRow IsLastRow
                ,RowIsCurrent RowIsCurrent
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,PropertyKey PropertyKey
                ,IsOnlineApplication IsOnlineApplication
                ,IsOnlineRenewal IsOnlineRenewal
                ,osl_LeaseStatusCode osl_LeaseStatusCode
                ,ToProcess ToProcess
                ,MonthToMonthDate MonthToMonthDate
                ,UtilityBillingStartDate UtilityBillingStartDate
                ,osl_ltermID osl_ltermID
                ,ModifiedByID ModifiedByID
                ,osl_MoveOutID osl_MoveOutID
                ,osl_CancelID osl_CancelID
                ,MonthToMonthRequestedFlag MonthToMonthRequestedFlag
                
FROM datalake_dev.pre_governed.realpage_dimleaseattributes_1708 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
