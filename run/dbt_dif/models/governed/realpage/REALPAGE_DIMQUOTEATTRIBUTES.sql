
  create or replace  view datalake_dev.governed.REALPAGE_DIMQUOTEATTRIBUTES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimquoteattributes_1712)
SELECT QuoteAttributesKey QuoteAttributesKey
                ,QuoteUniqueIdentifier QuoteUniqueIdentifier
                ,QuoteCreateDate QuoteCreateDate
                ,QuoteExpirationDate QuoteExpirationDate
                ,QuoteReservationExpireDate QuoteReservationExpireDate
                ,QuoteLeaseTermDescription QuoteLeaseTermDescription
                ,QuoteLeaseStartDate QuoteLeaseStartDate
                ,QuoteMoveInDate QuoteMoveInDate
                ,QuoteMoveOutDate QuoteMoveOutDate
                ,osl_PMCGuestCardID osl_PMCGuestCardID
                ,osl_UnitID osl_UnitID
                ,osl_LeaseTermID osl_LeaseTermID
                ,osl_ConcessionID osl_ConcessionID
                ,CreatedByID CreatedByID
                ,AmortizedFlag AmortizedFlag
                ,ReservedFlag ReservedFlag
                ,PresentationMonthlyFlag PresentationMonthlyFlag
                ,SystemCreatedFlag SystemCreatedFlag
                ,StabilizationUnitSetupID StabilizationUnitSetupID
                ,RowStartDate RowStartDate
                ,RowEndDate RowEndDate
                ,RowIsCurrent RowIsCurrent
                ,IsDeleted IsDeleted
                ,IsLastRow IsLastRow
                ,CDSPropertyID CDSPropertyID
                ,CDSPMCID CDSPMCID
                
FROM datalake_dev.pre_governed.realpage_dimquoteattributes_1712 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
