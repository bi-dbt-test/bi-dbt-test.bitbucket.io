
  create or replace  view datalake_dev.governed.REALPAGE_FACTFINANCIALKPI  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_factfinancialkpi_3116)
SELECT PostDate PostDate
                ,PropertyKey PropertyKey
                ,PropertyName PropertyName
                ,PeriodType PeriodType
                ,FiscalYear FiscalYear
                ,FiscalPeriod FiscalPeriod
                ,PeriodStartDate PeriodStartDate
                ,PeriodEndDate PeriodEndDate
                ,CalendarYear CalendarYear
                ,CalendarMonth CalendarMonth
                ,BookID BookID
                ,YTDBudgetRevenue YTDBudgetRevenue
                ,YTDBudgetExpenses YTDBudgetExpenses
                ,YTDActualRevenue YTDActualRevenue
                ,YTDActualExpenses YTDActualExpenses
                ,YTDMarketRent YTDMarketRent
                ,YTDNetRentIncome YTDNetRentIncome
                ,MTDNetRentIncome MTDNetRentIncome
                ,TotalRentSqFt TotalRentSqFt
                ,MTDNetRentIncomeNoVaccancy MTDNetRentIncomeNoVaccancy
                ,BudgetRentPSF BudgetRentPSF
                ,BudgetRentPSFNoVaccancy BudgetRentPSFNoVaccancy
                ,ActualExpense ActualExpense
                ,BudgetExpense BudgetExpense
                ,NOIYTDBudgetVsActual NOIYTDBudgetVsActual
                ,TotalPriorYearRevenue TotalPriorYearRevenue
                ,TotalPriorYearExpenses TotalPriorYearExpenses
                ,TotalRevenue TotalRevenue
                ,TotalBudget TotalBudget
                ,PriorRevenue PriorRevenue
                ,PriorBudget PriorBudget
                ,RentalIncome RentalIncome
                ,RentalLosses RentalLosses
                ,OtherIncome OtherIncome
                ,PriorRentalIncome PriorRentalIncome
                ,PriorRentalLosses PriorRentalLosses
                ,PriorOtherIncome PriorOtherIncome
                ,PayrollAndRelated PayrollAndRelated
                ,Administrative Administrative
                ,Marketing Marketing
                ,Utilities Utilities
                ,OperatingAndMaintenanceExpenses OperatingAndMaintenanceExpenses
                ,TaxesAndInsuranceExpenses TaxesAndInsuranceExpenses
                ,ManagementFees ManagementFees
                ,Controllable Controllable
                ,UnControllable UnControllable
                ,TurnoverCost TurnoverCost
                ,TurnoverExpense TurnoverExpense
                ,PriorPayrollAndRelated PriorPayrollAndRelated
                ,PriorAdministrative PriorAdministrative
                ,PriorMarketingExpenses PriorMarketingExpenses
                ,PriorUtilites PriorUtilites
                ,PriorOperatingAndMaintenanceExpenses PriorOperatingAndMaintenanceExpenses
                ,PriorTaxesandInsuranceExpenses PriorTaxesandInsuranceExpenses
                ,PriorManagementFees PriorManagementFees
                ,PriorControllable PriorControllable
                ,PriorUnControllable PriorUnControllable
                ,PriorTurnoverCost PriorTurnoverCost
                ,PriorTurnoverExpense PriorTurnoverExpense
                ,BudgetedRentalIncome BudgetedRentalIncome
                ,BudgetedRentalLosses BudgetedRentalLosses
                ,BudgetedOtherIncome BudgetedOtherIncome
                ,BudgetedPriorRentalIncome BudgetedPriorRentalIncome
                ,BudgetedPriorRentalLosses BudgetedPriorRentalLosses
                ,BudgetedPriorOtherIncome BudgetedPriorOtherIncome
                ,BudgetedPayrollAndRelated BudgetedPayrollAndRelated
                ,BudgetedAdministrative BudgetedAdministrative
                ,BudgetedMarketing BudgetedMarketing
                ,BudgetedUtilities BudgetedUtilities
                ,BudgetedOperatingAndMaintenanceExpenses BudgetedOperatingAndMaintenanceExpenses
                ,BudgetedTaxesAndInsuranceExpenses BudgetedTaxesAndInsuranceExpenses
                ,BudgetedManagementFees BudgetedManagementFees
                ,BudgetedControllable BudgetedControllable
                ,BudgetedUnControllable BudgetedUnControllable
                ,BudgetedTurnoverCost BudgetedTurnoverCost
                ,BudgetedTurnoverExpense BudgetedTurnoverExpense
                ,PriorBudgetedPayrollAndRelatedExpenses PriorBudgetedPayrollAndRelatedExpenses
                ,PriorBudgetedAdministrative PriorBudgetedAdministrative
                ,PriorBudgetedOperatingAndMaintenanceExpenses PriorBudgetedOperatingAndMaintenanceExpenses
                ,PriorBudgetedTaxesAndInsuranceExpenses PriorBudgetedTaxesAndInsuranceExpenses
                ,PriorBudgetedManagementFees PriorBudgetedManagementFees
                ,PriorBudgetedControllable PriorBudgetedControllable
                ,PriorBudgetedUnControllable PriorBudgetedUnControllable
                ,PriorBudgetedTurnoverCost PriorBudgetedTurnoverCost
                ,PriorBudgetedTurnoverExpense PriorBudgetedTurnoverExpense
                ,TotalUnitCount TotalUnitCount
                ,LoadDate LoadDate
                ,ChartOfAccountsID ChartOfAccountsID
                ,ChartOfAccountsName ChartOfAccountsName
                ,ChartOfAccountsID_Name ChartOfAccountsID_Name
                
FROM datalake_dev.pre_governed.realpage_factfinancialkpi_3116 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
