
  create or replace  view datalake_dev.governed.REALPAGE_DIMMARKETINGCHANNEL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimmarketingchannel_2203)
SELECT MarketingChannelKey MarketingChannelKey
                ,MarketingChannel MarketingChannel
                ,MarketingCategory MarketingCategory
                ,osl_CDSPMCID osl_CDSPMCID
                
FROM datalake_dev.pre_governed.realpage_dimmarketingchannel_2203 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
