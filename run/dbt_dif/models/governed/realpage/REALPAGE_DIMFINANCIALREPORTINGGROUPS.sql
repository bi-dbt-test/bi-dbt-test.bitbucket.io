
  create or replace  view datalake_dev.governed.REALPAGE_DIMFINANCIALREPORTINGGROUPS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimfinancialreportinggroups_1703)
SELECT ChartOfAccountsID ChartOfAccountsID
                ,ChartOfAccountsName ChartOfAccountsName
                ,ReportingObjectName ReportingObjectName
                ,RowDescription RowDescription
                ,RowType RowType
                ,GLAccountNumber GLAccountNumber
                ,TotalsIntoGLAccount TotalsIntoGLAccount
                ,GLAccountNumberKey GLAccountNumberKey
                ,TotalsIntoGLAccountKey TotalsIntoGLAccountKey
                ,NestingLevel NestingLevel
                ,NormalBalance NormalBalance
                ,Negate Negate
                ,Display Display
                ,UseMasterCOAFlag UseMasterCOAFlag
                ,Tablekey Tablekey
                ,CreateDate CreateDate
                
FROM datalake_dev.pre_governed.realpage_dimfinancialreportinggroups_1703 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
