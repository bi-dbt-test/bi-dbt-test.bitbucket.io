
  create or replace  view datalake_dev.governed.REALPAGE_DIMAMENITY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimamenity_1700)
SELECT AmenityKey AmenityKey
                ,AmenityName AmenityName
                ,AmenityGroup AmenityGroup
                ,ID ID
                ,AmenityCode AmenityCode
                ,MPFValueAmount MPFValueAmount
                ,DisplayFlag DisplayFlag
                ,AmenityDescription AmenityDescription
                ,SiteAmenityGroupID SiteAmenityGroupID
                ,AmenityGroupCode AmenityGroupCode
                ,AmenityGroupDescription AmenityGroupDescription
                
FROM datalake_dev.pre_governed.realpage_dimamenity_1700 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
