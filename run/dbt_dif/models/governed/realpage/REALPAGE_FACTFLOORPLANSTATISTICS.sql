
  create or replace  view datalake_dev.governed.REALPAGE_FACTFLOORPLANSTATISTICS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_factfloorplanstatistics_2719)
SELECT OrganizationKey OrganizationKey
                ,PropertyKey PropertyKey
                ,FloorPlanKey FloorPlanKey
                ,PostPropertyDateKey PostPropertyDateKey
                ,PostDate PostDate
                ,TotalGrossSquareFeet TotalGrossSquareFeet
                ,TotalRentSquareFeet TotalRentSquareFeet
                ,TotalUnitCount TotalUnitCount
                ,DownSquareFeet DownSquareFeet
                ,DownUnitCount DownUnitCount
                ,ModelSquareFeet ModelSquareFeet
                ,ModelUnitCount ModelUnitCount
                ,OtherUnavailableSquareFeet OtherUnavailableSquareFeet
                ,OtherUnavailableUnitCount OtherUnavailableUnitCount
                ,OccupiedSquareFeet OccupiedSquareFeet
                ,OccupiedUnitCount OccupiedUnitCount
                ,VacantUnitCount VacantUnitCount
                ,OccupiedScheduledEffectiveRevenue OccupiedScheduledEffectiveRevenue
                ,OccupiedScheduledConcessions OccupiedScheduledConcessions
                ,OccupiedScheduledRent OccupiedScheduledRent
                ,OccupiedEffectiveRent OccupiedEffectiveRent
                ,MarketRent MarketRent
                ,BudgetRent BudgetRent
                ,TotalAmenities TotalAmenities
                ,AverageAmenities AverageAmenities
                ,OnNoticeUnitCount OnNoticeUnitCount
                ,OnNoticeCurrentMonthUnitCount OnNoticeCurrentMonthUnitCount
                ,OnNoticeFutureMonthUnitCount OnNoticeFutureMonthUnitCount
                ,OnNoticeFutureMonth2UnitCount OnNoticeFutureMonth2UnitCount
                ,OnNoticeFutureMonth3UnitCount OnNoticeFutureMonth3UnitCount
                ,OnNoticePreleasedUnitCount OnNoticePreleasedUnitCount
                ,VacantPreleasedUnitCount VacantPreleasedUnitCount
                ,OnNoticeExposedCurrentMonthUnitCount OnNoticeExposedCurrentMonthUnitCount
                ,OnNoticeExposedFutureMonthUnitCount OnNoticeExposedFutureMonthUnitCount
                ,OnNoticeExposedFutureMonth2UnitCount OnNoticeExposedFutureMonth2UnitCount
                ,OnNoticeExposedFutureMonth3UnitCount OnNoticeExposedFutureMonth3UnitCount
                ,ExposedUnitCount ExposedUnitCount
                ,RenewalOccupiedSquareFeet RenewalOccupiedSquareFeet
                ,RenewalOccupiedUnitCount RenewalOccupiedUnitCount
                ,RenewalOccupiedScheduledEffectiveRevenue RenewalOccupiedScheduledEffectiveRevenue
                ,RenewalOccupiedScheduledConcessions RenewalOccupiedScheduledConcessions
                ,RenewalOccupiedScheduledRent RenewalOccupiedScheduledRent
                ,RenewalOccupiedTotalConcessions RenewalOccupiedTotalConcessions
                ,RenewalOccupiedTotalLeaseTerm RenewalOccupiedTotalLeaseTerm
                ,RenewalOccupiedEffectiveRent RenewalOccupiedEffectiveRent
                ,RenewalOccupiedEffectiveRentTradeOut RenewalOccupiedEffectiveRentTradeOut
                ,NewLeaseOccupiedSquareFeet NewLeaseOccupiedSquareFeet
                ,NewLeaseOccupiedUnitCount NewLeaseOccupiedUnitCount
                ,NewLeaseOccupiedScheduledEffectiveRevenue NewLeaseOccupiedScheduledEffectiveRevenue
                ,NewLeaseOccupiedScheduledConcessions NewLeaseOccupiedScheduledConcessions
                ,NewLeaseOccupiedScheduledRent NewLeaseOccupiedScheduledRent
                ,NewLeaseOccupiedTotalConcessions NewLeaseOccupiedTotalConcessions
                ,NewLeaseOccupiedTotalLeaseTerm NewLeaseOccupiedTotalLeaseTerm
                ,NewLeaseOccupiedEffectiveRent NewLeaseOccupiedEffectiveRent
                ,NewLeaseOccupiedEffectiveRentTradeOut NewLeaseOccupiedEffectiveRentTradeOut
                ,RenewalSignedSquareFeet RenewalSignedSquareFeet
                ,RenewalSignedUnitCount RenewalSignedUnitCount
                ,RenewalSignedScheduledEffectiveRevenue RenewalSignedScheduledEffectiveRevenue
                ,RenewalSignedScheduledConcessions RenewalSignedScheduledConcessions
                ,RenewalSignedScheduledRent RenewalSignedScheduledRent
                ,RenewalSignedTotalConcessions RenewalSignedTotalConcessions
                ,RenewalSignedTotalLeaseTerm RenewalSignedTotalLeaseTerm
                ,RenewalSignedEffectiveRent RenewalSignedEffectiveRent
                ,RenewalSignedEffectiveRentTradeOut RenewalSignedEffectiveRentTradeOut
                ,NewLeaseSignedSquareFeet NewLeaseSignedSquareFeet
                ,NewLeaseSignedUnitCount NewLeaseSignedUnitCount
                ,NewLeaseSignedScheduledEffectiveRevenue NewLeaseSignedScheduledEffectiveRevenue
                ,NewLeaseSignedScheduledConcessions NewLeaseSignedScheduledConcessions
                ,NewLeaseSignedScheduledRent NewLeaseSignedScheduledRent
                ,NewLeaseSignedTotalConcessions NewLeaseSignedTotalConcessions
                ,NewLeaseSignedTotalLeaseTerm NewLeaseSignedTotalLeaseTerm
                ,NewLeaseSignedEffectiveRent NewLeaseSignedEffectiveRent
                ,NewLeaseSignedEffectiveRentTradeOut NewLeaseSignedEffectiveRentTradeOut
                ,RenewalStartSquareFeet RenewalStartSquareFeet
                ,RenewalStartUnitCount RenewalStartUnitCount
                ,RenewalStartScheduledEffectiveRevenue RenewalStartScheduledEffectiveRevenue
                ,RenewalStartScheduledConcessions RenewalStartScheduledConcessions
                ,RenewalStartScheduledRent RenewalStartScheduledRent
                ,RenewalStartTotalConcessions RenewalStartTotalConcessions
                ,RenewalStartTotalLeaseTerm RenewalStartTotalLeaseTerm
                ,RenewalStartEffectiveRent RenewalStartEffectiveRent
                ,RenewalStartEffectiveRentTradeOut RenewalStartEffectiveRentTradeOut
                ,LeaseApplicationCount LeaseApplicationCount
                ,LeaseApplicationCancelledCount LeaseApplicationCancelledCount
                ,LeaseApplicationDeniedCount LeaseApplicationDeniedCount
                ,ExpiringLeaseCount ExpiringLeaseCount
                ,ScheduledExpiringLeaseCount ScheduledExpiringLeaseCount
                ,MoveInLeaseCount MoveInLeaseCount
                ,MoveOutLeaseCount MoveOutLeaseCount
                ,MonthToMonthLeaseCount MonthToMonthLeaseCount
                ,MonthToMonthRenewalOccupiedLeaseCount MonthToMonthRenewalOccupiedLeaseCount
                ,MonthToMonthNewLeaseOccupiedLeaseCount MonthToMonthNewLeaseOccupiedLeaseCount
                ,VacantMarketReadyUnitCount VacantMarketReadyUnitCount
                ,LoadTimestamp LoadTimestamp
                ,TransferMoveInLeaseCount TransferMoveInLeaseCount
                ,TransferMoveOutLeaseCount TransferMoveOutLeaseCount
                ,OccupiedScheduledRent_ResSubj OccupiedScheduledRent_ResSubj
                ,OccupiedEffectiveRent_ResSubj OccupiedEffectiveRent_ResSubj
                ,OccupiedEffectiveRent_Aff OccupiedEffectiveRent_Aff
                ,OnNoticeSquareFeet OnNoticeSquareFeet
                ,OnNoticePreleasedSquareFeet OnNoticePreleasedSquareFeet
                ,VacantSquareFeet VacantSquareFeet
                ,VacantPreleasedSquareFeet VacantPreleasedSquareFeet
                
FROM datalake_dev.pre_governed.realpage_factfloorplanstatistics_2719 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
