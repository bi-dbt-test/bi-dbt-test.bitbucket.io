
  create or replace  view datalake_dev.governed.REALPAGE_DIMSUBPROPERTY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.realpage_dimsubproperty_2858)
SELECT SubPropertyKey SubPropertyKey
                ,PropertyKey PropertyKey
                ,OrganizationKey OrganizationKey
                ,SubPropertyName SubPropertyName
                ,SubPropertyNumber SubPropertyNumber
                ,LocationType LocationType
                ,LocationID LocationID
                ,osl_PropertyID osl_PropertyID
                ,PeriodOpenDate PeriodOpenDate
                ,osl_SubPropertyID osl_SubPropertyID
                ,Source_LocationID Source_LocationID
                ,osl_PMCID osl_PMCID
                ,ChartOfAccountsID ChartOfAccountsID
                ,ChartOfAccountsName ChartOfAccountsName
                ,FirstMonth FirstMonth
                ,IsDeleted IsDeleted
                ,CreateDate CreateDate
                ,Default_Book Default_Book
                
FROM datalake_dev.pre_governed.realpage_dimsubproperty_2858 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
