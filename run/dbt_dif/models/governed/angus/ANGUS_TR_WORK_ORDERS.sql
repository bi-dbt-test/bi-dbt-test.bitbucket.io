
  create or replace  view datalake_dev.governed.ANGUS_TR_WORK_ORDERS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.angus_tr_work_orders_3557)
SELECT PropertyName PropertyName
                ,BuildingName BuildingName
                ,BuildingCode BuildingCode
                ,DisplayId DisplayId
                ,Proactive Proactive
                ,RequestedBy RequestedBy
                ,Status Status
                ,StatusDate StatusDate
                ,DateDue DateDue
                ,DateOpened DateOpened
                ,DateDispatched DateDispatched
                ,DateAccepted DateAccepted
                ,DateWorkStarted DateWorkStarted
                ,DateEstimateGenerated DateEstimateGenerated
                ,DateEstimateApproved DateEstimateApproved
                ,DateCompleted DateCompleted
                ,DateClosed DateClosed
                ,DateVerified DateVerified
                ,DateInvoiced DateInvoiced
                ,TenantName TenantName
                ,TenantID TenantID
                ,AssignedTo AssignedTo
                ,Floor Floor
                ,Suite Suite
                ,RequestType RequestType
                ,Priority Priority
                ,Trade Trade
                ,ContactName ContactName
                ,ContactEmailAddress ContactEmailAddress
                ,Owner Owner
                ,ScheduleId ScheduleId
                ,WoDetail WoDetail
                ,TimeTaken TimeTaken
                ,TotalAmountBilled TotalAmountBilled
                ,TotalAmountEstimated TotalAmountEstimated
                ,LeaseIdBilled LeaseIdBilled
                ,TotalAmountBillable TotalAmountBillable
                ,QRSRating QRSRating
                ,QRSComment QRSComment
                ,ClosureNotes ClosureNotes
                ,RequestSource RequestSource
                ,TimeAcceptance TimeAcceptance
                ,TimeResponse TimeResponse
                ,TimeCompletion TimeCompletion
                ,NumEmployees NumEmployees
                ,IsVendor IsVendor
                ,VendorCode VendorCode
                ,InspectionSchedule InspectionSchedule
                ,BulletinType BulletinType
                ,InspectionId InspectionId
                ,DateEscalated1 DateEscalated1
                ,DateEscalated2 DateEscalated2
                ,DateEscalated3 DateEscalated3
                ,DateDelayed DateDelayed
                ,DelayComment DelayComment
                
FROM datalake_dev.pre_governed.angus_tr_work_orders_3557 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
