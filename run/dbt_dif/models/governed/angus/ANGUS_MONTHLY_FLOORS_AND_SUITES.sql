
  create or replace  view datalake_dev.governed.ANGUS_MONTHLY_FLOORS_AND_SUITES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.angus_monthly_floors_and_suites_1747)
SELECT PropertyName PropertyName
                ,BuildingName BuildingName
                ,BuildingCode BuildingCode
                ,Floor Floor
                ,FloorCode FloorCode
                ,Suite Suite
                ,IsTrArea IsTrArea
                ,IsPmArea IsPmArea
                ,IsCommonArea IsCommonArea
                ,SquareFootage SquareFootage
                ,TenantName TenantName
                ,TenantCode TenantCode
                
FROM datalake_dev.pre_governed.angus_monthly_floors_and_suites_1747 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
