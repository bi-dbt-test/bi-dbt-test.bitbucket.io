
  create or replace  view datalake_dev.governed.ANGUS_MONTHLY_PROPERTIES_AND_BUILDINGS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.angus_monthly_properties_and_buildings_1748)
SELECT PropertyName PropertyName
                ,BuildingName BuildingName
                ,StreetAddress StreetAddress
                ,City City
                ,Province Province
                ,PostalCode PostalCode
                ,SquareFootage SquareFootage
                ,BuildingCode BuildingCode
                ,BuildingClass BuildingClass
                ,PropertyPhone PropertyPhone
                ,PropertyFax PropertyFax
                
FROM datalake_dev.pre_governed.angus_monthly_properties_and_buildings_1748 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
