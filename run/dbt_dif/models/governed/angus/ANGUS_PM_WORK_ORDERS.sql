
  create or replace  view datalake_dev.governed.ANGUS_PM_WORK_ORDERS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.angus_pm_work_orders_3883)
SELECT PropertyName PropertyName
                ,BuildingName BuildingName
                ,BuildingCode BuildingCode
                ,DisplayId DisplayId
                ,Status Status
                ,StatusDate StatusDate
                ,DateOpened DateOpened
                ,DateDue DateDue
                ,DateDispatched DateDispatched
                ,DateCompleted DateCompleted
                ,AssignedTo AssignedTo
                ,Floor Floor
                ,Suite Suite
                ,OnDemand OnDemand
                ,System System
                ,Equipment Equipment
                ,Priority Priority
                ,ScheduleId ScheduleId
                ,WOTitle WOTitle
                ,SchedTitle SchedTitle
                ,Trade Trade
                ,EstTime EstTime
                ,ActTime ActTime
                ,ClosureNotes ClosureNotes
                ,NotifySupervisor NotifySupervisor
                ,MissingValues MissingValues
                ,DateCancelled DateCancelled
                ,IsAutoCancelled IsAutoCancelled
                ,Period Period
                ,PeriodType PeriodType
                
FROM datalake_dev.pre_governed.angus_pm_work_orders_3883 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
