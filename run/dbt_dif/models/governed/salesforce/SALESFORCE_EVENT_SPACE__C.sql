
  create or replace  view datalake_dev.governed.SALESFORCE_EVENT_SPACE__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_event_space__c_4501)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Event_Id_c Event_Id_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Name Name
                ,OwnerId OwnerId
                ,Space_c Space_c
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_event_space__c_4501 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
