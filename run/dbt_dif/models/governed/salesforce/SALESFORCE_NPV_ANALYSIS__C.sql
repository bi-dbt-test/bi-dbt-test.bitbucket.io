
  create or replace  view datalake_dev.governed.SALESFORCE_NPV_ANALYSIS__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_npv_analysis__c_3471)
SELECT Annualized_Income_Lease_c Annualized_Income_Lease_c
                ,Annualized_Income_Proforma_c Annualized_Income_Proforma_c
                ,Base_Rent_Monthly_c Base_Rent_Monthly_c
                ,CAM_Achieved_c CAM_Achieved_c
                ,CAM_Budgeted_Increase_c CAM_Budgeted_Increase_c
                ,CAM_Budgeted_c CAM_Budgeted_c
                ,CAM_Increase_c CAM_Increase_c
                ,Cash_Flow_c Cash_Flow_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Escalations_Lease_c Escalations_Lease_c
                ,Escalations_Proforma_c Escalations_Proforma_c
                ,Favorable_c Favorable_c
                ,Financial_Analysis_Completed_c Financial_Analysis_Completed_c
                ,Free_Rent_Lease_Number_c Free_Rent_Lease_Number_c
                ,Free_Rent_Lease_c Free_Rent_Lease_c
                ,Free_Rent_Proforma_Number_c Free_Rent_Proforma_Number_c
                ,Free_Rent_Proforma_c Free_Rent_Proforma_c
                ,Free_Rent_c Free_Rent_c
                ,Id Id
                ,Inactive_NPV_Analysis_c Inactive_NPV_Analysis_c
                ,Initial_Rent_Lease_c Initial_Rent_Lease_c
                ,Initial_Rent_Proforma_c Initial_Rent_Proforma_c
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Lease_Favorable_c Lease_Favorable_c
                ,Lease_c Lease_c
                ,Leasing_Commission_Lease_c Leasing_Commission_Lease_c
                ,Leasing_Commission_Proforma_c Leasing_Commission_Proforma_c
                ,Meets_Threshold_Formula_c Meets_Threshold_Formula_c
                ,Month_c Month_c
                ,NER_Lease_c NER_Lease_c
                ,NER_Proforma_c NER_Proforma_c
                ,NER_Variance_c NER_Variance_c
                ,NER_w_D_Variance_c NER_w_D_Variance_c
                ,NPV_Lease_c NPV_Lease_c
                ,NPV_Proforma_c NPV_Proforma_c
                ,NPV_TI_Lease_c NPV_TI_Lease_c
                ,NPV_TI_Proforma_c NPV_TI_Proforma_c
                ,Name Name
                ,Operating_Expenses_c Operating_Expenses_c
                ,OwnerId OwnerId
                ,Pro_Rata_Taxes_Achieved_c Pro_Rata_Taxes_Achieved_c
                ,Pro_Rata_Taxes_Budgeted_Increase_c Pro_Rata_Taxes_Budgeted_Increase_c
                ,Pro_Rata_Taxes_Budgeted_c Pro_Rata_Taxes_Budgeted_c
                ,Pro_Rata_Taxes_Increase_c Pro_Rata_Taxes_Increase_c
                ,Property_Valuation_Lease_c Property_Valuation_Lease_c
                ,Property_Valuation_Proforma_c Property_Valuation_Proforma_c
                ,Recoveries_c Recoveries_c
                ,Route_to_President_c Route_to_President_c
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_npv_analysis__c_3471 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
