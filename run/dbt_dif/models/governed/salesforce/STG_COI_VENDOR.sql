

      create or replace  table datalake_dev.governed.STG_COI_VENDOR  as
      (




SELECT CASE WHEN VENDOR_NAME IS NULL THEN (CASE WHEN FORM_VENDOR_NAME IS NOT NULL THEN FORM_VENDOR_NAME 
          ELSE CASE WHEN VENDOR_NAME IS NOT NULL THEN vendor_name ELSE REPLACE(REPLACE(Counterparty_Tenant_COI_Contact_c,CHAR(13),''),CHAR(10),'') END END) 
          ELSE VENDOR_NAME END AS Vendor_Name 
       ,Form_ID AS SF_ContractRecNbr 
       ,COI_Name AS SF_COI_RecNbr 
       ,COI_Status_c AS COI_RecStatus 
       ,CASE WHEN CGL_Each_Occurence_Limit_c IS NULL AND CGL_General_Aggregate_Limit_c IS NOT NULL THEN CGL_General_Aggregate_Limit_c 
        WHEN CGL_Each_Occurence_Limit_c IS NOT NULL AND CGL_General_Aggregate_Limit_c IS NULL THEN CGL_Each_Occurence_Limit_c 
        WHEN CGL_Each_Occurence_Limit_c IS NOT NULL AND CGL_General_Aggregate_Limit_c IS NOT NULL THEN CGL_Each_Occurence_Limit_c || '/' || CGL_General_Aggregate_Limit_c  
          ELSE NULL END AS CGL_Amt 
       ,CGL_Policy_Expiration_Date_c AS CGL_ExpDate  
       ,Umbrella_Limit_c AS UL_Amt 
       ,UL_Policy_Expiration_Date_c AS UL_ExpDate 
       ,Employer_s_Liability_Limit_c AS WC_Amt 
       ,WC_EL_Policy_Expiration_c AS WC_ExpDate 
       ,Automobile_Policy_Limit_c AS AUTO_Amt 
       ,BA_Policy_Expiration_Date_c AS AUTO_ExpDate 
       ,Counterparty_Phone_c AS Vendor_Contact_Phn_Num 
       ,Counterparty_Email_c AS Vendor_Contact_Email 
       ,CASE WHEN Counterparty_Contact_c IS NULL THEN REPLACE(REPLACE(Counterparty_Tenant_COI_Contact_c,CHAR(13),''),CHAR(10),'') 
         ELSE Counterparty_Contact_c END AS Contract_Contact_Name    
       ,Salesforce_Lease_Id_c AS SF_LeaseRecNbr 
       ,Tenant_Contact_Name AS Tenant_Contact_Name 
       ,Contact_Phone_c AS Tenant_Contact_Phn_Num 
       ,Contact_Email_c AS Tenant_Contact_Email 
       ,'Contract' AS RecordType 
       ,Lease_Name 
       ,Counterparty_Tenant_COI_Contact_c AS Tenant_COI_Contact 
       ,Property_Policy_Expiration_Date_c AS LIAB_ExpDate 
       ,Form_Vendor_Name 
       ,property_name_c 
       ,COI_ID 
       ,FID 
      , null LEASE_ID

       ,Owner_name  COI_OWNER_NAME
       ,CASE WHEN Department_c IS NULL THEN '' ELSE Department_c END AS Department 
       ,New_Amendment_c 
       ,Contract_Type_c 
       ,to_varchar(Contract_Expiration_Date_c::date, 'mm/dd/yyyy') AS Contract_Expiration_Date_c    
       ,CASE WHEN Insurance_Needed_c = 'FALSE' THEN 'Yes' WHEN Insurance_Needed_c = 'TRUE' THEN 'No' ELSE 'Blanks' END AS Insurance_Needed 
       ,Insurance_Needed_Date_c 
       ,Master_Property 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,CGL_Policy_Expiration_Date_c) <= 30 AND DATEDIFF(day,CURRENT_DATE,CGL_Policy_Expiration_Date_c) > 0 THEN 1 ELSE 0 END CGL_EXPIRE_IN_30_DAYS 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,CGL_Policy_Expiration_Date_c) = 0 THEN 1 ELSE 0 END CGL_EXPIRE_TODAY 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,UL_Policy_Expiration_Date_c) <= 30 AND DATEDIFF(day,CURRENT_DATE,UL_Policy_Expiration_Date_c) > 0 THEN 1 ELSE 0 END UL_EXPIRE_IN_30_DAYS 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,UL_Policy_Expiration_Date_c) = 0 THEN 1 ELSE 0 END UL_EXPIRE_TODAY 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,WC_EL_Policy_Expiration_c) <= 30 AND DATEDIFF(day,CURRENT_DATE,WC_EL_Policy_Expiration_c) > 0 THEN 1 ELSE 0 END WC_EXPIRE_IN_30_DAYS 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,WC_EL_Policy_Expiration_c) = 0 THEN 1 ELSE 0 END WC_EXPIRE_TODAY 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,BA_Policy_Expiration_Date_c) <= 30 AND DATEDIFF(day,CURRENT_DATE,BA_Policy_Expiration_Date_c) > 0 THEN 1 ELSE 0 END AUTO_EXPIRE_IN_30_DAYS 
       ,CASE WHEN DATEDIFF(day,CURRENT_DATE,BA_Policy_Expiration_Date_c) = 0 THEN 1 ELSE 0 END AUTO_EXPIRE_TODAY 
       ,MASTER_PROPERTY_ID 
       ,PROPERTY_ID 
       ,OWNERID 
       FROM    datalake_dev.semantic.VIEW_COIMPC    
        WHERE RecordTypeID IN ('012i0000001NilmAAC','012i0000001NilrAAC','012i0000001NilwAAC') 
       AND Contract_Status_c <> 'S13: Dead'
      );
    