
  create or replace  view datalake_dev.governed.SALESFORCE_RENT_SCHEDULE_PERIOD__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_rent_schedule_period__c_4503)
SELECT Annualized_Rent_c Annualized_Rent_c
                ,Breakpoint_Amount_c Breakpoint_Amount_c
                ,Breakpoint_Type_c Breakpoint_Type_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Default_Name_c Default_Name_c
                ,External_Id_c External_Id_c
                ,Id Id
                ,Increase_Percent_c Increase_Percent_c
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Lease_ID_c Lease_ID_c
                ,Lease_c Lease_c
                ,Monthly_Rent_c Monthly_Rent_c
                ,Months_c Months_c
                ,Name Name
                ,Percentage_Rent_c Percentage_Rent_c
                ,Period_c Period_c
                ,Prior_Period_c Prior_Period_c
                ,Rate_psf_c Rate_psf_c
                ,Rent_Schedule_c Rent_Schedule_c
                ,Rent_Type_c Rent_Type_c
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_rent_schedule_period__c_4503 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
