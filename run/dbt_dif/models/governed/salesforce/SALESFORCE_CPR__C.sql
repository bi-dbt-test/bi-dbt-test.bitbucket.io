
  create or replace  view datalake_dev.governed.SALESFORCE_CPR__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_cpr__c_3466)
SELECT Additional_Comments_c Additional_Comments_c
                ,Amortization_Period_Years_c Amortization_Period_Years_c
                ,Amount_Over_Under_Budget_c Amount_Over_Under_Budget_c
                ,Amount_Spent_to_Date_c Amount_Spent_to_Date_c
                ,Anticipated_Project_Completion_c Anticipated_Project_Completion_c
                ,Anticipated_Project_Start_c Anticipated_Project_Start_c
                ,Approved_Funds_c Approved_Funds_c
                ,Architect_Engineer_Amount_c Architect_Engineer_Amount_c
                ,Bid_Summary_c Bid_Summary_c
                ,Bids_c Bids_c
                ,Budgeted_Amount_c Budgeted_Amount_c
                ,Budgeted_End_c Budgeted_End_c
                ,Budgeted_Start_c Budgeted_Start_c
                ,Budgeted_Year_c Budgeted_Year_c
                ,CPR_Columbia_Email_Notification_c CPR_Columbia_Email_Notification_c
                ,CPR_Id_c CPR_Id_c
                ,CPR_Sequence_c CPR_Sequence_c
                ,CPR_Status_c CPR_Status_c
                ,Capital_Work_c Capital_Work_c
                ,Category_c Category_c
                ,City_c City_c
                ,Company_Number_c Company_Number_c
                ,Consultant_Costs_c Consultant_Costs_c
                ,Contingency_Amount_c Contingency_Amount_c
                ,Contingency_Available_c Contingency_Available_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Date_Requested_c Date_Requested_c
                ,Dir_of_Facilities_Approval_Date_c Dir_of_Facilities_Approval_Date_c
                ,EVP_Management_Approval_Date_c EVP_Management_Approval_Date_c
                ,Expenditure_passed_through_to_Tenants_c Expenditure_passed_through_to_Tenants_c
                ,Expense_Indicator_1_c Expense_Indicator_1_c
                ,Expense_Indicator_2_c Expense_Indicator_2_c
                ,Expense_Indicator_3_c Expense_Indicator_3_c
                ,Expense_Indicator_4_c Expense_Indicator_4_c
                ,Expense_Indicator_5_c Expense_Indicator_5_c
                ,Expense_Indicator_6_c Expense_Indicator_6_c
                ,Expense_Indicator_7_c Expense_Indicator_7_c
                ,Expense_Indicator_Instructions_c Expense_Indicator_Instructions_c
                ,Final_Approval_Date_c Final_Approval_Date_c
                ,General_Portfolio_Manager_Approval_Date_c General_Portfolio_Manager_Approval_Date_c
                ,General_Portfolio_Manager_c General_Portfolio_Manager_c
                ,Id Id
                ,If_no_are_contingency_funds_available_c If_no_are_contingency_funds_available_c
                ,Ins_Reimbursement_Amount_Requested_c Ins_Reimbursement_Amount_Requested_c
                ,IsDeleted IsDeleted
                ,Is_this_a_budgeted_project_c Is_this_a_budgeted_project_c
                ,Is_this_a_phased_project_c Is_this_a_phased_project_c
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Name Name
                ,Other_Cost_Details_c Other_Cost_Details_c
                ,Other_Costs_c Other_Costs_c
                ,Other_Project_Related_Documents_c Other_Project_Related_Documents_c
                ,Override_to_EVP_c Override_to_EVP_c
                ,President_Approval_Date_c President_Approval_Date_c
                ,Project_Justification_Memo_c Project_Justification_Memo_c
                ,Project_Justification_c Project_Justification_c
                ,Project_Management_Amount_c Project_Management_Amount_c
                ,Project_Name1_c Project_Name1_c
                ,Project_Number_c Project_Number_c
                ,Property_Accountant_2_c Property_Accountant_2_c
                ,Property_Accountant_c Property_Accountant_c
                ,Property_Manager_Approval_Date_c Property_Manager_Approval_Date_c
                ,Property_Manager_c Property_Manager_c
                ,Property_c Property_c
                ,Rebate_Refund_Amount_Expected_c Rebate_Refund_Amount_Expected_c
                ,RecordTypeId RecordTypeId
                ,Regional_President_c Regional_President_c
                ,Remaining_Contingency_c Remaining_Contingency_c
                ,Request_for_Proposal_c Request_for_Proposal_c
                ,Requested_Funds_c Requested_Funds_c
                ,Required_by_Legal_Agreement_with_tenant_c Required_by_Legal_Agreement_with_tenant_c
                ,Secondary_Approval_Date_c Secondary_Approval_Date_c
                ,Select_Approved_Amount_c Select_Approved_Amount_c
                ,Shipping_if_Applicable_c Shipping_if_Applicable_c
                ,Short_Description_c Short_Description_c
                ,State_c State_c
                ,SystemModstamp SystemModstamp
                ,Tax_if_Applicable_c Tax_if_Applicable_c
                ,Total_Amount_Requested_c Total_Amount_Requested_c
                ,Total_Forecasted_Amount_c Total_Forecasted_Amount_c
                ,Type_of_Work_c Type_of_Work_c
                ,Useful_Life_of_Expenditure_Years_c Useful_Life_of_Expenditure_Years_c
                
FROM datalake_dev.pre_governed.salesforce_cpr__c_3466 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
