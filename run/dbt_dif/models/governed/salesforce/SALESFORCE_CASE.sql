
  create or replace  view datalake_dev.governed.SALESFORCE_CASE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_case_3461)
SELECT AccountId AccountId
                ,Application_Platform_c Application_Platform_c
                ,Approvals_c Approvals_c
                ,Approved_c Approved_c
                ,AssetId AssetId
                ,Available_Date_1_c Available_Date_1_c
                ,Available_Date_2_c Available_Date_2_c
                ,Available_Date_3_c Available_Date_3_c
                ,Building_c Building_c
                ,BusinessHoursId BusinessHoursId
                ,CIOs_c CIOs_c
                ,CaseNumber CaseNumber
                ,Case_Data_Quality_c Case_Data_Quality_c
                ,Case_Type_c Case_Type_c
                ,ClosedDate ClosedDate
                ,Comments Comments
                ,Company_Org_c Company_Org_c
                ,Conga_Access_c Conga_Access_c
                ,ContactEmail ContactEmail
                ,ContactFax ContactFax
                ,ContactId ContactId
                ,ContactMobile ContactMobile
                ,ContactPhone ContactPhone
                ,Contracts_c Contracts_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Current_System_IT_Assessment_c Current_System_IT_Assessment_c
                ,Dependencies_c Dependencies_c
                ,Deployment_Date_c Deployment_Date_c
                ,Deployment_Method_c Deployment_Method_c
                ,Deployment_Plan_Components_c Deployment_Plan_Components_c
                ,Description Description
                ,Description_c Description_c
                ,Dev_Sandbox_c Dev_Sandbox_c
                ,Distribution_Lists_c Distribution_Lists_c
                ,DocuSign_License_c DocuSign_License_c
                ,Effort_Level_c Effort_Level_c
                ,EmailToCaseEmail_c EmailToCaseEmail_c
                ,Email_c Email_c
                ,End_Date_c End_Date_c
                ,Estimated_Delivery_Date_c Estimated_Delivery_Date_c
                ,Estimated_Hours_c Estimated_Hours_c
                ,FeedItemId FeedItemId
                ,Forms_c Forms_c
                ,Frozen_c Frozen_c
                ,Function_c Function_c
                ,HH_Assignment_Backup_c HH_Assignment_Backup_c
                ,HH_Assignment_c HH_Assignment_c
                ,Id Id
                ,Impact_c Impact_c
                ,Inspection_Date_c Inspection_Date_c
                ,Integration_Sandbox_c Integration_Sandbox_c
                ,IsClosed IsClosed
                ,IsClosedOnCreate IsClosedOnCreate
                ,IsDeleted IsDeleted
                ,IsEscalated IsEscalated
                ,Issue_Characteristic_c Issue_Characteristic_c
                ,Issue_Location_c Issue_Location_c
                ,Issue_c Issue_c
                ,Language Language
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Leases_c Leases_c
                ,Link_to_Sharepoint_Document_c Link_to_Sharepoint_Document_c
                ,Location_c Location_c
                ,MasterRecordId MasterRecordId
                ,Milestone_Date_c Milestone_Date_c
                ,Mirror_Profile_Name_c Mirror_Profile_Name_c
                ,Mirror_c Mirror_c
                ,Mirrored_User_Role_c Mirrored_User_Role_c
                ,Opportunities_c Opportunities_c
                ,Origin Origin
                ,OwnerId OwnerId
                ,Pardot_c Pardot_c
                ,ParentId ParentId
                ,Permission_Sets_c Permission_Sets_c
                ,Phone_c Phone_c
                ,Post_Deployment_Steps_c Post_Deployment_Steps_c
                ,Preferred_Contact_Method_c Preferred_Contact_Method_c
                ,Primary_Contact_Name_c Primary_Contact_Name_c
                ,Priority Priority
                ,Priority_Order_c Priority_Order_c
                ,Properties_c Properties_c
                ,Reason Reason
                ,Reason_c Reason_c
                ,RecordTypeId RecordTypeId
                ,Repair_Date_c Repair_Date_c
                ,Resources_c Resources_c
                ,SME_c SME_c
                ,Scope_Notes_c Scope_Notes_c
                ,Scope_c Scope_c
                ,Secondary_Contact_Email_c Secondary_Contact_Email_c
                ,Serial_Number_c Serial_Number_c
                ,Servicing_Item_c Servicing_Item_c
                ,SourceId SourceId
                ,Sponsor_Business_c Sponsor_Business_c
                ,Sponsor_Exec_c Sponsor_Exec_c
                ,Sponsor_IT_c Sponsor_IT_c
                ,Start_Date_Term_Date_c Start_Date_Term_Date_c
                ,Start_Date_c Start_Date_c
                ,Status Status
                ,Storage_Unit_Number_c Storage_Unit_Number_c
                ,Subject Subject
                ,SuppliedCompany SuppliedCompany
                ,SuppliedEmail SuppliedEmail
                ,SuppliedName SuppliedName
                ,SuppliedPhone SuppliedPhone
                ,SystemModstamp SystemModstamp
                ,Training_Communication_c Training_Communication_c
                ,Type Type
                ,Unit_Number_lookup_c Unit_Number_lookup_c
                ,Unit_c Unit_c
                ,User_Name_c User_Name_c
                ,Workflows_c Workflows_c
                ,akaCRM_Assingment_c akaCRM_Assingment_c
                ,cirrusadv_Created_by_Cirrus_Insight_c cirrusadv_Created_by_Cirrus_Insight_c
                
FROM datalake_dev.pre_governed.salesforce_case_3461 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
