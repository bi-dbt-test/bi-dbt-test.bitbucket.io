
  create or replace  view datalake_dev.governed.SALESFORCE_LEASING_FORMS__HISTORY_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Leasing_Forms__History_4512)
SELECT CreatedById CreatedById,CreatedDate CreatedDate,Field Field,Id Id,IsDeleted IsDeleted,NewValue NewValue,OldValue OldValue,ParentId ParentId
FROM pre_governed.salesforce_Leasing_Forms__History_4512 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
