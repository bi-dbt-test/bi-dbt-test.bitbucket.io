
  create or replace  view datalake_dev.governed.SALESFORCE_OPPORTUNITYCONTACTROLE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_opportunitycontactrole_4505)
SELECT ContactId ContactId
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,IsPrimary IsPrimary
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,OpportunityId OpportunityId
                ,Role Role
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_opportunitycontactrole_4505 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
