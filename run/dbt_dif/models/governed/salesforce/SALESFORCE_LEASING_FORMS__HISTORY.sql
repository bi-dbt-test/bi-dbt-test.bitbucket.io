
  create or replace  view datalake_dev.governed.SALESFORCE_LEASING_FORMS__HISTORY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_leasing_forms__history_4737)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Field Field
                ,Id Id
                ,IsDeleted IsDeleted
                ,NewValue NewValue
                ,OldValue OldValue
                ,ParentId ParentId
                
FROM datalake_dev.pre_governed.salesforce_leasing_forms__history_4737 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
