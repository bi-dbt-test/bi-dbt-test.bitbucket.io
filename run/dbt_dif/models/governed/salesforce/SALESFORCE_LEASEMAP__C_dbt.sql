
  create or replace  view datalake_dev.governed.SALESFORCE_LEASEMAP__C_dbt  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_LeaseMap__c_2095)
SELECT CreatedById CreatedById,CreatedDate CreatedDate,FromObject_c FromObject_c,Id Id,IsDeleted IsDeleted,LastModifiedById LastModifiedById,Name Name,SetupOwnerId SetupOwnerId,SystemModstamp SystemModstamp,LastModifiedDate LastModifiedDate,Mapfield_c Mapfield_c
FROM pre_governed.salesforce_LeaseMap__c_2095 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
