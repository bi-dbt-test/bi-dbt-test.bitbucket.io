
  create or replace  view datalake_dev.governed.SALESFORCE_ZONE__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_zone__c_4499)
SELECT Base_Rent_psf_c Base_Rent_psf_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Name Name
                ,Property_c Property_c
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_zone__c_4499 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
