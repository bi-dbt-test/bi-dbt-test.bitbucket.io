
  create or replace  view datalake_dev.governed.SALESFORCE_DSFS__DOCUSIGNACCOUNTCONFIGURATION__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_dsfs__docusignaccountconfiguration__c_3464)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Name Name
                ,OwnerId OwnerId
                ,SystemModstamp SystemModstamp
                ,dsfs_AccountId_c dsfs_AccountId_c
                ,dsfs_ChatterActionsUndoAndSend_c dsfs_ChatterActionsUndoAndSend_c
                ,dsfs_ChatterEnvCompletedText_c dsfs_ChatterEnvCompletedText_c
                ,dsfs_ChatterEnvCompleted_c dsfs_ChatterEnvCompleted_c
                ,dsfs_ChatterEnvDeclinedText_c dsfs_ChatterEnvDeclinedText_c
                ,dsfs_ChatterEnvDeclined_c dsfs_ChatterEnvDeclined_c
                ,dsfs_ChatterEnvDeliveredText_c dsfs_ChatterEnvDeliveredText_c
                ,dsfs_ChatterEnvDelivered_c dsfs_ChatterEnvDelivered_c
                ,dsfs_ChatterEnvSentText_c dsfs_ChatterEnvSentText_c
                ,dsfs_ChatterEnvSent_c dsfs_ChatterEnvSent_c
                ,dsfs_ChatterEnvVoidedText_c dsfs_ChatterEnvVoidedText_c
                ,dsfs_ChatterEnvVoided_c dsfs_ChatterEnvVoided_c
                ,dsfs_ChatterEnvelopeSigning_c dsfs_ChatterEnvelopeSigning_c
                ,dsfs_ChatterRecipCompletedText_c dsfs_ChatterRecipCompletedText_c
                ,dsfs_ChatterRecipCompleted_c dsfs_ChatterRecipCompleted_c
                ,dsfs_ChatterRecipDeclinedText_c dsfs_ChatterRecipDeclinedText_c
                ,dsfs_ChatterRecipDeclined_c dsfs_ChatterRecipDeclined_c
                ,dsfs_ChatterRecipDeliveredText_c dsfs_ChatterRecipDeliveredText_c
                ,dsfs_ChatterRecipDelivered_c dsfs_ChatterRecipDelivered_c
                ,dsfs_ChatterRecipSentText_c dsfs_ChatterRecipSentText_c
                ,dsfs_ChatterRecipSent_c dsfs_ChatterRecipSent_c
                ,dsfs_ChatterRecipSignedText_c dsfs_ChatterRecipSignedText_c
                ,dsfs_ChatterRecipSigned_c dsfs_ChatterRecipSigned_c
                ,dsfs_ChatterSetting_c dsfs_ChatterSetting_c
                ,dsfs_DSProSFPassword_c dsfs_DSProSFPassword_c
                ,dsfs_DSProSFUsername_c dsfs_DSProSFUsername_c
                ,dsfs_DefaultExpireNDays_c dsfs_DefaultExpireNDays_c
                ,dsfs_DefaultReminderNDays_c dsfs_DefaultReminderNDays_c
                ,dsfs_DefaultRepeatReminderNDays_c dsfs_DefaultRepeatReminderNDays_c
                ,dsfs_DefaultRoleNames_c dsfs_DefaultRoleNames_c
                ,dsfs_DefaultRoleValues_c dsfs_DefaultRoleValues_c
                ,dsfs_DefaultWarnOfExpireNDays_c dsfs_DefaultWarnOfExpireNDays_c
                ,dsfs_Default_Apex_Timeout_c dsfs_Default_Apex_Timeout_c
                ,dsfs_DisplayInPersonSigningInOwnWindow_c dsfs_DisplayInPersonSigningInOwnWindow_c
                ,dsfs_DisplayTaggerInOwnWindow_c dsfs_DisplayTaggerInOwnWindow_c
                ,dsfs_DocuSignBaseURL_c dsfs_DocuSignBaseURL_c
                ,dsfs_DocuSignEnvironment_c dsfs_DocuSignEnvironment_c
                ,dsfs_EmailBody_c dsfs_EmailBody_c
                ,dsfs_EmailBody_de_c dsfs_EmailBody_de_c
                ,dsfs_EmailBody_es_c dsfs_EmailBody_es_c
                ,dsfs_EmailBody_fr_c dsfs_EmailBody_fr_c
                ,dsfs_EmailBody_it_c dsfs_EmailBody_it_c
                ,dsfs_EmailBody_ja_c dsfs_EmailBody_ja_c
                ,dsfs_EmailBody_ko_c dsfs_EmailBody_ko_c
                ,dsfs_EmailBody_nl_c dsfs_EmailBody_nl_c
                ,dsfs_EmailBody_pt_BR_c dsfs_EmailBody_pt_BR_c
                ,dsfs_EmailBody_ru_c dsfs_EmailBody_ru_c
                ,dsfs_EmailBody_zh_CN_c dsfs_EmailBody_zh_CN_c
                ,dsfs_EmailSubject_c dsfs_EmailSubject_c
                ,dsfs_EmailSubject_de_c dsfs_EmailSubject_de_c
                ,dsfs_EmailSubject_es_c dsfs_EmailSubject_es_c
                ,dsfs_EmailSubject_fr_c dsfs_EmailSubject_fr_c
                ,dsfs_EmailSubject_it_c dsfs_EmailSubject_it_c
                ,dsfs_EmailSubject_ja_c dsfs_EmailSubject_ja_c
                ,dsfs_EmailSubject_ko_c dsfs_EmailSubject_ko_c
                ,dsfs_EmailSubject_nl_c dsfs_EmailSubject_nl_c
                ,dsfs_EmailSubject_pt_BR_c dsfs_EmailSubject_pt_BR_c
                ,dsfs_EmailSubject_ru_c dsfs_EmailSubject_ru_c
                ,dsfs_EmailSubject_zh_CN_c dsfs_EmailSubject_zh_CN_c
                ,dsfs_EnableNewMobileTaggingUI_c dsfs_EnableNewMobileTaggingUI_c
                ,dsfs_FetchDocuments_c dsfs_FetchDocuments_c
                ,dsfs_HideChatter_c dsfs_HideChatter_c
                ,dsfs_HideEmailMessage_c dsfs_HideEmailMessage_c
                ,dsfs_HideEmailSubject_c dsfs_HideEmailSubject_c
                ,dsfs_HideRemindExpire_c dsfs_HideRemindExpire_c
                ,dsfs_HideSendNowButton_c dsfs_HideSendNowButton_c
                ,dsfs_HideTagButton_c dsfs_HideTagButton_c
                ,dsfs_IncludeDefaultAnchorTabs_c dsfs_IncludeDefaultAnchorTabs_c
                ,dsfs_Lightning_Component_Logging_c dsfs_Lightning_Component_Logging_c
                ,dsfs_LoadFiles_c dsfs_LoadFiles_c
                ,dsfs_LookupUser_c dsfs_LookupUser_c
                ,dsfs_ManageUsersFields_c dsfs_ManageUsersFields_c
                ,dsfs_OrganizationId_c dsfs_OrganizationId_c
                ,dsfs_SetupDone_c dsfs_SetupDone_c
                ,dsfs_UpgradeHelper_c dsfs_UpgradeHelper_c
                ,dsfs_UseAccountNotificationDefaults_c dsfs_UseAccountNotificationDefaults_c
                ,dsfs_UseSendOnBehalfOf_c dsfs_UseSendOnBehalfOf_c
                
FROM datalake_dev.pre_governed.salesforce_dsfs__docusignaccountconfiguration__c_3464 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
