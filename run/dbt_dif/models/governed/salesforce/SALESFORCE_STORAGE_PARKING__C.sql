
  create or replace  view datalake_dev.governed.SALESFORCE_STORAGE_PARKING__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_storage_parking__c_3910)
SELECT Account_c Account_c
                ,Additional_Buyer_1_Contract_c Additional_Buyer_1_Contract_c
                ,Additional_Buyer_2_Contract_c Additional_Buyer_2_Contract_c
                ,Additional_Buyer_3_Contract_c Additional_Buyer_3_Contract_c
                ,Cancelled_Date_c Cancelled_Date_c
                ,Closed_date_c Closed_date_c
                ,Contract_Address_c Contract_Address_c
                ,Contract_Out_Date_c Contract_Out_Date_c
                ,Contracted_Date_c Contracted_Date_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Deposit_Received_Amount_c Deposit_Received_Amount_c
                ,Deposit_Received_c Deposit_Received_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Name Name
                ,Note_c Note_c
                ,Opp_Building_c Opp_Building_c
                ,Opportunity_c Opportunity_c
                ,OwnerId OwnerId
                ,Parking_c Parking_c
                ,Past_Closing_c Past_Closing_c
                ,Space_Price_c Space_Price_c
                ,Space_c Space_c
                ,Stage_c Stage_c
                ,Storage_c Storage_c
                ,SystemModstamp SystemModstamp
                ,Unit_c Unit_c
                ,dev_Account_Billing_Formula_c dev_Account_Billing_Formula_c
                ,dev_CloseDate_c dev_CloseDate_c
                
FROM datalake_dev.pre_governed.salesforce_storage_parking__c_3910 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
