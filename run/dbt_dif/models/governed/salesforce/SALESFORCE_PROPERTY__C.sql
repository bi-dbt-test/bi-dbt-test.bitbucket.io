
  create or replace  view datalake_dev.governed.SALESFORCE_PROPERTY__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_property__c_4132)
SELECT Accounting_Manager_c Accounting_Manager_c
                ,Accounting_Queue_c Accounting_Queue_c
                ,Asset_Mgmt_Approver_c Asset_Mgmt_Approver_c
                ,Building_Permit_Approval_Queue_c Building_Permit_Approval_Queue_c
                ,CFO_Approver_User_c CFO_Approver_User_c
                ,COVID_19_Assigned_Legal_c COVID_19_Assigned_Legal_c
                ,COVID_19_Assigned_Paralegal_c COVID_19_Assigned_Paralegal_c
                ,Calendar_Id_c Calendar_Id_c
                ,Capital_Markets_Approver_User_c Capital_Markets_Approver_User_c
                ,Change_Order_Queue_c Change_Order_Queue_c
                ,Change_Request_Queue_c Change_Request_Queue_c
                ,City_c City_c
                ,Company_Code_c Company_Code_c
                ,Company_Description_c Company_Description_c
                ,Contracts_Min_CGL_Each_Occurrence_Limit_c Contracts_Min_CGL_Each_Occurrence_Limit_c
                ,Contracts_Min_CGL_Gen_Aggregate_Limit_c Contracts_Min_CGL_Gen_Aggregate_Limit_c
                ,Contracts_Min_Combined_Policy_Limit_c Contracts_Min_Combined_Policy_Limit_c
                ,Contracts_Min_Employer_s_Liability_Limit_c Contracts_Min_Employer_s_Liability_Limit_c
                ,Contracts_Min_Prod_Comp_Ops_Agg_Limit_c Contracts_Min_Prod_Comp_Ops_Agg_Limit_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Description_c Description_c
                ,Development_2_Approver_c Development_2_Approver_c
                ,Development_Approver_c Development_Approver_c
                ,Discount_Rate_c Discount_Rate_c
                ,Display_Name_Formula_c Display_Name_Formula_c
                ,Display_Name_c Display_Name_c
                ,EVP_Leasing_Approver_c EVP_Leasing_Approver_c
                ,EVP_Ops_c EVP_Ops_c
                ,EVP_of_Operations_Approver_c EVP_of_Operations_Approver_c
                ,End_Date_c End_Date_c
                ,Entity_c Entity_c
                ,External_Commission_c External_Commission_c
                ,First_Closing_Date_c First_Closing_Date_c
                ,G7_Member_c G7_Member_c
                ,GM_Approver_c GM_Approver_c
                ,Gross_Sq_Footage_c Gross_Sq_Footage_c
                ,Id Id
                ,Internal_Commission_c Internal_Commission_c
                ,IsDeleted IsDeleted
                ,IsMasterProperty_c IsMasterProperty_c
                ,JDE_Integrate_c JDE_Integrate_c
                ,JDE_Property_ID_c JDE_Property_ID_c
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Lead_Queue_c Lead_Queue_c
                ,Lease_Notification_Queue_c Lease_Notification_Queue_c
                ,Legal_Chatter_Group_c Legal_Chatter_Group_c
                ,Legal_Queue_c Legal_Queue_c
                ,Load_Factor_c Load_Factor_c
                ,MCRP24_c MCRP24_c
                ,MasterProperty_c MasterProperty_c
                ,Min_CGL_Personal_Ad_Injury_Limit_c Min_CGL_Personal_Ad_Injury_Limit_c
                ,Min_Prod_Completed_Ops_Aggregate_c Min_Prod_Completed_Ops_Aggregate_c
                ,Minimum_CGL_Each_Occurrence_Limit_c Minimum_CGL_Each_Occurrence_Limit_c
                ,Minimum_CGL_General_Aggregate_Limit_c Minimum_CGL_General_Aggregate_Limit_c
                ,Minimum_Combined_Policy_Limit_c Minimum_Combined_Policy_Limit_c
                ,Minimum_Employer_s_Liability_Limit_c Minimum_Employer_s_Liability_Limit_c
                ,Name Name
                ,Net_Rentable_Area_c Net_Rentable_Area_c
                ,No_Property_Name_c No_Property_Name_c
                ,Office_Address_c Office_Address_c
                ,Office_Leasing_Proxy_c Office_Leasing_Proxy_c
                ,Operating_Accounting_Approver_c Operating_Accounting_Approver_c
                ,OwnerId OwnerId
                ,President_Approver_c President_Approver_c
                ,Property_Abbr_c Property_Abbr_c
                ,Property_Accounting_1_c Property_Accounting_1_c
                ,Property_Accounting_2_c Property_Accounting_2_c
                ,Property_Budget_Details_c Property_Budget_Details_c
                ,Property_Manager_c Property_Manager_c
                ,Property_Name_Custom_c Property_Name_Custom_c
                ,Property_Notes_c Property_Notes_c
                ,Property_Photo_1_c Property_Photo_1_c
                ,Property_Photo_2_c Property_Photo_2_c
                ,Property_Status_c Property_Status_c
                ,Property_Type_c Property_Type_c
                ,Property_Use_Type_c Property_Use_Type_c
                ,Property_Website_c Property_Website_c
                ,RecordTypeId RecordTypeId
                ,Region_Code_c Region_Code_c
                ,Root_Property_c Root_Property_c
                ,SLM_Approver_c SLM_Approver_c
                ,Sales_Year_c Sales_Year_c
                ,Search_Description_Text_c Search_Description_Text_c
                ,Search_Description_c Search_Description_c
                ,Senior_Leasing_Approver_User_c Senior_Leasing_Approver_User_c
                ,Specialty_Leasing_Approver_c Specialty_Leasing_Approver_c
                ,Specialty_Min_CGL_Each_Occurrence_Limit_c Specialty_Min_CGL_Each_Occurrence_Limit_c
                ,Specialty_Min_CGL_Gen_Aggregate_Limit_c Specialty_Min_CGL_Gen_Aggregate_Limit_c
                ,Specialty_Min_CGL_Personal_Ad_Injury_c Specialty_Min_CGL_Personal_Ad_Injury_c
                ,Specialty_Min_Combined_Policy_Limit_c Specialty_Min_Combined_Policy_Limit_c
                ,Specialty_Min_Employer_s_Liability_Limit_c Specialty_Min_Employer_s_Liability_Limit_c
                ,Specialty_Min_Prod_Comp_Ops_Agg_Limit_c Specialty_Min_Prod_Comp_Ops_Agg_Limit_c
                ,Sq_Footage_c Sq_Footage_c
                ,Start_Date_c Start_Date_c
                ,State_c State_c
                ,Street_Address_c Street_Address_c
                ,Sum_of_All_Condo_Sqf_c Sum_of_All_Condo_Sqf_c
                ,Summary_of_Property_Key_Points_c Summary_of_Property_Key_Points_c
                ,SystemModstamp SystemModstamp
                ,Technical_Accounting_Approver_c Technical_Accounting_Approver_c
                ,Tenant_Coordination_2_c Tenant_Coordination_2_c
                ,Tenant_Coordination_Queue_c Tenant_Coordination_Queue_c
                ,Tenant_Coordinator_1_c Tenant_Coordinator_1_c
                ,Tenant_Coordinator_3_c Tenant_Coordinator_3_c
                ,Total_Common_Area_Sq_Footage_c Total_Common_Area_Sq_Footage_c
                ,Total_Leasable_Sq_Footage_c Total_Leasable_Sq_Footage_c
                ,Total_Unit_Count_c Total_Unit_Count_c
                ,Total_Unusable_Sq_Footage_c Total_Unusable_Sq_Footage_c
                ,Total_Usable_Sq_Footage_c Total_Usable_Sq_Footage_c
                ,Trade_Name_c Trade_Name_c
                ,VP_Leasing_Approver_c VP_Leasing_Approver_c
                ,Waiea_Total_Revenue_c Waiea_Total_Revenue_c
                ,ZIP_Code_c ZIP_Code_c
                ,eSitePlan_File_c eSitePlan_File_c
                ,eSitePlan_Path_c eSitePlan_Path_c
                
FROM datalake_dev.pre_governed.salesforce_property__c_4132 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
