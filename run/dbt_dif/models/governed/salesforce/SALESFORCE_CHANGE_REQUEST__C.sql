
  create or replace  view datalake_dev.governed.SALESFORCE_CHANGE_REQUEST__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_change_request__c_4494)
SELECT CR_Count_c CR_Count_c
                ,Comments1_c Comments1_c
                ,Comments_2_c Comments_2_c
                ,Comments_3_c Comments_3_c
                ,Comments_4_c Comments_4_c
                ,Comments_5_c Comments_5_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Date_Assigned_to_LA_c Date_Assigned_to_LA_c
                ,Date_Changes_Completed_c Date_Changes_Completed_c
                ,Days_to_Process_CR_c Days_to_Process_CR_c
                ,GM_c GM_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,JDE_Entry_Status_c JDE_Entry_Status_c
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Lease_Accounting_Assignee_c Lease_Accounting_Assignee_c
                ,Leasing_Form_c Leasing_Form_c
                ,Move_In_Out_Form_c Move_In_Out_Form_c
                ,Name Name
                ,New_Change_or_Modify_Existing1_c New_Change_or_Modify_Existing1_c
                ,New_Change_or_Modify_Existing_2_c New_Change_or_Modify_Existing_2_c
                ,New_Change_or_Modify_Existing_3_c New_Change_or_Modify_Existing_3_c
                ,New_Change_or_Modify_Existing_4_c New_Change_or_Modify_Existing_4_c
                ,New_Change_or_Modify_Existing_5_c New_Change_or_Modify_Existing_5_c
                ,New_Value_1_c New_Value_1_c
                ,New_Value_2_c New_Value_2_c
                ,New_Value_3_c New_Value_3_c
                ,New_Value_4_c New_Value_4_c
                ,New_Value_5_c New_Value_5_c
                ,Old_Value_1_c Old_Value_1_c
                ,Old_Value_2_c Old_Value_2_c
                ,Old_Value_3_c Old_Value_3_c
                ,Old_Value_4_c Old_Value_4_c
                ,Old_Value_5_c Old_Value_5_c
                ,OwnerId OwnerId
                ,Property_Accounting_1_c Property_Accounting_1_c
                ,RecordTypeId RecordTypeId
                ,Status_c Status_c
                ,SystemModstamp SystemModstamp
                ,Type_of_Change_1_c Type_of_Change_1_c
                ,Type_of_Change_2_c Type_of_Change_2_c
                ,Type_of_Change_3_c Type_of_Change_3_c
                ,Type_of_Change_4_c Type_of_Change_4_c
                ,Type_of_Change_5_c Type_of_Change_5_c
                ,What_is_Changing_1_c What_is_Changing_1_c
                ,What_is_Changing_2_c What_is_Changing_2_c
                ,What_is_Changing_3_c What_is_Changing_3_c
                ,What_is_Changing_4_c What_is_Changing_4_c
                ,What_is_Changing_5_c What_is_Changing_5_c
                
FROM datalake_dev.pre_governed.salesforce_change_request__c_4494 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
