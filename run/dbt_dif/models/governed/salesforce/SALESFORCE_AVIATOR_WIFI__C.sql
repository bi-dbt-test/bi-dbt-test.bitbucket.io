
  create or replace  view datalake_dev.governed.SALESFORCE_AVIATOR_WIFI__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_aviator_wifi__c_3457)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Email_c Email_c
                ,First_Name_c First_Name_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Last_Name_c Last_Name_c
                ,Name Name
                ,OwnerId OwnerId
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_aviator_wifi__c_3457 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
