
  create or replace  view datalake_dev.governed.SALESFORCE_SPACE_BUDGET_DETAILS__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_space_budget_details__c_2103)
SELECT Annual_Growth_Budgeted_Currenc_c Annual_Growth_Budgeted_Currenc_c
                ,Annual_Growth_per_Annum_Budgeted_c Annual_Growth_per_Annum_Budgeted_c
                ,Apr_c Apr_c
                ,Aug_c Aug_c
                ,Broker_Commissions_Budgeted_c Broker_Commissions_Budgeted_c
                ,Budget_1st_Yr_Cashflow_Prorated_c Budget_1st_Yr_Cashflow_Prorated_c
                ,Budget_1st_Yr_Cashflow_c Budget_1st_Yr_Cashflow_c
                ,Budget_Capital_Prorated_c Budget_Capital_Prorated_c
                ,Budget_GLA_to_be_Used_c Budget_GLA_to_be_Used_c
                ,Budget_Lease_c Budget_Lease_c
                ,Budget_RCD_c Budget_RCD_c
                ,Budget_Sq_Footage_c Budget_Sq_Footage_c
                ,Budget_Status_c Budget_Status_c
                ,Budget_Unit_Area_c Budget_Unit_Area_c
                ,Budget_Year_Space_c Budget_Year_Space_c
                ,Budget_Year_c Budget_Year_c
                ,CAM_Budgeted_Increase_c CAM_Budgeted_Increase_c
                ,CAM_Budgeted_c CAM_Budgeted_c
                ,CAM_Growth_Ann_c CAM_Growth_Ann_c
                ,CAM_Growth_Budgeted_c CAM_Growth_Budgeted_c
                ,CAM_Total_Budgeted_c CAM_Total_Budgeted_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Dec_c Dec_c
                ,Feb_c Feb_c
                ,Free_Rent_Budgeted_c Free_Rent_Budgeted_c
                ,Ground_Level_Budgeted_PSF_c Ground_Level_Budgeted_PSF_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,Jan_c Jan_c
                ,Jul_c Jul_c
                ,Jun_c Jun_c
                ,LL_Work_Budgeted_PSF_c LL_Work_Budgeted_PSF_c
                ,Landlord_Work_Est_Budgeted_c Landlord_Work_Est_Budgeted_c
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Lease_By_c Lease_By_c
                ,Leasing_Rev_Budgeted_Ann_c Leasing_Rev_Budgeted_Ann_c
                ,Leasing_Rev_Budgeted_PSF_c Leasing_Rev_Budgeted_PSF_c
                ,Mar_c Mar_c
                ,May_c May_c
                ,Min_Rent_Budgeted_Ann_c Min_Rent_Budgeted_Ann_c
                ,Min_Rent_Budgeted_PSF_c Min_Rent_Budgeted_PSF_c
                ,Name Name
                ,Nov_c Nov_c
                ,Oct_c Oct_c
                ,Promo_Budgeted_c Promo_Budgeted_c
                ,RecordTypeId RecordTypeId
                ,Rent_Budgeted_Ann_c Rent_Budgeted_Ann_c
                ,Rent_Budgeted_PSF_c Rent_Budgeted_PSF_c
                ,Sales_Budgeted_PSF_c Sales_Budgeted_PSF_c
                ,Sep_c Sep_c
                ,Space_c Space_c
                ,SystemModstamp SystemModstamp
                ,TA_Rate_PSF_Budgeted_c TA_Rate_PSF_Budgeted_c
                ,Tax_Growth_Budgeted_c Tax_Growth_Budgeted_c
                ,Taxes_Budgeted_c Taxes_Budgeted_c
                ,Tenant_Constr_Allowance_Budgeted_c Tenant_Constr_Allowance_Budgeted_c
                ,Term_c Term_c
                ,Total_Budgeted_Base_Rent_PSF_c Total_Budgeted_Base_Rent_PSF_c
                ,Total_Capital_Budgeted_Ann_c Total_Capital_Budgeted_Ann_c
                ,Total_Capital_Budgeted_PSF_c Total_Capital_Budgeted_PSF_c
                ,Total_Fixed_Fee_c Total_Fixed_Fee_c
                ,White_Box_Budgeted_PSF_c White_Box_Budgeted_PSF_c
                ,X2nd_Level_Budgeted_PSF_c X2nd_Level_Budgeted_PSF_c
                
FROM datalake_dev.pre_governed.salesforce_space_budget_details__c_2103 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
