
  create or replace  view datalake_dev.governed.SALESFORCE_ACCOUNTCONTACTRELATION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_accountcontactrelation_4504)
SELECT AccountId AccountId
                ,ContactId ContactId
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,EndDate EndDate
                ,Id Id
                ,IsActive IsActive
                ,IsDeleted IsDeleted
                ,IsDirect IsDirect
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Roles Roles
                ,StartDate StartDate
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_accountcontactrelation_4504 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
