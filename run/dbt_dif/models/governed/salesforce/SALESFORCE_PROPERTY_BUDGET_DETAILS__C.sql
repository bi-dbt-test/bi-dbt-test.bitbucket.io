
  create or replace  view datalake_dev.governed.SALESFORCE_PROPERTY_BUDGET_DETAILS__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_property_budget_details__c_3850)
SELECT Annual_Growth_per_Annum_Budgeted_c Annual_Growth_per_Annum_Budgeted_c
                ,Barricade_Amount_Budgeted_c Barricade_Amount_Budgeted_c
                ,Barricade_Graphic_Charge_Budgeted_c Barricade_Graphic_Charge_Budgeted_c
                ,Budget_Notes_c Budget_Notes_c
                ,Budget_Year_Property_c Budget_Year_Property_c
                ,Budget_Year_c Budget_Year_c
                ,Budgeted_Amount_c Budgeted_Amount_c
                ,CAM_Budgeted_c CAM_Budgeted_c
                ,Central_Plant_Budgeted_c Central_Plant_Budgeted_c
                ,Construction_Barricade_Budgeted_c Construction_Barricade_Budgeted_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Design_Fee_Budgeted_c Design_Fee_Budgeted_c
                ,Electricity_Budgeted_c Electricity_Budgeted_c
                ,Encl_Mall_HVAC_Budgeted_c Encl_Mall_HVAC_Budgeted_c
                ,FDS_Budgeted_c FDS_Budgeted_c
                ,Food_Court_Budgeted_c Food_Court_Budgeted_c
                ,Food_Court_Increase_Budgeted_c Food_Court_Increase_Budgeted_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Lease_Dep_Pre_Paid_Rent_Budgeted_c Lease_Dep_Pre_Paid_Rent_Budgeted_c
                ,Name Name
                ,OEInitial_Rate_PSF_Budgeted_c OEInitial_Rate_PSF_Budgeted_c
                ,Other_1_Budgeted_c Other_1_Budgeted_c
                ,Other_1_Description_c Other_1_Description_c
                ,Other_2_Budgeted_c Other_2_Budgeted_c
                ,Other_2_Description_c Other_2_Description_c
                ,Other_3_Budgeted_c Other_3_Budgeted_c
                ,Other_3_Description_c Other_3_Description_c
                ,Other_Landlord_Costs_Budgeted_c Other_Landlord_Costs_Budgeted_c
                ,Parking_Budgeted_c Parking_Budgeted_c
                ,Plan_Review_Fee_Budgeted_c Plan_Review_Fee_Budgeted_c
                ,Promo_Budgeted_c Promo_Budgeted_c
                ,Promo_Initial_Rate_Budgeted_PSF_c Promo_Initial_Rate_Budgeted_PSF_c
                ,Promo_Initial_Rate_PSF_Budgeted_c Promo_Initial_Rate_PSF_Budgeted_c
                ,Property_c Property_c
                ,SystemModstamp SystemModstamp
                ,Tax_Initial_Rate_PSF_Budgeted_c Tax_Initial_Rate_PSF_Budgeted_c
                ,Taxes_Budgeted_c Taxes_Budgeted_c
                ,Trash_Budgeted_c Trash_Budgeted_c
                ,Water_Budgeted_c Water_Budgeted_c
                
FROM datalake_dev.pre_governed.salesforce_property_budget_details__c_3850 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
