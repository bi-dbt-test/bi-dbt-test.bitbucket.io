
  create or replace  view datalake_dev.governed.SALESFORCE_CLUB_SEAT__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_club_seat__c_3460)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Form_Lookup_c Form_Lookup_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Name Name
                ,OwnerId OwnerId
                ,Quantity_c Quantity_c
                ,Row_c Row_c
                ,Seat_Numbers_c Seat_Numbers_c
                ,Section_c Section_c
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_club_seat__c_3460 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
