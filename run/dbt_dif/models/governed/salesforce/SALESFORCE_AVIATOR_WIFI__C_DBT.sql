
  create or replace  view datalake_dev.governed.SALESFORCE_AVIATOR_WIFI__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_aviator_wifi__c_3457)
SELECT CreatedById CreatedById,CreatedDate CreatedDate,Email_c Email_c,First_Name_c First_Name_c,Id Id,IsDeleted IsDeleted,LastModifiedDate LastModifiedDate,LastReferencedDate LastReferencedDate,LastViewedDate LastViewedDate,Name Name,SystemModstamp SystemModstamp,Last_Name_c Last_Name_c,LastModifiedById LastModifiedById,OwnerId OwnerId
FROM pre_governed.salesforce_aviator_wifi__c_3457 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
