
  create or replace  view datalake_dev.governed.SALESFORCE_OPPORTUNITY_SPACE__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_opportunity_space__c_4498)
SELECT Base_Rent_c Base_Rent_c
                ,Base_Rent_psf_c Base_Rent_psf_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,Is_Active_Opportunity_c Is_Active_Opportunity_c
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Leasable_Sq_Footage_c Leasable_Sq_Footage_c
                ,Leasable_Sq_Footage_on_Space_c Leasable_Sq_Footage_on_Space_c
                ,Name Name
                ,Opportunity_c Opportunity_c
                ,Property_c Property_c
                ,Space_c Space_c
                ,Sq_Footage_c Sq_Footage_c
                ,Sq_Footage_on_Space_c Sq_Footage_on_Space_c
                ,SystemModstamp SystemModstamp
                ,Used_for_Duplicate_Checks_c Used_for_Duplicate_Checks_c
                
FROM datalake_dev.pre_governed.salesforce_opportunity_space__c_4498 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
