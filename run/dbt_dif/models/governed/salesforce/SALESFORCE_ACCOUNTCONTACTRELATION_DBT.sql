
  create or replace  view datalake_dev.governed.SALESFORCE_ACCOUNTCONTACTRELATION_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_AccountContactRelation_4504)
SELECT AccountId AccountId,ContactId ContactId,CreatedById CreatedById,CreatedDate CreatedDate,EndDate EndDate,Id Id,IsActive IsActive,IsDeleted IsDeleted,IsDirect IsDirect,LastModifiedById LastModifiedById,SystemModstamp SystemModstamp,StartDate StartDate,LastModifiedDate LastModifiedDate,Roles Roles
FROM pre_governed.salesforce_AccountContactRelation_4504 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
