
  create or replace  view datalake_dev.governed.SALESFORCE_CONTACT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_contact_3846)
SELECT AccountId AccountId
                ,Account_Name_c Account_Name_c
                ,AssistantName AssistantName
                ,AssistantPhone AssistantPhone
                ,Birthdate Birthdate
                ,Comments_c Comments_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Days_Since_Last_Contact_Activity_c Days_Since_Last_Contact_Activity_c
                ,Department Department
                ,Description Description
                ,DoNotCall DoNotCall
                ,Email Email
                ,EmailBouncedDate EmailBouncedDate
                ,EmailBouncedReason EmailBouncedReason
                ,Fax Fax
                ,FirstName FirstName
                ,First_Contact_c First_Contact_c
                ,Friendly_Name_c Friendly_Name_c
                ,Function_Role_c Function_Role_c
                ,GA_Campaign_c GA_Campaign_c
                ,GA_Content_c GA_Content_c
                ,GA_Count_of_Pageviews_c GA_Count_of_Pageviews_c
                ,GA_Count_of_Sessions_c GA_Count_of_Sessions_c
                ,GA_Medium_c GA_Medium_c
                ,GA_Source_c GA_Source_c
                ,GA_Term_c GA_Term_c
                ,GA_Visitor_ID_c GA_Visitor_ID_c
                ,HasOptedOutOfEmail HasOptedOutOfEmail
                ,HasOptedOutOfFax HasOptedOutOfFax
                ,HomePhone HomePhone
                ,Id Id
                ,Inactive_c Inactive_c
                ,IndividualId IndividualId
                ,Interest_Level_c Interest_Level_c
                ,IsDeleted IsDeleted
                ,IsEmailBounced IsEmailBounced
                ,IsPersonAccount IsPersonAccount
                ,Jigsaw Jigsaw
                ,JigsawContactId JigsawContactId
                ,LastActivityDate LastActivityDate
                ,LastCURequestDate LastCURequestDate
                ,LastCUUpdateDate LastCUUpdateDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastName LastName
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,LeadSource LeadSource
                ,Level_c Level_c
                ,MailingAddress MailingAddress
                ,MailingCity MailingCity
                ,MailingCountry MailingCountry
                ,MailingCountryCode MailingCountryCode
                ,MailingGeocodeAccuracy MailingGeocodeAccuracy
                ,MailingLatitude MailingLatitude
                ,MailingLongitude MailingLongitude
                ,MailingPostalCode MailingPostalCode
                ,MailingState MailingState
                ,MailingStateCode MailingStateCode
                ,MailingStreet MailingStreet
                ,MasterRecordId MasterRecordId
                ,MeetingKey1_c MeetingKey1_c
                ,MeetingKey2_c MeetingKey2_c
                ,MobilePhone MobilePhone
                ,Name Name
                ,NumericPhone_c NumericPhone_c
                ,OtherAddress OtherAddress
                ,OtherCity OtherCity
                ,OtherCountry OtherCountry
                ,OtherCountryCode OtherCountryCode
                ,OtherGeocodeAccuracy OtherGeocodeAccuracy
                ,OtherLatitude OtherLatitude
                ,OtherLongitude OtherLongitude
                ,OtherPhone OtherPhone
                ,OtherPostalCode OtherPostalCode
                ,OtherState OtherState
                ,OtherStateCode OtherStateCode
                ,OtherStreet OtherStreet
                ,OwnerId OwnerId
                ,Partner_Name_c Partner_Name_c
                ,Personal_Information_c Personal_Information_c
                ,Phone Phone
                ,PhotoUrl PhotoUrl
                ,Professional_Details_c Professional_Details_c
                ,RecordTypeId RecordTypeId
                ,Record_Type_Name_c Record_Type_Name_c
                ,Relationship_c Relationship_c
                ,ReportsToId ReportsToId
                ,Retail_c Retail_c
                ,Salutation Salutation
                ,Secondary_Email_c Secondary_Email_c
                ,SystemModstamp SystemModstamp
                ,Title Title
                ,Touch_Compliance_c Touch_Compliance_c
                ,Touch_Frequency_c Touch_Frequency_c
                ,Unit_Number_c Unit_Number_c
                ,Unit_c Unit_c
                ,UseSalesforce UseSalesforce
                ,cirrusadv_Created_by_Cirrus_Insight_c cirrusadv_Created_by_Cirrus_Insight_c
                ,of_GSP_Opportunities_c of_GSP_Opportunities_c
                ,pi_Needs_Score_Synced_c pi_Needs_Score_Synced_c
                ,pi_Pardot_Last_Scored_At_c pi_Pardot_Last_Scored_At_c
                ,pi_campaign_c pi_campaign_c
                ,pi_comments_c pi_comments_c
                ,pi_conversion_date_c pi_conversion_date_c
                ,pi_conversion_object_name_c pi_conversion_object_name_c
                ,pi_conversion_object_type_c pi_conversion_object_type_c
                ,pi_created_date_c pi_created_date_c
                ,pi_first_activity_c pi_first_activity_c
                ,pi_first_search_term_c pi_first_search_term_c
                ,pi_first_search_type_c pi_first_search_type_c
                ,pi_first_touch_url_c pi_first_touch_url_c
                ,pi_grade_c pi_grade_c
                ,pi_last_activity_c pi_last_activity_c
                ,pi_notes_c pi_notes_c
                ,pi_pardot_hard_bounced_c pi_pardot_hard_bounced_c
                ,pi_score_c pi_score_c
                ,pi_url_c pi_url_c
                ,pi_utm_campaign_c pi_utm_campaign_c
                ,pi_utm_content_c pi_utm_content_c
                ,pi_utm_medium_c pi_utm_medium_c
                ,pi_utm_source_c pi_utm_source_c
                ,pi_utm_term_c pi_utm_term_c
                
FROM datalake_dev.pre_governed.salesforce_contact_3846 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
