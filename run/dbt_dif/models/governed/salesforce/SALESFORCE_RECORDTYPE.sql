
  create or replace  view datalake_dev.governed.SALESFORCE_RECORDTYPE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_recordtype_2108)
SELECT BusinessProcessId BusinessProcessId
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Description Description
                ,DeveloperName DeveloperName
                ,Id Id
                ,IsActive IsActive
                ,IsPersonType IsPersonType
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Name Name
                ,NamespacePrefix NamespacePrefix
                ,SobjectType SobjectType
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_recordtype_2108 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
