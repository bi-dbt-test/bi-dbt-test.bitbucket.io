
  create or replace  view datalake_dev.governed.SALESFORCE_RECORDTYPE_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_recordtype_2108)
SELECT BusinessProcessId BusinessProcessId,CreatedById CreatedById,CreatedDate CreatedDate,Description Description,DeveloperName DeveloperName,Id Id,IsActive IsActive,LastModifiedDate LastModifiedDate,Name Name,NamespacePrefix NamespacePrefix,SystemModstamp SystemModstamp,IsPersonType IsPersonType,SobjectType SobjectType,LastModifiedById LastModifiedById
FROM pre_governed.salesforce_recordtype_2108 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
