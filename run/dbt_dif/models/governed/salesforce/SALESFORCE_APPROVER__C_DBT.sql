
  create or replace  view datalake_dev.governed.SALESFORCE_APPROVER__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_approver__c_2179)
SELECT All_Departments_c All_Departments_c,All_Properties_c All_Properties_c,Approver_Role_c Approver_Role_c,Approver_c Approver_c,CreatedById CreatedById,CreatedDate CreatedDate,Department_c Department_c,IsDeleted IsDeleted,LastModifiedById LastModifiedById,LastReferencedDate LastReferencedDate,LastViewedDate LastViewedDate,Limit_c Limit_c,Name Name,No_Limit_c No_Limit_c,Over_Budget_c Over_Budget_c,Property_c Property_c,SystemModstamp SystemModstamp,Id Id,Master_Property_c Master_Property_c,LastModifiedDate LastModifiedDate,Master_Property_Ref_c Master_Property_Ref_c,RecordTypeId RecordTypeId,OwnerId OwnerId
FROM pre_governed.salesforce_approver__c_2179 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
