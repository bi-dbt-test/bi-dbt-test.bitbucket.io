
  create or replace  view datalake_dev.governed.SALESFORCE_CAMPAIGNMEMBER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_campaignmember_3459)
SELECT CampaignId CampaignId
                ,CampaignMTool_Address_Of_Member_c CampaignMTool_Address_Of_Member_c
                ,CampaignMTool_City_Of_Member_c CampaignMTool_City_Of_Member_c
                ,CampaignMTool_Company_Of_Member_c CampaignMTool_Company_Of_Member_c
                ,CampaignMTool_Country_Of_Member_c CampaignMTool_Country_Of_Member_c
                ,CampaignMTool_Do_Not_Call_Member_c CampaignMTool_Do_Not_Call_Member_c
                ,CampaignMTool_Email_Of_Member_c CampaignMTool_Email_Of_Member_c
                ,CampaignMTool_Fax_Of_Member_c CampaignMTool_Fax_Of_Member_c
                ,CampaignMTool_MobilePhone_Of_Member_c CampaignMTool_MobilePhone_Of_Member_c
                ,CampaignMTool_Name_Of_Member_c CampaignMTool_Name_Of_Member_c
                ,CampaignMTool_Phone_Of_Member_c CampaignMTool_Phone_Of_Member_c
                ,CampaignMTool_PostalCode_Of_Member_c CampaignMTool_PostalCode_Of_Member_c
                ,CampaignMTool_Related_To_c CampaignMTool_Related_To_c
                ,CampaignMTool_Salutation_Of_Member_c CampaignMTool_Salutation_Of_Member_c
                ,CampaignMTool_Title_Of_Member_c CampaignMTool_Title_Of_Member_c
                ,City City
                ,CompanyOrAccount CompanyOrAccount
                ,ContactId ContactId
                ,Country Country
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Description Description
                ,DoNotCall DoNotCall
                ,Email Email
                ,Fax Fax
                ,FirstName FirstName
                ,FirstRespondedDate FirstRespondedDate
                ,HasOptedOutOfEmail HasOptedOutOfEmail
                ,HasOptedOutOfFax HasOptedOutOfFax
                ,HasResponded HasResponded
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastName LastName
                ,LeadId LeadId
                ,LeadOrContactId LeadOrContactId
                ,LeadOrContactOwnerId LeadOrContactOwnerId
                ,LeadSource LeadSource
                ,MobilePhone MobilePhone
                ,Name Name
                ,Phone Phone
                ,PostalCode PostalCode
                ,Referrer_c Referrer_c
                ,Salutation Salutation
                ,State State
                ,Status Status
                ,Street Street
                ,SystemModstamp SystemModstamp
                ,Task_Comments_c Task_Comments_c
                ,Title Title
                ,Type Type
                ,TypeofReferral_c TypeofReferral_c
                ,cirrusadv_Created_by_Cirrus_Insight_c cirrusadv_Created_by_Cirrus_Insight_c
                ,cirrusadv_Email_Opened_c cirrusadv_Email_Opened_c
                ,cirrusadv_First_Clicked_c cirrusadv_First_Clicked_c
                ,cirrusadv_First_Opened_c cirrusadv_First_Opened_c
                ,cirrusadv_First_Reply_c cirrusadv_First_Reply_c
                ,cirrusadv_Hour_Sent_c cirrusadv_Hour_Sent_c
                ,cirrusadv_Last_Clicked_c cirrusadv_Last_Clicked_c
                ,cirrusadv_Last_Opened_c cirrusadv_Last_Opened_c
                ,cirrusadv_Last_Reply_c cirrusadv_Last_Reply_c
                ,cirrusadv_Links_Clicked_c cirrusadv_Links_Clicked_c
                ,cirrusadv_Num_of_Clicks_c cirrusadv_Num_of_Clicks_c
                ,cirrusadv_Num_of_Opens_c cirrusadv_Num_of_Opens_c
                ,cirrusadv_Num_of_Replies_c cirrusadv_Num_of_Replies_c
                ,cirrusadv_Time_Sent_c cirrusadv_Time_Sent_c
                ,cirrusadv_Time_to_Open_c cirrusadv_Time_to_Open_c
                ,cirrusadv_isTracked_c cirrusadv_isTracked_c
                
FROM datalake_dev.pre_governed.salesforce_campaignmember_3459 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
