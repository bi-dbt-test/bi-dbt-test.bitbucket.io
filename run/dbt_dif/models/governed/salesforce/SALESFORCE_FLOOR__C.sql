
  create or replace  view datalake_dev.governed.SALESFORCE_FLOOR__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_floor__c_2090)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Name Name
                ,Property_c Property_c
                ,RecordTypeId RecordTypeId
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_floor__c_2090 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
