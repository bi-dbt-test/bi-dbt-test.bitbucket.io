
  create or replace  view datalake_dev.governed.SALESFORCE_RELATIONSHIPS__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Relationships__c_4495)
SELECT Additional_Broker_1_c Additional_Broker_1_c,Additional_Broker_2_c Additional_Broker_2_c,Contact_c Contact_c,CreatedDate CreatedDate,Id Id,IsDeleted IsDeleted,LastModifiedDate LastModifiedDate,LastReferencedDate LastReferencedDate,LastViewedDate LastViewedDate,Lease_c Lease_c,SystemModstamp SystemModstamp,Entity_c Entity_c,OwnerId OwnerId,Primary_c Primary_c,CreatedById CreatedById,Role_c Role_c,Name Name,LastModifiedById LastModifiedById,Guarantor_Merge_c Guarantor_Merge_c
FROM pre_governed.salesforce_Relationships__c_4495 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
