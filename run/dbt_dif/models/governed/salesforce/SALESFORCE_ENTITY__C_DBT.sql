
  create or replace  view datalake_dev.governed.SALESFORCE_ENTITY__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Entity__c_2091)
SELECT CreatedById CreatedById,CreatedDate CreatedDate,Id Id,IsDeleted IsDeleted,Jurisdiction_c Jurisdiction_c,LastModifiedById LastModifiedById,LastReferencedDate LastReferencedDate,LastViewedDate LastViewedDate,Name Name,Ownership_Direct_Indirect_c Ownership_Direct_Indirect_c,Property_c Property_c,SystemModstamp SystemModstamp,OwnerId OwnerId,LastActivityDate LastActivityDate,LastModifiedDate LastModifiedDate
FROM pre_governed.salesforce_Entity__c_2091 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
