
  create or replace  view datalake_dev.governed.SALESFORCE_COI__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_coi__c_3844)
SELECT Account_c Account_c
                ,Addl_Loss_Payee_Landlord_Lender_Named_c Addl_Loss_Payee_Landlord_Lender_Named_c
                ,Agreement_Type_c Agreement_Type_c
                ,Aircraft_Limit_c Aircraft_Limit_c
                ,Any_autos_c Any_autos_c
                ,Approved_AM_Best_Ratings_of_A_VII_c Approved_AM_Best_Ratings_of_A_VII_c
                ,Automobile_Policy_Limit_c Automobile_Policy_Limit_c
                ,BA_Additional_Insured_Language_c BA_Additional_Insured_Language_c
                ,BA_Policy_Expiration_Date_c BA_Policy_Expiration_Date_c
                ,BA_Waiver_of_Subrogation_c BA_Waiver_of_Subrogation_c
                ,CGL_Additional_Insured_Language_c CGL_Additional_Insured_Language_c
                ,CGL_Each_Occurence_Limit_c CGL_Each_Occurence_Limit_c
                ,CGL_General_Aggregate_Limit_c CGL_General_Aggregate_Limit_c
                ,CGL_Personal_Advertising_Injury_Limit_c CGL_Personal_Advertising_Injury_Limit_c
                ,CGL_Policy_Expiration_Date_c CGL_Policy_Expiration_Date_c
                ,CGL_Prod_Completed_Ops_Aggregate_Limit_c CGL_Prod_Completed_Ops_Aggregate_Limit_c
                ,CGL_Waiver_of_Subrogation_c CGL_Waiver_of_Subrogation_c
                ,COI_Count_c COI_Count_c
                ,COI_Expired_1_c COI_Expired_1_c
                ,COI_Expired_Checked_c COI_Expired_Checked_c
                ,COI_Expired_c COI_Expired_c
                ,COI_Fix_batch_c COI_Fix_batch_c
                ,COI_Status_c COI_Status_c
                ,Comments_c Comments_c
                ,Comments_status_for_any_vaCombinedPolicy_c Comments_status_for_any_vaCombinedPolicy_c
                ,Comments_status_for_any_varEmplLiability_c Comments_status_for_any_varEmplLiability_c
                ,Comments_status_for_any_varPropExp_c Comments_status_for_any_varPropExp_c
                ,Comments_status_for_any_varUmbrella_c Comments_status_for_any_varUmbrella_c
                ,Comments_status_for_any_variaCGLGeneral_c Comments_status_for_any_variaCGLGeneral_c
                ,Comments_status_for_any_variaCGLPersonal_c Comments_status_for_any_variaCGLPersonal_c
                ,Comments_status_for_any_variaCGLProducts_c Comments_status_for_any_variaCGLProducts_c
                ,Comments_status_for_any_variaCGL_EACH_c Comments_status_for_any_variaCGL_EACH_c
                ,Company_Code_c Company_Code_c
                ,Contact_Risk_Management_c Contact_Risk_Management_c
                ,Contact_c Contact_c
                ,Correct_Additional_Insured_c Correct_Additional_Insured_c
                ,Counterparty_Name_c Counterparty_Name_c
                ,Counterparty_Tenant_COI_Contact_c Counterparty_Tenant_COI_Contact_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Crime_Fidelity_Bond_Limit_c Crime_Fidelity_Bond_Limit_c
                ,Cyber_Limit_c Cyber_Limit_c
                ,Default_Letter_Sent_History_c Default_Letter_Sent_History_c
                ,Default_Letter_Sent_c Default_Letter_Sent_c
                ,Does_Aggregate_Limit_apply_c Does_Aggregate_Limit_apply_c
                ,EVP_Ops_c EVP_Ops_c
                ,Employer_s_Liability_Limit_c Employer_s_Liability_Limit_c
                ,Entity_Name_c Entity_Name_c
                ,Event_Risk_Limit_c Event_Risk_Limit_c
                ,Excess_Umbrella_Liability_Limit_Requried_c Excess_Umbrella_Liability_Limit_Requried_c
                ,Expiration_Date_c Expiration_Date_c
                ,Fire_Liability_Damage_to_Premises_Limit_c Fire_Liability_Damage_to_Premises_Limit_c
                ,Fix_Batch_Inactive_c Fix_Batch_Inactive_c
                ,Follow_Up_Next_Steps_c Follow_Up_Next_Steps_c
                ,Form_ID_c Form_ID_c
                ,Form_Owner_c Form_Owner_c
                ,GM_c GM_c
                ,Garage_keepers_Limit_c Garage_keepers_Limit_c
                ,General_Aggregate_Applies_Per_Location_c General_Aggregate_Applies_Per_Location_c
                ,Have_Insufficient_Limits_been_Corrected_c Have_Insufficient_Limits_been_Corrected_c
                ,Have_you_defaulted_the_tenant_c Have_you_defaulted_the_tenant_c
                ,Hired_Non_Owned_c Hired_Non_Owned_c
                ,Id Id
                ,Insufficient_Limits_c Insufficient_Limits_c
                ,Insurance_Comments_History_c Insurance_Comments_History_c
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Lease_2_c Lease_2_c
                ,Lease_c Lease_c
                ,Legal_Team_Member_c Legal_Team_Member_c
                ,Liquor_Dram_Shop_Limit_c Liquor_Dram_Shop_Limit_c
                ,Liquor_Liability_Dram_Shop_Limit_c Liquor_Liability_Dram_Shop_Limit_c
                ,Long_Short_Term_c Long_Short_Term_c
                ,Marine_Watercraft_Limit_c Marine_Watercraft_Limit_c
                ,Master_Property_c Master_Property_c
                ,Matching_Exp_Dates_c Matching_Exp_Dates_c
                ,Min_CGL_Each_Occurance_Limit_Required_c Min_CGL_Each_Occurance_Limit_Required_c
                ,Min_CGL_Personal_Ad_Injury_Limit_Req_c Min_CGL_Personal_Ad_Injury_Limit_Req_c
                ,Min_CGL_Prod_Comp_Ops_Agg_Limit_c Min_CGL_Prod_Comp_Ops_Agg_Limit_c
                ,Minimum_CGL_General_Aggregate_Limit_c Minimum_CGL_General_Aggregate_Limit_c
                ,Minimum_Combined_Policy_Limit_c Minimum_Combined_Policy_Limit_c
                ,Minimum_Employer_s_Liability_Limit_c Minimum_Employer_s_Liability_Limit_c
                ,Name Name
                ,Owned_Hired_and_Non_Owned_c Owned_Hired_and_Non_Owned_c
                ,OwnerId OwnerId
                ,Permitted_Use_incl_Vehicle_Deliveries_c Permitted_Use_incl_Vehicle_Deliveries_c
                ,Pollution_Additional_Insured_Language_c Pollution_Additional_Insured_Language_c
                ,Pollution_Limit_c Pollution_Limit_c
                ,Portfolio_Manager_c Portfolio_Manager_c
                ,Primary_and_Non_contributory_Language_c Primary_and_Non_contributory_Language_c
                ,Professional_Limit_Aggregate_c Professional_Limit_Aggregate_c
                ,Professional_Limit_Per_Occurance_c Professional_Limit_Per_Occurance_c
                ,Property_Business_Income_Insurance_c Property_Business_Income_Insurance_c
                ,Property_Name_c Property_Name_c
                ,Property_Policy_Expiration_Date_c Property_Policy_Expiration_Date_c
                ,Property_Waiver_of_Subrogation_c Property_Waiver_of_Subrogation_c
                ,Property_c Property_c
                ,RCD_c RCD_c
                ,Rcvd_Endorsements_c Rcvd_Endorsements_c
                ,Rcvd_Net_Worth_Evidence_for_Self_Ins_c Rcvd_Net_Worth_Evidence_for_Self_Ins_c
                ,RecordTypeId RecordTypeId
                ,Renewal_Request_Sent_History_c Renewal_Request_Sent_History_c
                ,Renewal_Request_Sent_c Renewal_Request_Sent_c
                ,Retail_CGL_Exp_Process_Builder_c Retail_CGL_Exp_Process_Builder_c
                ,Risk_Level_c Risk_Level_c
                ,Save_now_c Save_now_c
                ,Scope_of_Work_c Scope_of_Work_c
                ,Special_Form_Negotiated_Limits_c Special_Form_Negotiated_Limits_c
                ,SystemModstamp SystemModstamp
                ,Tag_Language_c Tag_Language_c
                ,Tags_c Tags_c
                ,Type_of_Work_c Type_of_Work_c
                ,UL_Additional_Insured_Language_c UL_Additional_Insured_Language_c
                ,UL_Policy_Expiration_Date_c UL_Policy_Expiration_Date_c
                ,UL_Waiver_of_Subrogation_c UL_Waiver_of_Subrogation_c
                ,Umbrella_Limit_c Umbrella_Limit_c
                ,WC_EL_Policy_Expiration_c WC_EL_Policy_Expiration_c
                ,WC_EL_Waiver_of_Subrogation_c WC_EL_Waiver_of_Subrogation_c
                ,Workers_Comp_Limits_Per_State_Statute_c Workers_Comp_Limits_Per_State_Statute_c
                ,X30_Days_Notice_of_Cancellation_c X30_Days_Notice_of_Cancellation_c
                
FROM datalake_dev.pre_governed.salesforce_coi__c_3844 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
