
  create or replace  view datalake_dev.governed.SALESFORCE_DSFS__DOCUSIGN_RECIPIENT_STATUS__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_dsfs__docusign_recipient_status__c_3468)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Name Name
                ,OpportunityId_c OpportunityId_c
                ,Subject_c Subject_c
                ,SystemModstamp SystemModstamp
                ,dsfs_Account_c dsfs_Account_c
                ,dsfs_Contact_c dsfs_Contact_c
                ,dsfs_Date_Declined_c dsfs_Date_Declined_c
                ,dsfs_Date_Delivered_c dsfs_Date_Delivered_c
                ,dsfs_Date_Sent_c dsfs_Date_Sent_c
                ,dsfs_Date_Signed_c dsfs_Date_Signed_c
                ,dsfs_Decline_Reason_Extended_c dsfs_Decline_Reason_Extended_c
                ,dsfs_Decline_Reason_c dsfs_Decline_Reason_c
                ,dsfs_DocuSign_Recipient_Company_c dsfs_DocuSign_Recipient_Company_c
                ,dsfs_DocuSign_Recipient_Email_c dsfs_DocuSign_Recipient_Email_c
                ,dsfs_DocuSign_Recipient_Id_c dsfs_DocuSign_Recipient_Id_c
                ,dsfs_DocuSign_Recipient_Title_c dsfs_DocuSign_Recipient_Title_c
                ,dsfs_DocuSign_Routing_Order_c dsfs_DocuSign_Routing_Order_c
                ,dsfs_Envelope_Id_c dsfs_Envelope_Id_c
                ,dsfs_Lead_c dsfs_Lead_c
                ,dsfs_Parent_Status_Record_c dsfs_Parent_Status_Record_c
                ,dsfs_Recipient_Status_c dsfs_Recipient_Status_c
                
FROM datalake_dev.pre_governed.salesforce_dsfs__docusign_recipient_status__c_3468 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
