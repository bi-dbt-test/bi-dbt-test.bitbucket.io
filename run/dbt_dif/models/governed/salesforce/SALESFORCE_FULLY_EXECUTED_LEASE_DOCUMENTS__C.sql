
  create or replace  view datalake_dev.governed.SALESFORCE_FULLY_EXECUTED_LEASE_DOCUMENTS__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_fully_executed_lease_documents__c_4201)
SELECT Assigned_Legal_c Assigned_Legal_c
                ,Assigned_Paralegal_c Assigned_Paralegal_c
                ,Comments_c Comments_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Date_Sent_to_Tenant_c Date_Sent_to_Tenant_c
                ,Dealmaker_c Dealmaker_c
                ,DocuSign_Status_c DocuSign_Status_c
                ,Document_Link_c Document_Link_c
                ,Document_Name_c Document_Name_c
                ,Dragonfly_doc_url_c Dragonfly_doc_url_c
                ,Execution_Memo_c Execution_Memo_c
                ,Form_c Form_c
                ,Fully_Executed_Lease_c Fully_Executed_Lease_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Lease_2_c Lease_2_c
                ,Lease_File_Type_c Lease_File_Type_c
                ,Lease_Title_c Lease_Title_c
                ,Lease_c Lease_c
                ,Name Name
                ,Named_File_Upload_Sub_Type_c Named_File_Upload_Sub_Type_c
                ,Named_File_Upload_Type_c Named_File_Upload_Type_c
                ,Order_of_Tenant_c Order_of_Tenant_c
                ,OwnerId OwnerId
                ,Prepared_By_c Prepared_By_c
                ,Property_c Property_c
                ,Ready_for_Signatures_c Ready_for_Signatures_c
                ,RecordTypeId RecordTypeId
                ,Sent_to_Tenant_c Sent_to_Tenant_c
                ,Signature_Page_Number_c Signature_Page_Number_c
                ,SystemModstamp SystemModstamp
                ,Tenant_Signatory_E_mail_c Tenant_Signatory_E_mail_c
                ,Tenant_Signatory_Name_c Tenant_Signatory_Name_c
                ,Transmittal_Memo_to_Tenant_c Transmittal_Memo_to_Tenant_c
                ,eDocs_Number_c eDocs_Number_c
                
FROM datalake_dev.pre_governed.salesforce_fully_executed_lease_documents__c_4201 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
