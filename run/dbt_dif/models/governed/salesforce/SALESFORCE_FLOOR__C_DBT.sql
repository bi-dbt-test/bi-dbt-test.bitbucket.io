
  create or replace  view datalake_dev.governed.SALESFORCE_FLOOR__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Floor__c_2090)
SELECT CreatedById CreatedById,CreatedDate CreatedDate,Id Id,IsDeleted IsDeleted,LastModifiedById LastModifiedById,Name Name,Property_c Property_c,SystemModstamp SystemModstamp,LastActivityDate LastActivityDate,LastModifiedDate LastModifiedDate,RecordTypeId RecordTypeId
FROM pre_governed.salesforce_Floor__c_2090 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
