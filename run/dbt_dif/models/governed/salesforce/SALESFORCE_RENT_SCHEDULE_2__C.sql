
  create or replace  view datalake_dev.governed.SALESFORCE_RENT_SCHEDULE_2__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_rent_schedule_2__c_4502)
SELECT Average_Rate_PSF_c Average_Rate_PSF_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Default_Base_Rent_c Default_Base_Rent_c
                ,Default_Discount_Rate_c Default_Discount_Rate_c
                ,Default_External_Commission_c Default_External_Commission_c
                ,Default_Internal_Commission_c Default_Internal_Commission_c
                ,Default_Name_c Default_Name_c
                ,Discount_Rate_c Discount_Rate_c
                ,Escalations_Lease_c Escalations_Lease_c
                ,Escalations_Proforma_c Escalations_Proforma_c
                ,Expiration_Date_c Expiration_Date_c
                ,External_Commission_Amt_c External_Commission_Amt_c
                ,External_Commission_NER_c External_Commission_NER_c
                ,External_Commission_c External_Commission_c
                ,Free_Rent_Lease_c Free_Rent_Lease_c
                ,Free_Rent_Proforma_c Free_Rent_Proforma_c
                ,Gross_NPV_PSF_c Gross_NPV_PSF_c
                ,Gross_NPV_c Gross_NPV_c
                ,Gross_Rental_Revenue_PSf_c Gross_Rental_Revenue_PSf_c
                ,Gross_Rental_Revenue_c Gross_Rental_Revenue_c
                ,Id Id
                ,Initial_Rent_Lease_c Initial_Rent_Lease_c
                ,Initial_Rent_Proforma_c Initial_Rent_Proforma_c
                ,Internal_Commission_Amt_c Internal_Commission_Amt_c
                ,Internal_Commission_NER_c Internal_Commission_NER_c
                ,Internal_Commission_c Internal_Commission_c
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Lease_Term_Months_c Lease_Term_Months_c
                ,Lease_Type_c Lease_Type_c
                ,Lease_Use_c Lease_Use_c
                ,Lease_c Lease_c
                ,Leasing_Commission_Lease_c Leasing_Commission_Lease_c
                ,Leasing_Commission_Proforma_c Leasing_Commission_Proforma_c
                ,NER_Lease_c NER_Lease_c
                ,NER_Proforma_c NER_Proforma_c
                ,NER_Variance_c NER_Variance_c
                ,NER_w_D_Variance_c NER_w_D_Variance_c
                ,NPV_Lease_c NPV_Lease_c
                ,NPV_Proforma_c NPV_Proforma_c
                ,NPV_TI_Lease_c NPV_TI_Lease_c
                ,NPV_TI_Proforma_c NPV_TI_Proforma_c
                ,Name Name
                ,Net_NPV_PSF_c Net_NPV_PSF_c
                ,Net_NPV_c Net_NPV_c
                ,Net_Rental_Revenue_PSF_c Net_Rental_Revenue_PSF_c
                ,Net_Rental_Revenue_c Net_Rental_Revenue_c
                ,OpEx_NER_c OpEx_NER_c
                ,OpEx_c OpEx_c
                ,OpEx_sq_ft_year_c OpEx_sq_ft_year_c
                ,Opportunity_c Opportunity_c
                ,Other_LL_Costs_NER_c Other_LL_Costs_NER_c
                ,Other_LL_Costs_c Other_LL_Costs_c
                ,Other_LL_Costs_sq_ft_year_c Other_LL_Costs_sq_ft_year_c
                ,OwnerId OwnerId
                ,PV_NER_c PV_NER_c
                ,PV_c PV_c
                ,Parent_Id_c Parent_Id_c
                ,Rent_Commencement_Date_c Rent_Commencement_Date_c
                ,SystemModstamp SystemModstamp
                ,TI_Allowance_NER_c TI_Allowance_NER_c
                ,TI_Allowance_c TI_Allowance_c
                ,TI_Allowance_sq_ft_year_c TI_Allowance_sq_ft_year_c
                ,Total_NER_c Total_NER_c
                ,Total_Sq_Footage_c Total_Sq_Footage_c
                ,Weighted_Average_Rate_PSF_c Weighted_Average_Rate_PSF_c
                
FROM datalake_dev.pre_governed.salesforce_rent_schedule_2__c_4502 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
