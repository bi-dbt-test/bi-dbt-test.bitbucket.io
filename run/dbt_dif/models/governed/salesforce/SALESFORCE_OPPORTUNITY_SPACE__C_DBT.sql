
  create or replace  view datalake_dev.governed.SALESFORCE_OPPORTUNITY_SPACE__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Opportunity_Space__c_4498)
SELECT Base_Rent_psf_c Base_Rent_psf_c,Base_Rent_c Base_Rent_c,CreatedById CreatedById,Id Id,IsDeleted IsDeleted,LastModifiedById LastModifiedById,Leasable_Sq_Footage_c Leasable_Sq_Footage_c,Opportunity_c Opportunity_c,Property_c Property_c,Sq_Footage_on_Space_c Sq_Footage_on_Space_c,Sq_Footage_c Sq_Footage_c,Used_for_Duplicate_Checks_c Used_for_Duplicate_Checks_c,SystemModstamp SystemModstamp,Leasable_Sq_Footage_on_Space_c Leasable_Sq_Footage_on_Space_c,Space_c Space_c,CreatedDate CreatedDate,Is_Active_Opportunity_c Is_Active_Opportunity_c,LastModifiedDate LastModifiedDate,Name Name
FROM pre_governed.salesforce_Opportunity_Space__c_4498 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
