
  create or replace  view datalake_dev.governed.SALESFORCE_DSFS__DOCUSIGN_ENVELOPE_RECIPIENT__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_dsfs__docusign_envelope_recipient__c_3467)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Name Name
                ,SystemModstamp SystemModstamp
                ,dsfs_Access_Code_c dsfs_Access_Code_c
                ,dsfs_DSER_ContactID_c dsfs_DSER_ContactID_c
                ,dsfs_DSER_CustomFeaturesEx_c dsfs_DSER_CustomFeaturesEx_c
                ,dsfs_DSER_CustomFeatures_c dsfs_DSER_CustomFeatures_c
                ,dsfs_DSER_CustomId_c dsfs_DSER_CustomId_c
                ,dsfs_DSER_CustomName_c dsfs_DSER_CustomName_c
                ,dsfs_DSER_LeadID_c dsfs_DSER_LeadID_c
                ,dsfs_DSER_UserID_c dsfs_DSER_UserID_c
                ,dsfs_DocuSign_EnvelopeID_c dsfs_DocuSign_EnvelopeID_c
                ,dsfs_DocuSign_Recipient_Role_c dsfs_DocuSign_Recipient_Role_c
                ,dsfs_DocuSign_Signature_Name_c dsfs_DocuSign_Signature_Name_c
                ,dsfs_DocuSign_Signer_Type_c dsfs_DocuSign_Signer_Type_c
                ,dsfs_Email_Body_c dsfs_Email_Body_c
                ,dsfs_Email_Subject_c dsfs_Email_Subject_c
                ,dsfs_ID_Check_c dsfs_ID_Check_c
                ,dsfs_LanguageCode_c dsfs_LanguageCode_c
                ,dsfs_Language_c dsfs_Language_c
                ,dsfs_Recipient_Email_c dsfs_Recipient_Email_c
                ,dsfs_Recipient_ID_c dsfs_Recipient_ID_c
                ,dsfs_Recipient_Note_Long_c dsfs_Recipient_Note_Long_c
                ,dsfs_Recipient_Note_c dsfs_Recipient_Note_c
                ,dsfs_RoleName_c dsfs_RoleName_c
                ,dsfs_RoleValue_c dsfs_RoleValue_c
                ,dsfs_Routing_Order_c dsfs_Routing_Order_c
                ,dsfs_SMSAuthentication_PhoneNumber_c dsfs_SMSAuthentication_PhoneNumber_c
                ,dsfs_Salesforce_Recipient_Type_c dsfs_Salesforce_Recipient_Type_c
                ,dsfs_SignInPersonEmail_c dsfs_SignInPersonEmail_c
                ,dsfs_SignInPersonName_c dsfs_SignInPersonName_c
                ,dsfs_SignNow_c dsfs_SignNow_c
                ,dsfs_SigningGroupId_c dsfs_SigningGroupId_c
                ,dsfs_Validation_Message_c dsfs_Validation_Message_c
                
FROM datalake_dev.pre_governed.salesforce_dsfs__docusign_envelope_recipient__c_3467 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
