
  create or replace  view datalake_dev.governed.SALESFORCE_EVENT_SPACE__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Event_Space__c_4501)
SELECT CreatedById CreatedById,CreatedDate CreatedDate,Id Id,IsDeleted IsDeleted,LastModifiedById LastModifiedById,Name Name,OwnerId OwnerId,Space_c Space_c,SystemModstamp SystemModstamp,Event_Id_c Event_Id_c,LastModifiedDate LastModifiedDate
FROM pre_governed.salesforce_Event_Space__c_4501 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
