
  create or replace  view datalake_dev.governed.SALESFORCE_ENTITY__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_entity__c_2091)
SELECT CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,Jurisdiction_c Jurisdiction_c
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Name Name
                ,OwnerId OwnerId
                ,Ownership_Direct_Indirect_c Ownership_Direct_Indirect_c
                ,Property_c Property_c
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_entity__c_2091 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
