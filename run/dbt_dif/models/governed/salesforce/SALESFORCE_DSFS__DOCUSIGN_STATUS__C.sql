
  create or replace  view datalake_dev.governed.SALESFORCE_DSFS__DOCUSIGN_STATUS__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_dsfs__docusign_status__c_3549)
SELECT Concession_Rollup_c Concession_Rollup_c
                ,Concession_c Concession_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Fully_Executed_Document_c Fully_Executed_Document_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Name Name
                ,OwnerId OwnerId
                ,Storage_Parking_c Storage_Parking_c
                ,SystemModstamp SystemModstamp
                ,User_is_Sender_c User_is_Sender_c
                ,VictoriaWardLTDCleared_c VictoriaWardLTDCleared_c
                ,dsfs_Case_c dsfs_Case_c
                ,dsfs_Company_c dsfs_Company_c
                ,dsfs_Completed_Age_c dsfs_Completed_Age_c
                ,dsfs_Completed_Date_Time_c dsfs_Completed_Date_Time_c
                ,dsfs_Contact_c dsfs_Contact_c
                ,dsfs_Contract_c dsfs_Contract_c
                ,dsfs_Days_to_Complete_c dsfs_Days_to_Complete_c
                ,dsfs_Declined_Date_Time_c dsfs_Declined_Date_Time_c
                ,dsfs_Declined_Reason_Extended_c dsfs_Declined_Reason_Extended_c
                ,dsfs_Declined_Reason_c dsfs_Declined_Reason_c
                ,dsfs_DocuSign_Envelope_ID_c dsfs_DocuSign_Envelope_ID_c
                ,dsfs_EnvelopeConfiguration_c dsfs_EnvelopeConfiguration_c
                ,dsfs_Envelope_Link_c dsfs_Envelope_Link_c
                ,dsfs_Envelope_Status_c dsfs_Envelope_Status_c
                ,dsfs_Hours_to_Complete_c dsfs_Hours_to_Complete_c
                ,dsfs_Hrs_Sent_to_Sign_c dsfs_Hrs_Sent_to_Sign_c
                ,dsfs_Lead_c dsfs_Lead_c
                ,dsfs_Minutes_to_Complete_c dsfs_Minutes_to_Complete_c
                ,dsfs_Number_Completed_c dsfs_Number_Completed_c
                ,dsfs_Opportunity_c dsfs_Opportunity_c
                ,dsfs_Sender_Email_c dsfs_Sender_Email_c
                ,dsfs_Sender_c dsfs_Sender_c
                ,dsfs_Sent_Age_c dsfs_Sent_Age_c
                ,dsfs_Sent_Date_Time_c dsfs_Sent_Date_Time_c
                ,dsfs_Subject_c dsfs_Subject_c
                ,dsfs_Time_to_Complete_c dsfs_Time_to_Complete_c
                ,dsfs_Viewed_Date_Time_c dsfs_Viewed_Date_Time_c
                ,dsfs_Voided_Date_Time_c dsfs_Voided_Date_Time_c
                ,dsfs_Voided_Reason_Extended_c dsfs_Voided_Reason_Extended_c
                ,dsfs_Voided_Reason_c dsfs_Voided_Reason_c
                
FROM datalake_dev.pre_governed.salesforce_dsfs__docusign_status__c_3549 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
