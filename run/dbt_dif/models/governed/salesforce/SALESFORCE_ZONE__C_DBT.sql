
  create or replace  view datalake_dev.governed.SALESFORCE_ZONE__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Zone__c_4499)
SELECT Base_Rent_psf_c Base_Rent_psf_c,CreatedById CreatedById,Id Id,IsDeleted IsDeleted,LastModifiedById LastModifiedById,LastReferencedDate LastReferencedDate,LastViewedDate LastViewedDate,Name Name,Property_c Property_c,SystemModstamp SystemModstamp,LastActivityDate LastActivityDate,CreatedDate CreatedDate,LastModifiedDate LastModifiedDate
FROM pre_governed.salesforce_Zone__c_4499 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
