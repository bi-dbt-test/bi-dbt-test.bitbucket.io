
  create or replace  view datalake_dev.governed.SALESFORCE_CASECOMMENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_casecomment_4497)
SELECT CommentBody CommentBody
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Id Id
                ,IsDeleted IsDeleted
                ,IsPublished IsPublished
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,ParentId ParentId
                ,SystemModstamp SystemModstamp
                
FROM datalake_dev.pre_governed.salesforce_casecomment_4497 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
