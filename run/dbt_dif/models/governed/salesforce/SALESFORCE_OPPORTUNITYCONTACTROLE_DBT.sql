
  create or replace  view datalake_dev.governed.SALESFORCE_OPPORTUNITYCONTACTROLE_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_OpportunityContactRole_4505)
SELECT ContactId ContactId,CreatedById CreatedById,CreatedDate CreatedDate,Id Id,IsDeleted IsDeleted,IsPrimary IsPrimary,LastModifiedById LastModifiedById,Role Role,SystemModstamp SystemModstamp,OpportunityId OpportunityId,LastModifiedDate LastModifiedDate
FROM pre_governed.salesforce_OpportunityContactRole_4505 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
