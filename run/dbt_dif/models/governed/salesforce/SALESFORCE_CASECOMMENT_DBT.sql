
  create or replace  view datalake_dev.governed.SALESFORCE_CASECOMMENT_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_casecomment_4497)
SELECT CommentBody CommentBody,CreatedById CreatedById,CreatedDate CreatedDate,Id Id,IsDeleted IsDeleted,IsPublished IsPublished,LastModifiedById LastModifiedById,SystemModstamp SystemModstamp,ParentId ParentId,LastModifiedDate LastModifiedDate
FROM pre_governed.salesforce_casecomment_4497 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
