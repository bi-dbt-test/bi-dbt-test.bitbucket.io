
  create or replace  view datalake_dev.governed.SALESFORCE_LEASE_SPACE__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_lease_space__c_4506)
SELECT Base_Rent_c Base_Rent_c
                ,Base_Rent_psf_c Base_Rent_psf_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Effective_Date_c Effective_Date_c
                ,Expiration_Date_c Expiration_Date_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Leasable_Sq_Footage_c Leasable_Sq_Footage_c
                ,Leasable_Sq_Footage_on_Space_c Leasable_Sq_Footage_on_Space_c
                ,Lease_Stage_c Lease_Stage_c
                ,Lease_c Lease_c
                ,Name Name
                ,Space_c Space_c
                ,Sq_Footage_c Sq_Footage_c
                ,SystemModstamp SystemModstamp
                ,Tenant_c Tenant_c
                
FROM datalake_dev.pre_governed.salesforce_lease_space__c_4506 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
