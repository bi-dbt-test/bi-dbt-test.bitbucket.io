
  create or replace  view datalake_dev.governed.SALESFORCE_LEASE_SPACE__C_DBT  as (
    




WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM pre_governed.salesforce_Lease_Space__c_4506)
SELECT Base_Rent_psf_c Base_Rent_psf_c,Base_Rent_c Base_Rent_c,CreatedById CreatedById,Effective_Date_c Effective_Date_c,Expiration_Date_c Expiration_Date_c,IsDeleted IsDeleted,LastModifiedById LastModifiedById,Leasable_Sq_Footage_c Leasable_Sq_Footage_c,Lease_Stage_c Lease_Stage_c,Lease_c Lease_c,Sq_Footage_c Sq_Footage_c,SystemModstamp SystemModstamp,Tenant_c Tenant_c,Id Id,Leasable_Sq_Footage_on_Space_c Leasable_Sq_Footage_on_Space_c,CreatedDate CreatedDate,LastModifiedDate LastModifiedDate,Name Name,Space_c Space_c
FROM pre_governed.salesforce_Lease_Space__c_4506 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate
  );
