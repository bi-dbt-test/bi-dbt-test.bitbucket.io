

      create or replace  table datalake_dev.governed.STG_REPORT_EXCLUSION  as
      (
SELECT 'Contract Approval Duration Summary' REPORTNAME, 'Decision Maker' REPORTSECTION, 'Enna Allen' NAMEEXCLUDED, current_date() CREATEDATE UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Daryan Dehghanpisheh', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'John DeWolf', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Bill Flemm', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Michael Formanek', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Brent Habeck', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Caryn Kboudi', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Keith Laird', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Greg Parsons', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Bill Pisetsky', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Andrew Richardson', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Steve Robinson', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'John Simon', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'David Sizelove', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Michael Speicher', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Phillip St. Pierre', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Alex Sutton', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Andrew Zeitman', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Aaron Haas', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Jim McCaffrey', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Ana Marie Mormando', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Anthony Rossi', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Averyl Oates', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Bob Johnson', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Bobbie Lau', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Chris Curry', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'David Kautz', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'David Weinreb', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Grant Herlitz', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Jay Cline', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Mark Bulmash', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Mike Slosser', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Moshe Itzhakov', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Noni Hughes', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Randy Davis', current_date() UNION ALL   
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Reuben Davidsohn', current_date() UNION ALL   
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Richard Chung', current_date() UNION ALL   
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Rick Strauss', current_date() UNION ALL      
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Sarah Vasquez', current_date() UNION ALL      
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Sherveen Baftechi', current_date() UNION ALL      
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Simon Treacy', current_date() UNION ALL       
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Steven Cornwell', current_date() UNION ALL          
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Tim Welbes', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Venkat Kandru', current_date() UNION ALL           
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Vibha Gore', current_date() UNION ALL          
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Paul Layne', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Scott Timcoe', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Decision Maker', 'Todd Apo', current_date() UNION ALL
--
SELECT 'Contract Approval Duration Summary', 'Legal Review', 'Erika Strawn', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Legal Review', 'Aaron Grodin', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Legal Review', 'Theresa Terrell', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Legal Review', 'Jim Moomaw', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Legal Review', 'Unknown Unknown', current_date() UNION ALL
SELECT 'Contract Approval Duration Summary', 'Legal Review', 'Timothy Hubach', current_date()
      );
    