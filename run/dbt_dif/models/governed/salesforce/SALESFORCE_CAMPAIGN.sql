
  create or replace  view datalake_dev.governed.SALESFORCE_CAMPAIGN  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_campaign_3458)
SELECT Account_c Account_c
                ,Activity_Owner_c Activity_Owner_c
                ,ActualCost ActualCost
                ,AmountAllOpportunities AmountAllOpportunities
                ,AmountWonOpportunities AmountWonOpportunities
                ,BudgetedCost BudgetedCost
                ,Call_Goal_Timeframe_c Call_Goal_Timeframe_c
                ,Call_Goal_c Call_Goal_c
                ,CampaignImageId CampaignImageId
                ,CampaignMemberRecordTypeId CampaignMemberRecordTypeId
                ,Committed_c Committed_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Creative_Due_Date_c Creative_Due_Date_c
                ,Description Description
                ,Email_Goal_Timeframe_c Email_Goal_Timeframe_c
                ,Email_Goal_c Email_Goal_c
                ,EndDate EndDate
                ,ExpectedResponse ExpectedResponse
                ,ExpectedRevenue ExpectedRevenue
                ,HierarchyActualCost HierarchyActualCost
                ,HierarchyAmountAllOpportunities HierarchyAmountAllOpportunities
                ,HierarchyAmountWonOpportunities HierarchyAmountWonOpportunities
                ,HierarchyBudgetedCost HierarchyBudgetedCost
                ,HierarchyExpectedRevenue HierarchyExpectedRevenue
                ,HierarchyNumberOfContacts HierarchyNumberOfContacts
                ,HierarchyNumberOfConvertedLeads HierarchyNumberOfConvertedLeads
                ,HierarchyNumberOfLeads HierarchyNumberOfLeads
                ,HierarchyNumberOfOpportunities HierarchyNumberOfOpportunities
                ,HierarchyNumberOfResponses HierarchyNumberOfResponses
                ,HierarchyNumberOfWonOpportunities HierarchyNumberOfWonOpportunities
                ,HierarchyNumberSent HierarchyNumberSent
                ,Id Id
                ,IsActive IsActive
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,LeadChannel_c LeadChannel_c
                ,Lead_Channel_c Lead_Channel_c
                ,Lottery_Number_c Lottery_Number_c
                ,Make_Visible_to_NHA_s_c Make_Visible_to_NHA_s_c
                ,MarketingChannels_c MarketingChannels_c
                ,Marketing_Channel_c Marketing_Channel_c
                ,Meeting_Goal_Timeframe_c Meeting_Goal_Timeframe_c
                ,Meeting_Goal_c Meeting_Goal_c
                ,Name Name
                ,Notes_c Notes_c
                ,NumberOfContacts NumberOfContacts
                ,NumberOfConvertedLeads NumberOfConvertedLeads
                ,NumberOfLeads NumberOfLeads
                ,NumberOfOpportunities NumberOfOpportunities
                ,NumberOfResponses NumberOfResponses
                ,NumberOfWonOpportunities NumberOfWonOpportunities
                ,NumberSent NumberSent
                ,OwnerId OwnerId
                ,ParentId ParentId
                ,Project_s_c Project_s_c
                ,RecordTypeId RecordTypeId
                ,Region_c Region_c
                ,Sales_Talking_Points_c Sales_Talking_Points_c
                ,Spec_Size_c Spec_Size_c
                ,StartDate StartDate
                ,Status Status
                ,Studio_Collateral_Required_c Studio_Collateral_Required_c
                ,SystemModstamp SystemModstamp
                ,Target_Audience_c Target_Audience_c
                ,Type Type
                ,cirrusadv_Created_by_Cirrus_Insight_c cirrusadv_Created_by_Cirrus_Insight_c
                ,cirrusadv_Num_of_Clicks_c cirrusadv_Num_of_Clicks_c
                ,cirrusadv_Num_of_Emails_Clicked_c cirrusadv_Num_of_Emails_Clicked_c
                ,cirrusadv_Num_of_Emails_Opened_c cirrusadv_Num_of_Emails_Opened_c
                ,cirrusadv_Num_of_Opens_c cirrusadv_Num_of_Opens_c
                ,cirrusadv_Num_of_Replies_c cirrusadv_Num_of_Replies_c
                ,cirrusadv_Template_Name_c cirrusadv_Template_Name_c
                ,cirrusadv_mass_email_id_c cirrusadv_mass_email_id_c
                ,pi_Pardot_Campaign_Id_c pi_Pardot_Campaign_Id_c
                ,pi_Pardot_Has_Dependencies_c pi_Pardot_Has_Dependencies_c
                
FROM datalake_dev.pre_governed.salesforce_campaign_3458 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
