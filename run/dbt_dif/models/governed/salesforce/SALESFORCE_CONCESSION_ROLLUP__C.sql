
  create or replace  view datalake_dev.governed.SALESFORCE_CONCESSION_ROLLUP__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_concession_rollup__c_3462)
SELECT Closing_Credit_Rollup_c Closing_Credit_Rollup_c
                ,Co_Op_Commision_Rollup_c Co_Op_Commision_Rollup_c
                ,Co_Op_Commission_Rate_Adj_c Co_Op_Commission_Rate_Adj_c
                ,Commission_Impacted_Credit_c Commission_Impacted_Credit_c
                ,Count_of_Standard_Credits_c Count_of_Standard_Credits_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Fully_Approved_Date_c Fully_Approved_Date_c
                ,IRP_Commission_Rate_Adj_c IRP_Commission_Rate_Adj_c
                ,Id Id
                ,Individual_indicator_c Individual_indicator_c
                ,IsDeleted IsDeleted
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Max_Allowable_Concessions_c Max_Allowable_Concessions_c
                ,Max_Concession_passed_c Max_Concession_passed_c
                ,Name Name
                ,OppAdditional_Buyer_1_PAContactID_c OppAdditional_Buyer_1_PAContactID_c
                ,OppAdditional_Buyer_2_PAContactID_c OppAdditional_Buyer_2_PAContactID_c
                ,OppAdditional_Buyer_3_PAContactID_c OppAdditional_Buyer_3_PAContactID_c
                ,OppBuyer_s_AgentId_c OppBuyer_s_AgentId_c
                ,OppDocusign_signature_signee_c OppDocusign_signature_signee_c
                ,OppPAContactID_c OppPAContactID_c
                ,Opp_Addendum_Buyers_c Opp_Addendum_Buyers_c
                ,Opp_Total_Credits_difference_c Opp_Total_Credits_difference_c
                ,OwnerId OwnerId
                ,Pending_Closing_Credit_c Pending_Closing_Credit_c
                ,Pending_Concessions_c Pending_Concessions_c
                ,Place_of_Incorporation_c Place_of_Incorporation_c
                ,Proj_Broker_Commission_Rate_Adj_c Proj_Broker_Commission_Rate_Adj_c
                ,Purchaser_Type_c Purchaser_Type_c
                ,Related_To_c Related_To_c
                ,Stage_c Stage_c
                ,SystemModstamp SystemModstamp
                ,Total_Co_Op_Commission_Base_c Total_Co_Op_Commission_Base_c
                ,Total_Co_Op_Commission_New_c Total_Co_Op_Commission_New_c
                ,Total_Co_Op_Commission_Rate_c Total_Co_Op_Commission_Rate_c
                ,Total_Credits_Executed_Pending_c Total_Credits_Executed_Pending_c
                ,Total_Credits_Executed_c Total_Credits_Executed_c
                ,Total_Credits_c Total_Credits_c
                ,Total_Credits_no_ORP_c Total_Credits_no_ORP_c
                ,Total_IRP_Commission_Base_c Total_IRP_Commission_Base_c
                ,Total_IRP_Commission_Rate_c Total_IRP_Commission_Rate_c
                ,Total_IRP_Commission_c Total_IRP_Commission_c
                ,Total_Project_Broker_Commission_Rate_c Total_Project_Broker_Commission_Rate_c
                
FROM datalake_dev.pre_governed.salesforce_concession_rollup__c_3462 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
