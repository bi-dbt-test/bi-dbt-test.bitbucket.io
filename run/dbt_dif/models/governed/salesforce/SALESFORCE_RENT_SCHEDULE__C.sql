
  create or replace  view datalake_dev.governed.SALESFORCE_RENT_SCHEDULE__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_rent_schedule__c_3851)
SELECT Additional_Fee_c Additional_Fee_c
                ,Annualized_Rent_c Annualized_Rent_c
                ,Apr_c Apr_c
                ,Aug_c Aug_c
                ,Breakpoint_c Breakpoint_c
                ,Calculator_Amount_c Calculator_Amount_c
                ,Calculator_Percentage_c Calculator_Percentage_c
                ,Calculator_Unit_c Calculator_Unit_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Dec_c Dec_c
                ,Due_Date_c Due_Date_c
                ,End_Date_c End_Date_c
                ,Feb_c Feb_c
                ,Free_Rent_Month_c Free_Rent_Month_c
                ,Id Id
                ,IncreasePercentage_c IncreasePercentage_c
                ,IsDeleted IsDeleted
                ,Jan_c Jan_c
                ,Jul_c Jul_c
                ,Jun_c Jun_c
                ,LastActivityDate LastActivityDate
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,Lease_2_c Lease_2_c
                ,Lease_c Lease_c
                ,Mar_c Mar_c
                ,May_c May_c
                ,Monthly_Fee_c Monthly_Fee_c
                ,Monthly_Percentage_of_Sales_c Monthly_Percentage_of_Sales_c
                ,Monthly_Rent_Cap_c Monthly_Rent_Cap_c
                ,Monthly_Rent_c Monthly_Rent_c
                ,Name Name
                ,Nov_c Nov_c
                ,Number_of_Months_c Number_of_Months_c
                ,Oct_c Oct_c
                ,Opportunity_c Opportunity_c
                ,Option_c Option_c
                ,Percentage_Rent_c Percentage_Rent_c
                ,RatePSF_c RatePSF_c
                ,RecordTypeId RecordTypeId
                ,Security_Deposit_Fee_c Security_Deposit_Fee_c
                ,Sep_c Sep_c
                ,Space_c Space_c
                ,Start_Date_c Start_Date_c
                ,Starting_Month_c Starting_Month_c
                ,SystemModstamp SystemModstamp
                ,Tax_c Tax_c
                ,Total_Due_c Total_Due_c
                ,Total_Due_with_Tax_c Total_Due_with_Tax_c
                ,Year_1_c Year_1_c
                
FROM datalake_dev.pre_governed.salesforce_rent_schedule__c_3851 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
