
  create or replace  view datalake_dev.governed.SALESFORCE_EVENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_event_4081)
SELECT A_ali_i_related_c A_ali_i_related_c
                ,AccountId AccountId
                ,ActivityDate ActivityDate
                ,ActivityDateTime ActivityDateTime
                ,Activity_Count_c Activity_Count_c
                ,Activity_Type_2_c Activity_Type_2_c
                ,Additional_Attendees_c Additional_Attendees_c
                ,Always_One_DO_NOT_USE_c Always_One_DO_NOT_USE_c
                ,Assigned_Unit_c Assigned_Unit_c
                ,Back_Up_Interest_c Back_Up_Interest_c
                ,Beverage_Notes_c Beverage_Notes_c
                ,Beverage_Service_c Beverage_Service_c
                ,Building_c Building_c
                ,ClientGuid ClientGuid
                ,Contact_type_c Contact_type_c
                ,Count_of_Days_Passed_c Count_of_Days_Passed_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Date_Completed_c Date_Completed_c
                ,Description Description
                ,DurationInMinutes DurationInMinutes
                ,EDM_Status_c EDM_Status_c
                ,EndDate EndDate
                ,EndDateTime EndDateTime
                ,EventSubtype EventSubtype
                ,First_Choice_Unit_c First_Choice_Unit_c
                ,Followed_Up_c Followed_Up_c
                ,Gift_Card_Number_c Gift_Card_Number_c
                ,GroupEventType GroupEventType
                ,Headcount_c Headcount_c
                ,Id Id
                ,IsAllDayEvent IsAllDayEvent
                ,IsArchived IsArchived
                ,IsChild IsChild
                ,IsClientManaged IsClientManaged
                ,IsDeleted IsDeleted
                ,IsException IsException
                ,IsGroupEvent IsGroupEvent
                ,IsPrivate IsPrivate
                ,IsRecurrence IsRecurrence
                ,IsRecurrence2 IsRecurrence2
                ,IsRecurrence2Exception IsRecurrence2Exception
                ,IsRecurrence2Exclusion IsRecurrence2Exclusion
                ,IsReminderSet IsReminderSet
                ,IsVisibleInSelfService IsVisibleInSelfService
                ,Japan_Gallery_c Japan_Gallery_c
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,Level_of_Interest_c Level_of_Interest_c
                ,Location Location
                ,Notes_c Notes_c
                ,Number_in_Group_c Number_in_Group_c
                ,OO_Potential_c OO_Potential_c
                ,OriginalInstanceDateTime OriginalInstanceDateTime
                ,OwnerId OwnerId
                ,Price_Range_c Price_Range_c
                ,Project_c Project_c
                ,Ranking_c Ranking_c
                ,Real_Estate_Professional_Firm_c Real_Estate_Professional_Firm_c
                ,Realestate_Professional_c Realestate_Professional_c
                ,RecordTypeId RecordTypeId
                ,Recurrence2PatternStartDate Recurrence2PatternStartDate
                ,Recurrence2PatternText Recurrence2PatternText
                ,Recurrence2PatternTimeZone Recurrence2PatternTimeZone
                ,Recurrence2PatternVersion Recurrence2PatternVersion
                ,RecurrenceActivityId RecurrenceActivityId
                ,RecurrenceDayOfMonth RecurrenceDayOfMonth
                ,RecurrenceDayOfWeekMask RecurrenceDayOfWeekMask
                ,RecurrenceEndDateOnly RecurrenceEndDateOnly
                ,RecurrenceInstance RecurrenceInstance
                ,RecurrenceInterval RecurrenceInterval
                ,RecurrenceMonthOfYear RecurrenceMonthOfYear
                ,RecurrenceStartDateTime RecurrenceStartDateTime
                ,RecurrenceTimeZoneSidKey RecurrenceTimeZoneSidKey
                ,RecurrenceType RecurrenceType
                ,ReminderDateTime ReminderDateTime
                ,Scheduled_Tour_Date_c Scheduled_Tour_Date_c
                ,Second_Choice_c Second_Choice_c
                ,ShowAs ShowAs
                ,StartDateTime StartDateTime
                ,Subject Subject
                ,SystemModstamp SystemModstamp
                ,Tour_Location_c Tour_Location_c
                ,Tower_Orientation_c Tower_Orientation_c
                ,Type Type
                ,Unit_Type_c Unit_Type_c
                ,WhatCount WhatCount
                ,WhatId WhatId
                ,WhoCount WhoCount
                ,WhoId WhoId
                ,Who_initiated_this_activity_c Who_initiated_this_activity_c
                ,cirrusadv_Created_by_Cirrus_Insight_c cirrusadv_Created_by_Cirrus_Insight_c
                ,cirrusadv_Day_Activity_Created_c cirrusadv_Day_Activity_Created_c
                ,cirrusadv_Email_Opened_c cirrusadv_Email_Opened_c
                ,cirrusadv_First_Clicked_c cirrusadv_First_Clicked_c
                ,cirrusadv_First_Opened_c cirrusadv_First_Opened_c
                ,cirrusadv_First_Reply_c cirrusadv_First_Reply_c
                ,cirrusadv_Hour_Sent_c cirrusadv_Hour_Sent_c
                ,cirrusadv_Hour_c cirrusadv_Hour_c
                ,cirrusadv_Last_Clicked_c cirrusadv_Last_Clicked_c
                ,cirrusadv_Last_Opened_c cirrusadv_Last_Opened_c
                ,cirrusadv_Last_Reply_c cirrusadv_Last_Reply_c
                ,cirrusadv_Links_Clicked_c cirrusadv_Links_Clicked_c
                ,cirrusadv_Num_of_Clicks_c cirrusadv_Num_of_Clicks_c
                ,cirrusadv_Num_of_Opens_c cirrusadv_Num_of_Opens_c
                ,cirrusadv_Num_of_Replies_c cirrusadv_Num_of_Replies_c
                ,cirrusadv_Outcome_c cirrusadv_Outcome_c
                ,cirrusadv_Reply_Received_c cirrusadv_Reply_Received_c
                ,cirrusadv_Template_Name_c cirrusadv_Template_Name_c
                ,cirrusadv_Time_to_Open_c cirrusadv_Time_to_Open_c
                ,cirrusadv_isTracked_c cirrusadv_isTracked_c
                ,pi_pardot_source_id_c pi_pardot_source_id_c
                
FROM datalake_dev.pre_governed.salesforce_event_4081 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
