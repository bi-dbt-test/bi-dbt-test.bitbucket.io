
  create or replace  view datalake_dev.governed.SALESFORCE_CONCESSION__C  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.salesforce_concession__c_2079)
SELECT Additional_Buyer_1_Contract_c Additional_Buyer_1_Contract_c
                ,Additional_Buyer_2_Contract_c Additional_Buyer_2_Contract_c
                ,Additional_Buyer_3_Contract_c Additional_Buyer_3_Contract_c
                ,Addl_Payment_Options_Executed_c Addl_Payment_Options_Executed_c
                ,Amount_c Amount_c
                ,Approved_Date_c Approved_Date_c
                ,Co_Op_Commission_Rate_Adj_c Co_Op_Commission_Rate_Adj_c
                ,Comission_Factor_c Comission_Factor_c
                ,Concession_Notes_c Concession_Notes_c
                ,Concession_Rollup_c Concession_Rollup_c
                ,Concession_Status_c Concession_Status_c
                ,Concession_SubType_c Concession_SubType_c
                ,Concession_Type_c Concession_Type_c
                ,Contract_Address_c Contract_Address_c
                ,CreatedById CreatedById
                ,CreatedDate CreatedDate
                ,Credit_Amdt_Executed_c Credit_Amdt_Executed_c
                ,Credit_Percentage_c Credit_Percentage_c
                ,Employee_Purchase_Agmt_Executed_c Employee_Purchase_Agmt_Executed_c
                ,IRP_Commission_Rate_Adj_c IRP_Commission_Rate_Adj_c
                ,Id Id
                ,IsDeleted IsDeleted
                ,LastModifiedById LastModifiedById
                ,LastModifiedDate LastModifiedDate
                ,LastReferencedDate LastReferencedDate
                ,LastViewedDate LastViewedDate
                ,LocationsORPSigner_c LocationsORPSigner_c
                ,Name Name
                ,ORPAddendumTemplate_c ORPAddendumTemplate_c
                ,Opportunity_Stage_c Opportunity_Stage_c
                ,Opportunity_c Opportunity_c
                ,OwnerReferralAddress_c OwnerReferralAddress_c
                ,Owner_Referral_Form_Executed_c Owner_Referral_Form_Executed_c
                ,Proj_Broker_Commission_Rate_Adj_c Proj_Broker_Commission_Rate_Adj_c
                ,Property_From_Owner_Referral_c Property_From_Owner_Referral_c
                ,Property_from_Opportunity_c Property_from_Opportunity_c
                ,Purchaser_Account_Name_c Purchaser_Account_Name_c
                ,RecordTypeId RecordTypeId
                ,ReferralContactID_c ReferralContactID_c
                ,ReferralContractsignedbybyer_c ReferralContractsignedbybyer_c
                ,Referral_Opportunity_c Referral_Opportunity_c
                ,Referred_c Referred_c
                ,Storage_Addendum_Executed_c Storage_Addendum_Executed_c
                ,Submitted_Date_c Submitted_Date_c
                ,SystemModstamp SystemModstamp
                ,TEMPORARY_NOTE_FIELD_FOR_BACKFILL_c TEMPORARY_NOTE_FIELD_FOR_BACKFILL_c
                ,Total_Co_Op_Commission_Rate_c Total_Co_Op_Commission_Rate_c
                ,Total_IRP_Commission_Rate_c Total_IRP_Commission_Rate_c
                ,Total_Project_Broker_Commission_Rate_c Total_Project_Broker_Commission_Rate_c
                ,Unit_From_Owner_Referral_c Unit_From_Owner_Referral_c
                ,Unit_from_Opportunity_c Unit_from_Opportunity_c
                ,Upgrades_Addendum_Executed_c Upgrades_Addendum_Executed_c
                
FROM datalake_dev.pre_governed.salesforce_concession__c_2079 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
