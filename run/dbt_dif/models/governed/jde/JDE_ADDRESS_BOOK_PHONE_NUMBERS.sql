
  create or replace  view datalake_dev.governed.JDE_ADDRESS_BOOK_PHONE_NUMBERS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0115_1929)
SELECT WPAN8 AddressNumber
                ,WPIDLN LineNumberID
                ,WPRCK7 SequenceNumber70
                ,WPCNLN WPCNLN
                ,WPPHTP WPPHTP
                ,WPAR1 WPAR1
                ,WPPH1 WPPH1
                ,WPUSER UserId
                ,WPPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(WPUPMJ) DateUpdated
                ,WPJOBN WorkStationId
                ,WPUPMT TimeLastUpdated
                ,WPCFNO1 GenericNumber1
                ,WPGEN1 GeneralInformation1
                ,WPFALGE FutureFlagUse
                ,WPSYNCS SynchronizationStatus
                ,WPCAAD ServerStatus
                
FROM datalake_dev.pre_governed.jde_f0115_1929 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
