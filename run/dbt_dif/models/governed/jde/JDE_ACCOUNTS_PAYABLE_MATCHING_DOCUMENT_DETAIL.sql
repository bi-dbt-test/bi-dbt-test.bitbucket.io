
  create or replace  view datalake_dev.governed.JDE_ACCOUNTS_PAYABLE_MATCHING_DOCUMENT_DETAIL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0414_1958)
SELECT RNPYID PaymentID
                ,RNRC5 RNRC5
                ,RNDCTM DocTypeMatching
                ,RNKCO CompanyKey
                ,RNDCT DocumentType
                ,RNDOC DocumentVoucher
                ,RNSFX DocumentPayItem
                ,RNSFXE RNSFXE
                ,CAST(RNPAAP/100 AS DECIMAL(18,2)) PaymntAmount
                ,CAST(RNADSC/100 AS DECIMAL(18,2)) RNADSC
                ,CAST(RNADSA/100 AS DECIMAL(18,2)) RNADSA
                ,CAST(RNPFAP/100 AS DECIMAL(18,2)) RNPFAP
                ,CAST(RNCDS/100 AS DECIMAL(18,2)) RNCDS
                ,CAST(RNCDSA/100 AS DECIMAL(18,2)) RNCDSA
                ,RNCRRM RNCRRM
                ,RNCRCD CurrencyCodeFrom
                ,RNCRR RNCRR
                ,RNGLC BillCode
                ,RNPOST GLPostedCode
                ,RNALT6 RNALT6
                ,RNPN PeriodNumberGeneralLedger
                ,RNFY FiscalYear
                ,RNCTRY Century
                ,RNFNLP RNFNLP
                ,RNAN8 AddressNumber
                ,RNCO Company
                ,RNMCU BusinessUnit
                ,RNPO PurchaseOrder
                ,RNRMK NameRemark
                ,RNHCRR RNHCRR
                ,RNUSER UserId
                ,RNPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RNUPMJ) DateUpdated
                ,RNUPMT TimeLastUpdated
                ,RNJOBN WorkStationId
                ,RNBCRC RNBCRC
                ,RNLRFL RNLRFL
                ,RNGFL7 RNGFL7
                ,RNGFL8 RNGFL8
                ,CAST(RNGAM3/100 AS DECIMAL(18,2)) RNGAM3
                ,CAST(RNGAM4/100 AS DECIMAL(18,2)) RNGAM4
                ,RNGEN6 RNGEN6
                ,RNGEN7 RNGEN7
                ,RNDRCO RNDRCO
                ,RNNETTCID RNNETTCID
                ,RNNETDOC RNNETDOC
                ,RNNETRC5 RNNETRC5
                ,RNCNTRTID RNCNTRTID
                ,RNCNTRTCD RNCNTRTCD
                ,RNWVID RNWVID
                ,RNBLSCD2 RNBLSCD2
                ,RNHARPER RNHARPER
                ,RNHARSFX RNHARSFX
                
FROM datalake_dev.pre_governed.jde_f0414_1958 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
