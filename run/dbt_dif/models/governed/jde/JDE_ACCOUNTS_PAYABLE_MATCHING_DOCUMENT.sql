
  create or replace  view datalake_dev.governed.JDE_ACCOUNTS_PAYABLE_MATCHING_DOCUMENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0413_1953)
SELECT RMPYID PaymentID
                ,RMDCTM DocTypeMatching
                ,RMDOCM DocMatchingCheckOr
                ,RMPYE RMPYE
                ,RMGLBA GlBankAccount
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RMDMTJ) DateMatchingCheckOr
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RMVDGJ) VoidDateForGLn
                ,RMICU BatchNumber
                ,RMICUT BatchType
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RMDICJ) DateBatchJulian
                ,CAST(RMPAAP/100 AS DECIMAL(18,2)) PaymntAmount
                ,RMCRCD CurrencyCodeFrom
                ,RMCRRM RMCRRM
                ,RMAM RMAM
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RMVLDT) DateValue
                ,RMPYIN PaymentInstrument
                ,RMISTP RMISTP
                ,RMCBNK CustBankAcctNumber
                ,RMBKTR RMBKTR
                ,RMTORG TransactionOriginator
                ,RMUSER UserId
                ,RMPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RMUPMJ) DateUpdated
                ,RMUPMT TimeLastUpdated
                ,RMJOBN WorkStationId
                ,RMMIP RMMIP
                ,RMLRFL RMLRFL
                ,RMPRGF RMPRGF
                ,RMGFL7 RMGFL7
                ,RMGFL8 RMGFL8
                ,CAST(RMGAM3/100 AS DECIMAL(18,2)) RMGAM3
                ,CAST(RMGAM4/100 AS DECIMAL(18,2)) RMGAM4
                ,RMGEN6 RMGEN6
                ,RMGEN7 RMGEN7
                ,RMNETTCID RMNETTCID
                ,RMNETDOC RMNETDOC
                ,RMRCND RMRCND
                ,RMR3 RMR3
                ,RMCNTRTID RMCNTRTID
                ,RMCNTRTCD RMCNTRTCD
                ,RMWVID RMWVID
                ,RMBLSCD2 RMBLSCD2
                ,RMHARPER RMHARPER
                ,RMHARSFX RMHARSFX
                
FROM datalake_dev.pre_governed.jde_f0413_1953 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
