
  create or replace  view datalake_dev.governed.JDE_LEGAL_CLAUSE_AMOUNTS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f15703_1924)
SELECT NIDOCO DocumentOrderInvoice
                ,NIAN8 AddressNumber
                ,NICLNO LegalClauseNumber
                ,NISEQN SequenceNumber2
                ,CAST(NIAG/100 AS DECIMAL(18,2)) AmountGross
                ,NITYAM AmountType
                ,NIUM UnitOfMeasure
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NIEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NIEFTE) DateEndingEffective
                ,NILSVR LeaseVersion
                ,NIURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NIURDT) UserReservedDate
                ,CAST(NIURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NIURAB UserReservedNumber
                ,NIURRF UserReservedReference
                ,NIUSER UserId
                ,NIPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NIUPMJ) DateUpdated
                ,NIUPMT TimeLastUpdated
                ,NIJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NIENTJ) NIENTJ
                ,NITORG TransactionOriginator
                ,NICRCD CurrencyCodeFrom
                ,NICRRM NICRRM
                
FROM datalake_dev.pre_governed.jde_f15703_1924 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
