
  create or replace  view datalake_dev.governed.JDE_ADDRESS_BY_DATE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0116_1931)
SELECT ALAN8 AddressNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(ALEFTB) DateBeginningEffective
                ,ALEFTF ALEFTF
                ,ALADD1 ALADD1
                ,ALADD2 ALADD2
                ,ALADD3 ALADD3
                ,ALADD4 ALADD4
                ,ALADDZ ALADDZ
                ,ALCTY1 ALCTY1
                ,ALCOUN ALCOUN
                ,ALADDS ALADDS
                ,ALCRTE ALCRTE
                ,ALBKML ALBKML
                ,ALCTR Country
                ,ALUSER UserId
                ,ALPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(ALUPMJ) DateUpdated
                ,ALJOBN WorkStationId
                ,ALUPMT TimeLastUpdated
                ,ALSYNCS SynchronizationStatus
                ,ALCAAD ServerStatus
                
FROM datalake_dev.pre_governed.jde_f0116_1931 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
