
  create or replace  view datalake_dev.governed.JDE_ACCOUNT_BALANCES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0902_1963)
SELECT GBAID AccountID
                ,GBCTRY Century
                ,GBFY FiscalYear
                ,GBFQ GBFQ
                ,GBLT LedgerType
                ,GBSBL Subledger
                ,GBCO Company
                ,CAST(GBAPYC/100 AS DECIMAL(18,2)) AmtBeginningBalancePy
                ,CAST(GBAN01/100 AS DECIMAL(18,2)) AmountNetPosting001
                ,CAST(GBAN02/100 AS DECIMAL(18,2)) AmountNetPosting002
                ,CAST(GBAN03/100 AS DECIMAL(18,2)) AmountNetPosting003
                ,CAST(GBAN04/100 AS DECIMAL(18,2)) AmountNetPosting004
                ,CAST(GBAN05/100 AS DECIMAL(18,2)) AmountNetPosting005
                ,CAST(GBAN06/100 AS DECIMAL(18,2)) AmountNetPosting006
                ,CAST(GBAN07/100 AS DECIMAL(18,2)) AmountNetPosting007
                ,CAST(GBAN08/100 AS DECIMAL(18,2)) AmountNetPosting008
                ,CAST(GBAN09/100 AS DECIMAL(18,2)) AmountNetPosting009
                ,CAST(GBAN10/100 AS DECIMAL(18,2)) AmountNetPosting010
                ,CAST(GBAN11/100 AS DECIMAL(18,2)) AmountNetPosting011
                ,CAST(GBAN12/100 AS DECIMAL(18,2)) AmountNetPosting012
                ,CAST(GBAN13/100 AS DECIMAL(18,2)) GBAN13
                ,CAST(GBAN14/100 AS DECIMAL(18,2)) GBAN14
                ,CAST(GBAPYN/100 AS DECIMAL(18,2)) GBAPYN
                ,CAST(GBAWTD/100 AS DECIMAL(18,2)) AmountWtd
                ,CAST(GBBORG/100 AS DECIMAL(18,2)) AmtOriginalBeginBud
                ,CAST(GBPOU/100 AS DECIMAL(18,2)) GBPOU
                ,GBPC GBPC
                ,CAST(GBTKER/100 AS DECIMAL(18,2)) GBTKER
                ,CAST(GBBREQ/100 AS DECIMAL(18,2)) GBBREQ
                ,CAST(GBBAPR/100 AS DECIMAL(18,2)) GBBAPR
                ,GBMCU BusinessUnit
                ,GBOBJ ObjectAccount
                ,GBSUB Subsidiary
                ,GBUSER UserId
                ,GBPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GBUPMJ) DateUpdated
                ,GBJOBN WorkStationId
                ,GBSBLT SubledgerType
                ,GBUPMT TimeLastUpdated
                ,GBCRCD CurrencyCodeFrom
                ,GBCRCX GBCRCX
                ,GBPRGF GBPRGF
                ,CAST(GBAND01/100 AS DECIMAL(18,2)) GBAND01
                ,CAST(GBAND02/100 AS DECIMAL(18,2)) GBAND02
                ,CAST(GBAND03/100 AS DECIMAL(18,2)) GBAND03
                ,CAST(GBAND04/100 AS DECIMAL(18,2)) GBAND04
                ,CAST(GBAND05/100 AS DECIMAL(18,2)) GBAND05
                ,CAST(GBAND06/100 AS DECIMAL(18,2)) GBAND06
                ,CAST(GBAND07/100 AS DECIMAL(18,2)) GBAND07
                ,CAST(GBAND08/100 AS DECIMAL(18,2)) GBAND08
                ,CAST(GBAND09/100 AS DECIMAL(18,2)) GBAND09
                ,CAST(GBAND10/100 AS DECIMAL(18,2)) GBAND10
                ,CAST(GBAND11/100 AS DECIMAL(18,2)) GBAND11
                ,CAST(GBAND12/100 AS DECIMAL(18,2)) GBAND12
                ,CAST(GBAND13/100 AS DECIMAL(18,2)) GBAND13
                ,CAST(GBAND14/100 AS DECIMAL(18,2)) GBAND14
                
FROM datalake_dev.pre_governed.jde_f0902_1963 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
