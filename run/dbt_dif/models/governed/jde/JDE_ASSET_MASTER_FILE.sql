
  create or replace  view datalake_dev.governed.JDE_ASSET_MASTER_FILE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1201_1922)
SELECT FACO Company
                ,FANUMB FANUMB
                ,FAAPID FAAPID
                ,FAAAID FAAAID
                ,FAASID FAASID
                ,FASEQ FASEQ
                ,FAACL1 FAACL1
                ,FAACL2 FAACL2
                ,FAACL3 FAACL3
                ,FAACL4 FAACL4
                ,FAACL5 FAACL5
                ,FAMCU BusinessUnit
                ,FADL01 Description
                ,FADL02 Description02
                ,FADL03 FADL03
                ,FADSCC FADSCC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FADAJ) FADAJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FADSP) FADSP
                ,FAEQST FAEQST
                ,FANORU FANORU
                ,CAST(FAAESV/100 AS DECIMAL(18,2)) FAAESV
                ,CAST(FAARPC/100 AS DECIMAL(18,2)) FAARPC
                ,CAST(FAALRC/100 AS DECIMAL(18,2)) FAALRC
                ,FAAMCU FAAMCU
                ,FAAOBJ FAAOBJ
                ,FAASUB FAASUB
                ,FADMCU FADMCU
                ,FADOBJ FADOBJ
                ,FADSUB FADSUB
                ,FAXMCU FAXMCU
                ,FAXOBJ FAXOBJ
                ,FAXSUB FAXSUB
                ,FARMCU FARMCU
                ,FAROBJ FAROBJ
                ,FARSUB FARSUB
                ,CAST(FAARCQ/100 AS DECIMAL(18,2)) FAARCQ
                ,CAST(FAAROQ/100 AS DECIMAL(18,2)) FAAROQ
                ,FATXJS FATXJS
                ,CAST(FAAITY/100 AS DECIMAL(18,2)) FAAITY
                ,CAST(FAAITP/100 AS DECIMAL(18,2)) FAAITP
                ,FAFINC FAFINC
                ,FAITCO FAITCO
                ,FAPURO FAPURO
                ,CAST(FAAPUR/100 AS DECIMAL(18,2)) FAAPUR
                ,CAST(FAPURP/100 AS DECIMAL(18,2)) FAPURP
                ,CAST(FAAPOM/100 AS DECIMAL(18,2)) FAAPOM
                ,FALANO FALANO
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FAJCD) FAJCD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FADEXJ) FADEXJ
                ,CAST(FAAMF/100 AS DECIMAL(18,2)) FAAMF
                ,FARMK NameRemark
                ,FARMK2 FARMK2
                ,FAINSP FAINSP
                ,FAINSC FAINSC
                ,FAINSM FAINSM
                ,CAST(FAINSA/100 AS DECIMAL(18,2)) FAINSA
                ,CAST(FAAIV/100 AS DECIMAL(18,2)) FAAIV
                ,FAINSI FAINSI
                ,FAUSER UserId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FALCT) FALCT
                ,FALOC FALOC
                ,FAADDS FAADDS
                ,FAPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FAEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FADER) FADER
                ,FAMSGA FAMSGA
                ,FAEX FAEX
                ,FAEXR NameRemarkExplanation
                ,FAAN8 AddressNumber
                ,FAACL6 FAACL6
                ,FAACL7 FAACL7
                ,FAACL8 FAACL8
                ,FAACL9 FAACL9
                ,FAACL0 FAACL0
                ,CAST(FASFC/100 AS DECIMAL(18,2)) FASFC
                ,FADADC FADADC
                ,FADADO FADADO
                ,FADADS FADADS
                ,FAUNIT Unit
                ,FAKIT FAKIT
                ,FAKITL FAKITL
                ,FAAFE FAAFE
                ,FAJBCD FAJBCD
                ,FAJBST FAJBST
                ,FAUN FAUN
                ,FASBLI FASBLI
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FAUPMJ) DateUpdated
                ,FAJOBN WorkStationId
                ,FAUPMT TimeLastUpdated
                ,FAFA0 FAFA0
                ,FAFA1 FAFA1
                ,FAFA2 FAFA2
                ,FAFA3 FAFA3
                ,FAFA4 FAFA4
                ,FAFA5 FAFA5
                ,FAFA6 FAFA6
                ,FAFA7 FAFA7
                ,FAFA8 FAFA8
                ,FAFA9 FAFA9
                ,FAFA21 FAFA21
                ,FAFA22 FAFA22
                ,FAFA23 FAFA23
                ,FAWOYN FAWOYN
                ,FACRTL FACRTL
                ,FAWRFL FAWRFL
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FAWARJ) FAWARJ
                
FROM datalake_dev.pre_governed.jde_f1201_1922 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
