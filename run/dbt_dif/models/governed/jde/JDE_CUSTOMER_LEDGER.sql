
  create or replace  view datalake_dev.governed.JDE_CUSTOMER_LEDGER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f03b11_1976)
SELECT RPDOC DocumentVoucher
                ,RPDCT DocumentType
                ,RPKCO CompanyKey
                ,RPSFX DocumentPayItem
                ,RPAN8 AddressNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDGJ) DateForGLandVoucher
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDIVJ) DateInvoiceJulian
                ,RPICUT BatchType
                ,RPICU BatchNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDICJ) DateBatchJulian
                ,RPFY FiscalYear
                ,RPCTRY Century
                ,RPPN PeriodNumberGeneralLedger
                ,RPCO Company
                ,RPGLC BillCode
                ,RPAID AccountID
                ,RPPA8 RPPA8
                ,RPAN8J AddNoAlternatePayee
                ,RPPYR RPPYR
                ,RPPOST GLPostedCode
                ,RPISTR RPISTR
                ,RPBALJ RPBALJ
                ,RPPST PayStatusCode
                ,CAST(RPAG/100 AS DECIMAL(18,2)) AmountGross
                ,CAST(RPAAP/100 AS DECIMAL(18,2)) AmountOpen
                ,CAST(RPADSC/100 AS DECIMAL(18,2)) RPADSC
                ,CAST(RPADSA/100 AS DECIMAL(18,2)) RPADSA
                ,CAST(RPATXA/100 AS DECIMAL(18,2)) RPATXA
                ,CAST(RPATXN/100 AS DECIMAL(18,2)) RPATXN
                ,CAST(RPSTAM/100 AS DECIMAL(18,2)) RPSTAM
                ,RPBCRC RPBCRC
                ,RPCRRM RPCRRM
                ,RPCRCD CurrencyCodeFrom
                ,RPCRR RPCRR
                ,RPDMCD RPDMCD
                ,CAST(RPACR/100 AS DECIMAL(18,2)) RPACR
                ,CAST(RPFAP/100 AS DECIMAL(18,2)) RPFAP
                ,CAST(RPCDS/100 AS DECIMAL(18,2)) RPCDS
                ,CAST(RPCDSA/100 AS DECIMAL(18,2)) RPCDSA
                ,CAST(RPCTXA/100 AS DECIMAL(18,2)) RPCTXA
                ,CAST(RPCTXN/100 AS DECIMAL(18,2)) RPCTXN
                ,CAST(RPCTAM/100 AS DECIMAL(18,2)) RPCTAM
                ,RPTXA1 TaxArea1
                ,RPEXR1 TaxExplanationCode1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDSVJ) DateServiceCurrency
                ,RPGLBA GlBankAccount
                ,RPAM RPAM
                ,RPAID2 RPAID2
                ,RPAM2 RPAM2
                ,RPMCU BusinessUnit
                ,RPOBJ ObjectAccount
                ,RPSUB Subsidiary
                ,RPSBLT SubledgerType
                ,RPSBL Subledger
                ,RPPTC RPPTC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDDJ) DateNetDue
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDDNJ) RPDDNJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPRDDJ) RPRDDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPRDSJ) RPRDSJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPLFCJ) RPLFCJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPSMTJ) DateStatement
                ,RPNBRR RPNBRR
                ,RPRDRL RPRDRL
                ,RPRMDS RPRMDS
                ,RPCOLL RPCOLL
                ,RPCORC RPCORC
                ,RPAFC RPAFC
                ,RPDNLT RPDNLT
                ,RPRSCO RPRSCO
                ,RPODOC RPODOC
                ,RPODCT RPODCT
                ,RPOKCO RPOKCO
                ,RPOSFX RPOSFX
                ,RPVINV SupplierInvoiceNumber
                ,RPPO PurchaseOrder
                ,RPPDCT RPPDCT
                ,RPPKCO RPPKCO
                ,RPDCTO OrderType
                ,CAST(RPLNID/100 AS DECIMAL(18,2)) LineNumber
                ,RPSDOC RPSDOC
                ,RPSDCT RPSDCT
                ,RPSKCO RPSKCO
                ,RPSFXO RPSFXO
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPVLDT) DateValue
                ,RPCMC1 RPCMC1
                ,RPVR01 RPVR01
                ,RPUNIT Unit
                ,RPMCU2 BusinessUnit2
                ,RPRMK NameRemark
                ,RPALPH NameAlpha
                ,RPRF RPRF
                ,RPDRF RPDRF
                ,RPCTL RPCTL
                ,RPFNLP RPFNLP
                ,RPITM RPITM
                ,CAST(RPU/100 AS DECIMAL(18,2)) RPU
                ,RPUM UnitOfMeasure
                ,RPALT6 RPALT6
                ,RPRYIN RPRYIN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPVDGJ) VoidDateForGLn
                ,RPVOD VoidFlag
                ,RPRP1 RPRP1
                ,RPRP2 RPRP2
                ,RPRP3 RPRP3
                ,RPAR01 RPAR01
                ,RPAR02 RPAR02
                ,RPAR03 RPAR03
                ,RPAR04 RPAR04
                ,RPAR05 RPAR05
                ,RPAR06 RPAR06
                ,RPAR07 RPAR07
                ,RPAR08 RPAR08
                ,RPAR09 RPAR09
                ,RPAR10 RPAR10
                ,RPISTC RPISTC
                ,RPPYID PaymentID
                ,RPURC1 RPURC1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPURDT) UserReservedDate
                ,CAST(RPURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,RPURAB UserReservedNumber
                ,RPURRF UserReservedReference
                ,RPRNID RPRNID
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPPPDI) RPPPDI
                ,RPTORG TransactionOriginator
                ,RPUSER UserId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPJCL) DateInvoiceClosed
                ,RPPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPUPMJ) DateUpdated
                ,RPUPMT TimeLastUpdated
                ,RPDDEX RPDDEX
                ,RPJOBN WorkStationId
                ,RPHCRR RPHCRR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPHDGJ) RPHDGJ
                ,RPSHPN RPSHPN
                ,RPDTXS RPDTXS
                ,RPOMOD RPOMOD
                ,RPCLMG RPCLMG
                ,RPCMGR RPCMGR
                ,CAST(RPATAD/100 AS DECIMAL(18,2)) RPATAD
                ,CAST(RPCTAD/100 AS DECIMAL(18,2)) RPCTAD
                ,CAST(RPNRTA/100 AS DECIMAL(18,2)) RPNRTA
                ,CAST(RPFNRT/100 AS DECIMAL(18,2)) RPFNRT
                ,RPPRGF RPPRGF
                ,RPGFL1 RPGFL1
                ,RPGFL2 RPGFL2
                ,RPDOCO DocumentOrderInvoice
                ,RPKCOO RPKCOO
                ,RPSOTF RPSOTF
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDTPB) RPDTPB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPERDJ) RPERDJ
                ,RPPWPG RPPWPG
                ,RPNETTCID RPNETTCID
                ,RPNETDOC RPNETDOC
                ,RPNETRC5 RPNETRC5
                ,RPNETST RPNETST
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPAJCL) RPAJCL
                ,RPRMR1 RPRMR1
                
FROM datalake_dev.pre_governed.jde_f03b11_1976 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
