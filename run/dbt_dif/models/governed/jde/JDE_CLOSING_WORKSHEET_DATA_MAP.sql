
  create or replace  view datalake_dev.governed.JDE_CLOSING_WORKSHEET_DATA_MAP  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h599_2474)
SELECT CMCWTMPT Closing_Worksheet_Template
                ,CMHBMCUS Community
                ,CMCLDATA Closing_Worksheet_Data
                ,CMOPTYP Option_Type
                ,CMCLSRT Closing_Worksheet_Line_Item
                ,CMCRTU Created_by_user
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMCRTJ) Date_Created
                ,CMCRTT Time_Created
                ,CMWRKSTNID Original_Work_Station_ID
                ,CMHBOPID Original_Program_ID
                ,CMUPMB Updated_By_User
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUPMJ) Date_Updated
                ,CMUPMT Time_Last_Updated
                ,CMJOBN Work_Station_ID
                ,CMPID Program_ID
                
FROM datalake_dev.pre_governed.jde_f44h599_2474 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
