
  create or replace  view datalake_dev.governed.JDE_PURCHASE_ORDER_DETAIL_FILE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f4311_1933)
SELECT PDKCOO PDKCOO
                ,PDDOCO DocumentOrderInvoice
                ,PDDCTO OrderType
                ,PDSFXO PDSFXO
                ,CAST(PDLNID/100 AS DECIMAL(18,2)) LineNumber
                ,PDMCU BusinessUnit
                ,PDCO Company
                ,PDOKCO PDOKCO
                ,PDOORN PDOORN
                ,PDOCTO PDOCTO
                ,CAST(PDOGNO/100 AS DECIMAL(18,2)) PDOGNO
                ,PDRKCO PDRKCO
                ,PDRORN PDRORN
                ,PDRCTO PDRCTO
                ,CAST(PDRLLN/100 AS DECIMAL(18,2)) PDRLLN
                ,PDDMCT PDDMCT
                ,PDDMCS PDDMCS
                ,PDBALU PDBALU
                ,PDAN8 AddressNumber
                ,PDSHAN PDSHAN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDDRQJ) PDDRQJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDTRDJ) PDTRDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDPDDJ) PDPDDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDOPDJ) PDOPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDADDJ) PDADDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDCNDJ) PDCNDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDPEFJ) PDPEFJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDPPDJ) PDPPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDPSDJ) PDPSDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDDSVJ) DateServiceCurrency
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDDGL) PDDGL
                ,PDPN PeriodNumberGeneralLedger
                ,PDVR01 PDVR01
                ,PDVR02 PDVR02
                ,PDITM PDITM
                ,PDLITM PDLITM
                ,PDAITM PDAITM
                ,PDLOCN PDLOCN
                ,PDLOTN PDLOTN
                ,PDFRGD PDFRGD
                ,PDTHGD PDTHGD
                ,CAST(PDFRMP/100 AS DECIMAL(18,2)) PDFRMP
                ,CAST(PDTHRP/100 AS DECIMAL(18,2)) PDTHRP
                ,PDDSC1 PDDSC1
                ,PDDSC2 PDDSC2
                ,PDLNTY PDLNTY
                ,PDNXTR PDNXTR
                ,PDLTTR PDLTTR
                ,PDRLIT PDRLIT
                ,PDPDS1 PDPDS1
                ,PDPDS2 PDPDS2
                ,PDPDS3 PDPDS3
                ,PDPDS4 PDPDS4
                ,PDPDS5 PDPDS5
                ,PDPDP1 PDPDP1
                ,PDPDP2 PDPDP2
                ,PDPDP3 PDPDP3
                ,PDPDP4 PDPDP4
                ,PDPDP5 PDPDP5
                ,PDUOM PDUOM
                ,PDUORG PDUORG
                ,PDUCHG PDUCHG
                ,PDUOPN PDUOPN
                ,PDUREC PDUREC
                ,PDCREC PDCREC
                ,PDURLV PDURLV
                ,PDOTQY PDOTQY
                ,CAST(PDPRRC/100 AS DECIMAL(18,2)) PDPRRC
                ,CAST(PDAEXP/100 AS DECIMAL(18,2)) PDAEXP
                ,CAST(PDACHG/100 AS DECIMAL(18,2)) PDACHG
                ,CAST(PDAOPN/100 AS DECIMAL(18,2)) PDAOPN
                ,CAST(PDAREC/100 AS DECIMAL(18,2)) PDAREC
                ,CAST(PDARLV/100 AS DECIMAL(18,2)) PDARLV
                ,CAST(PDFTN1/100 AS DECIMAL(18,2)) PDFTN1
                ,CAST(PDTRLV/100 AS DECIMAL(18,2)) PDTRLV
                ,PDPROV PDPROV
                ,CAST(PDAMC3/100 AS DECIMAL(18,2)) PDAMC3
                ,CAST(PDECST/100 AS DECIMAL(18,2)) PDECST
                ,PDCSTO PDCSTO
                ,PDCSMP PDCSMP
                ,PDINMG PrintMessage1
                ,PDASN PriceAdjustmentScheduleN
                ,PDPRGR PDPRGR
                ,PDCLVL PDCLVL
                ,PDCATN PDCATN
                ,CAST(PDDSPR/100 AS DECIMAL(18,2)) PDDSPR
                ,PDPTC PDPTC
                ,PDTX PDTX
                ,PDEXR1 TaxExplanationCode1
                ,PDTXA1 TaxArea1
                ,PDATXT PDATXT
                ,PDCNID PDCNID
                ,PDCDCD PDCDCD
                ,PDNTR PDNTR
                ,PDFRTH FreightHandlingCode
                ,PDFRTC PDFRTC
                ,PDZON ZoneNumber
                ,PDFRAT PDFRAT
                ,PDRATT PDRATT
                ,PDANBY PDANBY
                ,PDANCR CarrierNumber
                ,PDMOT PDMOT
                ,PDCOT PDCOT
                ,PDSHCM PDSHCM
                ,PDSHCN PDSHCN
                ,PDUOM1 PDUOM1
                ,PDPQOR PDPQOR
                ,PDUOM2 PDUOM2
                ,PDSQOR PDSQOR
                ,PDUOM3 PDUOM3
                ,CAST(PDITWT/100 AS DECIMAL(18,2)) PDITWT
                ,PDWTUM PDWTUM
                ,CAST(PDITVL/100 AS DECIMAL(18,2)) PDITVL
                ,PDVLUM PDVLUM
                ,PDGLC BillCode
                ,PDCTRY Century
                ,PDFY FiscalYear
                ,PDSTTS PDSTTS
                ,PDRCD PDRCD
                ,PDFUF1 PDFUF1
                ,PDFUF2 PDFUF2
                ,CAST(PDGRWT/100 AS DECIMAL(18,2)) PDGRWT
                ,PDGWUM PDGWUM
                ,PDLT LedgerType
                ,PDANI AcctNoInputMode
                ,PDAID AccountID
                ,PDOMCU PDOMCU
                ,PDOBJ ObjectAccount
                ,PDSUB Subsidiary
                ,PDSBLT SubledgerType
                ,PDSBL Subledger
                ,PDASID PDASID
                ,PDCCMP PDCCMP
                ,PDTAG PDTAG
                ,PDWR01 PDWR01
                ,PDPL PDPL
                ,PDELEV PDELEV
                ,PDR001 PDR001
                ,PDRTNR PDRTNR
                ,PDLCOD PDLCOD
                ,PDPURG PDPURG
                ,PDPROM PDPROM
                ,PDFNLP PDFNLP
                ,PDAVCH CodeAutomaticVoucher
                ,PDPRPY PDPRPY
                ,PDUNCD PDUNCD
                ,PDMATY PDMATY
                ,PDRTGC PDRTGC
                ,PDRCPF PDRCPF
                ,PDPS01 PDPS01
                ,PDPS02 PDPS02
                ,PDPS03 PDPS03
                ,PDPS04 PDPS04
                ,PDPS05 PDPS05
                ,PDPS06 PDPS06
                ,PDPS07 PDPS07
                ,PDPS08 PDPS08
                ,PDPS09 PDPS09
                ,PDPS10 PDPS10
                ,PDCRMD CorrespondenceMethod
                ,PDARTG PDARTG
                ,PDCORD PDCORD
                ,PDCHDT PDCHDT
                ,PDDOCC PDDOCC
                ,CAST(PDCHLN/100 AS DECIMAL(18,2)) PDCHLN
                ,PDCRCD CurrencyCodeFrom
                ,PDCRR PDCRR
                ,CAST(PDFRRC/100 AS DECIMAL(18,2)) PDFRRC
                ,CAST(PDFEA/100 AS DECIMAL(18,2)) PDFEA
                ,CAST(PDFUC/100 AS DECIMAL(18,2)) PDFUC
                ,CAST(PDFEC/100 AS DECIMAL(18,2)) PDFEC
                ,CAST(PDFCHG/100 AS DECIMAL(18,2)) PDFCHG
                ,CAST(PDFAP/100 AS DECIMAL(18,2)) PDFAP
                ,CAST(PDFREC/100 AS DECIMAL(18,2)) PDFREC
                ,PDURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDURDT) UserReservedDate
                ,CAST(PDURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,PDURAB UserReservedNumber
                ,PDURRF UserReservedReference
                ,PDTORG TransactionOriginator
                ,PDUSER UserId
                ,PDPID ProgramId
                ,PDJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDUPMJ) DateUpdated
                ,PDTDAY PDTDAY
                ,PDVR05 PDVR05
                ,PDVR04 PDVR04
                ,PDSHPN PDSHPN
                ,PDRSHT PDRSHT
                ,PDPRJM PDPRJM
                ,PDOSFX PDOSFX
                ,PDMERL PDMERL
                ,CAST(PDMCLN/100 AS DECIMAL(18,2)) PDMCLN
                ,PDMACT PDMACT
                ,CAST(PDKTLN/100 AS DECIMAL(18,2)) PDKTLN
                ,CAST(PDFTRL/100 AS DECIMAL(18,2)) PDFTRL
                ,PDDUAL PDDUAL
                ,PDDRQT PDDRQT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDDLEJ) PDDLEJ
                ,CAST(PDCTAM/100 AS DECIMAL(18,2)) PDCTAM
                ,CAST(PDCPNT/100 AS DECIMAL(18,2)) PDCPNT
                ,PDCHT PDCHT
                ,PDCHRT PDCHRT
                ,PDCHRS PDCHRS
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PDCHMJ) PDCHMJ
                ,PDBCRC PDBCRC
                ,PDVR03 PDVR03
                ,PDLDNM PDLDNM
                ,PDMKFR PDMKFR
                ,PDPMTN PDPMTN
                ,PDUKID UniqueKeyIDInternal
                ,PDUNSPSC PDUNSPSC
                ,PDCMDCDE PDCMDCDE
                ,PDRSFX PDRSFX
                ,PDWVID PDWVID
                ,PDCNTRTID PDCNTRTID
                ,PDCNTRTDID PDCNTRTDID
                ,PDMOADJ PDMOADJ
                ,PDPODC01 PDPODC01
                ,PDPODC02 PDPODC02
                ,PDPODC03 PDPODC03
                ,PDPODC04 PDPODC04
                ,PDJBCD PDJBCD
                ,CAST(PDSRQTY/100 AS DECIMAL(18,2)) PDSRQTY
                ,PDSRUOM PDSRUOM
                ,PDCFGFL PDCFGFL
                ,PDPMPN PDPMPN
                ,PDPNS PDPNS
                
FROM datalake_dev.pre_governed.jde_f4311_1933 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
