
  create or replace  view datalake_dev.governed.JDE_ADDRESS_ORGANIZATION_STRUCTURE_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0150_1946)
SELECT MAOSTP MAOSTP
                ,MAPA8 MAPA8
                ,MAAN8 AddressNumber
                ,MADSS7 MADSS7
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MABEFD) MABEFD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MAEEFD) MAEEFD
                ,MARMK NameRemark
                ,MAUSER UserId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MAUPMJ) DateUpdated
                ,MAPID ProgramId
                ,MAJOBN WorkStationId
                ,MAUPMT TimeLastUpdated
                ,MASYNCS SynchronizationStatus
                
FROM datalake_dev.pre_governed.jde_f0150_1946 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
