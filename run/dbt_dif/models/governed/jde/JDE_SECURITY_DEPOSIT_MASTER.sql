
  create or replace  view datalake_dev.governed.JDE_SECURITY_DEPOSIT_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1565_1978)
SELECT JFDOCO DocumentOrderInvoice
                ,JFLSVR LeaseVersion
                ,JFCO Company
                ,JFMCU BusinessUnit
                ,JFUNIT Unit
                ,JFGLC BillCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(JFEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(JFEFTE) DateEndingEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(JFSUDT) DateSuspended
                ,JFSUSP SuspendCode
                ,JFVRSC VersionSuspendCode
                ,JFEDT EscrowDepositType
                ,JFSDAP SecurityDepositAppliedStatus
                ,CAST(JFAG/100 AS DECIMAL(18,2)) AmountGross
                ,SEMANTIC.JDE_JULIAN_TO_DATE(JFTKDT) TickleDate
                ,JFBLMN BillManually
                ,JFEXR NameRemarkExplanation
                ,JFBCI BillingControlId
                ,JFBCI3 BillingControlIDThird
                ,CAST(JFLNID/100 AS DECIMAL(18,2)) LineNumber
                ,JFAN8 AddressNumber
                ,JFAN8J AddNoAlternatePayee
                ,JFCRYR CurrencyConversion
                ,JFCRRM JFCRRM
                ,JFCRCD CurrencyCodeFrom
                ,JFCRCX JFCRCX
                ,JFCRR JFCRR
                ,CAST(JFCTXA/100 AS DECIMAL(18,2)) JFCTXA
                ,CAST(JFCTXN/100 AS DECIMAL(18,2)) JFCTXN
                ,CAST(JFCTAM/100 AS DECIMAL(18,2)) JFCTAM
                ,JFURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(JFURDT) UserReservedDate
                ,CAST(JFURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,JFURAB UserReservedNumber
                ,JFURRF UserReservedReference
                ,JFUSER UserId
                ,JFPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(JFUPMJ) DateUpdated
                ,JFUPMT TimeLastUpdated
                ,JFJOBN WorkStationId
                
FROM datalake_dev.pre_governed.jde_f1565_1978 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
