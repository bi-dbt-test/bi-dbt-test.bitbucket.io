
  create or replace  view datalake_dev.governed.JDE_SALES_CONTRACT_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5544h03_1985)
SELECT SCLOANNUM LoanNumber
                ,SCHBMCUS Community
                ,SCBYR BuyerNumberA
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCSDJ) SalesDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCMAD) MortgageApprovalDate
                ,SCDL01 Description
                ,SCLOANTYP LoanType
                ,SCSMC01 SalesMasterCategoryCode1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCBSWD) BuyerScheduledWalkThruDate
                ,SCY55ESPER Escalation_pct
                ,SCY55ESFRQ EscalationFrequency
                ,SCY55ESCDT EscalationStartDate
                ,CAST(SCURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,SCURAB UserReservedNumber
                ,SCURRF UserReservedReference
                ,SCURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCURDT) UserReservedDate
                ,SCUSER UserId
                ,SCPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCUPMJ) DateUpdated
                ,SCUPMT TimeLastUpdated
                ,SCJOBN WorkStationId
                ,SCY55BPP1 BuilderParticipationPercent1
                ,SCY55BPP2 BuilderParticipationPercent2
                ,SCY55NP1 BuilderParticipationPercNet1
                ,SCY55NP2 BuilderParticipationPercofNet2
                ,SCY55NP3 BuilderParticipationPercofNet3
                ,SCY55PCR01 PHSRCalcRule01
                ,SCY55PCR02 PHSRCalcRule02
                ,SCY55PCR03 PHSRCalcRule03
                ,SCY55PCR04 PHSRCalcRule04
                ,SCY55PCR05 PHSRCalcRule05
                ,SCY55PCR06 PHSRCalcRule06
                ,SCY55PCR07 PHSRCalcRule07
                ,SCY55PCR08 PHSRCalcRule08
                ,SCY55PCR09 PHSRCalcRule09
                ,SCY55PCR10 PHSRCalcRule10
                ,SCY55ADP AdvertisingFeePercent
                ,SCY55ADCP AdvertisingFeeMultiplier_pct
                ,SCY55COP CoOpFeePercent
                ,SCY55COCP CoOpFeeMultiplier_pct
                ,SCY55BCP BrokerFeeCapPercent
                ,SCY55BECP BuilderExpenseCapPercent
                ,SCY55CCCP ClosingCostFeeCapPercent
                ,SCY55SID SpecialIncentivesDiscounts
                ,SCY55SIDR SpecialIDRemark
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCDATF01) DateFuture01
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCDATF02) DateFuture02
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCDATF03) DateFuture03
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCDATF04) DateFuture04
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SCDATF05) DateFuture05
                ,SCFLD6 FutureUse6
                ,SCFLD7 FutureUse7
                ,SCFLD8 FutureUse8
                ,SCFLD9 FutureUse9
                ,SCFLD0 FutureUse0
                ,SCFUAN1 FutureAddressNumber1
                ,SCFUAN2 FutureAddressNumber2
                ,SCFUAN3 FutureAddressNumber3
                ,SCFUAN4 FutureAddressNumber4
                ,SCFUAN5 FutureAddressNumber5
                ,SCY55MPH01 MPCHomePrice01
                ,SCY55MPH02 MPCHomePrice02
                ,SCY55MPH03 MPCHomePrice03
                ,SCY55MPH04 MPCHomePrice04
                ,SCY55MPH05 MPCHomePrice05
                ,SCY55MPFA1 MPCFutureAmount01
                ,SCY55MPFA2 MPCFutureAmount02
                ,SCY55MPFA3 MPCFutureAmount03
                ,SCY55MPFA4 MPCFutureAmount04
                ,SCY55MPFA5 MPCFutureAmount05
                ,SCY55MPFA6 MPCFutureAmount06
                ,SCY55MPFA7 MPCFutureAmount07
                ,SCY55MPFA8 MPCFutureAmount08
                ,SCY55MPFA9 MPCFutureAmount09
                ,SCY55MPA10 MPCFutureAmount10
                ,SCY55MPFP1 MPCFuturePercent01
                ,SCY55MPFP2 MPCFuturePercent02
                ,SCY55MPFP3 MPCFuturePercent03
                ,SCY55MPFP4 MPCFuturePercent04
                ,SCY55MPFP5 MPCFuturePercent05
                ,SCY55MPFP6 MPCFuturePercent06
                ,SCY55MPFP7 MPCFuturePercent07
                ,SCY55MPFP8 MPCFuturePercent08
                ,SCY55MPFP9 MPCFuturePercent09
                ,SCY55MPP10 MPCFuturePercent10
                ,SCY55MPEX1 MPCFutureExplanation1
                ,SCY55MPEX2 MPCFutureExplanation2
                ,SCY55MPEX3 MPCFutureExplanation3
                ,SCY55MPEX4 MPCFutureExplanation4
                ,SCY55MPEX5 MPCFutureExplanation5
                ,SCY55MPFD6 MPCFutureCode6
                ,SCY55MPFD7 MPCFutureCode7
                ,SCY55MPFD8 MPCFutureCode8
                ,SCY55MPFD9 MPCFutureCode9
                ,SCY55MPD10 MPCFutureCode10
                ,SCY55MPTD1 MPCTakedownReference1
                ,SCY55MPTD2 MPCTakedownReference2
                ,SCY55MPALT MPCNumberOfLots
                ,SCY55ESCFR EscalationFrequency2
                
FROM datalake_dev.pre_governed.jde_f5544h03_1985 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
