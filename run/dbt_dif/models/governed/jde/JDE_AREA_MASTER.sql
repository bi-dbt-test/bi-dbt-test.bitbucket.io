
  create or replace  view datalake_dev.governed.JDE_AREA_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1514_3587)
SELECT NWMCU BusinessUnit
                ,NWFLOR PropmanFloor
                ,NWUNIT Unit
                ,NWARTY AreaType
                ,NWARGC NWARGC
                ,NWARGV NWARGV
                ,NWDL01 Description
                ,NWARLL AreaLevel
                ,NWSEQ NWSEQ
                ,CAST(NWPMU1/100 AS DECIMAL(18,2)) PropmanUnits001
                ,NWUM UnitOfMeasure
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWEFTE) DateEndingEffective
                ,NWURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWURDT) UserReservedDate
                ,CAST(NWURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NWURAB UserReservedNumber
                ,NWURRF UserReservedReference
                ,NWUSER UserId
                ,NWPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWUPMJ) DateUpdated
                ,NWUPMT TimeLastUpdated
                ,NWJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWENTJ) NWENTJ
                ,NWTORG TransactionOriginator
                
FROM datalake_dev.pre_governed.jde_f1514_3587 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
