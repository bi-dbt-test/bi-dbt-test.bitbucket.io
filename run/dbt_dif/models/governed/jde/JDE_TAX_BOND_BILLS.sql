
  create or replace  view datalake_dev.governed.JDE_TAX_BOND_BILLS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5544h10_1942)
SELECT MBICU BatchNumber
                ,MBICUT BatchType
                ,MBMCU BusinessUnit
                ,MBHBLOT HomeBuilderLotNumber
                ,CAST(MBLNID/100 AS DECIMAL(18,2)) LineNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBDIVJ) DateInvoiceJulian
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBDDJ) DateNetDue
                ,MBY55MPTDS MBY55MPTDS
                ,MBTA1 MBTA1
                ,MBY55MPTBR MBY55MPTBR
                ,MBY55MPTXY MBY55MPTXY
                ,MBY55MPPAY MBY55MPPAY
                ,MBY55MPB10 MBY55MPB10
                ,MBY55MPTB5 MBY55MPTB5
                ,MBY55MPTB6 MBY55MPTB6
                ,MBY55MPTBE MBY55MPTBE
                ,MBURAB UserReservedNumber
                ,CAST(MBURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBURDT) UserReservedDate
                ,MBURCD UserReservedCode
                ,MBURRF UserReservedReference
                ,MBUSER UserId
                ,MBPID ProgramId
                ,MBJOBN WorkStationId
                ,MBUPMT TimeLastUpdated
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBUPMJ) DateUpdated
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBDGJ) DateForGLandVoucher
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBFDADT1) MBFDADT1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBFDADT2) MBFDADT2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBFDADT3) MBFDADT3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBFUTDATE1) MBFUTDATE1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBFUTDATE2) MBFUTDATE2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MBFUTDATE3) MBFUTDATE3
                ,MBCFA1 MBCFA1
                ,MBCFA2 MBCFA2
                ,MBCFA3 MBCFA3
                ,MBCFA4 MBCFA4
                ,MBACTFU3 MBACTFU3
                ,MBCM01 MBCM01
                ,MBCM02 MBCM02
                ,MBCM03 MBCM03
                ,MBCM04 MBCM04
                ,CAST(MBAFAT/100 AS DECIMAL(18,2)) MBAFAT
                ,CAST(MBCFL/100 AS DECIMAL(18,2)) MBCFL
                ,CAST(MBFUAM/100 AS DECIMAL(18,2)) MBFUAM
                ,CAST(MBH76FUA/100 AS DECIMAL(18,2)) MBH76FUA
                ,MBEV01 MBEV01
                ,MBEV02 MBEV02
                ,MBEV03 MBEV03
                ,MBEV04 MBEV04
                ,MBEV05 MBEV05
                ,MBEDBT MBEDBT
                ,MBCNTF MBCNTF
                ,MBHBMCUS Community
                
FROM datalake_dev.pre_governed.jde_f5544h10_1942 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
