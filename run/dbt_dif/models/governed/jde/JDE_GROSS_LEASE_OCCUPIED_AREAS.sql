
  create or replace  view datalake_dev.governed.JDE_GROSS_LEASE_OCCUPIED_AREAS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f15141_1977)
SELECT NCMCUS CostCenterSubsequent
                ,NCMCU BusinessUnit
                ,NCFLOR PropmanFloor
                ,NCUNIT Unit
                ,NCCENTYR CenturyYear
                ,NCMT CalendarMonth
                ,CAST(NCGLOA/100 AS DECIMAL(18,2)) GrossLeaseOccupiedArea
                ,NCGOCF NCGOCF
                ,CAST(NCGLAA/100 AS DECIMAL(18,2)) GrossLeaseableArea
                ,NCGLCF NCGLCF
                ,NCDYOC NCDYOC
                ,NCUM UnitOfMeasure
                ,NCARTY AreaType
                ,NCAM01 NCAM01
                ,NCGLMD NCGLMD
                ,NCDOCO DocumentOrderInvoice
                ,NCURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NCURDT) UserReservedDate
                ,CAST(NCURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NCURAB UserReservedNumber
                ,NCURRF UserReservedReference
                ,NCUSER UserId
                ,NCPID ProgramId
                ,NCJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NCUPMJ) DateUpdated
                ,NCUPMT TimeLastUpdated
                ,NCARDEFID AreaDefinitionID
                ,CAST(NCGLOAU/100 AS DECIMAL(18,2)) NCGLOAU
                ,CAST(NCGLAAU/100 AS DECIMAL(18,2)) NCGLAAU
                
FROM datalake_dev.pre_governed.jde_f15141_1977 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
