
  create or replace  view datalake_dev.governed.JDE_CLOSING_WORKSHEET_ENTRIES_ALL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h591_2482)
SELECT CWHBMCUS Community
                ,CWHBLOT Lot_Number
                ,CWCLSRT Closing_Worksheet_Line_Item
                ,CWOPTION Option_Number
                ,CWEXR Name_Remark_Explanation
                ,CWCLDESC Closing_Line_Item_Description
                ,CWCLMCD Closing_Business_Unit_Value
                ,CWMCU Business_Unit
                ,CWOBJ Object_Account
                ,CWSUB Subsidiary
                ,CWLT Ledger_Types
                ,CWCLSCD Closing_Subledger_Value
                ,CWSBL Subledger_GL
                ,CWSBLT Subledger_Type
                ,CWCLDBCR Closing_Debit_Credit_Code
                ,CAST(CWAA/100 AS DECIMAL(18,2)) Amount
                ,CWDMF Data_Map_Flag
                ,CWRECERR Record_Error_Code
                ,CAST(CWU/100 AS DECIMAL(18,2)) Units
                ,CWCRTU Created_by_user
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CWCRTJ) Date_Created
                ,CWCRTT Time_Created
                ,CWWRKSTNID Original_Work_Station_ID
                ,CWHBOPID Original_Program_ID
                ,CWUPMB Updated_By_User
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CWUPMJ) Date_Updated
                ,CWUPMT Time_Last_Updated
                ,CWJOBN Work_Station_ID
                ,CWPID Program_ID
                
FROM datalake_dev.pre_governed.jde_f44h591_2482 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
