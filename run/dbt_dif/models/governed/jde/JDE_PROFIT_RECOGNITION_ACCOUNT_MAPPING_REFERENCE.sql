
  create or replace  view datalake_dev.governed.JDE_PROFIT_RECOGNITION_ACCOUNT_MAPPING_REFERENCE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5551004_1952)
SELECT PRSUB Subsidiary
                ,PROBJ ObjectAccount
                ,PRDL01 Description
                ,PRSUB2 PRSUB2
                ,PROBJ2 PROBJ2
                ,PRDL02 Description02
                ,PRSUB5 PRSUB5
                ,PROBJ5 PROBJ5
                ,PRDL03 PRDL03
                ,PRSUB6 PRSUB6
                ,PROBJ6 PROBJ6
                ,PRDL04 PRDL04
                ,PRUSER UserId
                ,PRPID ProgramId
                ,PRJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRUPMJ) DateUpdated
                ,PRUPMT TimeLastUpdated
                ,PRURCD UserReservedCode
                ,CAST(PRURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,PRURAB UserReservedNumber
                ,PRURRF UserReservedReference
                
FROM datalake_dev.pre_governed.jde_f5551004_1952 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
