
  create or replace  view datalake_dev.governed.JDE_ELECTRONIC_ADDRESS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f01151_1975)
SELECT EAAN8 AddressNumber
                ,EAIDLN LineNumberID
                ,EARCK7 SequenceNumber70
                ,EAETP ElectronicAddressType
                ,EAEMAL EmailAddressow
                ,EAUSER UserId
                ,EAPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(EAUPMJ) DateUpdated
                ,EAJOBN WorkStationId
                ,EAUPMT TimeLastUpdated
                ,EAEHIER EmailHierarchy
                ,EAEFOR EmailFormat
                ,EAECLASS EmailClassification
                ,EACFNO1 GenericNumber1
                ,EAGEN1 GeneralInformation1
                ,EAFALGE FutureFlagUse
                ,EASYNCS SynchronizationStatus
                ,EACAAD ServerStatus
                
FROM datalake_dev.pre_governed.jde_f01151_1975 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
