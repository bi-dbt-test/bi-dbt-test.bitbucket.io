
  create or replace  view datalake_dev.governed.JDE_ADDITIONAL_LOT_INFO  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5544h13_2481)
SELECT SGHBMCUS Community
                ,SGHBLOT HomeBuilderLotNumber
                ,SGCSSEQ CurrentSalesSequence
                ,SGDL011 Description011
                ,SGDSE9 DescriptionE9
                ,SGALPH1 NameAlphContact1
                ,SGALPH2 NameAlphContact2
                ,SGALPH3 NameAlphContact3
                ,SGALPH4 NameAlphContact4
                ,SGALPH5 NameAlphContact5
                ,SGEV01 EverestEventPoint01
                ,SGEV02 EverestEventPoint02
                ,SGEV03 EverestEventPoint03
                ,SGALPHDES AlphaDescription
                ,SGALPH6 NameAlphaContact9
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SGDATF10) DateFuture10
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SGDATF09) DateFuture09
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SGDATF08) DateFuture08
                ,SGURCD UserReservedCode
                ,SGURAB UserReservedNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SGURDT) UserReservedDate
                ,SGURRF UserReservedReference
                ,CAST(SGURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,SGPID Program_Id
                ,SGJOBN Work_Stn_ID
                ,SGUPMT Time_Updated
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SGUPMJ) Date_Updated
                ,SGUSER User_ID
                
FROM datalake_dev.pre_governed.jde_f5544h13_2481 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
