
  create or replace  view datalake_dev.governed.JDE_BUSINESS_UNIT_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0006_1947)
SELECT MCMCU BusinessUnit
                ,MCSTYL CostCenterType
                ,MCDC DescripCompressed
                ,MCLDM LevelOfDetailCcCode
                ,MCCO Company
                ,MCAN8 AddressNumber
                ,MCAN8O MCAN8O
                ,MCCNTY MCCNTY
                ,MCADDS MCADDS
                ,MCFMOD MCFMOD
                ,MCDL01 Description
                ,MCDL02 Description02
                ,MCDL03 MCDL03
                ,MCDL04 MCDL04
                ,MCRP01 CategoryCodeCostCt001
                ,MCRP02 CategoryCodeCostCt002
                ,MCRP03 CategoryCodeCostCt003
                ,MCRP04 CategoryCodeCostCt004
                ,MCRP05 CategoryCodeCostCt005
                ,MCRP06 MCRP06
                ,MCRP07 CategoryCodeCostCt007
                ,MCRP08 MCRP08
                ,MCRP09 CategoryCodeCostCt009
                ,MCRP10 CategoryCodeCostCt010
                ,MCRP11 CategoryCodeCostCt011
                ,MCRP12 CategoryCodeCostCt012
                ,MCRP13 CategoryCodeCostCt013
                ,MCRP14 CategoryCodeCostCt014
                ,MCRP15 CategoryCodeCostCt015
                ,MCRP16 MCRP16
                ,MCRP17 MCRP17
                ,MCRP18 CategoryCodeCostCt018
                ,MCRP19 CategoryCodeCostCt019
                ,MCRP20 CategoryCodeCostCt020
                ,MCRP21 CategoryCodeCostCt021
                ,MCRP22 CategoryCodeCostCt022
                ,MCRP23 CategoryCodeCostCt023
                ,MCRP24 CategoryCodeCostCenter24
                ,MCRP25 MCRP25
                ,MCRP26 CategoryCodeCostCenter26
                ,MCRP27 CategoryCodeCostCenter27
                ,MCRP28 CategoryCodeCostCenter28
                ,MCRP29 MCRP29
                ,MCRP30 MCRP30
                ,MCTA MCTA
                ,MCTXJS MCTXJS
                ,MCTXA1 TaxArea1
                ,MCEXR1 TaxExplanationCode1
                ,MCTC01 MCTC01
                ,MCTC02 MCTC02
                ,MCTC03 MCTC03
                ,MCTC04 MCTC04
                ,MCTC05 MCTC05
                ,MCTC06 MCTC06
                ,MCTC07 MCTC07
                ,MCTC08 MCTC08
                ,MCTC09 MCTC09
                ,MCTC10 MCTC10
                ,MCND01 MCND01
                ,MCND02 MCND02
                ,MCND03 MCND03
                ,MCND04 MCND04
                ,MCND05 MCND05
                ,MCND06 MCND06
                ,MCND07 MCND07
                ,MCND08 MCND08
                ,MCND09 MCND09
                ,MCND10 MCND10
                ,MCCC01 MCCC01
                ,MCCC02 MCCC02
                ,MCCC03 MCCC03
                ,MCCC04 MCCC04
                ,MCCC05 MCCC05
                ,MCCC06 MCCC06
                ,MCCC07 MCCC07
                ,MCCC08 MCCC08
                ,MCCC09 MCCC09
                ,MCCC10 MCCC10
                ,MCPECC PostingEditCostCenter
                ,MCALS MCALS
                ,MCISS MCISS
                ,MCGLBA GlBankAccount
                ,MCALCL MCALCL
                ,MCLMTH MCLMTH
                ,CAST(MCLF/100 AS DECIMAL(18,2)) MCLF
                ,MCOBJ1 MCOBJ1
                ,MCOBJ2 MCOBJ2
                ,MCOBJ3 MCOBJ3
                ,MCSUB1 MCSUB1
                ,CAST(MCTOU/100 AS DECIMAL(18,2)) MCTOU
                ,MCSBLI MCSBLI
                ,MCANPA MCANPA
                ,MCCT MCCT
                ,MCCERT MCCERT
                ,MCMCUS CostCenterSubsequent
                ,MCBTYP MCBTYP
                ,MCPC MCPC
                ,CAST(MCPCA/100 AS DECIMAL(18,2)) MCPCA
                ,CAST(MCPCC/100 AS DECIMAL(18,2)) MCPCC
                ,MCINTA MCINTA
                ,MCINTL MCINTL
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCD1J) MCD1J
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCD2J) MCD2J
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCD3J) MCD3J
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCD4J) MCD4J
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCD5J) MCD5J
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCD6J) MCD6J
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCFPDJ) MCFPDJ
                ,CAST(MCCAC/100 AS DECIMAL(18,2)) MCCAC
                ,CAST(MCPAC/100 AS DECIMAL(18,2)) MCPAC
                ,MCEEO MCEEO
                ,MCERC MCERC
                ,MCUSER UserId
                ,MCPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCUPMJ) DateUpdated
                ,MCJOBN WorkStationId
                ,MCUPMT TimeLastUpdated
                ,MCBPTP MCBPTP
                ,MCAPSB MCAPSB
                ,MCTSBU MCTSBU
                ,MCRP31 MCRP31
                ,MCRP32 MCRP32
                ,MCRP33 MCRP33
                ,MCRP34 MCRP34
                ,MCRP35 MCRP35
                ,MCRP36 MCRP36
                ,MCRP37 MCRP37
                ,MCRP38 MCRP38
                ,MCRP39 MCRP39
                ,MCRP40 MCRP40
                ,MCRP41 MCRP41
                ,MCRP42 MCRP42
                ,MCRP43 MCRP43
                ,MCRP44 MCRP44
                ,MCRP45 MCRP45
                ,MCRP46 MCRP46
                ,MCRP47 MCRP47
                ,MCRP48 MCRP48
                ,MCRP49 MCRP49
                ,MCRP50 MCRP50
                ,MCAN8GCA1 MCAN8GCA1
                ,MCAN8GCA2 MCAN8GCA2
                ,MCAN8GCA3 MCAN8GCA3
                ,MCAN8GCA4 MCAN8GCA4
                ,MCAN8GCA5 MCAN8GCA5
                ,MCRMCU1 MCRMCU1
                ,MCDOCO DocumentOrderInvoice
                ,MCPCTN MCPCTN
                ,MCCLNU MCCLNU
                ,MCBUCA MCBUCA
                ,MCADJENT MCADJENT
                ,MCUAFL MCUAFL
                
FROM datalake_dev.pre_governed.jde_f0006_1947 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
