
  create or replace  view datalake_dev.governed.JDE_LEASE_MASTER_DETAIL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f15017_2972)
SELECT NWDOCO DocumentOrderInvoice
                ,NWLSVR LeaseVersion
                ,NWMCU BusinessUnit
                ,NWUNIT Unit
                ,NWAN8A NWAN8A
                ,NWAN8S NWAN8S
                ,NWMCUS CostCenterSubsequent
                ,NWSIC2 NWSIC2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWMIDT) DateMoveIn
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWSPAD) DteSpaceAvailableDte
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWMODT) DateMoveOut
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWSBDT) SubleaseDate
                ,CAST(NWRNTA/100 AS DECIMAL(18,2)) UnitRentableAreaSqFt
                ,CAST(NWUSEA/100 AS DECIMAL(18,2)) UnitsUsableAreaSqFt
                ,NWRM01 NWRM01
                ,NWRM02 NWRM02
                ,NWRM03 NWRM03
                ,NWRM04 NWRM04
                ,NWRM05 NWRM05
                ,NWRM06 NWRM06
                ,NWRM07 NWRM07
                ,NWRM08 NWRM08
                ,NWRM09 NWRM09
                ,NWRM10 NWRM10
                ,NWUNGP NWUNGP
                ,NWUTTY PropmanUnitType
                ,NWUTUS NWUTUS
                ,CAST(NWWTDL/100 AS DECIMAL(18,2)) LeaseRenewalTerm
                ,NWCO Company
                ,NWSMCU NWSMCU
                ,NWDBUI NWDBUI
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWSPND) NWSPND
                ,NWDBAN NWDBAN
                ,CAST(NWARA1/100 AS DECIMAL(18,2)) NWARA1
                ,NWAAT1 NWAAT1
                ,CAST(NWARA2/100 AS DECIMAL(18,2)) NWARA2
                ,NWAAT2 NWAAT2
                ,CAST(NWARA3/100 AS DECIMAL(18,2)) NWARA3
                ,NWAAT3 NWAAT3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWCMTB) DateBeginningCommitment
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWCMTE) NWCMTE
                ,NWDEAL DealNumber
                ,NWURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWURDT) UserReservedDate
                ,CAST(NWURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NWURAB UserReservedNumber
                ,NWURRF UserReservedReference
                ,NWUSER UserId
                ,NWPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWUPMJ) DateUpdated
                ,NWUPMT TimeLastUpdated
                ,NWJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NWENTJ) NWENTJ
                ,NWTORG TransactionOriginator
                ,NWCRR NWCRR
                ,NWCRCD CurrencyCodeFrom
                ,NWCRRM NWCRRM
                ,NWLELC NWLELC
                ,NWLERCEO NWLERCEO
                ,NWLESPA NWLESPA
                ,NWLETO NWLETO
                ,NWLEFVBD NWLEFVBD
                ,NWLEFVED NWLEFVED
                ,NWLEPGRV NWLEPGRV
                ,NWLEFGRV NWLEFGRV
                ,NWLEGRP NWLEGRP
                ,NWLEPOA NWLEPOA
                ,NWLEOP NWLEOP
                ,NWLERLL NWLERLL
                ,NWLEDC11 NWLEDC11
                ,NWLEDC12 NWLEDC12
                ,NWLEDC13 NWLEDC13
                ,NWLEDC14 NWLEDC14
                ,NWLEDC15 NWLEDC15
                ,NWICU NWICU
                ,NWLELLS NWLELLS
                ,NWLELCD NWLELCD
                ,NWLESIR NWLESIR
                ,NWLEJTD NWLEJTD
                ,NWLZNPA NWLZNPA
                ,NWNUMB NWNUMB
                ,NWLEBR NWLEBR
                ,NWLZRED NWLZRED
                
FROM datalake_dev.pre_governed.jde_f15017_2972 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
