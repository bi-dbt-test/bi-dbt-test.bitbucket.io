
  create or replace  view datalake_dev.governed.JDE_OPTION_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h401_1934)
SELECT OCHBAREA HomeBuilderArea
                ,OCHBMCUS Community
                ,OCCPHASE Phase
                ,OCHBPLAN Plan
                ,OCHBELEV Elevation
                ,OCOPTION OCOPTION
                ,OCDL01 Description
                ,OCDL02 Description02
                ,OCDL03 OCDL03
                ,OCDL04 OCDL04
                ,OCDL05 OCDL05
                ,OCDL06 OCDL06
                ,OCOPTYP OCOPTYP
                ,OCOPKG OCOPKG
                ,OCOPKGC OCOPKGC
                ,OCOCSTM OCOCSTM
                ,OCHBLOT HomeBuilderLotNumber
                ,CAST(OCDPTP/100 AS DECIMAL(18,2)) OCDPTP
                ,CAST(OCCSPR/100 AS DECIMAL(18,2)) OCCSPR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCSPJ) SalesPriceEffectiveDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCEXDE) OCEXDE
                ,CAST(OCHGPP/100 AS DECIMAL(18,2)) OCHGPP
                ,CAST(OCPSPR/100 AS DECIMAL(18,2)) OCPSPR
                ,CAST(OCAMTO/100 AS DECIMAL(18,2)) OCAMTO
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCSTRT) OCSTRT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCINACDTJ) OCINACDTJ
                ,OCPLNQ OCPLNQ
                ,OCOPM01 OCOPM01
                ,OCOPM02 OCOPM02
                ,OCOPM03 OCOPM03
                ,OCOPM04 OCOPM04
                ,OCOPM05 OCOPM05
                ,OCOPM06 OCOPM06
                ,OCOPM07 OCOPM07
                ,OCOPM08 OCOPM08
                ,OCOPM09 OCOPM09
                ,OCOPM10 OCOPM10
                ,OCOPM11 OCOPM11
                ,OCOPM12 OCOPM12
                ,OCOPM13 OCOPM13
                ,OCOPM14 OCOPM14
                ,OCOPM15 OCOPM15
                ,OCOPM16 OCOPM16
                ,OCOPM17 OCOPM17
                ,OCOPM18 OCOPM18
                ,OCOPM19 OCOPM19
                ,OCOPM20 OCOPM20
                ,OCOPSTAV OCOPSTAV
                ,OCOPSTCO OCOPSTCO
                ,OCOPLVL OCOPLVL
                ,OCHBST1 OCHBST1
                ,OCHBST2 OCHBST2
                ,OCHBST3 OCHBST3
                ,OCHBST4 OCHBST4
                ,OCHBST5 OCHBST5
                ,OCUC01 OCUC01
                ,OCUC02 OCUC02
                ,OCUC03 OCUC03
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCUSD1) OCUSD1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCUSD2) OCUSD2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCUSD3) OCUSD3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCUSD4) UserDate4
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCUSD5) OCUSD5
                ,CAST(OCUAMT01/100 AS DECIMAL(18,2)) OCUAMT01
                ,CAST(OCUAMT02/100 AS DECIMAL(18,2)) AmountUserDefinedAmount02
                ,CAST(OCUAMT03/100 AS DECIMAL(18,2)) AmountUserDefinedAmount03
                ,CAST(OCUAMT04/100 AS DECIMAL(18,2)) AmountUserDefinedAmount04
                ,CAST(OCUAMT05/100 AS DECIMAL(18,2)) OCUAMT05
                ,OCF1A OCF1A
                ,OCF2A OCF2A
                ,OCF3A OCF3A
                ,OCF4A OCF4A
                ,OCF5A OCF5A
                ,OCCRTU OCCRTU
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCCRTJ) OCCRTJ
                ,OCCRTT OCCRTT
                ,OCWRKSTNID OCWRKSTNID
                ,OCHBOPID OCHBOPID
                ,OCUPMB OCUPMB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OCUPMJ) DateUpdated
                ,OCUPMT TimeLastUpdated
                ,OCJOBN WorkStationId
                ,OCPID ProgramId
                
FROM datalake_dev.pre_governed.jde_f44h401_1934 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
