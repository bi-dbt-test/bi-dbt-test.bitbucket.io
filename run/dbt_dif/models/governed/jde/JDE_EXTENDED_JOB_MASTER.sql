
  create or replace  view datalake_dev.governed.JDE_EXTENDED_JOB_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5108_1960)
SELECT MDMCU BusinessUnit
                ,MDBSCT MDBSCT
                ,MDBSFY MDBSFY
                ,MDBTCT MDBTCT
                ,MDBTFY MDBTFY
                ,MDRCS1 MDRCS1
                ,MDRCS2 MDRCS2
                ,MDRCS3 MDRCS3
                ,MDJCF6 MDJCF6
                ,MDJCF7 MDJCF7
                ,MDJCF8 MDJCF8
                ,MDJCF9 MDJCF9
                ,MDJCF0 MDJCF0
                
FROM datalake_dev.pre_governed.jde_f5108_1960 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
