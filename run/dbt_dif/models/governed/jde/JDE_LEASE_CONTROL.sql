
  create or replace  view datalake_dev.governed.JDE_LEASE_CONTROL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1513b_2970)
SELECT O3DOCO DocumentOrderInvoice
                ,O3MCU BusinessUnit
                ,O3UNIT Unit
                ,O3CTRY Century
                ,O3YR CalendarYear
                ,O3ICU BatchNumber
                ,O3PSTF O3PSTF
                ,O3GENC O3GENC
                ,O3AN8 AddressNumber
                ,O3AN8J AddNoAlternatePayee
                ,SEMANTIC.JDE_JULIAN_TO_DATE(O3EFTB) DateBeginningEffective
                ,O3PMTC TermOfLease
                ,CAST(O3AB01/100 AS DECIMAL(18,2)) O3AB01
                ,CAST(O3AB02/100 AS DECIMAL(18,2)) O3AB02
                ,CAST(O3AB03/100 AS DECIMAL(18,2)) O3AB03
                ,CAST(O3AB04/100 AS DECIMAL(18,2)) O3AB04
                ,CAST(O3AB05/100 AS DECIMAL(18,2)) O3AB05
                ,CAST(O3AB06/100 AS DECIMAL(18,2)) O3AB06
                ,CAST(O3AB07/100 AS DECIMAL(18,2)) O3AB07
                ,CAST(O3AB08/100 AS DECIMAL(18,2)) O3AB08
                ,CAST(O3AB09/100 AS DECIMAL(18,2)) O3AB09
                ,CAST(O3AB10/100 AS DECIMAL(18,2)) O3AB10
                ,CAST(O3AB11/100 AS DECIMAL(18,2)) O3AB11
                ,CAST(O3AB12/100 AS DECIMAL(18,2)) O3AB12
                ,CAST(O3AB13/100 AS DECIMAL(18,2)) O3AB13
                ,CAST(O3BS01/100 AS DECIMAL(18,2)) O3BS01
                ,CAST(O3BS02/100 AS DECIMAL(18,2)) O3BS02
                ,CAST(O3BS03/100 AS DECIMAL(18,2)) O3BS03
                ,CAST(O3BS04/100 AS DECIMAL(18,2)) O3BS04
                ,CAST(O3BS05/100 AS DECIMAL(18,2)) O3BS05
                ,CAST(O3BS06/100 AS DECIMAL(18,2)) O3BS06
                ,CAST(O3BS07/100 AS DECIMAL(18,2)) O3BS07
                ,CAST(O3BS08/100 AS DECIMAL(18,2)) O3BS08
                ,CAST(O3BS09/100 AS DECIMAL(18,2)) O3BS09
                ,CAST(O3BS10/100 AS DECIMAL(18,2)) O3BS10
                ,CAST(O3BS11/100 AS DECIMAL(18,2)) O3BS11
                ,CAST(O3BS12/100 AS DECIMAL(18,2)) O3BS12
                ,CAST(O3BS13/100 AS DECIMAL(18,2)) O3BS13
                ,CAST(O3BALF/100 AS DECIMAL(18,2)) O3BALF
                ,CAST(O3CACH/100 AS DECIMAL(18,2)) O3CACH
                ,SEMANTIC.JDE_JULIAN_TO_DATE(O3CAHD) O3CAHD
                ,CAST(O3BA01/100 AS DECIMAL(18,2)) O3BA01
                ,CAST(O3BA02/100 AS DECIMAL(18,2)) O3BA02
                ,CAST(O3BA03/100 AS DECIMAL(18,2)) O3BA03
                ,CAST(O3BA04/100 AS DECIMAL(18,2)) O3BA04
                ,CAST(O3BA05/100 AS DECIMAL(18,2)) O3BA05
                ,CAST(O3BA06/100 AS DECIMAL(18,2)) O3BA06
                ,CAST(O3BA07/100 AS DECIMAL(18,2)) O3BA07
                ,CAST(O3BA08/100 AS DECIMAL(18,2)) O3BA08
                ,CAST(O3BA09/100 AS DECIMAL(18,2)) O3BA09
                ,CAST(O3BA10/100 AS DECIMAL(18,2)) O3BA10
                ,CAST(O3BA11/100 AS DECIMAL(18,2)) O3BA11
                ,CAST(O3BA12/100 AS DECIMAL(18,2)) O3BA12
                ,CAST(O3BA13/100 AS DECIMAL(18,2)) O3BA13
                ,O3URCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(O3URDT) UserReservedDate
                ,CAST(O3URAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,O3URAB UserReservedNumber
                ,O3URRF UserReservedReference
                ,O3USER UserId
                ,O3PID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(O3UPMJ) DateUpdated
                ,O3UPMT TimeLastUpdated
                ,O3JOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(O3ENTJ) O3ENTJ
                ,O3TORG TransactionOriginator
                ,O3CRCD CurrencyCodeFrom
                ,O3CRRM O3CRRM
                ,O3CRR O3CRR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(O3EFTE) O3EFTE
                
FROM datalake_dev.pre_governed.jde_f1513b_2970 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
