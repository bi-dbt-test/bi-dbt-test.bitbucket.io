
  create or replace  view datalake_dev.governed.JDE_MANAGEMENT_FEE_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1505b_1967)
SELECT NDDOCO DocumentOrderInvoice
                ,NDLSVR LeaseVersion
                ,NDMCU BusinessUnit
                ,NDBCI BillingControlId
                ,NDBCI3 BillingControlIDThird
                ,NDGLC BillCode
                ,NDFETY NDFETY
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDEFTE) DateEndingEffective
                ,CAST(NDFERT/100 AS DECIMAL(18,2)) NDFERT
                ,NDFEBS NDFEBS
                ,NDGLC3 NDGLC3
                ,NDCO3 NDCO3
                ,NDAN83 NDAN83
                ,NDPTC3 NDPTC3
                ,NDGLC4 NDGLC4
                ,NDCO4 NDCO4
                ,NDAN84 NDAN84
                ,NDPTC4 NDPTC4
                ,NDMCU2 BusinessUnit2
                ,NDUNIT Unit
                ,NDPINV NDPINV
                ,NDDEAL DealNumber
                ,CAST(NDMIND/100 AS DECIMAL(18,2)) NDMIND
                ,CAST(NDDMAX/100 AS DECIMAL(18,2)) NDDMAX
                ,NDSUSP SuspendCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDSUDT) DateSuspended
                ,NDBLFC BillingFrequencyCode
                ,NDBF01 BillingPeriod01
                ,NDBF02 BillingPeriod02
                ,NDBF03 BillingPeriod03
                ,NDBF04 BillingPeriod04
                ,NDBF05 BillingPeriod05
                ,NDBF06 BillingPeriod06
                ,NDBF07 BillingPeriod07
                ,NDBF08 BillingPeriod08
                ,NDBF09 BillingPeriod09
                ,NDBF10 BillingPeriod10
                ,NDBF11 BillingPeriod11
                ,NDBF12 BillingPeriod12
                ,NDBF13 BillingPeriod13
                ,NDGENM NDGENM
                ,NDAID4 NDAID4
                ,NDSBL4 NDSBL4
                ,NDSBT4 NDSBT4
                ,NDAID3 NDAID3
                ,NDSBL3 NDSBL3
                ,NDSBT3 NDSBT3
                ,NDURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDURDT) UserReservedDate
                ,CAST(NDURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NDURAB UserReservedNumber
                ,NDURRF UserReservedReference
                ,NDUSER UserId
                ,NDPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDUPMJ) DateUpdated
                ,NDUPMT TimeLastUpdated
                ,NDJOBN WorkStationId
                ,NDCRCD CurrencyCodeFrom
                ,NDCRRM NDCRRM
                
FROM datalake_dev.pre_governed.jde_f1505b_1967 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
