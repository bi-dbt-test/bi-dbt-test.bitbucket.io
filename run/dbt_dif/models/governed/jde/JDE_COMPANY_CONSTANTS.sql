
  create or replace  view datalake_dev.governed.JDE_COMPANY_CONSTANTS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0010_1965)
SELECT CCCO Company
                ,CCNAME Name
                ,CCALTC CCALTC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CCDFYJ) Datefiscalyearbeginsj
                ,CCPNC PeriodNumberCurrent
                ,CCDOT1 CCDOT1
                ,CCCRYR CurrencyConversion
                ,CCCRYC CCCRYC
                ,CCDOT2 CCDOT2
                ,CCMCUA CCMCUA
                ,CCMCUD CCMCUD
                ,CCMCUC CCMCUC
                ,CCMCUB CCMCUB
                ,CCDPRC CCDPRC
                ,CCBKTX CCBKTX
                ,CCTXBM CCTXBM
                ,CCTXBO CCTXBO
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CCNWXJ) CCNWXJ
                ,CCGLIE CCGLIE
                ,CCABIN CCABIN
                ,CCCALD CCCALD
                ,CCDTPN CCDTPN
                ,CCPNF CCPNF
                ,CCDFF CCDFF
                ,CCCRCD CurrencyCodeFrom
                ,CCSMI CCSMI
                ,CCSMU CCSMU
                ,CCSMS CCSMS
                ,CCDNLT CCDNLT
                ,CCSTMT CCSTMT
                ,CCATCS CCATCS
                ,CCALGM CCALGM
                ,CCAGEM CCAGEM
                ,CCCRDY CCCRDY
                ,CCAGR1 CCAGR1
                ,CCAGR2 CCAGR2
                ,CCAGR3 CCAGR3
                ,CCAGR4 CCAGR4
                ,CCAGR5 CCAGR5
                ,CCAGR6 CCAGR6
                ,CCAGR7 CCAGR7
                ,CCFRY CCFRY
                ,CCFRP CCFRP
                ,CCNNP CCNNP
                ,CAST(CCFP/100 AS DECIMAL(18,2)) CCFP
                ,CCAGE CCAGE
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CCDAG) CCDAG
                ,CCARPN CCARPN
                ,CCAPPN CCAPPN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CCARFJ) CCARFJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CCAPFJ) CCAPFJ
                ,CCAN8 AddressNumber
                ,CCSGBK CCSGBK
                ,CCPTCO CCPTCO
                ,CCX1 CCX1
                ,CCX2 CCX2
                ,CCUSER UserId
                ,CCPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CCUPMJ) DateUpdated
                ,CCJOBN WorkStationId
                ,CCUPMT TimeLastUpdated
                ,CCTSID CCTSID
                ,CCTSCO CCTSCO
                ,CCTHCO CCTHCO
                ,CCAN8C CCAN8C
                
FROM datalake_dev.pre_governed.jde_f0010_1965 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
