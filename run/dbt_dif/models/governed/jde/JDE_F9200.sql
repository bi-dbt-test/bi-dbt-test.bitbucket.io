
  create or replace  view datalake_dev.governed.JDE_F9200  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f9200_2500)
SELECT FRDTAI FRDTAI
                ,FRSY FRSY
                ,FRSYR FRSYR
                ,FRGG FRGG
                ,FRUSER FRUSER
                ,FRPID FRPID
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FRUPMJ) FRUPMJ
                ,FRJOBN FRJOBN
                ,FRUPMT FRUPMT
                
FROM datalake_dev.pre_governed.jde_f9200_2500 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
