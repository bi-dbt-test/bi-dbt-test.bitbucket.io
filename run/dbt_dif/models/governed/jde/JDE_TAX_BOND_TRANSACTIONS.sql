
  create or replace  view datalake_dev.governed.JDE_TAX_BOND_TRANSACTIONS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5544h02_1936)
SELECT MXHBMCUS Community
                ,MXHBLOT HomeBuilderLotNumber
                ,MXY55MPTDS MXY55MPTDS
                ,MXTA1 MXTA1
                ,MXY55MPTBR MXY55MPTBR
                ,MXY55MPTXY MXY55MPTXY
                ,MXY55MPPAY MXY55MPPAY
                ,MXY55MPASC MXY55MPASC
                ,MXY55MPASY MXY55MPASY
                ,MXY55MPRVY MXY55MPRVY
                ,MXY55MPTB1 MXY55MPTB1
                ,MXY55MPTB2 MXY55MPTB2
                ,MXY55MPTB3 MXY55MPTB3
                ,MXY55MPTB4 MXY55MPTB4
                ,MXY55MPTB5 MXY55MPTB5
                ,MXY55MPTB6 MXY55MPTB6
                ,MXY55MPTB7 MXY55MPTB7
                ,MXY55MPTB8 MXY55MPTB8
                ,MXY55MPTB9 MXY55MPTB9
                ,MXY55MPB10 MXY55MPB10
                ,MXY55MPB11 MXY55MPB11
                ,MXY55MPB12 MXY55MPB12
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXDIVJ) DateInvoiceJulian
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXDDJ) DateNetDue
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXDGJ) DateForGLandVoucher
                ,MXICU BatchNumber
                ,MXICUT BatchType
                ,MXY55MPAED MXY55MPAED
                ,MXKCO CompanyKey
                ,MXDOC DocumentVoucher
                ,MXDCT DocumentType
                ,MXSFX DocumentPayItem
                ,MXURCD UserReservedCode
                ,CAST(MXURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXURDT) UserReservedDate
                ,MXURRF UserReservedReference
                ,MXUSER UserId
                ,MXPID ProgramId
                ,MXJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXUPMJ) DateUpdated
                ,MXUPMT TimeLastUpdated
                ,CAST(MXMATH01/100 AS DECIMAL(18,2)) MXMATH01
                ,CAST(MXMATH02/100 AS DECIMAL(18,2)) MXMATH02
                ,CAST(MXAN01/100 AS DECIMAL(18,2)) AmountNetPosting001
                ,CAST(MXAN02/100 AS DECIMAL(18,2)) AmountNetPosting002
                ,CAST(MXAN03/100 AS DECIMAL(18,2)) AmountNetPosting003
                ,CAST(MXAN04/100 AS DECIMAL(18,2)) AmountNetPosting004
                ,CAST(MXAN05/100 AS DECIMAL(18,2)) AmountNetPosting005
                ,CAST(MXAN06/100 AS DECIMAL(18,2)) AmountNetPosting006
                ,CAST(MXAN07/100 AS DECIMAL(18,2)) AmountNetPosting007
                ,CAST(MXAN08/100 AS DECIMAL(18,2)) AmountNetPosting008
                ,MXDOCO DocumentOrderInvoice
                ,MXDCTO OrderType
                ,MXKCOO MXKCOO
                ,MXSFXO MXSFXO
                ,MXDL01 Description
                ,MXDL02 Description02
                ,MXFUTANON1 MXFUTANON1
                ,MXFUTANON10 MXFUTANON10
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXFUTDATE1) MXFUTDATE1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXFUTDATE2) MXFUTDATE2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MXFUTDATE3) MXFUTDATE3
                ,MXFUTCHAR2 MXFUTCHAR2
                ,MXFUTCHAR3 MXFUTCHAR3
                ,MXEDBT MXEDBT
                ,MXEV02 MXEV02
                ,CAST(MXLNID/100 AS DECIMAL(18,2)) LineNumber
                ,MXCO Company
                ,MXY55MPTBE MXY55MPTBE
                ,MXCPHASE Phase
                
FROM datalake_dev.pre_governed.jde_f5544h02_1936 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
