
  create or replace  view datalake_dev.governed.JDE_PURCHASE_ORDER_DETAIL_TAG_FILE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f4311t_1935)
SELECT PDKCOO PDKCOO
                ,PDDOCO DocumentOrderInvoice
                ,PDDCTO OrderType
                ,PDSFXO PDSFXO
                ,CAST(PDLNID/100 AS DECIMAL(18,2)) LineNumber
                ,CAST(PDRETA/100 AS DECIMAL(18,2)) PDRETA
                ,CAST(PDRETT/100 AS DECIMAL(18,2)) PDRETT
                ,CAST(PDREFA/100 AS DECIMAL(18,2)) PDREFA
                ,CAST(PDREFT/100 AS DECIMAL(18,2)) PDREFT
                ,PDCRCD CurrencyCodeFrom
                ,PDCRDC PDCRDC
                ,PDABT1 PDABT1
                ,PDABR1 PDABR1
                ,PDABT2 PDABT2
                ,PDABR2 PDABR2
                ,PDABT3 PDABT3
                ,PDABR3 PDABR3
                ,PDABT4 PDABT4
                ,PDABR4 PDABR4
                
FROM datalake_dev.pre_governed.jde_f4311t_1935 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
