
  create or replace  view datalake_dev.governed.JDE_SALES_CONTRACT_DETAIL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5544h09_1986)
SELECT SDHBLOT HomeBuilderLotNumber
                ,SDHBMCUS Community
                ,SDCSSEQ CurrentSalesSequence
                ,SDY55HPSC1 MPCPHSRCalculation01
                ,SDY55H01MJ MPCPostHomeCalcDate01
                ,SDY55HCU01 MPCPHSRCalcUser01
                ,SDY55HPCR1 MPCPHSRCalcRule01
                ,SDY55HPSC2 MPCPHSRCalculation02
                ,SDY55H02MJ MPCPostHomeCalcDate02
                ,SDY55HCU02 MPCPHSRCalcUser02
                ,SDY55HPCR2 MPCPHSRCalcRule02
                ,SDY55HPSC3 MPCPHSRCalculation03
                ,SDY55H03MJ MPCPostHomeCalcDate03
                ,SDY55HCU03 MPCPHSRCalcUser03
                ,SDY55HPCR3 MPCPHSRCalcRule03
                ,SDY55HPSC4 MPCPHSRCalculation04
                ,SDY55H04MJ MPCPostHomeCalcDate04
                ,SDY55HCU04 MPCPHSRCalcUser04
                ,SDY55HPCR4 MPCPHSRCalcRule04
                ,SDY55HPSC5 MPCPHSRCalculation05
                ,SDY55H05MJ MPCPostHomeCalcDate05
                ,SDY55HCU05 MPCPHSRCalcUser05
                ,SDY55HPCR5 MPCPHSRCalcRule05
                ,SDY55HPSC6 MPCPHSRCalculation06
                ,SDY55H06MJ MPCPostHomeCalcDate06
                ,SDY55HCU06 MPCPHSRCalcUser06
                ,SDY55HPCR6 MPCPHSRCalcRule06
                ,SDY55HPSC7 MPCPHSRCalculation07
                ,SDY55H07MJ MPCPostHomeCalcDate07
                ,SDY55HCU07 MPCPHSRCalcUser07
                ,SDY55HPCR7 MPCPHSRCalcRule07
                ,SDY55HPSC8 MPCPHSRCalculation08
                ,SDY55H08MJ MPCPostHomeCalcDate08
                ,SDY55HCU08 MPCPHSRCalcUser08
                ,SDY55HPCR8 MPCPHSRCalcRule08
                ,SDY55HPSC9 MPCPHSRCalculation09
                ,SDY55H09MJ MPCPostHomeCalcDate09
                ,SDY55HCU09 MPCPHSRCalcUser09
                ,SDY55HPCR9 MPCPHSRCalcRule09
                ,SDY55HSC10 MPCPHSRCalculation10
                ,SDY55H10MJ MPCPostHomeCalcDate10
                ,SDY55HCU10 MPCPHSRCalcUser10
                ,SDY55HPR10 MPCPHSRCalcRule10
                ,SDUSER UserId
                ,SDPID ProgramId
                ,SDJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SDUPMJ) DateUpdated
                ,SDUPMT TimeLastUpdated
                ,SDURCD UserReservedCode
                ,SDURRF UserReservedReference
                ,SDURAB UserReservedNumber
                ,CAST(SDURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SDURDT) UserReservedDate
                ,SDY55MPFA1 MPCFutureAmount01
                ,SDY55MPFA2 MPCFutureAmount02
                ,SDY55MPFA3 MPCFutureAmount03
                ,SDY55MPFA4 MPCFutureAmount04
                ,SDY55MPFA5 MPCFutureAmount05
                ,SDY55MPFA6 MPCFutureAmount06
                ,SDY55MPFA7 MPCFutureAmount07
                ,SDY55MPFA8 MPCFutureAmount08
                ,SDY55MPFA9 MPCFutureAmount09
                ,SDY55MPA10 MPCFutureAmount10
                ,SDY55MP1MJ MPCFutureDate1
                ,SDY55MP2MJ MPCFutureDate2
                ,SDY55MP3MJ MPCFutureDate3
                ,SDY55MP4MJ MPCFutureDate4
                ,SDY55MP5MJ MPCFutureDate5
                ,SDLOANNUM LoanNumber
                ,SDY55HRA01 MPCPHSRReceivedAmounts01
                ,SDY55HRA02 MPCPHSRReceivedAmounts02
                ,SDY55HRA03 MPCPHSRReceivedAmounts03
                ,SDY55HRA04 MPCPHSRReceivedAmounts04
                ,SDY55HRA05 MPCPHSRReceivedAmounts05
                ,SDY55HRA06 MPCPHSRReceivedAmounts06
                ,SDY55HRA07 MPCPHSRReceivedAmounts07
                ,SDY55HRA08 MPCPHSRReceivedAmounts08
                ,SDY55HRA09 MPCPHSRReceivedAmounts09
                ,SDY55HRA10 MPCPHSRReceivedAmounts10
                ,SDY55HJE01 MPCPHSRJEAmounts01
                ,SDY55HJE02 MPCPHSRJEAmounts02
                ,SDY55HJE03 MPCPHSRJEAmounts03
                ,SDY55HJE04 MPCPHSRJEAmounts04
                ,SDY55HJE05 MPCPHSRJEAmounts05
                ,SDY55HJE06 MPCPHSRJEAmounts06
                ,SDY55HJE07 MPCPHSRJEAmounts07
                ,SDY55HJE08 MPCPHSRJEAmounts08
                ,SDY55HJE09 MPCPHSRJEAmounts09
                ,SDY55HJE10 MPCPHSRJEAmounts10
                ,SDY55BPP1 BuilderParticipationPercent1
                ,SDY55MPFP1 MPCFuturePercent01
                ,SDY55MPFP2 MPCFuturePercent02
                ,SDY55MPFP3 MPCFuturePercent03
                ,SDY55MPFP4 MPCFuturePercent04
                ,SDY55MPFP5 MPCFuturePercent05
                ,SDY55MPFP6 MPCFuturePercent06
                ,SDY55MPFP7 MPCFuturePercent07
                ,SDY55MPFP8 MPCFuturePercent08
                ,SDY55MPFP9 MPCFuturePercent09
                ,SDY55MPP10 MPCFuturePercent10
                ,SDY55BPP2 BuilderParticipationPercent2
                ,SDY55LPPER LotPremiumPercent
                ,SDY55INCRE IncentiveCredit
                ,SDY55ICPER IncentiveCapPercent
                ,SDY55AMCRE AmenityCredit
                ,SDY55BRCRE BrokerCredit
                ,SDY55BCP BrokerFeeCapPercent
                ,SDY55BRFEE BrokerFee
                ,SDY55CCCP ClosingCostFeeCapPercent
                ,SDY55CLCST ClosingCost
                ,SDY55COP CoOpFeePercent
                ,SDY55COCP CoOpFeeMultiplier_pct
                ,SDY55ADP AdvertisingFeePercent
                ,SDY55PKFEE ParkFee
                ,SDY55OTAMT OtherAmountHomeSales
                ,SDY55PPOVE PriceParticipationOverride
                
FROM datalake_dev.pre_governed.jde_f5544h09_1986 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
