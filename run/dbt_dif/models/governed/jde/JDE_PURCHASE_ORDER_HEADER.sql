
  create or replace  view datalake_dev.governed.JDE_PURCHASE_ORDER_HEADER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f4301_1973)
SELECT PHKCOO PHKCOO
                ,PHDOCO DocumentOrderInvoice
                ,PHDCTO OrderType
                ,PHSFXO PHSFXO
                ,PHMCU BusinessUnit
                ,PHOKCO PHOKCO
                ,PHOORN PHOORN
                ,PHOCTO PHOCTO
                ,PHRKCO PHRKCO
                ,PHRORN PHRORN
                ,PHRCTO PHRCTO
                ,PHAN8 AddressNumber
                ,PHSHAN PHSHAN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHDRQJ) PHDRQJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHTRDJ) PHTRDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHPDDJ) PHPDDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHOPDJ) PHOPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHADDJ) PHADDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHCNDJ) PHCNDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHPEFJ) PHPEFJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHPPDJ) PHPPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHPSDJ) PHPSDJ
                ,PHVR01 PHVR01
                ,PHVR02 PHVR02
                ,PHDEL1 DeliveryInstructLine1
                ,PHDEL2 DeliveryInstructLine2
                ,PHRMK NameRemark
                ,PHDESC PHDESC
                ,PHINMG PrintMessage1
                ,PHASN PriceAdjustmentScheduleN
                ,PHPRGP PHPRGP
                ,PHPTC PHPTC
                ,PHEXR1 TaxExplanationCode1
                ,PHTXA1 TaxArea1
                ,PHTXCT PHTXCT
                ,PHHOLD HoldOrdersCode
                ,PHATXT PHATXT
                ,PHINVC InvoiceCopies
                ,PHNTR PHNTR
                ,PHCNID PHCNID
                ,PHFRTH FreightHandlingCode
                ,PHZON ZoneNumber
                ,PHANBY PHANBY
                ,PHANCR CarrierNumber
                ,PHMOT PHMOT
                ,PHCOT PHCOT
                ,PHRCD PHRCD
                ,PHFRTC PHFRTC
                ,PHFUF1 PHFUF1
                ,PHFUF2 PHFUF2
                ,CAST(PHOTOT/100 AS DECIMAL(18,2)) PHOTOT
                ,CAST(PHPCRT/100 AS DECIMAL(18,2)) PHPCRT
                ,PHRTNR PHRTNR
                ,PHWUMD UnitOfMeasureWhtDisp
                ,PHVUMD UnitOfMeasureVolDisp
                ,PHPURG PHPURG
                ,PHLGCT PHLGCT
                ,PHPROM PHPROM
                ,PHMATY PHMATY
                ,PHOSTS PHOSTS
                ,PHAVCH CodeAutomaticVoucher
                ,PHPRPY PHPRPY
                ,PHCRMD CorrespondenceMethod
                ,PHPRP5 PurchasingReportCode5
                ,PHARTG PHARTG
                ,PHCORD PHCORD
                ,PHCRRM PHCRRM
                ,PHCRCD CurrencyCodeFrom
                ,PHCRR PHCRR
                ,PHLNGP PHLNGP
                ,CAST(PHFAP/100 AS DECIMAL(18,2)) PHFAP
                ,PHORBY PHORBY
                ,PHTKBY PHTKBY
                ,PHURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHURDT) UserReservedDate
                ,CAST(PHURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,PHURAB UserReservedNumber
                ,PHURRF UserReservedReference
                ,PHUSER UserId
                ,PHPID ProgramId
                ,PHJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHUPMJ) DateUpdated
                ,PHTDAY PHTDAY
                ,PHRSHT PHRSHT
                ,PHDRQT PHDRQT
                ,PHDOC1 PHDOC1
                ,PHDCT4 PHDCT4
                ,PHBCRC PHBCRC
                ,PHMKFR PHMKFR
                ,PHPOHP01 PHPOHP01
                ,PHPOHP02 PHPOHP02
                ,PHPOHP03 PHPOHP03
                ,PHPOHP04 PHPOHP04
                ,PHPOHP05 PHPOHP05
                ,PHPOHP06 PHPOHP06
                ,PHPOHP07 PHPOHP07
                ,PHPOHP08 PHPOHP08
                ,PHPOHP09 PHPOHP09
                ,PHPOHP10 PHPOHP10
                ,PHPOHP11 PHPOHP11
                ,PHPOHP12 PHPOHP12
                ,PHPOHC01 PHPOHC01
                ,PHPOHC02 PHPOHC02
                ,PHPOHC03 PHPOHC03
                ,PHPOHC04 PHPOHC04
                ,PHPOHC05 PHPOHC05
                ,PHPOHC06 PHPOHC06
                ,PHPOHC07 PHPOHC07
                ,PHPOHC08 PHPOHC08
                ,PHPOHC09 PHPOHC09
                ,PHPOHC10 PHPOHC10
                ,PHPOHC11 PHPOHC11
                ,PHPOHC12 PHPOHC12
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PHPOHD01) PHPOHD01
                ,PHPOHD02 PHPOHD02
                ,PHPOHAB01 PHPOHAB01
                ,PHPOHAB02 PHPOHAB02
                ,PHCUKID PHCUKID
                ,PHPOHP13 PHPOHP13
                ,PHPOHU01 PHPOHU01
                ,PHPOHU02 PHPOHU02
                ,PHRETI PHRETI
                ,PHCLASS01 PHCLASS01
                ,PHCLASS02 PHCLASS02
                ,PHCLASS03 PHCLASS03
                ,PHCLASS04 PHCLASS04
                ,PHCLASS05 PHCLASS05
                
FROM datalake_dev.pre_governed.jde_f4301_1973 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
