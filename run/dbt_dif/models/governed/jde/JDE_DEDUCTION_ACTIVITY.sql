
  create or replace  view datalake_dev.governed.JDE_DEDUCTION_ACTIVITY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f03b41_2989)
SELECT RCDCID RCDCID
                ,CAST(RCLNID/100 AS DECIMAL(18,2)) RCLNID
                ,RCDDSC RCDDSC
                ,CAST(RCDDDA/100 AS DECIMAL(18,2)) RCDDDA
                ,CAST(RCFDA/100 AS DECIMAL(18,2)) RCFDA
                ,CAST(RCTDDA/100 AS DECIMAL(18,2)) RCTDDA
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RCDSPJ) RCDSPJ
                ,RCDPYD RCDPYD
                ,RCDRC5 RCDRC5
                ,RCRC52 RCRC52
                ,RCBCRC RCBCRC
                ,RCCRCD RCCRCD
                ,RCTCRC RCTCRC
                ,RCCO RCCO
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RCDGJ) RCDGJ
                ,RCGLC RCGLC
                ,RCVRE RCVRE
                ,RCNFVD RCNFVD
                ,RCUSER RCUSER
                ,RCPID RCPID
                ,RCJOBN RCJOBN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RCUPMJ) RCUPMJ
                ,RCUPMT RCUPMT
                
FROM datalake_dev.pre_governed.jde_f03b41_2989 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
