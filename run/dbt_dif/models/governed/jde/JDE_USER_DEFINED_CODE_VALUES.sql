
  create or replace  view datalake_dev.governed.JDE_USER_DEFINED_CODE_VALUES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0005_1943)
SELECT DRSY ProductCode
                ,DRRT UserDefinedCodes
                ,DRKY UserDefinedCode
                ,DRDL01 Description
                ,DRDL02 Description02
                ,DRSPHD SpecialHandlingCode
                ,DRUDCO UdcOwnershipflag
                ,DRHRDC HardCodedYN
                ,DRUSER UserId
                ,DRPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(DRUPMJ) DateUpdated
                ,DRJOBN WorkStationId
                ,DRUPMT TimeLastUpdated
                
FROM datalake_dev.pre_governed.jde_f0005_1943 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
