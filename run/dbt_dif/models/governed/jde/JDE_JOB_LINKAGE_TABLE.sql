
  create or replace  view datalake_dev.governed.JDE_JOB_LINKAGE_TABLE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5651001_1961)
SELECT MCMCU BusinessUnit
                ,MCSTYL CostCenterType
                ,MCLDM LevelOfDetailCcCode
                ,MCDL01 Description
                ,MCCO Company
                ,MCMCUS CostCenterSubsequent
                ,MCRP10 CategoryCodeCostCt010
                ,MCPECC PostingEditCostCenter
                ,MCUAFL MCUAFL
                ,MCMCU2 BusinessUnit2
                ,MCALTY MCALTY
                ,MCLBT MCLBT
                ,MCPEC PostingEdit
                ,MCNAME Name
                ,CAST(MCAA/100 AS DECIMAL(18,2)) AmountField
                ,MCUSER UserId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(MCUPMJ) DateUpdated
                ,MCUPMT TimeLastUpdated
                ,MCPID ProgramId
                ,MCJOBN WorkStationId
                ,MCURCD UserReservedCode
                ,CAST(MCURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,MCURAB UserReservedNumber
                ,MCURRF UserReservedReference
                
FROM datalake_dev.pre_governed.jde_f5651001_1961 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
