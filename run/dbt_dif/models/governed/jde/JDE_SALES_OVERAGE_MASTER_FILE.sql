
  create or replace  view datalake_dev.governed.JDE_SALES_OVERAGE_MASTER_FILE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f15013b_1948)
SELECT NODOCO DocumentOrderInvoice
                ,NOSUSP SuspendCode
                ,NOVRSC VersionSuspendCode
                ,NOBCI3 BillingControlIDThird
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NOSUDT) DateSuspended
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NOEFTE) DateEndingEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NOEFTB) DateBeginningEffective
                ,NOLSVR LeaseVersion
                ,NOMCU BusinessUnit
                ,NOUNIT Unit
                ,NOSTNR NOSTNR
                ,NOYEOV NOYEOV
                ,NOBCI BillingControlId
                ,NOAN8J AddNoAlternatePayee
                ,NOBLFC BillingFrequencyCode
                ,NODCTO OrderType
                ,NOAN8 AddressNumber
                ,NOMCUS CostCenterSubsequent
                ,NOBLMR NOBLMR
                ,NOCMPM SalesOverageComputa
                ,NOPROL PercentageRentOnly
                ,NOFYMO FiscalYearMo
                ,NOFYDY FiscalYearDay
                ,NORPFC SaleReprtFrequencyCde
                ,NORFDY NORFDY
                ,NORADY AnnualReportDueDays
                ,NORYEM SalesReportYearEndMo
                ,NOFYR FiscalYearFull
                ,NOBGPN BegPeriodNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NOATDT) DateAudit
                ,NOBLGR BillingGroup
                ,NOULI NOULI
                ,NOULI2 NOULI2
                ,CAST(NOUSEA/100 AS DECIMAL(18,2)) UnitsUsableAreaSqFt
                ,NOTRAR PaymentTermsAR
                ,CAST(NOBKPT/100 AS DECIMAL(18,2)) ProductDollarBreakpt
                ,NOIBOE InvoiceBasedOnEst
                ,NOCENTYR CenturyYear
                ,NOGLC BillCode
                ,NODEAL DealNumber
                ,NOCRCD CurrencyCodeFrom
                ,NOTXA1 TaxArea1
                ,NOEXR1 TaxExplanationCode1
                ,NORM11 ReprtCdePropertyMg11
                ,NORM12 ReprtCdePropertyMg12
                ,NORM21 ReprtCdePropertyMg21
                ,NORM22 ReprtCdePropertyMg22
                ,NORM23 ReprtCdePropertyMg23
                ,NORF01 NORF01
                ,NORF02 NORF02
                ,NORF03 NORF03
                ,NORF04 NORF04
                ,NORF05 NORF05
                ,NORF06 NORF06
                ,NORF07 NORF07
                ,NORF08 NORF08
                ,NORF09 NORF09
                ,NORF10 NORF10
                ,NORF11 NORF11
                ,NORF12 NORF12
                ,NORF13 NORF13
                ,NOBF01 BillingPeriod01
                ,NOBF02 BillingPeriod02
                ,NOBF03 BillingPeriod03
                ,NOBF04 BillingPeriod04
                ,NOBF05 BillingPeriod05
                ,NOBF06 BillingPeriod06
                ,NOBF07 BillingPeriod07
                ,NOBF08 BillingPeriod08
                ,NOBF09 BillingPeriod09
                ,NOBF10 BillingPeriod10
                ,NOBF11 BillingPeriod11
                ,NOBF12 BillingPeriod12
                ,NOBF13 BillingPeriod13
                ,NOURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NOURDT) UserReservedDate
                ,CAST(NOURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NOURAB UserReservedNumber
                ,NOURRF UserReservedReference
                ,NOUSER UserId
                ,NOPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NOUPMJ) DateUpdated
                ,NOUPMT TimeLastUpdated
                ,NOJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NOENTJ) NOENTJ
                ,NOTORG TransactionOriginator
                ,NOCRR NOCRR
                ,NOCRRM NOCRRM
                ,NOLDUE NOLDUE
                
FROM datalake_dev.pre_governed.jde_f15013b_1948 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
