
  create or replace  view datalake_dev.governed.JDE_BILLING_CODE_EXTENSION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1512_2977)
SELECT NAGLC BillCode
                ,NAMCU BusinessUnit
                ,NADL01 Description
                ,NAGRPC NAGRPC
                ,NATNAR NATNAR
                ,NALTFE NALTFE
                ,NAMGFE NAMGFE
                ,NAPRAP NAPRAP
                ,NAAPPC NAAPPC
                ,NAALLW NAALLW
                ,NACONS NACONS
                ,NAAUTS NAAUTS
                ,NASPCD NASPCD
                ,NASECG NASECG
                ,NAGRPU NAGRPU
                ,NABRAJ NABRAJ
                ,NASTAX NASTAX
                ,NAAPLY NAAPLY
                ,NACMNT NACMNT
                ,NAADEP NAADEP
                ,NACLSS NACLSS
                ,NAADJR NAADJR
                ,NAJOBN WorkStationId
                ,NAUSER UserId
                ,NAPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NAUPMJ) DateUpdated
                ,NATBL NATBL
                ,NAMGTF NAMGTF
                ,NAICST NAICST
                ,NARNTC NARNTC
                ,NAPJCC NAPJCC
                ,NAUPMT TimeLastUpdated
                ,NALAFF NALAFF
                ,NALELL NALELL
                ,NALEROUA NALEROUA
                
FROM datalake_dev.pre_governed.jde_f1512_2977 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
