
  create or replace  view datalake_dev.governed.JDE_F9210  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f9210_2501)
SELECT FRDTAI FRDTAI
                ,FRCLAS FRCLAS
                ,FRDTAT FRDTAT
                ,FRDTAS FRDTAS
                ,FRDTAD FRDTAD
                ,FRPDTA FRPDTA
                ,FRARRN FRARRN
                ,FRDVAL FRDVAL
                ,FRLR FRLR
                ,FRCDEC FRCDEC
                ,FRDRUL FRDRUL
                ,FRDRO1 FRDRO1
                ,FRERUL FRERUL
                ,FRERO1 FRERO1
                ,FRERO2 FRERO2
                ,FRHLP1 FRHLP1
                ,FRHLP2 FRHLP2
                ,FRNNIX FRNNIX
                ,FRNSY FRNSY
                ,FRRLS FRRLS
                ,FRUSER FRUSER
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FRUPMJ) FRUPMJ
                ,FRPID FRPID
                ,FRJOBN FRJOBN
                ,FRUPMT FRUPMT
                ,FROWDI FROWDI
                ,FROWTP FROWTP
                ,FRCNTT FRCNTT
                ,FRSCFG FRSCFG
                ,FRUPER FRUPER
                ,FRALBK FRALBK
                ,FROWER FROWER
                ,FROER1 FROER1
                ,FROER2 FROER2
                ,FROWDR FROWDR
                ,FRODR1 FRODR1
                ,FRDBID FRDBID
                ,FRBFDN FRBFDN
                ,FREBID FREBID
                ,FRBFEN FRBFEN
                ,FRSFID FRSFID
                ,FRSFMN FRSFMN
                ,FRBVID FRBVID
                ,FRBVNM FRBVNM
                ,FRPLFG FRPLFG
                ,FRDDID FRDDID
                ,FRAUIN FRAUIN
                
FROM datalake_dev.pre_governed.jde_f9210_2501 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
