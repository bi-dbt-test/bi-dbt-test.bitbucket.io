
  create or replace  view datalake_dev.governed.JDE_BILLING_FREQUENCY_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f15019_1981)
SELECT NLBLFC BillingFrequencyCode
                ,NLDL01 Description
                ,NLBF01 BillingPeriod01
                ,NLBF02 BillingPeriod02
                ,NLBF03 BillingPeriod03
                ,NLBF04 BillingPeriod04
                ,NLBF05 BillingPeriod05
                ,NLBF06 BillingPeriod06
                ,NLBF07 BillingPeriod07
                ,NLBF08 BillingPeriod08
                ,NLBF09 BillingPeriod09
                ,NLBF10 BillingPeriod10
                ,NLBF11 BillingPeriod11
                ,NLBF12 BillingPeriod12
                ,NLBF13 BillingPeriod13
                ,NLBF14 NLBF14
                ,NLDTPN NLDTPN
                ,NLFDYS NLFDYS
                ,NLNBR0 Numberof
                ,NLURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NLURDT) UserReservedDate
                ,CAST(NLURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NLURAB UserReservedNumber
                ,NLURRF UserReservedReference
                ,NLUSER UserId
                ,NLPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NLUPMJ) DateUpdated
                ,NLUPMT TimeLastUpdated
                ,NLJOBN WorkStationId
                
FROM datalake_dev.pre_governed.jde_f15019_1981 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
