
  create or replace  view datalake_dev.governed.JDE_ASSET_ACCOUNT_BALANCES_FILE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1202_1926)
SELECT FLAID AccountID
                ,FLCTRY Century
                ,FLFY FiscalYear
                ,FLFQ FLFQ
                ,FLLT LedgerType
                ,FLSBL Subledger
                ,FLCO Company
                ,CAST(FLAPYC/100 AS DECIMAL(18,2)) AmtBeginningBalancePy
                ,CAST(FLAN01/100 AS DECIMAL(18,2)) AmountNetPosting001
                ,CAST(FLAN02/100 AS DECIMAL(18,2)) AmountNetPosting002
                ,CAST(FLAN03/100 AS DECIMAL(18,2)) AmountNetPosting003
                ,CAST(FLAN04/100 AS DECIMAL(18,2)) AmountNetPosting004
                ,CAST(FLAN05/100 AS DECIMAL(18,2)) AmountNetPosting005
                ,CAST(FLAN06/100 AS DECIMAL(18,2)) AmountNetPosting006
                ,CAST(FLAN07/100 AS DECIMAL(18,2)) AmountNetPosting007
                ,CAST(FLAN08/100 AS DECIMAL(18,2)) AmountNetPosting008
                ,CAST(FLAN09/100 AS DECIMAL(18,2)) AmountNetPosting009
                ,CAST(FLAN10/100 AS DECIMAL(18,2)) AmountNetPosting010
                ,CAST(FLAN11/100 AS DECIMAL(18,2)) AmountNetPosting011
                ,CAST(FLAN12/100 AS DECIMAL(18,2)) AmountNetPosting012
                ,CAST(FLAN13/100 AS DECIMAL(18,2)) FLAN13
                ,CAST(FLAN14/100 AS DECIMAL(18,2)) FLAN14
                ,CAST(FLAPYN/100 AS DECIMAL(18,2)) FLAPYN
                ,CAST(FLAWTD/100 AS DECIMAL(18,2)) AmountWtd
                ,CAST(FLBORG/100 AS DECIMAL(18,2)) AmtOriginalBeginBud
                ,CAST(FLPOU/100 AS DECIMAL(18,2)) FLPOU
                ,FLPC FLPC
                ,CAST(FLTKER/100 AS DECIMAL(18,2)) FLTKER
                ,CAST(FLBREQ/100 AS DECIMAL(18,2)) FLBREQ
                ,CAST(FLBAPR/100 AS DECIMAL(18,2)) FLBAPR
                ,FLMCU BusinessUnit
                ,FLOBJ ObjectAccount
                ,FLSUB Subsidiary
                ,FLNUMB FLNUMB
                ,FLADLM FLADLM
                ,FLADM FLADM
                ,FLITAC FLITAC
                ,FLADMP FLADMP
                ,FLADSN FLADSN
                ,FLDIR1 FLDIR1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FLDSD) FLDSD
                ,FLUSER UserId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FLLCT) FLLCT
                ,FLPID ProgramId
                ,FLSBLT SubledgerType
                ,FLCRCD CurrencyCodeFrom
                ,SEMANTIC.JDE_JULIAN_TO_DATE(FLUPMJ) DateUpdated
                ,FLJOBN WorkStationId
                ,FLUPMT TimeLastUpdated
                ,FLCHCD FLCHCD
                ,FLDPCF FLDPCF
                ,FLCBXR FLCBXR
                
FROM datalake_dev.pre_governed.jde_f1202_1926 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
