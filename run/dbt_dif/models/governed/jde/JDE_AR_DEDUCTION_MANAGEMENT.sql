
  create or replace  view datalake_dev.governed.JDE_AR_DEDUCTION_MANAGEMENT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f03b40_2990)
SELECT RBDCID RBDCID
                ,RBCLTY RBCLTY
                ,RBPSDD RBPSDD
                ,CAST(RBDDOA/100 AS DECIMAL(18,2)) RBDDOA
                ,CAST(RBDDA/100 AS DECIMAL(18,2)) RBDDA
                ,CAST(RBFDOA/100 AS DECIMAL(18,2)) RBFDOA
                ,CAST(RBFDA/100 AS DECIMAL(18,2)) RBFDA
                ,CAST(RBTDOA/100 AS DECIMAL(18,2)) RBTDOA
                ,CAST(RBTDA/100 AS DECIMAL(18,2)) RBTDA
                ,RBDDEX RBDDEX
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RBDDDO) RBDDDO
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RBDDTI) RBDDTI
                ,RBMAN8 RBMAN8
                ,RBODOC RBODOC
                ,RBODCT RBODCT
                ,RBOSFX RBOSFX
                ,RBOKCO RBOKCO
                ,RBGDOC RBGDOC
                ,RBGDCT RBGDCT
                ,RBGSFX RBGSFX
                ,RBGKCO RBGKCO
                ,RBPYID RBPYID
                ,RBRC5 RBRC5
                ,RBVR01 RBVR01
                ,RBCO RBCO
                ,RBTCRC RBTCRC
                ,RBCRCD RBCRCD
                ,RBBCRC RBBCRC
                ,RBCRRM RBCRRM
                ,RBNFVD RBNFVD
                ,RBVRE RBVRE
                ,RBAN8 RBAN8
                ,RBUSER RBUSER
                ,RBPID RBPID
                ,RBJOBN RBJOBN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RBUPMJ) RBUPMJ
                ,RBUPMT RBUPMT
                ,RBSHPN RBSHPN
                ,RBSMIFL RBSMIFL
                
FROM datalake_dev.pre_governed.jde_f03b40_2990 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
