
  create or replace  view datalake_dev.governed.JDE_SALES_REPORT_CONTROL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1540b_1959)
SELECT NRDOCO DocumentOrderInvoice
                ,NRMCU BusinessUnit
                ,NRUNIT Unit
                ,NRDBAN NRDBAN
                ,NRSTNR NRSTNR
                ,NRAN8 AddressNumber
                ,NRPRDC NRPRDC
                ,CAST(NRLIN/100 AS DECIMAL(18,2)) NRLIN
                ,NRRPRD NRRPRD
                ,NRYR CalendarYear
                ,NRCTRY Century
                ,NRRPFC SaleReprtFrequencyCde
                ,NRSOSS NRSOSS
                ,NRULI NRULI
                ,CAST(NRPSLS/100 AS DECIMAL(18,2)) NRPSLS
                ,NRULI2 NRULI2
                ,CAST(NRSUSA/100 AS DECIMAL(18,2)) SalesUsableArea
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NRDTRT) NRDTRT
                ,NRDCTO OrderType
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NRDTSR) NRDTSR
                ,NRICU BatchNumber
                ,NRICUT BatchType
                ,NRDIC NRDIC
                ,NRANRP NRANRP
                ,NRSOIC NRSOIC
                ,NRSRTY NRSRTY
                ,NRPROL PercentageRentOnly
                ,CAST(NRU/100 AS DECIMAL(18,2)) NRU
                ,NRDL01 Description
                ,NRSADJ NRSADJ
                ,NRSATY NRSATY
                ,NRMCUS CostCenterSubsequent
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NRDSLD) NRDSLD
                ,NRLTRC NRLTRC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NRDLTR) NRDLTR
                ,NRRM11 ReprtCdePropertyMg11
                ,NRRM12 ReprtCdePropertyMg12
                ,NRRM13 NRRM13
                ,NRRM14 NRRM14
                ,NRRM15 NRRM15
                ,NRRM21 ReprtCdePropertyMg21
                ,NRRM22 ReprtCdePropertyMg22
                ,NRRM23 ReprtCdePropertyMg23
                ,NRRM24 NRRM24
                ,NRRM25 NRRM25
                ,NRBCRC NRBCRC
                ,NRCRRM NRCRRM
                ,CAST(NRACR/100 AS DECIMAL(18,2)) NRACR
                ,NRCRR NRCRR
                ,NRDMCD NRDMCD
                ,NRCRCD CurrencyCodeFrom
                ,CAST(NRCTXA/100 AS DECIMAL(18,2)) NRCTXA
                ,CAST(NRCTXN/100 AS DECIMAL(18,2)) NRCTXN
                ,CAST(NRCTAM/100 AS DECIMAL(18,2)) NRCTAM
                ,NRCRCX NRCRCX
                ,NRCRYR CurrencyConversion
                ,NRURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NRURDT) UserReservedDate
                ,CAST(NRURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NRURRF UserReservedReference
                ,NRUSER UserId
                ,NRPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NRUPMJ) DateUpdated
                ,NRUPMT TimeLastUpdated
                ,NRJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NRENTJ) NRENTJ
                ,NRTORG TransactionOriginator
                
FROM datalake_dev.pre_governed.jde_f1540b_1959 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
