
  create or replace  view datalake_dev.governed.JDE_ACCOUNT_LEDGER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0911_1966)
SELECT GLKCO CompanyKey
                ,GLDCT DocumentType
                ,GLDOC DocumentVoucher
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLDGJ) DateForGLandVoucher
                ,GLJELN JournalEntryLineNo
                ,GLEXTL GLEXTL
                ,GLPOST GLPostedCode
                ,GLICU BatchNumber
                ,GLICUT BatchType
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLDICJ) DateBatchJulian
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLDSYJ) GLDSYJ
                ,GLTICU GLTICU
                ,GLCO Company
                ,GLANI AcctNoInputMode
                ,GLAM GLAM
                ,GLAID AccountID
                ,GLMCU BusinessUnit
                ,GLOBJ ObjectAccount
                ,GLSUB Subsidiary
                ,GLSBL Subledger
                ,GLSBLT SubledgerType
                ,GLLT LedgerType
                ,GLPN PeriodNumberGeneralLedger
                ,GLCTRY Century
                ,GLFY FiscalYear
                ,GLFQ GLFQ
                ,GLCRCD CurrencyCodeFrom
                ,GLCRR GLCRR
                ,GLHCRR GLHCRR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLHDGJ) GLHDGJ
                ,CAST(GLAA/100 AS DECIMAL(18,2)) AmountField
                ,CAST(GLU/100 AS DECIMAL(18,2)) GLU
                ,GLUM UnitOfMeasure
                ,GLGLC BillCode
                ,GLRE GLRE
                ,GLEXA NameAlphaExplanation
                ,GLEXR NameRemarkExplanation
                ,GLR1 GLR1
                ,GLR2 GLR2
                ,GLR3 GLR3
                ,GLSFX DocumentPayItem
                ,GLODOC GLODOC
                ,GLODCT GLODCT
                ,GLOSFX GLOSFX
                ,GLPKCO GLPKCO
                ,GLOKCO GLOKCO
                ,GLPDCT GLPDCT
                ,GLAN8 AddressNumber
                ,GLCN CheckNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLDKJ) DateCheckJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLDKC) DateCheckCleared
                ,GLASID GLASID
                ,GLBRE GLBRE
                ,GLRCND GLRCND
                ,GLSUMM GLSUMM
                ,GLPRGE GLPRGE
                ,GLTNN GLTNN
                ,GLALT1 GLALT1
                ,GLALT2 GLALT2
                ,GLALT3 GLALT3
                ,GLALT4 GLALT4
                ,GLALT5 GLALT5
                ,GLALT6 GLALT6
                ,GLALT7 GLALT7
                ,GLALT8 GLALT8
                ,GLALT9 GLALT9
                ,GLALT0 GLALT0
                ,GLALTT GLALTT
                ,GLALTU GLALTU
                ,GLALTV GLALTV
                ,GLALTW GLALTW
                ,GLALTX GLALTX
                ,GLALTZ GLALTZ
                ,GLDLNA GLDLNA
                ,GLCFF1 GLCFF1
                ,GLCFF2 GLCFF2
                ,GLASM GLASM
                ,GLBC GLBC
                ,GLVINV SupplierInvoiceNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLIVD) DateInvoicen
                ,GLWR01 GLWR01
                ,GLPO PurchaseOrder
                ,GLPSFX GLPSFX
                ,GLDCTO OrderType
                ,CAST(GLLNID/100 AS DECIMAL(18,2)) LineNumber
                ,GLWY GLWY
                ,GLWN GLWN
                ,GLFNLP GLFNLP
                ,CAST(GLOPSQ/100 AS DECIMAL(18,2)) GLOPSQ
                ,GLJBCD GLJBCD
                ,GLJBST GLJBST
                ,GLHMCU GLHMCU
                ,GLDOI GLDOI
                ,GLALID GLALID
                ,GLALTY GLALTY
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLDSVJ) DateServiceCurrency
                ,GLTORG TransactionOriginator
                ,GLREG_NUM GLREG_NUM
                ,GLPYID PaymentID
                ,GLUSER UserId
                ,GLPID ProgramId
                ,GLJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GLUPMJ) DateUpdated
                ,GLUPMT TimeLastUpdated
                ,GLABR1 GLABR1
                ,GLABR2 GLABR2
                ,GLABR3 GLABR3
                ,GLABR4 GLABR4
                ,GLABT1 GLABT1
                ,GLABT2 GLABT2
                ,GLABT3 GLABT3
                ,GLABT4 GLABT4
                ,GLITM GLITM
                ,GLPM01 GLPM01
                ,GLPM02 GLPM02
                ,GLPM03 GLPM03
                ,GLPM04 GLPM04
                ,GLPM05 GLPM05
                ,GLPM06 GLPM06
                ,GLPM07 GLPM07
                ,GLPM08 GLPM08
                ,GLPM09 GLPM09
                ,GLPM10 GLPM10
                ,GLBCRC GLBCRC
                ,GLCRRM GLCRRM
                ,GLPRGF GLPRGF
                ,GLTXA1 TaxArea1
                ,GLEXR1 TaxExplanationCode1
                ,GLTXITM GLTXITM
                ,GLACTB GLACTB
                ,GLGPF1 GLGPF1
                ,CAST(GLACR/100 AS DECIMAL(18,2)) GLACR
                ,CAST(GLDLNID/100 AS DECIMAL(18,2)) GLDLNID
                ,GLCKNU CkNumber
                ,GLBUPC GLBUPC
                ,GLAHBU GLAHBU
                ,GLEPGC GLEPGC
                ,GLJPGC GLJPGC
                ,GLRC5 GLRC5
                ,GLSFXE GLSFXE
                ,GLOFM GLOFM
                
FROM datalake_dev.pre_governed.jde_f0911_1966 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
