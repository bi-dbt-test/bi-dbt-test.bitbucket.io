
  create or replace  view datalake_dev.governed.JDE_LOT_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h201_1930)
SELECT LMHBMCUS Community
                ,LMHBLOT HomeBuilderLotNumber
                ,LMLOTADD LMLOTADD
                ,LMCTY1 LMCTY1
                ,LMADDS LMADDS
                ,LMADDZ LMADDZ
                ,LMLOTCMT LMLOTCMT
                ,LMCPHASE Phase
                ,LMMCU BusinessUnit
                ,LMCONSSEQ LMCONSSEQ
                ,LMHBAREA HomeBuilderArea
                ,LMHBPLAN Plan
                ,LMHBELEV Elevation
                ,LMSWING LMSWING
                ,LMPSG LMPSG
                ,LMLSWVER LMLSWVER
                ,LMLSWPEND LMLSWPEND
                ,LMLSTATHB LMLSTATHB
                ,LMOSELCF LMOSELCF
                ,LMCONNXT LMCONNXT
                ,LMCONLST LMCONLST
                ,LMSSACS LMSSACS
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMDCG) LMDCG
                ,LMUSC LMUSC
                ,LMHBSCS SalesContractStatus
                ,LMCSSEQ CurrentSalesSequence
                ,LMBYR BuyerNumberA
                ,LMMCUTJ LMMCUTJ
                ,LMSCHTMPT LMSCHTMPT
                ,LMLGLLOT LegalLotNumber
                ,LMHBBLOCK HomebuilderBlock
                ,LMHBTRACT HomebuilderTract
                ,LMPARC ParcelNumber
                ,LMPARCT LMPARCT
                ,LMBUILD LMBUILD
                ,LMUNT3 LMUNT3
                ,LMSFTL LotSquareFootage
                ,CAST(LMWLT/100 AS DECIMAL(18,2)) LMWLT
                ,CAST(LMDPLT/100 AS DECIMAL(18,2)) LMDPLT
                ,CAST(LMACLT/100 AS DECIMAL(18,2)) AcresLot
                ,LMMDLHF LMMDLHF
                ,LMSPECHF LMSPECHF
                ,CAST(LMBSP/100 AS DECIMAL(18,2)) AmountBasePrice
                ,CAST(LMLTP/100 AS DECIMAL(18,2)) LMLTP
                ,CAST(LMLPP/100 AS DECIMAL(18,2)) LMLPP
                ,LMBLDR LMBLDR
                ,LMAN8CM01 LMAN8CM01
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMPTDJ) LMPTDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMPHD) LMPHD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMCTYJ) LMCTYJ
                ,LMPMN LMPMN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMPRD) LMPRD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMCPJ) LMCPJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMATFN) LMATFN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMRLSDTE) LMRLSDTE
                ,LMCOMJ LMCOMJ
                ,LMLSRJ LMLSRJ
                ,LMLCSJ LMLCSJ
                ,LMLCCJ LMLCCJ
                ,LMUCSMSF LMUCSMSF
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD1) LMUSD1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD2) LMUSD2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD3) LMUSD3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD4) UserDate4
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD5) LMUSD5
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD6) LMUSD6
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD7) LMUSD7
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD8) LMUSD8
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD9) LMUSD9
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUSD10) LMUSD10
                ,CAST(LMUAMT01/100 AS DECIMAL(18,2)) LMUAMT01
                ,CAST(LMUAMT02/100 AS DECIMAL(18,2)) AmountUserDefinedAmount02
                ,CAST(LMUAMT03/100 AS DECIMAL(18,2)) AmountUserDefinedAmount03
                ,CAST(LMUAMT04/100 AS DECIMAL(18,2)) AmountUserDefinedAmount04
                ,CAST(LMUAMT05/100 AS DECIMAL(18,2)) LMUAMT05
                ,CAST(LMUAMT06/100 AS DECIMAL(18,2)) LMUAMT06
                ,LMOA1 LMOA1
                ,LMOA2 LMOA2
                ,LMUC01 LMUC01
                ,LMUC02 LMUC02
                ,LMUC03 LMUC03
                ,LMUC04 LMUC04
                ,LMUC05 LMUC05
                ,LMUC06 LMUC06
                ,LMLCC01 LotCategory01
                ,LMLCC02 LotCategory02
                ,LMLCC03 LotCategory03
                ,LMLCC04 LMLCC04
                ,LMLCC05 LotCategory05
                ,LMLCC06 LotCategory06
                ,LMLCC07 LMLCC07
                ,LMLCC08 LMLCC08
                ,LMLCC09 LMLCC09
                ,LMLCC10 LMLCC10
                ,LMACCL LMACCL
                ,LMCLRH LMCLRH
                ,LMROF LMROF
                ,LMBDC LMBDC
                ,LMFSC LMFSC
                ,LMCLPK LMCLPK
                ,LMDOC01 LMDOC01
                ,LMDOC02 LMDOC02
                ,LMDOC03 LMDOC03
                ,LMDCT01 LMDCT01
                ,LMDCT02 LMDCT02
                ,LMDCT03 LMDCT03
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMCDJ) CloseDate
                ,LMSUB Subsidiary
                ,LMCO Company
                ,LMRLSN LMRLSN
                ,LMHBST1 LMHBST1
                ,LMHBST2 LMHBST2
                ,LMHBST3 LMHBST3
                ,LMHBST4 LMHBST4
                ,LMHBST5 LMHBST5
                ,LMCRTU LMCRTU
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMCRTJ) LMCRTJ
                ,LMCRTT LMCRTT
                ,LMWRKSTNID LMWRKSTNID
                ,LMHBOPID LMHBOPID
                ,LMUPMB LMUPMB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LMUPMJ) DateUpdated
                ,LMUPMT TimeLastUpdated
                ,LMJOBN WorkStationId
                ,LMPID ProgramId
                
FROM datalake_dev.pre_governed.jde_f44h201_1930 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
