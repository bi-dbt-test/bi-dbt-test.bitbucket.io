
  create or replace  view datalake_dev.governed.JDE_MEDIA_OBJECTS_STORAGE  as (
    
SELECT gdobnm AS objectname,
 gdtxky AS generictextkey,
 gdmoseqn AS mediaobjectsequencenumber,
 gdgtmotype AS generictextmediaobjecttype,
 gdlngp,
 gduser AS userid,
 SEMANTIC.JDE_JULIAN_TO_DATE(gdupmj) AS dateupdated,
 gdtday,
 gdgtitnm,
 gdqunam,
 gdgtfilenm,
 gdgtfuts1,
 gdgtfuts2,
 gdgtfuts3,
 gdgtfuts4,
 gdgtfutm1,
 gdgtfutm2,
 gdtxft AS generictextblobbuffer
 FROM datalake_dev.pre_governed.JDE_MEDIA_OBJECTS_STORAGE
  );
