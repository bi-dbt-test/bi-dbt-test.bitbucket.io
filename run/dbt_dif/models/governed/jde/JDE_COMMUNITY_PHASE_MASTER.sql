
  create or replace  view datalake_dev.governed.JDE_COMMUNITY_PHASE_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h101_3588)
SELECT CMHBMCUS Community
                ,CMCPHASE Phase
                ,CMPHSENB PhaseEnablingFlag
                ,CMHBAREA HomeBuilderArea
                ,CMDL01 Description
                ,CMNBRPHS NumberOfPhases
                ,CMPMPT PlanMasterProductType
                ,CMPMPG PlanMasterProductGroup
                ,CMAN8MCUS CMAN8MCUS
                ,CMAN8SOF CMAN8SOF
                ,CMAN8COF CMAN8COF
                ,CMAN8CM01 CMAN8CM01
                ,CMAN8CM02 CMAN8CM02
                ,CMAN8CM03 CMAN8CM03
                ,CMAN8CM04 CMAN8CM04
                ,CMAN8CM05 CMAN8CM05
                ,CMAN8CM06 CMAN8CM06
                ,CMAN8CM07 CMAN8CM07
                ,CMAN8CM08 CMAN8CM08
                ,CMAN8CM09 CMAN8CM09
                ,CMAN8CM10 CMAN8CM10
                ,CMAN8CM11 CMAN8CM11
                ,CMAN8CM12 CMAN8CM12
                ,CMAN8CM13 CMAN8CM13
                ,CMAN8CM14 CMAN8CM14
                ,CMAN8CM15 CMAN8CM15
                ,CMAN8CM16 CMAN8CM16
                ,CMAN8CM17 CMAN8CM17
                ,CMAN8CM18 CMAN8CM18
                ,CMAN8CM19 CMAN8CM19
                ,CMAN8CM20 CMAN8CM20
                ,CMAN8UR01 CMAN8UR01
                ,CMAN8UR02 CMAN8UR02
                ,CMAN8UR03 CMAN8UR03
                ,CMAN8UR04 CMAN8UR04
                ,CMAN8UR05 CMAN8UR05
                ,CMCONSSEQ CMCONSSEQ
                ,CMNBRLOT CMNBRLOT
                ,CMNBRSLD CMNBRSLD
                ,CMNBRCLSD CMNBRCLSD
                ,CMNBRSTRD CMNBRSTRD
                ,CMNBRFIN CMNBRFIN
                ,CAST(CMAOCST01/100 AS DECIMAL(18,2)) CMAOCST01
                ,CAST(CMAOCST02/100 AS DECIMAL(18,2)) CMAOCST02
                ,CAST(CMAOCST03/100 AS DECIMAL(18,2)) CMAOCST03
                ,CAST(CMAOCST04/100 AS DECIMAL(18,2)) CMAOCST04
                ,CAST(CMAOCST05/100 AS DECIMAL(18,2)) CMAOCST05
                ,CAST(CMAOREV01/100 AS DECIMAL(18,2)) CMAOREV01
                ,CAST(CMAOREV02/100 AS DECIMAL(18,2)) CMAOREV02
                ,CAST(CMAOREV03/100 AS DECIMAL(18,2)) CMAOREV03
                ,CAST(CMAOREV04/100 AS DECIMAL(18,2)) CMAOREV04
                ,CAST(CMAOREV05/100 AS DECIMAL(18,2)) CMAOREV05
                ,CAST(CMCSTPCT01/100 AS DECIMAL(18,2)) CMCSTPCT01
                ,CAST(CMCSTPCT02/100 AS DECIMAL(18,2)) CMCSTPCT02
                ,CAST(CMCSTPCT03/100 AS DECIMAL(18,2)) CMCSTPCT03
                ,CAST(CMCSTPCT04/100 AS DECIMAL(18,2)) CMCSTPCT04
                ,CAST(CMCSTPCT05/100 AS DECIMAL(18,2)) CMCSTPCT05
                ,CAST(CMCSTPCT06/100 AS DECIMAL(18,2)) CMCSTPCT06
                ,CAST(CMCSTPCT07/100 AS DECIMAL(18,2)) CMCSTPCT07
                ,CAST(CMCSTPCT08/100 AS DECIMAL(18,2)) CMCSTPCT08
                ,CAST(CMCSTPCT09/100 AS DECIMAL(18,2)) CMCSTPCT09
                ,CAST(CMCSTPCT10/100 AS DECIMAL(18,2)) CMCSTPCT10
                ,CMHBRLTPC CMHBRLTPC
                ,CMHBRLTPS CMHBRLTPS
                ,CMINCMCU CMINCMCU
                ,CMWAMCU CMWAMCU
                ,CMCWTMPT CMCWTMPT
                ,CAST(CMUAMT01/100 AS DECIMAL(18,2)) CMUAMT01
                ,CAST(CMUAMT02/100 AS DECIMAL(18,2)) AmountUserDefinedAmount02
                ,CAST(CMUAMT03/100 AS DECIMAL(18,2)) AmountUserDefinedAmount03
                ,CAST(CMUAMT04/100 AS DECIMAL(18,2)) AmountUserDefinedAmount04
                ,CAST(CMUAMT05/100 AS DECIMAL(18,2)) CMUAMT05
                ,CAST(CMUAMT06/100 AS DECIMAL(18,2)) CMUAMT06
                ,CAST(CMUAMT07/100 AS DECIMAL(18,2)) CMUAMT07
                ,CAST(CMUAMT08/100 AS DECIMAL(18,2)) AmountUserDefinedAmount08
                ,CAST(CMUAMT09/100 AS DECIMAL(18,2)) AmountUserDefinedAmount09
                ,CAST(CMUAMT10/100 AS DECIMAL(18,2)) AmountUserDefinedAmount10
                ,CMF1A CMF1A
                ,CMF2A CMF2A
                ,CMF3A CMF3A
                ,CMF4A CMF4A
                ,CMF5A CMF5A
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD1) CMUSD1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD2) CMUSD2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD3) CMUSD3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD4) UserDate4
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD5) CMUSD5
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD6) CMUSD6
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD7) CMUSD7
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD8) CMUSD8
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD9) CMUSD9
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUSD10) CMUSD10
                ,CMCRTU CMCRTU
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMCRTJ) CMCRTJ
                ,CMCRTT CMCRTT
                ,CMWRKSTNID CMWRKSTNID
                ,CMHBOPID CMHBOPID
                ,CMUPMB CMUPMB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CMUPMJ) DateUpdated
                ,CMUPMT TimeLastUpdated
                ,CMJOBN WorkStationId
                ,CMPID ProgramId
                ,CMSCHENB CMSCHENB
                ,CMHBST1 CMHBST1
                ,CMHBST2 CMHBST2
                ,CMHBST3 CMHBST3
                ,CMHBST4 CMHBST4
                ,CMHBST5 CMHBST5
                
FROM datalake_dev.pre_governed.jde_f44h101_3588 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
