
  create or replace  view datalake_dev.governed.JDE_CLOSING_WORKSHEET_ACCT_INSTR  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h590_2473)
SELECT CICWTMPT Closing_Worksheet_Template
                ,CICLSRT Closing_Worksheet_Line_Item
                ,CICLDESC Closing_Line_Item_Description
                ,CIMCU Business_Unit
                ,CICLMCD Closing_Business_Unit_Value
                ,CIOBJ Object_Account
                ,CISUB Subsidiary
                ,CILT Ledger_Types
                ,CIBDLT Budget_Ledger_Type
                ,CISBL Subledger_GL
                ,CISBLT Subledger_Type
                ,CICLSCD Closing_Subledger_Value
                ,CICLDBCR Closing_Debit_Credit_Code
                ,CAST(CICLTAMT/100 AS DECIMAL(18,2)) ClosingTemplateScheduledAmount
                ,CAST(CIAA/100 AS DECIMAL(18,2)) Amount
                ,CAST(CIU/100 AS DECIMAL(18,2)) Units
                ,CICRTU Created_by_user
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CICRTJ) Date_Created
                ,CICRTT Time_Created
                ,CIWRKSTNID Original_Work_Station_ID
                ,CIHBOPID Original_Program_ID
                ,CIUPMB Updated_By_User
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CIUPMJ) Date_Updated
                ,CIUPMT Time_Last_Updated
                ,CIJOBN Work_Station_ID
                ,CIPID Program_ID
                
FROM datalake_dev.pre_governed.jde_f44h590_2473 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
