
  create or replace  view datalake_dev.governed.JDE_LOT_SALES_HISTORY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h501h_1941)
SELECT LSHBMCUS Community
                ,LSHBLOT HomeBuilderLotNumber
                ,LSCSSEQ CurrentSalesSequence
                ,LSSHSEQ LSSHSEQ
                ,LSACTVSEQ LSACTVSEQ
                ,LSMCU BusinessUnit
                ,LSHBAREA HomeBuilderArea
                ,LSCPHASE Phase
                ,LSBYR BuyerNumberA
                ,LSMLTBYR LSMLTBYR
                ,LSHBPLAN Plan
                ,LSHBELEV Elevation
                ,LSSWING LSSWING
                ,LSSALLST SalesActivityCodeLast
                ,LSSALNXT SalesActivityCodeNext
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSDCG) LSDCG
                ,LSCONACT LSCONACT
                ,LSHBSCS SalesContractStatus
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSSDJ) SalesDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSSRPDJ) LSSRPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSSRDJ) LSSRDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCAPRVL) LSCAPRVL
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSECDJ) LSECDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCDJ) CloseDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSEDJ) LSEDJ
                ,LSCSRCD LSCSRCD
                ,LSCSN1 LSCSN1
                ,LSCSN2 LSCSN2
                ,LSCSN3 LSCSN3
                ,CAST(LSBHPRICE/100 AS DECIMAL(18,2)) BaseHouseSalesPrice
                ,CAST(LSLOTPREM/100 AS DECIMAL(18,2)) LSLOTPREM
                ,CAST(LSINCA1/100 AS DECIMAL(18,2)) LSINCA1
                ,CAST(LSINCA2/100 AS DECIMAL(18,2)) LSINCA2
                ,CAST(LSINCA3/100 AS DECIMAL(18,2)) LSINCA3
                ,CAST(LSUPG/100 AS DECIMAL(18,2)) LSUPG
                ,CAST(LSNETBP/100 AS DECIMAL(18,2)) NetBasePrice
                ,CAST(LSOPRV1/100 AS DECIMAL(18,2)) LSOPRV1
                ,CAST(LSOPRV2/100 AS DECIMAL(18,2)) LSOPRV2
                ,CAST(LSOPRV3/100 AS DECIMAL(18,2)) LSOPRV3
                ,CAST(LSOPRV4/100 AS DECIMAL(18,2)) LSOPRV4
                ,CAST(LSOPRV5/100 AS DECIMAL(18,2)) LSOPRV5
                ,CAST(LSOPRV6/100 AS DECIMAL(18,2)) LSOPRV6
                ,CAST(LSOPRV7/100 AS DECIMAL(18,2)) LSOPRV7
                ,CAST(LSOPRV8/100 AS DECIMAL(18,2)) LSOPRV8
                ,CAST(LSOPRV9/100 AS DECIMAL(18,2)) LSOPRV9
                ,CAST(LSOPINCA/100 AS DECIMAL(18,2)) LSOPINCA
                ,CAST(LSNETOPT/100 AS DECIMAL(18,2)) LSNETOPT
                ,CAST(LSTOTSAL/100 AS DECIMAL(18,2)) TotalSalesPrice
                ,LSANSM01 LSANSM01
                ,LSANSM02 LSANSM02
                ,LSANSM03 LSANSM03
                ,LSANSM04 LSANSM04
                ,LSANSM05 LSANSM05
                ,LSANSM06 LSANSM06
                ,LSANSM07 LSANSM07
                ,LSANSM08 LSANSM08
                ,LSANSM09 LSANSM09
                ,LSANSM10 LSANSM10
                ,CAST(LSCMPCT1/100 AS DECIMAL(18,2)) LSCMPCT1
                ,CAST(LSCMPCT2/100 AS DECIMAL(18,2)) LSCMPCT2
                ,CAST(LSCMPCT3/100 AS DECIMAL(18,2)) LSCMPCT3
                ,CAST(LSCMPCT4/100 AS DECIMAL(18,2)) LSCMPCT4
                ,CAST(LSCMPCT5/100 AS DECIMAL(18,2)) LSCMPCT5
                ,CAST(LSCMPCT6/100 AS DECIMAL(18,2)) LSCMPCT6
                ,CAST(LSCMPCT7/100 AS DECIMAL(18,2)) LSCMPCT7
                ,CAST(LSCMPCT8/100 AS DECIMAL(18,2)) LSCMPCT8
                ,CAST(LSCMPCT9/100 AS DECIMAL(18,2)) LSCMPCT9
                ,CAST(LSCMPCT10/100 AS DECIMAL(18,2)) LSCMPCT10
                ,CAST(LSCM1/100 AS DECIMAL(18,2)) LSCM1
                ,CAST(LSCM2/100 AS DECIMAL(18,2)) LSCM2
                ,CAST(LSCM3/100 AS DECIMAL(18,2)) LSCM3
                ,LSCM4 LSCM4
                ,CAST(LSCM5/100 AS DECIMAL(18,2)) LSCM5
                ,CAST(LSCM6/100 AS DECIMAL(18,2)) LSCM6
                ,CAST(LSCM7/100 AS DECIMAL(18,2)) LSCM7
                ,CAST(LSCM8/100 AS DECIMAL(18,2)) LSCM8
                ,CAST(LSCM9/100 AS DECIMAL(18,2)) LSCM9
                ,CAST(LSCM10/100 AS DECIMAL(18,2)) LSCM10
                ,LSLDR LSLDR
                ,LSLOANNUM LoanNumber
                ,LSLOANTYP LoanType
                ,CAST(LSMGA/100 AS DECIMAL(18,2)) LSMGA
                ,CAST(LSINRTE/100 AS DECIMAL(18,2)) LSINRTE
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSINRLCK) LSINRLCK
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSPXD) LSPXD
                ,CAST(LSDNPT/100 AS DECIMAL(18,2)) LSDNPT
                ,CAST(LSEMD/100 AS DECIMAL(18,2)) LSEMD
                ,CAST(LSDTKN/100 AS DECIMAL(18,2)) LSDTKN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSMAD) MortgageApprovalDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSTAD) LSTAD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSLADJ) LSLADJ
                ,LSCTG LSCTG
                ,LSCTGS LSCTGS
                ,LSPTX LSPTX
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCTGJ) LSCTGJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSLRD) LSLRD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCCNVD) LSCCNVD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCLJ) LSCLJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCXD) LSCXD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCRD) LSCRD
                ,LSCN1 LSCN1
                ,LSCN2 LSCN2
                ,LSLAG LSLAG
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSDCAPPT) LSDCAPPT
                ,LSDCSTM LSDCSTM
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSDCADT) LSDCADT
                ,LSCLT LSCLT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSTEDJ) LSTEDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSTIDJ) LSTIDJ
                ,LSTLN1 LSTLN1
                ,LSTLN2 LSTLN2
                ,LSTLN3 LSTLN3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSBSWD) BuyerScheduledWalkThruDate
                ,LSBSWT LSBSWT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSBAWD) LSBAWD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD1) LSUSD1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD2) LSUSD2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD3) LSUSD3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD4) UserDate4
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD5) LSUSD5
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD6) LSUSD6
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD7) LSUSD7
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD8) LSUSD8
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD9) LSUSD9
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD10) LSUSD10
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD11) LSUSD11
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD12) LSUSD12
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD13) LSUSD13
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD14) LSUSD14
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD15) LSUSD15
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD16) LSUSD16
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD17) LSUSD17
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD18) LSUSD18
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD19) LSUSD19
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUSD20) LSUSD20
                ,CAST(LSUAMT01/100 AS DECIMAL(18,2)) LSUAMT01
                ,CAST(LSUAMT02/100 AS DECIMAL(18,2)) AmountUserDefinedAmount02
                ,CAST(LSUAMT03/100 AS DECIMAL(18,2)) AmountUserDefinedAmount03
                ,CAST(LSUAMT04/100 AS DECIMAL(18,2)) AmountUserDefinedAmount04
                ,CAST(LSUAMT05/100 AS DECIMAL(18,2)) LSUAMT05
                ,CAST(LSUAMT06/100 AS DECIMAL(18,2)) LSUAMT06
                ,CAST(LSUAMT07/100 AS DECIMAL(18,2)) LSUAMT07
                ,CAST(LSUAMT08/100 AS DECIMAL(18,2)) AmountUserDefinedAmount08
                ,CAST(LSUAMT09/100 AS DECIMAL(18,2)) AmountUserDefinedAmount09
                ,CAST(LSUAMT10/100 AS DECIMAL(18,2)) AmountUserDefinedAmount10
                ,LSSMC01 SalesMasterCategoryCode1
                ,LSSMC02 LSSMC02
                ,LSSMC03 LSSMC03
                ,LSSMC04 LSSMC04
                ,LSSMC05 LSSMC05
                ,LSSMC06 LSSMC06
                ,LSSMC07 LSSMC07
                ,LSSMC08 LSSMC08
                ,LSSMC09 LSSMC09
                ,LSSMC10 LSSMC10
                ,LSUSAN01 LSUSAN01
                ,LSUSAN02 LSUSAN02
                ,LSUSAN03 LSUSAN03
                ,LSUSAN04 LSUSAN04
                ,LSUSAN05 LSUSAN05
                ,LSHBST1 LSHBST1
                ,LSHBST2 LSHBST2
                ,LSHBST3 LSHBST3
                ,LSHBST4 LSHBST4
                ,LSHBST5 LSHBST5
                ,LSCONLST LSCONLST
                ,LSCONNXT LSCONNXT
                ,LSLSTATHB LSLSTATHB
                ,LSCONSSEQ LSCONSSEQ
                ,LSCTY1 LSCTY1
                ,LSADDS LSADDS
                ,LSADDZ LSADDZ
                ,LSLOTADD LSLOTADD
                ,CAST(LSLPP/100 AS DECIMAL(18,2)) LSLPP
                ,LSCRTU LSCRTU
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSCRTJ) LSCRTJ
                ,LSCRTT LSCRTT
                ,LSWRKSTNID LSWRKSTNID
                ,LSHBOPID LSHBOPID
                ,LSUPMB LSUPMB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(LSUPMJ) DateUpdated
                ,LSUPMT TimeLastUpdated
                ,LSJOBN WorkStationId
                ,LSPID ProgramId
                
FROM datalake_dev.pre_governed.jde_f44h501h_1941 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
