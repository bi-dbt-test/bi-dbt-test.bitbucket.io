
  create or replace  view datalake_dev.governed.JDE_ACCOUNTS_PAYABLE_LEDGER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0411_1949)
SELECT RPKCO CompanyKey
                ,RPDOC DocumentVoucher
                ,RPDCT DocumentType
                ,RPSFX DocumentPayItem
                ,RPSFXE RPSFXE
                ,RPDCTA RPDCTA
                ,RPAN8 AddressNumber
                ,RPPYE RPPYE
                ,RPSNTO AddressNumberSentTo
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDIVJ) DateInvoiceJulian
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDSVJ) DateServiceCurrency
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDDJ) DateNetDue
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDDNJ) RPDDNJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDGJ) DateForGLandVoucher
                ,RPFY FiscalYear
                ,RPCTRY Century
                ,RPPN PeriodNumberGeneralLedger
                ,RPCO Company
                ,RPICU BatchNumber
                ,RPICUT BatchType
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPDICJ) DateBatchJulian
                ,RPBALJ RPBALJ
                ,RPPST PayStatusCode
                ,CAST(RPAG/100 AS DECIMAL(18,2)) AmountGross
                ,CAST(RPAAP/100 AS DECIMAL(18,2)) AmountOpen
                ,CAST(RPADSC/100 AS DECIMAL(18,2)) RPADSC
                ,CAST(RPADSA/100 AS DECIMAL(18,2)) RPADSA
                ,CAST(RPATXA/100 AS DECIMAL(18,2)) RPATXA
                ,CAST(RPATXN/100 AS DECIMAL(18,2)) RPATXN
                ,CAST(RPSTAM/100 AS DECIMAL(18,2)) RPSTAM
                ,RPTXA1 TaxArea1
                ,RPEXR1 TaxExplanationCode1
                ,RPCRRM RPCRRM
                ,RPCRCD CurrencyCodeFrom
                ,RPCRR RPCRR
                ,CAST(RPACR/100 AS DECIMAL(18,2)) RPACR
                ,CAST(RPFAP/100 AS DECIMAL(18,2)) RPFAP
                ,CAST(RPCDS/100 AS DECIMAL(18,2)) RPCDS
                ,CAST(RPCDSA/100 AS DECIMAL(18,2)) RPCDSA
                ,CAST(RPCTXA/100 AS DECIMAL(18,2)) RPCTXA
                ,CAST(RPCTXN/100 AS DECIMAL(18,2)) RPCTXN
                ,CAST(RPCTAM/100 AS DECIMAL(18,2)) RPCTAM
                ,RPGLC BillCode
                ,RPGLBA GlBankAccount
                ,RPPOST GLPostedCode
                ,RPAM RPAM
                ,RPAID2 RPAID2
                ,RPMCU BusinessUnit
                ,RPOBJ ObjectAccount
                ,RPSUB Subsidiary
                ,RPSBLT SubledgerType
                ,RPSBL Subledger
                ,RPBAID BankTransitShortId
                ,RPPTC RPPTC
                ,RPVOD VoidFlag
                ,RPOKCO RPOKCO
                ,RPODCT RPODCT
                ,RPODOC RPODOC
                ,RPOSFX RPOSFX
                ,RPCRC RPCRC
                ,RPVINV SupplierInvoiceNumber
                ,RPPKCO RPPKCO
                ,RPPO PurchaseOrder
                ,RPPDCT RPPDCT
                ,CAST(RPLNID/100 AS DECIMAL(18,2)) LineNumber
                ,RPSFXO RPSFXO
                ,CAST(RPOPSQ/100 AS DECIMAL(18,2)) RPOPSQ
                ,RPVR01 RPVR01
                ,RPUNIT Unit
                ,RPMCU2 BusinessUnit2
                ,RPRMK NameRemark
                ,RPRF RPRF
                ,RPDRF RPDRF
                ,RPCTL RPCTL
                ,RPFNLP RPFNLP
                ,CAST(RPU/100 AS DECIMAL(18,2)) RPU
                ,RPUM UnitOfMeasure
                ,RPPYIN PaymentInstrument
                ,RPTXA3 TaxRateArea3Withholding
                ,RPEXR3 TaxExplCode3Withhholding
                ,RPRP1 RPRP1
                ,RPRP2 RPRP2
                ,RPRP3 RPRP3
                ,RPAC07 RPAC07
                ,RPTNN RPTNN
                ,RPDMCD RPDMCD
                ,RPITM RPITM
                ,RPHCRR RPHCRR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPHDGJ) RPHDGJ
                ,RPURC1 RPURC1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPURDT) UserReservedDate
                ,CAST(RPURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,RPURAB UserReservedNumber
                ,RPURRF UserReservedReference
                ,RPTORG TransactionOriginator
                ,RPUSER UserId
                ,RPPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RPUPMJ) DateUpdated
                ,RPUPMT TimeLastUpdated
                ,RPJOBN WorkStationId
                ,RPTNST BankTransitNumber
                ,RPYC01 RPYC01
                ,RPYC02 RPYC02
                ,RPYC03 RPYC03
                ,RPYC04 RPYC04
                ,RPYC05 RPYC05
                ,RPYC06 RPYC06
                ,RPYC07 RPYC07
                ,RPYC08 RPYC08
                ,RPYC09 RPYC09
                ,RPYC10 RPYC10
                ,RPDTXS RPDTXS
                ,RPBCRC RPBCRC
                ,CAST(RPATAD/100 AS DECIMAL(18,2)) RPATAD
                ,CAST(RPCTAD/100 AS DECIMAL(18,2)) RPCTAD
                ,CAST(RPNRTA/100 AS DECIMAL(18,2)) RPNRTA
                ,CAST(RPFNRT/100 AS DECIMAL(18,2)) RPFNRT
                ,RPTAXP RPTAXP
                ,RPPRGF RPPRGF
                ,RPGFL5 RPGFL5
                ,RPGFL6 RPGFL6
                ,CAST(RPGAM1/100 AS DECIMAL(18,2)) RPGAM1
                ,CAST(RPGAM2/100 AS DECIMAL(18,2)) RPGAM2
                ,RPGEN4 RPGEN4
                ,RPGEN5 RPGEN5
                ,CAST(RPWTAD/100 AS DECIMAL(18,2)) RPWTAD
                ,CAST(RPWTAF/100 AS DECIMAL(18,2)) RPWTAF
                ,RPSMMF RPSMMF
                ,RPPYWP RPPYWP
                ,RPPWPG RPPWPG
                ,RPNETTCID RPNETTCID
                ,RPNETDOC RPNETDOC
                ,RPNETRC5 RPNETRC5
                ,RPNETST RPNETST
                ,RPCNTRTID RPCNTRTID
                ,RPCNTRTCD RPCNTRTCD
                ,RPWVID RPWVID
                ,RPBLSCD2 RPBLSCD2
                ,RPHARPER RPHARPER
                ,RPHARSFX RPHARSFX
                ,RPDDRL RPDDRL
                ,RPSEQN SequenceNumber2
                ,RPCLASS01 RPCLASS01
                ,RPCLASS02 RPCLASS02
                ,RPCLASS03 RPCLASS03
                ,RPCLASS04 RPCLASS04
                ,RPCLASS05 RPCLASS05
                
FROM datalake_dev.pre_governed.jde_f0411_1949 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
