
  create or replace  view datalake_dev.governed.JDE_USER_DEFINED_CODE_TYPES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0004_1945)
SELECT DTSY ProductCode
                ,DTRT UserDefinedCodes
                ,DTDL01 Description
                ,CAST(DTUSEQ/100 AS DECIMAL(18,2)) UserDefinedCodeSequenceN
                ,DTUCD1 UserDefinedCodeCategoryC
                ,DTCDL CodeLength
                ,DTLN2 Line2DesiredYN
                ,DTCNUM NumericYN
                ,DTMRCT MergeControl
                ,DTMRTY MergeType
                ,DTUSER UserId
                ,DTPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(DTUPMJ) DateUpdated
                ,DTJOBN WorkStationId
                ,DTUPMT TimeLastUpdated
                
FROM datalake_dev.pre_governed.jde_f0004_1945 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
