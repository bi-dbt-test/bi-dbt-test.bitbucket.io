
  create or replace  view datalake_dev.governed.JDE_BANK_TRANSIT_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0030_1984)
SELECT AYBKTP BankTransitRecordType
                ,AYTNST BankTransitNumber
                ,AYCBNK CustBankAcctNumber
                ,AYAN8 AddressNumber
                ,AYDL01 Description
                ,AYAID AccountID
                ,AYNXTC NextCheckNumber
                ,AYCHKD ControlDigit
                ,AYCRCD CurrencyCodeFrom
                ,AYRLN ReferenceRollNumber
                ,AYBACS BacsUserNumber
                ,AYRFNM BacsReferenceName
                ,AYBAID BankTransitShortId
                ,AYMCU BusinessUnit
                ,AYSWFT SwiftCode
                ,AYADPI PreNoteOptionCode
                ,AYCHKQ CheckPrintQueue
                ,AYATTQ CheckAttachmentsPrint
                ,AYDBTQ DebitMemoPrintQueue
                ,AYALGN NumberOfAlignmentChecks
                ,AYSDTL DetailLinesperAPPaymentS
                ,AYFLR FloatDaysforChecksRcv
                ,AYFLP FloatDaysforChecksPayabl
                ,AYCKSV CheckingorSavingsAccount
                ,AYUKID UniqueKeyIDInternal
                ,AYCTR Country
                ,AYNXTA NextAutomaticDebitNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AYUPMJ) DateUpdated
                ,AYUPMT TimeLastUpdated
                ,AYPID ProgramId
                ,AYUSER UserId
                ,AYJOBN WorkStationId
                ,AYIBAN InternationalBankAccountNumber
                ,AYAN8BK AddressNumberBank
                
FROM datalake_dev.pre_governed.jde_f0030_1984 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
