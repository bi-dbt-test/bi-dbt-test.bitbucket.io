
  create or replace  view datalake_dev.governed.JDE_ADDRESS_BOOK_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0101_1974)
SELECT ABAN8 AddressNumber
                ,ABALKY AlternateAddressKey
                ,ABTAX TaxId
                ,ABALPH NameAlpha
                ,ABDC DescripCompressed
                ,ABMCU BusinessUnit
                ,ABSIC ABSIC
                ,ABLNGP ABLNGP
                ,ABAT1 AddressType1
                ,ABCM ABCM
                ,ABTAXC ABTAXC
                ,ABAT2 ABAT2
                ,ABAT3 ABAT3
                ,ABAT4 ABAT4
                ,ABAT5 ABAT5
                ,ABATP ABATP
                ,ABATR ABATR
                ,ABATPR ABATPR
                ,ABAB3 ABAB3
                ,ABATE ABATE
                ,ABSBLI ABSBLI
                ,SEMANTIC.JDE_JULIAN_TO_DATE(ABEFTB) DateBeginningEffective
                ,ABAN81 ABAN81
                ,ABAN82 ABAN82
                ,ABAN83 ABAN83
                ,ABAN84 ABAN84
                ,ABAN86 ABAN86
                ,ABAN85 ABAN85
                ,ABAC01 ABAC01
                ,ABAC02 ABAC02
                ,ABAC03 ABAC03
                ,ABAC04 ABAC04
                ,ABAC05 ABAC05
                ,ABAC06 ABAC06
                ,ABAC07 ABAC07
                ,ABAC08 ABAC08
                ,ABAC09 ABAC09
                ,ABAC10 ABAC10
                ,ABAC11 ABAC11
                ,ABAC12 ABAC12
                ,ABAC13 ABAC13
                ,ABAC14 ABAC14
                ,ABAC15 ABAC15
                ,ABAC16 ABAC16
                ,ABAC17 ABAC17
                ,ABAC18 ABAC18
                ,ABAC19 ABAC19
                ,ABAC20 ABAC20
                ,ABAC21 ABAC21
                ,ABAC22 ABAC22
                ,ABAC23 ABAC23
                ,ABAC24 ABAC24
                ,ABAC25 ABAC25
                ,ABAC26 ABAC26
                ,ABAC27 ABAC27
                ,ABAC28 ABAC28
                ,ABAC29 ABAC29
                ,ABAC30 ABAC30
                ,ABGLBA GlBankAccount
                ,ABPTI ABPTI
                ,SEMANTIC.JDE_JULIAN_TO_DATE(ABPDI) ABPDI
                ,ABMSGA ABMSGA
                ,ABRMK NameRemark
                ,ABTXCT ABTXCT
                ,ABTX2 ABTX2
                ,ABALP1 ABALP1
                ,ABURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(ABURDT) UserReservedDate
                ,CAST(ABURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,ABURAB UserReservedNumber
                ,ABURRF UserReservedReference
                ,ABUSER UserId
                ,ABPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(ABUPMJ) DateUpdated
                ,ABJOBN WorkStationId
                ,ABUPMT TimeLastUpdated
                ,ABPRGF ABPRGF
                ,ABSCCLTP ABSCCLTP
                ,ABTICKER ABTICKER
                ,ABEXCHG ABEXCHG
                ,ABDUNS ABDUNS
                ,ABCLASS01 ABCLASS01
                ,ABCLASS02 ABCLASS02
                ,ABCLASS03 ABCLASS03
                ,ABCLASS04 ABCLASS04
                ,ABCLASS05 ABCLASS05
                ,ABNOE ABNOE
                ,ABGROWTHR ABGROWTHR
                ,ABYEARSTAR ABYEARSTAR
                ,ABAEMPGP ABAEMPGP
                ,ABACTIN ABACTIN
                ,ABREVRNG ABREVRNG
                ,ABSYNCS SynchronizationStatus
                ,ABPERRS ABPERRS
                ,ABCAAD ServerStatus
                
FROM datalake_dev.pre_governed.jde_f0101_1974 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
