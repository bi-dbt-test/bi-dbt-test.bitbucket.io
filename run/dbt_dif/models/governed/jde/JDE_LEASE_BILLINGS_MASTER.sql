
  create or replace  view datalake_dev.governed.JDE_LEASE_BILLINGS_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1511b_2976)
SELECT NJGENT NJGENT
                ,NJICU BatchNumber
                ,NJDOC DocumentVoucher
                ,NJSFX DocumentPayItem
                ,NJDOCO DocumentOrderInvoice
                ,NJDCTO OrderType
                ,NJSOSQ NJSOSQ
                ,NJMCUS CostCenterSubsequent
                ,NJAN8 AddressNumber
                ,NJAN8J AddNoAlternatePayee
                ,NJDL01 Description
                ,NJMCU BusinessUnit
                ,NJOBJ ObjectAccount
                ,NJSUB Subsidiary
                ,NJSBL Subledger
                ,NJAID AccountID
                ,NJCO Company
                ,NJDG NJDG
                ,NJDSV NJDSV
                ,NJGLC BillCode
                ,NJBPN NJBPN
                ,NJYR CalendarYear
                ,CAST(NJAG/100 AS DECIMAL(18,2)) AmountGross
                ,CAST(NJAN01/100 AS DECIMAL(18,2)) AmountNetPosting001
                ,NJDI NJDI
                ,NJDD NJDD
                ,NJPTC NJPTC
                ,NJTRAN NJTRAN
                ,NJSEPI NJSEPI
                ,NJITMG NJITMG
                ,CAST(NJLNID/100 AS DECIMAL(18,2)) LineNumber
                ,NJDIC NJDIC
                ,NJPDUE PercentageDueOnSales
                ,NJEPCL NJEPCL
                ,NJDPER NJDPER
                ,NJDPAR NJDPAR
                ,NJEXCD NJEXCD
                ,NJBCI BillingControlId
                ,NJPRDC NJPRDC
                ,NJYRO NJYRO
                ,NJCTRY Century
                ,NJRPRD NJRPRD
                ,NJCRCD CurrencyCodeFrom
                ,NJCRR NJCRR
                ,CAST(NJACR/100 AS DECIMAL(18,2)) NJACR
                ,NJTXA1 TaxArea1
                ,NJEXR1 TaxExplanationCode1
                ,CAST(NJSTAM/100 AS DECIMAL(18,2)) NJSTAM
                ,CAST(NJATXN/100 AS DECIMAL(18,2)) NJATXN
                ,CAST(NJATXA/100 AS DECIMAL(18,2)) NJATXA
                ,NJSBLT SubledgerType
                ,NJCRRM NJCRRM
                ,CAST(NJFAP/100 AS DECIMAL(18,2)) NJFAP
                ,CAST(NJCDS/100 AS DECIMAL(18,2)) NJCDS
                ,CAST(NJCDSA/100 AS DECIMAL(18,2)) NJCDSA
                ,CAST(NJCTAM/100 AS DECIMAL(18,2)) NJCTAM
                ,CAST(NJCTXA/100 AS DECIMAL(18,2)) NJCTXA
                ,CAST(NJCTXN/100 AS DECIMAL(18,2)) NJCTXN
                ,NJMI NJMI
                ,NJDBAN NJDBAN
                ,NJUNIT Unit
                ,NJBLMR NJBLMR
                ,NJEFFC NJEFFC
                ,NJSOBT NJSOBT
                ,NJSBJR NJSBJR
                ,NJICB1 NJICB1
                ,NJICB2 NJICB2
                ,NJICB3 NJICB3
                ,NJICB4 NJICB4
                ,NJICB5 NJICB5
                ,NJINVP NJINVP
                ,NJPRRB NJPRRB
                ,NJBCIR NJBCIR
                ,NJICBC NJICBC
                ,NJPOST GLPostedCode
                ,NJSOTY NJSOTY
                ,CAST(NJADSC/100 AS DECIMAL(18,2)) NJADSC
                ,NJODOC NJODOC
                ,NJODCT NJODCT
                ,NJOSFX NJOSFX
                ,NJKCO CompanyKey
                ,NJOKCO NJOKCO
                ,NJLSVR LeaseVersion
                ,NJBCI3 BillingControlIDThird
                ,NJVINV SupplierInvoiceNumber
                ,NJURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NJURDT) UserReservedDate
                ,CAST(NJURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NJURAB UserReservedNumber
                ,NJURRF UserReservedReference
                ,NJUSER UserId
                ,NJPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NJUPMJ) DateUpdated
                ,NJUPMT TimeLastUpdated
                ,NJJOBN WorkStationId
                ,NJLEROUA NJLEROUA
                ,NJNUMB NJNUMB
                ,NJLZNPA NJLZNPA
                
FROM datalake_dev.pre_governed.jde_f1511b_2976 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
