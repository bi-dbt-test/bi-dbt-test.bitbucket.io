
  create or replace  view datalake_dev.governed.JDE_TENANT_SALES_HISTORY  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1541b_1964)
SELECT N8DOCO DocumentOrderInvoice
                ,N8MCU BusinessUnit
                ,N8UNIT Unit
                ,N8DBAN N8DBAN
                ,N8STNR N8STNR
                ,N8PRDC N8PRDC
                ,N8CTRY Century
                ,N8YR CalendarYear
                ,N8AN8 AddressNumber
                ,N8CO Company
                ,N8MCUS CostCenterSubsequent
                ,N8ULI N8ULI
                ,N8ULI2 N8ULI2
                ,CAST(N8SH01/100 AS DECIMAL(18,2)) SalesHistoryDollars001
                ,CAST(N8SH02/100 AS DECIMAL(18,2)) SalesHistoryDollars002
                ,CAST(N8SH03/100 AS DECIMAL(18,2)) SalesHistoryDollars003
                ,CAST(N8SH04/100 AS DECIMAL(18,2)) SalesHistoryDollars004
                ,CAST(N8SH05/100 AS DECIMAL(18,2)) SalesHistoryDollars005
                ,CAST(N8SH06/100 AS DECIMAL(18,2)) SalesHistoryDollars006
                ,CAST(N8SH07/100 AS DECIMAL(18,2)) SalesHistoryDollars007
                ,CAST(N8SH08/100 AS DECIMAL(18,2)) SalesHistoryDollars008
                ,CAST(N8SH09/100 AS DECIMAL(18,2)) SalesHistoryDollars009
                ,CAST(N8SH10/100 AS DECIMAL(18,2)) SalesHistoryDollars010
                ,CAST(N8SH11/100 AS DECIMAL(18,2)) SalesHistoryDollars011
                ,CAST(N8SH12/100 AS DECIMAL(18,2)) SalesHistoryDollars012
                ,N8RM11 ReprtCdePropertyMg11
                ,N8RM12 ReprtCdePropertyMg12
                ,N8RM13 N8RM13
                ,N8RM14 N8RM14
                ,N8RM15 N8RM15
                ,N8RM21 ReprtCdePropertyMg21
                ,N8RM22 ReprtCdePropertyMg22
                ,N8RM23 ReprtCdePropertyMg23
                ,N8RM24 N8RM24
                ,N8RM25 N8RM25
                ,N8FLOR PropmanFloor
                ,N8URCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(N8URDT) UserReservedDate
                ,CAST(N8URAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,N8URRF UserReservedReference
                ,N8USER UserId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(N8UPMJ) DateUpdated
                ,N8PID ProgramId
                ,N8JOBN WorkStationId
                ,N8UPMT TimeLastUpdated
                ,N8TORG TransactionOriginator
                ,SEMANTIC.JDE_JULIAN_TO_DATE(N8ENTJ) N8ENTJ
                ,N8CRCD CurrencyCodeFrom
                ,N8CRR N8CRR
                ,N8CRRM N8CRRM
                
FROM datalake_dev.pre_governed.jde_f1541b_1964 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
