
  create or replace  view datalake_dev.governed.JDE_ADDRESS_BOOK_WHOS_WHO  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0111_1923)
SELECT WWAN8 AddressNumber
                ,WWIDLN LineNumberID
                ,WWDSS5 WWDSS5
                ,WWMLNM WWMLNM
                ,WWATTL WWATTL
                ,WWREM1 WWREM1
                ,WWSLNM WWSLNM
                ,WWALPH NameAlpha
                ,WWDC DescripCompressed
                ,WWGNNM WWGNNM
                ,WWMDNM WWMDNM
                ,WWSRNM WWSRNM
                ,WWTYC WWTYC
                ,WWW001 WWW001
                ,WWW002 WWW002
                ,WWW003 WWW003
                ,WWW004 WWW004
                ,WWW005 WWW005
                ,WWW006 WWW006
                ,WWW007 WWW007
                ,WWW008 WWW008
                ,WWW009 WWW009
                ,WWW010 WWW010
                ,WWMLN1 WWMLN1
                ,WWALP1 WWALP1
                ,WWUSER UserId
                ,WWPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(WWUPMJ) DateUpdated
                ,WWJOBN WorkStationId
                ,WWUPMT TimeLastUpdated
                ,WWNTYP WWNTYP
                ,WWNICK WWNICK
                ,WWGEND WWGEND
                ,WWDDATE WWDDATE
                ,WWDMON WWDMON
                ,WWDYR WWDYR
                ,WWWN001 WWWN001
                ,WWWN002 WWWN002
                ,WWWN003 WWWN003
                ,WWWN004 WWWN004
                ,WWWN005 WWWN005
                ,WWWN006 WWWN006
                ,WWWN007 WWWN007
                ,WWWN008 WWWN008
                ,WWWN009 WWWN009
                ,WWWN010 WWWN010
                ,WWFUCO WWFUCO
                ,WWPCM WWPCM
                ,WWPCF WWPCF
                ,WWACTIN WWACTIN
                ,WWCFRGUID WWCFRGUID
                ,WWSYNCS SynchronizationStatus
                ,WWCAAD ServerStatus
                
FROM datalake_dev.pre_governed.jde_f0111_1923 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
