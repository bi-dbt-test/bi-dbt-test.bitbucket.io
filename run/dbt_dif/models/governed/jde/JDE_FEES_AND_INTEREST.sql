
  create or replace  view datalake_dev.governed.JDE_FEES_AND_INTEREST  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1525b_1940)
SELECT NMFETP NMFETP
                ,NMDOCO DocumentOrderInvoice
                ,NMAN8 AddressNumber
                ,NMMCU BusinessUnit
                ,NMGLC BillCode
                ,CAST(NMLNID/100 AS DECIMAL(18,2)) LineNumber
                ,NMAGSN NMAGSN
                ,NMFFC NMFFC
                ,CAST(NMAG/100 AS DECIMAL(18,2)) AmountGross
                ,CAST(NMFERT/100 AS DECIMAL(18,2)) NMFERT
                ,NMRTSC NMRTSC
                ,NMAAPF NMAAPF
                ,NMBRCD NMBRCD
                ,NMFISD NMFISD
                ,NMNGP NMNGP
                ,NMGPT NMGPT
                ,NMRSDC NMRSDC
                ,NMMXP NMMXP
                ,NMMPT NMMPT
                ,CAST(NMCMXA/100 AS DECIMAL(18,2)) NMCMXA
                ,CAST(NMCMXP/100 AS DECIMAL(18,2)) NMCMXP
                ,CAST(NMHMXA/100 AS DECIMAL(18,2)) NMHMXA
                ,CAST(NMHMXP/100 AS DECIMAL(18,2)) NMHMXP
                ,NMCOPF NMCOPF
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NMEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NMEFTE) DateEndingEffective
                ,NMBCI BillingControlId
                ,NMRAIF NMRAIF
                ,NMPPCF NMPPCF
                ,NMSLEV NMSLEV
                ,NMPTC NMPTC
                ,NMTXA1 TaxArea1
                ,NMEXR1 TaxExplanationCode1
                ,NMURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NMURDT) UserReservedDate
                ,CAST(NMURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NMURAB UserReservedNumber
                ,NMURRF UserReservedReference
                ,NMUSER UserId
                ,NMPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NMUPMJ) DateUpdated
                ,NMUPMT TimeLastUpdated
                ,NMJOBN WorkStationId
                ,NMCRCD CurrencyCodeFrom
                ,NMCRRM NMCRRM
                
FROM datalake_dev.pre_governed.jde_f1525b_1940 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
