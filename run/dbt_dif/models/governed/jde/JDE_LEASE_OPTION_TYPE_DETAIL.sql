
  create or replace  view datalake_dev.governed.JDE_LEASE_OPTION_TYPE_DETAIL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f157012_1983)
SELECT NCOPTY LeaseOptionType
                ,NCODET OptionTypeDetail
                ,NCDTLD OptionDetailDescription
                ,NCSTDO StandardOption
                ,NCSTDL StandardOptionLevel
                ,NCAN8 AddressNumber
                ,NCLSET LeaseType
                ,NCRECO NCRECO
                ,NCURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NCURDT) UserReservedDate
                ,CAST(NCURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NCURAB UserReservedNumber
                ,NCURRF UserReservedReference
                ,NCUSER UserId
                ,NCPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NCUPMJ) DateUpdated
                ,NCUPMT TimeLastUpdated
                ,NCJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NCENTJ) NCENTJ
                ,NCTORG TransactionOriginator
                
FROM datalake_dev.pre_governed.jde_f157012_1983 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
