
  create or replace  view datalake_dev.governed.JDE_SALES_MASTER_TABLE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h501_1939)
SELECT SMHBMCUS Community
                ,SMHBLOT HomeBuilderLotNumber
                ,SMCSSEQ CurrentSalesSequence
                ,SMACTVSEQ SMACTVSEQ
                ,SMMCU BusinessUnit
                ,SMHBAREA HomeBuilderArea
                ,SMCPHASE Phase
                ,SMBYR BuyerNumberA
                ,SMMLTBYR SMMLTBYR
                ,SMHBPLAN Plan
                ,SMHBELEV Elevation
                ,SMSWING SMSWING
                ,SMSALLST SalesActivityCodeLast
                ,SMSALNXT SalesActivityCodeNext
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMDCG) SMDCG
                ,SMCONACT SMCONACT
                ,SMHBSCS SalesContractStatus
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMSDJ) SalesDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMSRPDJ) SMSRPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMSRDJ) SMSRDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCAPRVL) SMCAPRVL
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMECDJ) SMECDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCDJ) CloseDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMEDJ) SMEDJ
                ,SMCSRCD SMCSRCD
                ,SMCSN1 SMCSN1
                ,SMCSN2 SMCSN2
                ,SMCSN3 SMCSN3
                ,CAST(SMBHPRICE/100 AS DECIMAL(18,2)) BaseHouseSalesPrice
                ,CAST(SMLOTPREM/100 AS DECIMAL(18,2)) SMLOTPREM
                ,CAST(SMINCA1/100 AS DECIMAL(18,2)) SMINCA1
                ,CAST(SMINCA2/100 AS DECIMAL(18,2)) SMINCA2
                ,CAST(SMINCA3/100 AS DECIMAL(18,2)) SMINCA3
                ,CAST(SMUPG/100 AS DECIMAL(18,2)) SMUPG
                ,CAST(SMNETBP/100 AS DECIMAL(18,2)) NetBasePrice
                ,CAST(SMOPRV1/100 AS DECIMAL(18,2)) SMOPRV1
                ,CAST(SMOPRV2/100 AS DECIMAL(18,2)) SMOPRV2
                ,CAST(SMOPRV3/100 AS DECIMAL(18,2)) SMOPRV3
                ,CAST(SMOPRV4/100 AS DECIMAL(18,2)) SMOPRV4
                ,CAST(SMOPRV5/100 AS DECIMAL(18,2)) SMOPRV5
                ,CAST(SMOPRV6/100 AS DECIMAL(18,2)) SMOPRV6
                ,CAST(SMOPRV7/100 AS DECIMAL(18,2)) SMOPRV7
                ,CAST(SMOPRV8/100 AS DECIMAL(18,2)) SMOPRV8
                ,CAST(SMOPRV9/100 AS DECIMAL(18,2)) SMOPRV9
                ,CAST(SMOPINCA/100 AS DECIMAL(18,2)) SMOPINCA
                ,CAST(SMNETOPT/100 AS DECIMAL(18,2)) SMNETOPT
                ,CAST(SMTOTSAL/100 AS DECIMAL(18,2)) TotalSalesPrice
                ,SMANSM01 SMANSM01
                ,SMANSM02 SMANSM02
                ,SMANSM03 SMANSM03
                ,SMANSM04 SMANSM04
                ,SMANSM05 SMANSM05
                ,SMANSM06 SMANSM06
                ,SMANSM07 SMANSM07
                ,SMANSM08 SMANSM08
                ,SMANSM09 SMANSM09
                ,SMANSM10 SMANSM10
                ,CAST(SMCMPCT1/100 AS DECIMAL(18,2)) SMCMPCT1
                ,CAST(SMCMPCT2/100 AS DECIMAL(18,2)) SMCMPCT2
                ,CAST(SMCMPCT3/100 AS DECIMAL(18,2)) SMCMPCT3
                ,CAST(SMCMPCT4/100 AS DECIMAL(18,2)) SMCMPCT4
                ,CAST(SMCMPCT5/100 AS DECIMAL(18,2)) SMCMPCT5
                ,CAST(SMCMPCT6/100 AS DECIMAL(18,2)) SMCMPCT6
                ,CAST(SMCMPCT7/100 AS DECIMAL(18,2)) SMCMPCT7
                ,CAST(SMCMPCT8/100 AS DECIMAL(18,2)) SMCMPCT8
                ,CAST(SMCMPCT9/100 AS DECIMAL(18,2)) SMCMPCT9
                ,CAST(SMCMPCT10/100 AS DECIMAL(18,2)) SMCMPCT10
                ,CAST(SMCM1/100 AS DECIMAL(18,2)) SMCM1
                ,CAST(SMCM2/100 AS DECIMAL(18,2)) SMCM2
                ,CAST(SMCM3/100 AS DECIMAL(18,2)) SMCM3
                ,SMCM4 SMCM4
                ,CAST(SMCM5/100 AS DECIMAL(18,2)) SMCM5
                ,CAST(SMCM6/100 AS DECIMAL(18,2)) SMCM6
                ,CAST(SMCM7/100 AS DECIMAL(18,2)) SMCM7
                ,CAST(SMCM8/100 AS DECIMAL(18,2)) SMCM8
                ,CAST(SMCM9/100 AS DECIMAL(18,2)) SMCM9
                ,CAST(SMCM10/100 AS DECIMAL(18,2)) SMCM10
                ,SMLDR SMLDR
                ,SMLOANNUM LoanNumber
                ,SMLOANTYP LoanType
                ,CAST(SMMGA/100 AS DECIMAL(18,2)) SMMGA
                ,CAST(SMINRTE/100 AS DECIMAL(18,2)) SMINRTE
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMINRLCK) SMINRLCK
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMPXD) SMPXD
                ,CAST(SMDNPT/100 AS DECIMAL(18,2)) SMDNPT
                ,CAST(SMEMD/100 AS DECIMAL(18,2)) SMEMD
                ,CAST(SMDTKN/100 AS DECIMAL(18,2)) SMDTKN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMMAD) MortgageApprovalDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMTAD) SMTAD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMLADJ) SMLADJ
                ,SMCTG SMCTG
                ,SMCTGS SMCTGS
                ,SMPTX SMPTX
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCTGJ) SMCTGJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMLRD) SMLRD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCCNVD) SMCCNVD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCLJ) SMCLJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCXD) SMCXD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCRD) SMCRD
                ,SMCN1 SMCN1
                ,SMCN2 SMCN2
                ,SMLAG SMLAG
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMDCAPPT) SMDCAPPT
                ,SMDCSTM SMDCSTM
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMDCADT) SMDCADT
                ,SMCLT SMCLT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMTEDJ) SMTEDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMTIDJ) SMTIDJ
                ,SMTLN1 SMTLN1
                ,SMTLN2 SMTLN2
                ,SMTLN3 SMTLN3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMBSWD) BuyerScheduledWalkThruDate
                ,SMBSWT SMBSWT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMBAWD) SMBAWD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD1) SMUSD1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD2) SMUSD2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD3) SMUSD3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD4) UserDate4
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD5) SMUSD5
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD6) SMUSD6
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD7) SMUSD7
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD8) SMUSD8
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD9) SMUSD9
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD10) SMUSD10
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD11) SMUSD11
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD12) SMUSD12
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD13) SMUSD13
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD14) SMUSD14
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD15) SMUSD15
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD16) SMUSD16
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD17) SMUSD17
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD18) SMUSD18
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD19) SMUSD19
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUSD20) SMUSD20
                ,CAST(SMUAMT01/100 AS DECIMAL(18,2)) SMUAMT01
                ,CAST(SMUAMT02/100 AS DECIMAL(18,2)) AmountUserDefinedAmount02
                ,CAST(SMUAMT03/100 AS DECIMAL(18,2)) AmountUserDefinedAmount03
                ,CAST(SMUAMT04/100 AS DECIMAL(18,2)) AmountUserDefinedAmount04
                ,CAST(SMUAMT05/100 AS DECIMAL(18,2)) SMUAMT05
                ,CAST(SMUAMT06/100 AS DECIMAL(18,2)) SMUAMT06
                ,CAST(SMUAMT07/100 AS DECIMAL(18,2)) SMUAMT07
                ,CAST(SMUAMT08/100 AS DECIMAL(18,2)) AmountUserDefinedAmount08
                ,CAST(SMUAMT09/100 AS DECIMAL(18,2)) AmountUserDefinedAmount09
                ,CAST(SMUAMT10/100 AS DECIMAL(18,2)) AmountUserDefinedAmount10
                ,SMSMC01 SalesMasterCategoryCode1
                ,SMSMC02 SMSMC02
                ,SMSMC03 SMSMC03
                ,SMSMC04 SMSMC04
                ,SMSMC05 SMSMC05
                ,SMSMC06 SMSMC06
                ,SMSMC07 SMSMC07
                ,SMSMC08 SMSMC08
                ,SMSMC09 SMSMC09
                ,SMSMC10 SMSMC10
                ,SMUSAN01 SMUSAN01
                ,SMUSAN02 SMUSAN02
                ,SMUSAN03 SMUSAN03
                ,SMUSAN04 SMUSAN04
                ,SMUSAN05 SMUSAN05
                ,SMHBST1 SMHBST1
                ,SMHBST2 SMHBST2
                ,SMHBST3 SMHBST3
                ,SMHBST4 SMHBST4
                ,SMHBST5 SMHBST5
                ,SMCRTU SMCRTU
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMCRTJ) SMCRTJ
                ,SMCRTT SMCRTT
                ,SMWRKSTNID SMWRKSTNID
                ,SMHBOPID SMHBOPID
                ,SMUPMB SMUPMB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(SMUPMJ) DateUpdated
                ,SMUPMT TimeLastUpdated
                ,SMJOBN WorkStationId
                ,SMPID ProgramId
                
FROM datalake_dev.pre_governed.jde_f44h501_1939 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
