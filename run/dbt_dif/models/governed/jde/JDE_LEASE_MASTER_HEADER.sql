
  create or replace  view datalake_dev.governed.JDE_LEASE_MASTER_HEADER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1501b_3590)
SELECT NEDOCO DocumentOrderInvoice
                ,NELSVR LeaseVersion
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEVREF) VersionEffectiveDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEVRED) VersionEndDate
                ,NEVRS NEVRS
                ,NEDCTO OrderType
                ,NEDL01 Description
                ,NEAN8 AddressNumber
                ,NEAN8J AddNoAlternatePayee
                ,NEANSA AddressNumberManager
                ,NEAN8B NEAN8B
                ,NEAN8P AddNoAltPaySource
                ,NELSST LeaseStatus
                ,NEPMTC TermOfLease
                ,NELSCD LeaseCondition
                ,NELSET LeaseType
                ,NEOWLS OwnOrLease
                ,NEMGTF NEMGTF
                ,NETRAR PaymentTermsAR
                ,NEINVP NEINVP
                ,NESTMP NESTMP
                ,NESECN NESECN
                ,NEDSPN NEDSPN
                ,NESIC NESIC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEEFTB) DateBeginningEffective
                ,NESTMB NESTMB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEEFTE) DateEndingEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEDSR) DateRentStart
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEMP01) DateProperty001
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEMP02) NEMP02
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEOLED) DateOriginalLeaseEndDate
                ,NECOMG NECOMG
                ,NERYRB RentYearBeginMonth
                ,NELYRB LeaseYearBeginningMth
                ,CAST(NEWTDL/100 AS DECIMAL(18,2)) LeaseRenewalTerm
                ,NEPLNM NEPLNM
                ,NESBLI NESBLI
                ,NECO Company
                ,NECTRY Century
                ,CAST(NEALP/100 AS DECIMAL(18,2)) NEALP
                ,CAST(NETOTD/100 AS DECIMAL(18,2)) NETOTD
                ,NEAVD NEAVD
                ,NEDLI NEDLI
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEDLP) NEDLP
                ,NEURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEURDT) UserReservedDate
                ,CAST(NEURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NEURAB UserReservedNumber
                ,NEOLSE NEOLSE
                ,NEURRF UserReservedReference
                ,NEUSER UserId
                ,NEPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEUPMJ) DateUpdated
                ,NEUPMT TimeLastUpdated
                ,NEJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NEENTJ) NEENTJ
                ,NETORG TransactionOriginator
                ,NECRR NECRR
                ,NEDCRF NEDCRF
                ,NECRCD CurrencyCodeFrom
                ,NECRRM NECRRM
                ,NELELC NELELC
                ,NELERCEO NELERCEO
                ,NELEBR NELEBR
                ,NELEHC01 NELEHC01
                ,NELEHC02 NELEHC02
                ,NELEHC03 NELEHC03
                ,NELEHC04 NELEHC04
                ,NELEHC05 NELEHC05
                ,NELEETF NELEETF
                
FROM datalake_dev.pre_governed.jde_f1501b_3590 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
