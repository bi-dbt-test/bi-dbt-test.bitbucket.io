
  create or replace  view datalake_dev.governed.JDE_PURCHASE_ORDER_RECIEVER_FILE  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f43121_1938)
SELECT PRMATC PRMATC
                ,PRAN8 AddressNumber
                ,PRKCOO PRKCOO
                ,PRDOCO DocumentOrderInvoice
                ,PRDCTO OrderType
                ,PRSFXO PRSFXO
                ,CAST(PRLNID/100 AS DECIMAL(18,2)) LineNumber
                ,PRNLIN PRNLIN
                ,PRALIN PRALIN
                ,PRRTBY PRRTBY
                ,PRDMCT PRDMCT
                ,PRDMCS PRDMCS
                ,PRBALU PRBALU
                ,PRITM PRITM
                ,PRLITM PRLITM
                ,PRAITM PRAITM
                ,PRIMCU PRIMCU
                ,PRLOCN PRLOCN
                ,PRLOTN PRLOTN
                ,PRLOTG PRLOTG
                ,CAST(PRLOTP/100 AS DECIMAL(18,2)) PRLOTP
                ,PRVRMK PRVRMK
                ,PRCNID PRCNID
                ,PRNXTR PRNXTR
                ,PRLTTR PRLTTR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRTRDJ) PRTRDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRRCDJ) PRRCDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRDRQJ) PRDRQJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRPDDJ) PRPDDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PROPDJ) PROPDJ
                ,PRRCD PRRCD
                ,PRVINV SupplierInvoiceNumber
                ,PRPTC PRPTC
                ,PRPST PayStatusCode
                ,PRLAND PRLAND
                ,PRPRP5 PurchasingReportCode5
                ,PRLVLA PRLVLA
                ,PRILOG PRILOG
                ,PRDLOG PRDLOG
                ,PRRTGC PRRTGC
                ,PRRCPF PRRCPF
                ,PRAVCH CodeAutomaticVoucher
                ,PRLNTY PRLNTY
                ,PRGLC BillCode
                ,PRMCU BusinessUnit
                ,PRCO Company
                ,PRAID AccountID
                ,PRANI AcctNoInputMode
                ,PROMCU PROMCU
                ,PROBJ ObjectAccount
                ,PRSUB Subsidiary
                ,PRASID PRASID
                ,PRSBL Subledger
                ,PRSBLT SubledgerType
                ,PRKCO CompanyKey
                ,PRDOC DocumentVoucher
                ,PRDCT DocumentType
                ,PRSFX DocumentPayItem
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRDGL) PRDGL
                ,PRJELN JournalEntryLineNo
                ,PRVANI PRVANI
                ,PRTX PRTX
                ,PREXR1 TaxExplanationCode1
                ,PRTXA1 TaxArea1
                ,PRUOM PRUOM
                ,PRUORG PRUORG
                ,PRUPTD PRUPTD
                ,PRUOPN PRUOPN
                ,PRUREC PRUREC
                ,PRUCLO PRUCLO
                ,PRQTYS PRQTYS
                ,PRQTYR PRQTYR
                ,PRQTYW PRQTYW
                ,PRQTYC PRQTYC
                ,PRQTYJ PRQTYJ
                ,PRQTYA PRQTYA
                ,PRUOM3 PRUOM3
                ,CAST(PRPRRC/100 AS DECIMAL(18,2)) PRPRRC
                ,CAST(PRECST/100 AS DECIMAL(18,2)) PRECST
                ,CAST(PRAPTD/100 AS DECIMAL(18,2)) PRAPTD
                ,CAST(PRAOPN/100 AS DECIMAL(18,2)) PRAOPN
                ,CAST(PRAREC/100 AS DECIMAL(18,2)) PRAREC
                ,CAST(PRACLO/100 AS DECIMAL(18,2)) PRACLO
                ,CAST(PRAVCO/100 AS DECIMAL(18,2)) PRAVCO
                ,CAST(PRVARC/100 AS DECIMAL(18,2)) PRVARC
                ,PRCRCD CurrencyCodeFrom
                ,PRCRR PRCRR
                ,CAST(PRFRRC/100 AS DECIMAL(18,2)) PRFRRC
                ,CAST(PRFEC/100 AS DECIMAL(18,2)) PRFEC
                ,CAST(PRFAP/100 AS DECIMAL(18,2)) PRFAP
                ,CAST(PRFAPT/100 AS DECIMAL(18,2)) PRFAPT
                ,CAST(PRFREC/100 AS DECIMAL(18,2)) PRFREC
                ,CAST(PRFCLO/100 AS DECIMAL(18,2)) PRFCLO
                ,PRURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRURDT) UserReservedDate
                ,CAST(PRURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,PRURAB UserReservedNumber
                ,PRURRF UserReservedReference
                ,PRTORG TransactionOriginator
                ,PRUSER UserId
                ,PRPID ProgramId
                ,PRJOBN WorkStationId
                ,PRTERM PRTERM
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PRUPMJ) DateUpdated
                ,PRTDAY PRTDAY
                ,PRBCRC PRBCRC
                ,CAST(PRSTAM/100 AS DECIMAL(18,2)) PRSTAM
                ,CAST(PRCTAM/100 AS DECIMAL(18,2)) PRCTAM
                ,PRMACT PRMACT
                ,PRPLT PRPLT
                ,PRPAK PRPAK
                ,PRSCCN PRSCCN
                ,PRSCCQ PRSCCQ
                ,PRSCUM PRSCUM
                ,PRUPCN PRUPCN
                ,PRUPQT PRUPQT
                ,PRUPUM PRUPUM
                ,PRSHPN PRSHPN
                ,PRVR01 PRVR01
                ,PRVR02 PRVR02
                ,PRFUF3 PRFUF3
                ,PRFUF4 PRFUF4
                ,PRFUF5 PRFUF5
                ,PRFUF6 PRFUF6
                ,PRMERL PRMERL
                ,CAST(PRKTLN/100 AS DECIMAL(18,2)) PRKTLN
                ,CAST(PRCPNT/100 AS DECIMAL(18,2)) PRCPNT
                ,PRVR04 PRVR04
                ,PRVR05 PRVR05
                ,PRUKID UniqueKeyIDInternal
                ,PRUOM2 PRUOM2
                ,PRDUAL PRDUAL
                ,PRSQOR PRSQOR
                ,PRXDCK PRXDCK
                ,PRPOE PRPOE
                ,PRVR03 PRVR03
                ,PRLDNM PRLDNM
                ,PRCTSN PRCTSN
                ,PRPROV PRPROV
                ,PRASN PriceAdjustmentScheduleN
                ,PRPRFLG1 PRPRFLG1
                ,PRPRFLG2 PRPRFLG2
                ,PRMOADJ PRMOADJ
                ,PRPS09 PRPS09
                ,PRQTYPY PRQTYPY
                ,PRWVID PRWVID
                ,PRCNTRTID PRCNTRTID
                ,PRCNTRTDID PRCNTRTDID
                ,PRREVFLG PRREVFLG
                ,PRAPPFLG PRAPPFLG
                ,PRREVAN8 PRREVAN8
                ,PRAPPAN8 PRAPPAN8
                ,PRWOPID PRWOPID
                ,PRWVTY PRWVTY
                ,PRJBCD PRJBCD
                ,CAST(PRSRQTY/100 AS DECIMAL(18,2)) PRSRQTY
                ,PRSRUOM PRSRUOM
                ,PRNBRU PRNBRU
                ,PRNEWR PRNEWR
                ,CAST(PRSRRC/100 AS DECIMAL(18,2)) PRSRRC
                ,CAST(PRFSRRC/100 AS DECIMAL(18,2)) PRFSRRC
                ,PRPMPN PRPMPN
                ,PRPNS PRPNS
                
FROM datalake_dev.pre_governed.jde_f43121_1938 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
