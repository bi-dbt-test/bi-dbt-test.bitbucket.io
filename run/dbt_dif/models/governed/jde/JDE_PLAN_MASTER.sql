
  create or replace  view datalake_dev.governed.JDE_PLAN_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f44h301_1932)
SELECT PMHBMCUS Community
                ,PMCPHASE Phase
                ,PMHBPLAN Plan
                ,PMHBELEV Elevation
                ,PMHBAREA HomeBuilderArea
                ,CAST(PMBSP/100 AS DECIMAL(18,2)) AmountBasePrice
                ,CAST(PMBPP/100 AS DECIMAL(18,2)) AmountBasePricePrevious
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMSPJ) SalesPriceEffectiveDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMEXDE) PMEXDE
                ,PMMCUTJ PMMCUTJ
                ,PMSCHTMPT PMSCHTMPT
                ,PMDL01 Description
                ,PMDL02 Description02
                ,PMDL03 PMDL03
                ,PMPMPT PlanMasterProductType
                ,PMPMPG PlanMasterProductGroup
                ,CAST(PMSQFEET/100 AS DECIMAL(18,2)) PMSQFEET
                ,PMDSC1 PMDSC1
                ,PMDSC2 PMDSC2
                ,PMPLNMX PMPLNMX
                ,PMPLNMXRL PMPLNMXRL
                ,CAST(PMESTDISC1/100 AS DECIMAL(18,2)) PMESTDISC1
                ,CAST(PMESTDISC2/100 AS DECIMAL(18,2)) PMESTDISC2
                ,CAST(PMESTDISC3/100 AS DECIMAL(18,2)) PMESTDISC3
                ,CAST(PMOPTMGN1/100 AS DECIMAL(18,2)) PMOPTMGN1
                ,CAST(PMOPTMGN2/100 AS DECIMAL(18,2)) PMOPTMGN2
                ,CAST(PMOPTMGN3/100 AS DECIMAL(18,2)) PMOPTMGN3
                ,CAST(PMOPTMGN4/100 AS DECIMAL(18,2)) PMOPTMGN4
                ,CAST(PMOPTMGN5/100 AS DECIMAL(18,2)) PMOPTMGN5
                ,CAST(PMESTOPR1/100 AS DECIMAL(18,2)) PMESTOPR1
                ,CAST(PMESTOPR2/100 AS DECIMAL(18,2)) PMESTOPR2
                ,CAST(PMESTOPR3/100 AS DECIMAL(18,2)) PMESTOPR3
                ,CAST(PMESTOPR4/100 AS DECIMAL(18,2)) PMESTOPR4
                ,CAST(PMESTOPR5/100 AS DECIMAL(18,2)) PMESTOPR5
                ,CAST(PMESTOPIN/100 AS DECIMAL(18,2)) PMESTOPIN
                ,CAST(PMESTPLANC/100 AS DECIMAL(18,2)) PMESTPLANC
                ,CAST(PMCSTAMT01/100 AS DECIMAL(18,2)) PMCSTAMT01
                ,CAST(PMCSTAMT02/100 AS DECIMAL(18,2)) PMCSTAMT02
                ,CAST(PMCSTAMT03/100 AS DECIMAL(18,2)) PMCSTAMT03
                ,CAST(PMCSTAMT04/100 AS DECIMAL(18,2)) PMCSTAMT04
                ,CAST(PMCSTAMT05/100 AS DECIMAL(18,2)) PMCSTAMT05
                ,CAST(PMCSTAMT06/100 AS DECIMAL(18,2)) PMCSTAMT06
                ,CAST(PMCSTAMT07/100 AS DECIMAL(18,2)) PMCSTAMT07
                ,CAST(PMCSTAMT08/100 AS DECIMAL(18,2)) PMCSTAMT08
                ,CAST(PMCSTAMT09/100 AS DECIMAL(18,2)) PMCSTAMT09
                ,CAST(PMCSTAMT10/100 AS DECIMAL(18,2)) PMCSTAMT10
                ,CAST(PMESTWKSL/100 AS DECIMAL(18,2)) PMESTWKSL
                ,PMPLC01 PMPLC01
                ,PMPLC02 PMPLC02
                ,PMPLC03 PMPLC03
                ,PMPLC04 PMPLC04
                ,PMPLC05 PMPLC05
                ,PMPLC06 PMPLC06
                ,PMPLC07 PMPLC07
                ,PMPLC08 PMPLC08
                ,PMPLC09 PMPLC09
                ,PMPLC10 PMPLC10
                ,PMPLC11 PMPLC11
                ,PMPLC12 PMPLC12
                ,PMPLC13 PMPLC13
                ,PMPLC14 PMPLC14
                ,PMPLC15 PMPLC15
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMUSD1) PMUSD1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMUSD2) PMUSD2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMUSD3) PMUSD3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMUSD4) UserDate4
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMUSD5) PMUSD5
                ,PMCRTU PMCRTU
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMCRTJ) PMCRTJ
                ,PMCRTT PMCRTT
                ,PMWRKSTNID PMWRKSTNID
                ,PMHBOPID PMHBOPID
                ,PMUPMB PMUPMB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(PMUPMJ) DateUpdated
                ,PMUPMT TimeLastUpdated
                ,PMJOBN WorkStationId
                ,PMPID ProgramId
                ,PMHBST1 PMHBST1
                ,PMHBST2 PMHBST2
                ,PMHBST3 PMHBST3
                ,PMHBST4 PMHBST4
                ,PMHBST5 PMHBST5
                
FROM datalake_dev.pre_governed.jde_f44h301_1932 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
