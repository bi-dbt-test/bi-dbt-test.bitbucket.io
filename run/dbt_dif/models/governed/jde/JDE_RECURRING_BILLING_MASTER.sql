
  create or replace  view datalake_dev.governed.JDE_RECURRING_BILLING_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1502b_2974)
SELECT NFDOCO DocumentOrderInvoice
                ,NFLSET LeaseType
                ,CAST(NFLNID/100 AS DECIMAL(18,2)) LineNumber
                ,NFBLGR BillingGroup
                ,NFTRAN NFTRAN
                ,NFGLC BillCode
                ,NFRMK NameRemark
                ,CAST(NFAG/100 AS DECIMAL(18,2)) AmountGross
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NFEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NFEFTE) DateEndingEffective
                ,NFBLFC BillingFrequencyCode
                ,NFBF01 BillingPeriod01
                ,NFBF02 BillingPeriod02
                ,NFBF03 BillingPeriod03
                ,NFBF04 BillingPeriod04
                ,NFBF05 BillingPeriod05
                ,NFBF06 BillingPeriod06
                ,NFBF07 BillingPeriod07
                ,NFBF08 BillingPeriod08
                ,NFBF09 BillingPeriod09
                ,NFBF10 BillingPeriod10
                ,NFBF11 BillingPeriod11
                ,NFBF12 BillingPeriod12
                ,NFBF13 BillingPeriod13
                ,NFMCU BusinessUnit
                ,NFSBL Subledger
                ,NFAN8J AddNoAlternatePayee
                ,NFUNIT Unit
                ,NFSMCU NFSMCU
                ,NFTRAR PaymentTermsAR
                ,NFSEPI NFSEPI
                ,NFITMG NFITMG
                ,NFSUSP SuspendCode
                ,NFDEAL DealNumber
                ,NFRN01 NFRN01
                ,NFRN02 NFRN02
                ,NFRN03 NFRN03
                ,NFRN04 NFRN04
                ,NFRN05 NFRN05
                ,NFAGSN NFAGSN
                ,NFGENT NFGENT
                ,NFBCI BillingControlId
                ,NFCRCD CurrencyCodeFrom
                ,CAST(NFACR/100 AS DECIMAL(18,2)) NFACR
                ,NFTXA1 TaxArea1
                ,NFEXR1 TaxExplanationCode1
                ,CAST(NFSTAM/100 AS DECIMAL(18,2)) NFSTAM
                ,CAST(NFATXN/100 AS DECIMAL(18,2)) NFATXN
                ,CAST(NFATXA/100 AS DECIMAL(18,2)) NFATXA
                ,NFSBLT SubledgerType
                ,NFCRRM NFCRRM
                ,CAST(NFCTAM/100 AS DECIMAL(18,2)) NFCTAM
                ,CAST(NFCTXA/100 AS DECIMAL(18,2)) NFCTXA
                ,CAST(NFCTXN/100 AS DECIMAL(18,2)) NFCTXN
                ,NFYT NFYT
                ,NFLRYF NFLRYF
                ,NFLRYT NFLRYT
                ,NFINVP NFINVP
                ,NFDBAN NFDBAN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NFSUDT) DateSuspended
                ,NFPPNM NFPPNM
                ,NFALTB NFALTB
                ,CAST(NFAPSF/100 AS DECIMAL(18,2)) AnnualRatePerSquare
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NFBTDT) NFBTDT
                ,CAST(NFRNTA/100 AS DECIMAL(18,2)) UnitRentableAreaSqFt
                ,NFUNGR NFUNGR
                ,NFLSVR LeaseVersion
                ,NFBCI3 BillingControlIDThird
                ,NFVRSC VersionSuspendCode
                ,NFURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NFURDT) UserReservedDate
                ,CAST(NFURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NFURAB UserReservedNumber
                ,NFURRF UserReservedReference
                ,NFUSER UserId
                ,NFPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NFUPMJ) DateUpdated
                ,NFUPMT TimeLastUpdated
                ,NFJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NFENTJ) NFENTJ
                ,NFTORG TransactionOriginator
                ,NFCRR NFCRR
                ,NFLAFF NFLAFF
                ,NFLELL NFLELL
                ,NFLEPM NFLEPM
                ,NFNUMB NFNUMB
                ,NFLZNPA NFLZNPA
                
FROM datalake_dev.pre_governed.jde_f1502b_2974 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
