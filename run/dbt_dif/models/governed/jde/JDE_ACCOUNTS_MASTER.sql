
  create or replace  view datalake_dev.governed.JDE_ACCOUNTS_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0901_3589)
SELECT GMCO Company
                ,GMAID AccountID
                ,GMMCU BusinessUnit
                ,GMOBJ ObjectAccount
                ,GMSUB Subsidiary
                ,GMANS GMANS
                ,GMDL01 Description
                ,GMLDA LevelOfDetailAcctCde
                ,GMBPC GMBPC
                ,GMPEC PostingEdit
                ,GMBILL GMBILL
                ,GMCRCD CurrencyCodeFrom
                ,GMUM UnitOfMeasure
                ,GMR001 GMR001
                ,GMR002 GMR002
                ,GMR003 GMR003
                ,GMR004 CategoryCodeGl004
                ,GMR005 CategoryCodeGl005
                ,GMR006 CategoryCodeGl006
                ,GMR007 GMR007
                ,GMR008 CategoryCodeGl008
                ,GMR009 CategoryCodeGl009
                ,GMR010 CategoryCodeGl010
                ,GMR011 CategoryCodeGl011
                ,GMR012 CategoryCodeGl012
                ,GMR013 CategoryCodeGl013
                ,GMR014 CategoryCodeGl014
                ,GMR015 CategoryCodeGl015
                ,GMR016 CategoryCodeGl016
                ,GMR017 CategoryCodeGl017
                ,GMR018 CategoryCodeGl018
                ,GMR019 CategoryCodeGl019
                ,GMR020 GMR020
                ,GMR021 GMR021
                ,GMR022 GMR022
                ,GMR023 GMR023
                ,GMOBJA ObjectAccountAlternate
                ,GMSUBA GMSUBA
                ,GMWCMP GMWCMP
                ,GMCCT GMCCT
                ,GMERC GMERC
                ,GMHTC GMHTC
                ,GMQLDA GMQLDA
                ,GMCCC GMCCC
                ,GMFMOD GMFMOD
                ,GMUSER UserId
                ,GMPID ProgramId
                ,GMJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(GMUPMJ) DateUpdated
                ,GMUPMT TimeLastUpdated
                ,GMCEC1 GMCEC1
                ,GMCEC2 GMCEC2
                ,GMCEC3 GMCEC3
                ,GMCEC4 GMCEC4
                ,GMIEC GMIEC
                ,GMFPEC GMFPEC
                ,GMSTPC GMSTPC
                ,GMTXGL GMTXGL
                ,GMTOBJ GMTOBJ
                ,GMTSUB GMTSUB
                ,GMPRGF GMPRGF
                ,GMTXA1 TaxArea1
                ,GMR024 GMR024
                ,GMR025 GMR025
                ,GMR026 GMR026
                ,GMR027 GMR027
                ,GMR028 GMR028
                ,GMR029 GMR029
                ,GMR030 GMR030
                ,GMR031 GMR031
                ,GMR032 GMR032
                ,GMR033 GMR033
                ,GMR034 GMR034
                ,GMR035 GMR035
                ,GMR036 GMR036
                ,GMR037 GMR037
                ,GMR038 GMR038
                ,GMR039 GMR039
                ,GMR040 GMR040
                ,GMR041 GMR041
                ,GMR042 GMR042
                ,GMR043 GMR043
                ,GMADJENT GMADJENT
                ,GMUAFL GMUAFL
                
FROM datalake_dev.pre_governed.jde_f0901_3589 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
