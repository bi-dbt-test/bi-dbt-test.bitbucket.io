
  create or replace  view datalake_dev.governed.JDE_RECEIPTS_HEADER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f03b13_1968)
SELECT RYPYID PaymentID
                ,RYCKNU CkNumber
                ,RYAN8 AddressNumber
                ,RYPYR RYPYR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYDMTJ) DateMatchingCheckOr
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYDGJ) DateForGLandVoucher
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYVLDT) DateValue
                ,RYPOST GLPostedCode
                ,RYISTR RYISTR
                ,RYISTC RYISTC
                ,RYEULP RYEULP
                ,RYGLC BillCode
                ,RYAID AccountID
                ,RYCTRY Century
                ,RYFY FiscalYear
                ,RYPN PeriodNumberGeneralLedger
                ,RYCO Company
                ,RYICUT BatchType
                ,RYICU BatchNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYDICJ) DateBatchJulian
                ,RYPA8 RYPA8
                ,CAST(RYCKAM/100 AS DECIMAL(18,2)) AmountCheckAmount
                ,CAST(RYAAP/100 AS DECIMAL(18,2)) AmountOpen
                ,RYBCRC RYBCRC
                ,RYCRRM RYCRRM
                ,RYCRCD CurrencyCodeFrom
                ,RYCRR RYCRR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYERDJ) RYERDJ
                ,RYCRCM RYCRCM
                ,RYCRR1 RYCRR1
                ,RYCRR2 RYCRR2
                ,CAST(RYFCAM/100 AS DECIMAL(18,2)) RYFCAM
                ,CAST(RYFAP/100 AS DECIMAL(18,2)) RYFAP
                ,RYGLBA GlBankAccount
                ,RYAM RYAM
                ,RYTYIN RYTYIN
                ,RYEXR NameRemarkExplanation
                ,RYALT6 RYALT6
                ,RYRYIN RYRYIN
                ,RYBKTR RYBKTR
                ,RYALPH NameAlpha
                ,RYDOCG DocumentNumberJE
                ,RYDCTG DocumentTypeJE
                ,RYKCOG DocumentCompanyJE
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYVDGJ) VoidDateForGLn
                ,RYVRE RYVRE
                ,RYNFVD RYNFVD
                ,RYDOCQ RYDOCQ
                ,RYDCTQ RYDCTQ
                ,RYKCOQ RYKCOQ
                ,RYICTQ RYICTQ
                ,RYICUQ RYICUQ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYDIQJ) RYDIQJ
                ,RYPSTQ RYPSTQ
                ,RYCBNK CustBankAcctNumber
                ,RYTNST BankTransitNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYDKC) DateCheckCleared
                ,RYATCS RYATCS
                ,RYEAAC RYEAAC
                ,RYEUFB RYEUFB
                ,RYECLK RYECLK
                ,RYARL RYARL
                ,RYARS RYARS
                ,RYALGO RYALGO
                ,RYVERS RYVERS
                ,RYASTA RYASTA
                ,RYDDST RYDDST
                ,RYRREF RYRREF
                ,RYRRID RYRRID
                ,RYDCDS RYDCDS
                ,RYDREG RYDREG
                ,RYURC1 RYURC1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYURDT) UserReservedDate
                ,CAST(RYURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,RYURAB UserReservedNumber
                ,RYURRF UserReservedReference
                ,RYGFL1 RYGFL1
                ,RYPRGF RYPRGF
                ,RYRNID RYRNID
                ,RYCUID RYCUID
                ,RYTORG TransactionOriginator
                ,RYUSER UserId
                ,RYPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RYUPMJ) DateUpdated
                ,RYUPMT TimeLastUpdated
                ,RYJOBN WorkStationId
                ,RYVFY RYVFY
                ,RYVFP RYVFP
                ,RYVFC RYVFC
                ,RYOMOD RYOMOD
                ,RYMIP RYMIP
                ,RYLRFL RYLRFL
                ,RYGFL2 RYGFL2
                ,RYNETTCID RYNETTCID
                ,RYNETDOC RYNETDOC
                ,RYRCND RYRCND
                ,RYR3 RYR3
                
FROM datalake_dev.pre_governed.jde_f03b13_1968 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
