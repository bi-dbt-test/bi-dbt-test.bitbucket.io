
  create or replace  view datalake_dev.governed.JDE_RECEIPTS_DETAIL  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f03b14_1972)
SELECT RZPYID PaymentID
                ,RZRC5 RZRC5
                ,RZCKNU CkNumber
                ,RZDOC DocumentVoucher
                ,RZDCT DocumentType
                ,RZKCO CompanyKey
                ,RZSFX DocumentPayItem
                ,RZAN8 AddressNumber
                ,RZDCTM DocTypeMatching
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZDMTJ) DateMatchingCheckOr
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZDGJ) DateForGLandVoucher
                ,RZPOST GLPostedCode
                ,RZGLC BillCode
                ,RZAID AccountID
                ,RZCTRY Century
                ,RZFY FiscalYear
                ,RZPN PeriodNumberGeneralLedger
                ,RZCO Company
                ,RZICUT BatchType
                ,RZICU BatchNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZDICJ) DateBatchJulian
                ,RZPA8 RZPA8
                ,RZRPID RZRPID
                ,RZRLIN RZRLIN
                ,CAST(RZPAAP/100 AS DECIMAL(18,2)) PaymntAmount
                ,CAST(RZADSC/100 AS DECIMAL(18,2)) RZADSC
                ,CAST(RZADSA/100 AS DECIMAL(18,2)) RZADSA
                ,CAST(RZAAAJ/100 AS DECIMAL(18,2)) AmountAdjustments
                ,CAST(RZECBA/100 AS DECIMAL(18,2)) RZECBA
                ,CAST(RZDDA/100 AS DECIMAL(18,2)) RZDDA
                ,RZBCRC RZBCRC
                ,RZCRRM RZCRRM
                ,RZCRCD CurrencyCodeFrom
                ,RZCRR RZCRR
                ,CAST(RZPFAP/100 AS DECIMAL(18,2)) RZPFAP
                ,CAST(RZCDS/100 AS DECIMAL(18,2)) RZCDS
                ,CAST(RZCDSA/100 AS DECIMAL(18,2)) RZCDSA
                ,CAST(RZFCHG/100 AS DECIMAL(18,2)) RZFCHG
                ,CAST(RZECBF/100 AS DECIMAL(18,2)) RZECBF
                ,CAST(RZFDA/100 AS DECIMAL(18,2)) RZFDA
                ,CAST(RZAGL/100 AS DECIMAL(18,2)) RZAGL
                ,RZAIDT RZAIDT
                ,RZTCRC RZTCRC
                ,CAST(RZTAAP/100 AS DECIMAL(18,2)) RZTAAP
                ,CAST(RZTADA/100 AS DECIMAL(18,2)) RZTADA
                ,CAST(RZTAAJ/100 AS DECIMAL(18,2)) RZTAAJ
                ,CAST(RZTCBA/100 AS DECIMAL(18,2)) RZTCBA
                ,CAST(RZTDA/100 AS DECIMAL(18,2)) RZTDA
                ,RZACRR RZACRR
                ,RZACR1 RZACR1
                ,RZACR2 RZACR2
                ,RZACRM RZACRM
                ,CAST(RZAGLA/100 AS DECIMAL(18,2)) RZAGLA
                ,RZAIDA RZAIDA
                ,RZAIDD RZAIDD
                ,RZRSCO RZRSCO
                ,RZAIDW RZAIDW
                ,RZECBR RZECBR
                ,RZGLCC RZGLCC
                ,RZAIDC RZAIDC
                ,RZDDEX RZDDEX
                ,RZDAID RZDAID
                ,RZTYIN RZTYIN
                ,RZUTIC RZUTIC
                ,RZUCTF RZUCTF
                ,RZAID2 RZAID2
                ,RZAM2 RZAM2
                ,RZMCU BusinessUnit
                ,RZSBL Subledger
                ,RZSBLT SubledgerType
                ,RZPKCO RZPKCO
                ,RZPO PurchaseOrder
                ,RZPDCT RZPDCT
                ,RZNUMB RZNUMB
                ,RZUNIT Unit
                ,RZMCU2 BusinessUnit2
                ,RZRMK NameRemark
                ,RZFNLP RZFNLP
                ,RZALT6 RZALT6
                ,RZODOC RZODOC
                ,RZODCT RZODCT
                ,RZOKCO RZOKCO
                ,RZOSFX RZOSFX
                ,RZGDOC RZGDOC
                ,RZGDCT RZGDCT
                ,RZGKCO RZGKCO
                ,RZGSFX RZGSFX
                ,RZDCTG DocumentTypeJE
                ,RZDOCG DocumentNumberJE
                ,RZKCOG DocumentCompanyJE
                ,RZCTL RZCTL
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZSMTJ) DateStatement
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZVDGJ) VoidDateForGLn
                ,RZVRE RZVRE
                ,RZNFVD RZNFVD
                ,RZHCRR RZHCRR
                ,RZISTC RZISTC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZLFCJ) RZLFCJ
                ,RZPDLT RZPDLT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZDDJ) DateNetDue
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZDDNJ) RZDDNJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZIDGJ) RZIDGJ
                ,RZDDST RZDDST
                ,RZVR01 RZVR01
                ,RZRFID RZRFID
                ,RZRIDC RZRIDC
                ,RZPRGF RZPRGF
                ,RZGFL1 RZGFL1
                ,RZRNID RZRNID
                ,RZTORG TransactionOriginator
                ,RZUSER UserId
                ,RZPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZUPMJ) DateUpdated
                ,RZUPMT TimeLastUpdated
                ,RZJOBN WorkStationId
                ,RZURAB UserReservedNumber
                ,CAST(RZURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,RZURC1 RZURC1
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZURDT) UserReservedDate
                ,RZURRF UserReservedReference
                ,RZSHPN RZSHPN
                ,RZOMOD RZOMOD
                ,RZDOCO DocumentOrderInvoice
                ,RZRASI RZRASI
                ,RZSFXO RZSFXO
                ,RZKCOO RZKCOO
                ,RZDCTO OrderType
                ,CAST(RZRAMT/100 AS DECIMAL(18,2)) RZRAMT
                ,RZLRFL RZLRFL
                ,RZGFL2 RZGFL2
                ,RZDRCO RZDRCO
                ,RZNETTCID RZNETTCID
                ,RZNETDOC RZNETDOC
                ,RZNETRC5 RZNETRC5
                ,SEMANTIC.JDE_JULIAN_TO_DATE(RZADGJ) RZADGJ
                
FROM datalake_dev.pre_governed.jde_f03b14_1972 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
