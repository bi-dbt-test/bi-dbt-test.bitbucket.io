
  create or replace  view datalake_dev.governed.JDE_CLOSING_WORKSHEET_ENTRIES_MULTI  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f5544h51_2484)
SELECT CWICU Batch_Number
                ,CWHBMCUS Community
                ,CWHBLOT HomeBuilderLotNumber
                ,CWCLSRT ClosingWorksheetLineItem
                ,CWOPTION OptionNumber
                ,CWEXR NameRemarkExplanation
                ,CWCLDESC ClosingLineItemDescription
                ,CWCLMCD ClosingCostCenterValue
                ,CWMCU CostCenter
                ,CWOBJ ObjectAccount
                ,CWSUB Subsidiary
                ,CWLT LedgerType
                ,CWCLSCD ClosingSubledgerValue
                ,CWSBL Subledger
                ,CWSBLT SubledgerType
                ,CWCLDBCR ClosingDebitCreditCode
                ,CAST(CWAA/100 AS DECIMAL(18,2)) AmountField
                ,CWDMF DataMapFlag
                ,CWRECERR RecordErrorCode
                ,CAST(CWU/100 AS DECIMAL(18,2)) Units
                ,CWCRTU CreatedByUser
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CWCRTJ) DateCreated
                ,CWCRTT TimeCreated
                ,CWWRKSTNID OriginalWorkStationId
                ,CWHBOPID OriginalProgramId
                ,CWUPMB UpdatedByUser
                ,SEMANTIC.JDE_JULIAN_TO_DATE(CWUPMJ) Date_Updated
                ,CWUPMT Time_Updated
                ,CWJOBN Work_Stn_ID
                ,CWPID Program_Id
                
FROM datalake_dev.pre_governed.jde_f5544h51_2484 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
