
  create or replace  view datalake_dev.governed.JDE_PO_DETAIL_LEDGER_FILE_FLEXIBLE_VERSION  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f43199_1944)
SELECT OLKCOO OLKCOO
                ,OLDOCO DocumentOrderInvoice
                ,OLDCTO OrderType
                ,OLSFXO OLSFXO
                ,CAST(OLLNID/100 AS DECIMAL(18,2)) LineNumber
                ,OLMCU BusinessUnit
                ,OLCO Company
                ,OLOKCO OLOKCO
                ,OLOORN OLOORN
                ,OLOCTO OLOCTO
                ,CAST(OLOGNO/100 AS DECIMAL(18,2)) OLOGNO
                ,OLRKCO OLRKCO
                ,OLRORN OLRORN
                ,OLRCTO OLRCTO
                ,CAST(OLRLLN/100 AS DECIMAL(18,2)) OLRLLN
                ,OLDMCT OLDMCT
                ,OLDMCS OLDMCS
                ,OLBALU OLBALU
                ,OLAN8 AddressNumber
                ,OLSHAN OLSHAN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLDRQJ) OLDRQJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLTRDJ) OLTRDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLPDDJ) OLPDDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLOPDJ) OLOPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLADDJ) OLADDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLCNDJ) OLCNDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLPEFJ) OLPEFJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLPPDJ) OLPPDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLPSDJ) OLPSDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLDSVJ) DateServiceCurrency
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLDGL) OLDGL
                ,OLPN PeriodNumberGeneralLedger
                ,OLVR01 OLVR01
                ,OLVR02 OLVR02
                ,OLITM OLITM
                ,OLLITM OLLITM
                ,OLAITM OLAITM
                ,OLLOCN OLLOCN
                ,OLLOTN OLLOTN
                ,OLFRGD OLFRGD
                ,OLTHGD OLTHGD
                ,CAST(OLFRMP/100 AS DECIMAL(18,2)) OLFRMP
                ,CAST(OLTHRP/100 AS DECIMAL(18,2)) OLTHRP
                ,OLDSC1 OLDSC1
                ,OLDSC2 OLDSC2
                ,OLLNTY OLLNTY
                ,OLNXTR OLNXTR
                ,OLLTTR OLLTTR
                ,OLRLIT OLRLIT
                ,OLPDS1 OLPDS1
                ,OLPDS2 OLPDS2
                ,OLPDS3 OLPDS3
                ,OLPDS4 OLPDS4
                ,OLPDS5 OLPDS5
                ,OLPDP1 OLPDP1
                ,OLPDP2 OLPDP2
                ,OLPDP3 OLPDP3
                ,OLPDP4 OLPDP4
                ,OLPDP5 OLPDP5
                ,OLUOM OLUOM
                ,OLUORG OLUORG
                ,OLUCHG OLUCHG
                ,OLUOPN OLUOPN
                ,OLUREC OLUREC
                ,OLCREC OLCREC
                ,OLURLV OLURLV
                ,OLOTQY OLOTQY
                ,CAST(OLPRRC/100 AS DECIMAL(18,2)) OLPRRC
                ,CAST(OLAEXP/100 AS DECIMAL(18,2)) OLAEXP
                ,CAST(OLACHG/100 AS DECIMAL(18,2)) OLACHG
                ,CAST(OLAOPN/100 AS DECIMAL(18,2)) OLAOPN
                ,CAST(OLAREC/100 AS DECIMAL(18,2)) OLAREC
                ,CAST(OLARLV/100 AS DECIMAL(18,2)) OLARLV
                ,CAST(OLFTN1/100 AS DECIMAL(18,2)) OLFTN1
                ,CAST(OLTRLV/100 AS DECIMAL(18,2)) OLTRLV
                ,OLPROV OLPROV
                ,CAST(OLAMC3/100 AS DECIMAL(18,2)) OLAMC3
                ,CAST(OLECST/100 AS DECIMAL(18,2)) OLECST
                ,OLCSTO OLCSTO
                ,OLCSMP OLCSMP
                ,OLINMG PrintMessage1
                ,OLASN PriceAdjustmentScheduleN
                ,OLPRGR OLPRGR
                ,OLCLVL OLCLVL
                ,OLCATN OLCATN
                ,CAST(OLDSPR/100 AS DECIMAL(18,2)) OLDSPR
                ,OLPTC OLPTC
                ,OLTX OLTX
                ,OLEXR1 TaxExplanationCode1
                ,OLTXA1 TaxArea1
                ,OLATXT OLATXT
                ,OLCNID OLCNID
                ,OLCDCD OLCDCD
                ,OLNTR OLNTR
                ,OLFRTH FreightHandlingCode
                ,OLFRTC OLFRTC
                ,OLZON ZoneNumber
                ,OLFRAT OLFRAT
                ,OLRATT OLRATT
                ,OLANBY OLANBY
                ,OLANCR CarrierNumber
                ,OLMOT OLMOT
                ,OLCOT OLCOT
                ,OLSHCM OLSHCM
                ,OLSHCN OLSHCN
                ,OLUOM1 OLUOM1
                ,OLPQOR OLPQOR
                ,OLUOM2 OLUOM2
                ,OLSQOR OLSQOR
                ,OLUOM3 OLUOM3
                ,CAST(OLITWT/100 AS DECIMAL(18,2)) OLITWT
                ,OLWTUM OLWTUM
                ,CAST(OLITVL/100 AS DECIMAL(18,2)) OLITVL
                ,OLVLUM OLVLUM
                ,OLGLC BillCode
                ,OLCTRY Century
                ,OLFY FiscalYear
                ,OLSTTS OLSTTS
                ,OLRCD OLRCD
                ,OLFUF1 OLFUF1
                ,OLFUF2 OLFUF2
                ,CAST(OLGRWT/100 AS DECIMAL(18,2)) OLGRWT
                ,OLGWUM OLGWUM
                ,OLLT LedgerType
                ,OLANI AcctNoInputMode
                ,OLAID AccountID
                ,OLOMCU OLOMCU
                ,OLOBJ ObjectAccount
                ,OLSUB Subsidiary
                ,OLSBLT SubledgerType
                ,OLSBL Subledger
                ,OLASID OLASID
                ,OLCCMP OLCCMP
                ,OLTAG OLTAG
                ,OLWR01 OLWR01
                ,OLPL OLPL
                ,OLELEV OLELEV
                ,OLR001 OLR001
                ,OLRTNR OLRTNR
                ,OLLCOD OLLCOD
                ,OLPURG OLPURG
                ,OLPROM OLPROM
                ,OLFNLP OLFNLP
                ,OLAVCH CodeAutomaticVoucher
                ,OLPRPY OLPRPY
                ,OLUNCD OLUNCD
                ,OLMATY OLMATY
                ,OLRTGC OLRTGC
                ,OLRCPF OLRCPF
                ,OLPS01 OLPS01
                ,OLPS02 OLPS02
                ,OLPS03 OLPS03
                ,OLPS04 OLPS04
                ,OLPS05 OLPS05
                ,OLPS06 OLPS06
                ,OLPS07 OLPS07
                ,OLPS08 OLPS08
                ,OLPS09 OLPS09
                ,OLPS10 OLPS10
                ,OLCRMD CorrespondenceMethod
                ,OLARTG OLARTG
                ,OLCORD OLCORD
                ,OLCHDT OLCHDT
                ,OLDOCC OLDOCC
                ,CAST(OLCHLN/100 AS DECIMAL(18,2)) OLCHLN
                ,OLCRCD CurrencyCodeFrom
                ,OLCRR OLCRR
                ,CAST(OLFRRC/100 AS DECIMAL(18,2)) OLFRRC
                ,CAST(OLFEA/100 AS DECIMAL(18,2)) OLFEA
                ,CAST(OLFUC/100 AS DECIMAL(18,2)) OLFUC
                ,CAST(OLFEC/100 AS DECIMAL(18,2)) OLFEC
                ,CAST(OLFCHG/100 AS DECIMAL(18,2)) OLFCHG
                ,CAST(OLFAP/100 AS DECIMAL(18,2)) OLFAP
                ,CAST(OLFREC/100 AS DECIMAL(18,2)) OLFREC
                ,OLURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLURDT) UserReservedDate
                ,CAST(OLURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,OLURAB UserReservedNumber
                ,OLURRF UserReservedReference
                ,OLTORG TransactionOriginator
                ,OLUSER UserId
                ,OLPID ProgramId
                ,OLJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLUPMJ) DateUpdated
                ,OLTDAY OLTDAY
                ,OLVR05 OLVR05
                ,OLVR04 OLVR04
                ,OLSHPN OLSHPN
                ,OLSHMT OLSHMT
                ,OLRSHT OLRSHT
                ,OLPRJM OLPRJM
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLDLEJ) OLDLEJ
                ,OLOSFX OLOSFX
                ,OLMERL OLMERL
                ,CAST(OLMCLN/100 AS DECIMAL(18,2)) OLMCLN
                ,OLMACT OLMACT
                ,CAST(OLKTLN/100 AS DECIMAL(18,2)) OLKTLN
                ,CAST(OLFTRL/100 AS DECIMAL(18,2)) OLFTRL
                ,OLDRQT OLDRQT
                ,CAST(OLCTAM/100 AS DECIMAL(18,2)) OLCTAM
                ,CAST(OLCPNT/100 AS DECIMAL(18,2)) OLCPNT
                ,OLCHT OLCHT
                ,OLPMTN OLPMTN
                ,OLCHRT OLCHRT
                ,OLCHRS OLCHRS
                ,SEMANTIC.JDE_JULIAN_TO_DATE(OLCHMJ) OLCHMJ
                ,OLBCRC OLBCRC
                ,OLDUAL OLDUAL
                ,OLUKID UniqueKeyIDInternal
                ,OLLDNM OLLDNM
                ,OLMKFR OLMKFR
                ,OLMFLG OLMFLG
                ,CAST(OLDLNID/100 AS DECIMAL(18,2)) OLDLNID
                ,OLCMDCDE OLCMDCDE
                ,OLUNSPSC OLUNSPSC
                ,OLRSFX OLRSFX
                ,OLWVID OLWVID
                ,OLCNTRTID OLCNTRTID
                ,OLCNTRTDID OLCNTRTDID
                ,OLMOADJ OLMOADJ
                ,OLPODC01 OLPODC01
                ,OLPODC02 OLPODC02
                ,OLPODC03 OLPODC03
                ,OLPODC04 OLPODC04
                ,OLJBCD OLJBCD
                ,CAST(OLSRQTY/100 AS DECIMAL(18,2)) OLSRQTY
                ,OLSRUOM OLSRUOM
                ,OLCFGFL OLCFGFL
                ,OLPMPN OLPMPN
                ,OLPNS OLPNS
                
FROM datalake_dev.pre_governed.jde_f43199_1944 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
