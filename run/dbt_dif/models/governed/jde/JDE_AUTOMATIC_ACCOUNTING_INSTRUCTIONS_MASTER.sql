
  create or replace  view datalake_dev.governed.JDE_AUTOMATIC_ACCOUNTING_INSTRUCTIONS_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0012_3591)
SELECT KGITEM ItemNumber
                ,KGCO Company
                ,KGMCU BusinessUnit
                ,KGOBJ ObjectAccount
                ,KGSUB Subsidiary
                ,KGDL01 Description
                ,KGDL02 Description02
                ,KGDL03 KGDL03
                ,KGDL04 KGDL04
                ,KGDL05 KGDL05
                ,KGMOPT KGMOPT
                ,KGOOPT KGOOPT
                ,KGSOPT KGSOPT
                ,KGSY ProductCode
                ,KGSEQN SequenceNumber2
                ,KGUSER UserId
                ,KGPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(KGUPMJ) DateUpdated
                ,KGJOBN WorkStationId
                ,KGUPMT TimeLastUpdated
                
FROM datalake_dev.pre_governed.jde_f0012_3591 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
