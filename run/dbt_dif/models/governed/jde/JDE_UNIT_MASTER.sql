
  create or replace  view datalake_dev.governed.JDE_UNIT_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1507_2975)
SELECT NHMCUS CostCenterSubsequent
                ,NHMCU BusinessUnit
                ,NHUNIT Unit
                ,NHFLOR PropmanFloor
                ,NHDL01 Description
                ,NHDL02 Description02
                ,NHDL03 NHDL03
                ,NHDL04 NHDL04
                ,NHDL05 NHDL05
                ,NHUTTY PropmanUnitType
                ,NHUTUS NHUTUS
                ,NHMSUT NHMSUT
                ,NHMSTU NHMSTU
                ,NHRL01 NHRL01
                ,NHRL02 NHRL02
                ,NHRL03 NHRL03
                ,NHRL04 NHRL04
                ,NHRL05 NHRL05
                ,NHADDS NHADDS
                ,NHADDZ NHADDZ
                ,NHCTR Country
                ,NHUSER UserId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NHUPMJ) DateUpdated
                ,NHPID ProgramId
                ,NHJOBN WorkStationId
                ,CAST(NHPRRT/100 AS DECIMAL(18,2)) NHPRRT
                ,CAST(NHPORT/100 AS DECIMAL(18,2)) NHPORT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NHDV) NHDV
                ,CAST(NHMART/100 AS DECIMAL(18,2)) NHMART
                ,CAST(NHRERT/100 AS DECIMAL(18,2)) NHRERT
                ,NHGLC BillCode
                ,NHUST StatusUnit
                ,NHAM01 NHAM01
                ,NHAM02 NHAM02
                ,NHAM03 NHAM03
                ,NHAM04 NHAM04
                ,NHAM05 NHAM05
                ,NHAM06 NHAM06
                ,NHAM07 NHAM07
                ,NHAM08 NHAM08
                ,NHAM09 NHAM09
                ,NHAM10 NHAM10
                ,NHAM11 NHAM11
                ,NHAM12 NHAM12
                ,NHAM13 NHAM13
                ,NHAM14 NHAM14
                ,NHAM15 NHAM15
                ,NHAM16 NHAM16
                ,NHAM17 NHAM17
                ,NHAM18 NHAM18
                ,NHAM19 NHAM19
                ,NHAM20 NHAM20
                ,NHCO Company
                ,NHUST1 NHUST1
                ,NHUST2 NHUST2
                ,NHUST3 NHUST3
                ,NHUST4 NHUST4
                ,CAST(NHASTD/100 AS DECIMAL(18,2)) NHASTD
                ,NHBFRQ NHBFRQ
                ,NHUPMT TimeLastUpdated
                ,NHNUMB NHNUMB
                
FROM datalake_dev.pre_governed.jde_f1507_2975 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
