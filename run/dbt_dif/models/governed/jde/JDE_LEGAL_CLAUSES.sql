
  create or replace  view datalake_dev.governed.JDE_LEGAL_CLAUSES  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1570_1970)
SELECT NDDOCO DocumentOrderInvoice
                ,NDAN8 AddressNumber
                ,NDCLNO LegalClauseNumber
                ,NDLSVR LeaseVersion
                ,NDOPTY LeaseOptionType
                ,NDODET OptionTypeDetail
                ,NDOSTA OptionStatus
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDEFTE) DateEndingEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDTKDT) TickleDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDDNOT) NoticeDate
                ,NDRNOT NoticeResponsibility
                ,NDRDOC LeaseDocumentReference
                ,NDURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDURDT) UserReservedDate
                ,CAST(NDURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NDURAB UserReservedNumber
                ,NDURRF UserReservedReference
                ,NDUSER UserId
                ,NDPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDUPMJ) DateUpdated
                ,NDUPMT TimeLastUpdated
                ,NDJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NDENTJ) NDENTJ
                ,NDTORG TransactionOriginator
                
FROM datalake_dev.pre_governed.jde_f1570_1970 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
