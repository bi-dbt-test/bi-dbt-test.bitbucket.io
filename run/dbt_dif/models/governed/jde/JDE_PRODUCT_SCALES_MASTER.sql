
  create or replace  view datalake_dev.governed.JDE_PRODUCT_SCALES_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f15014b_1950)
SELECT NPDOCO DocumentOrderInvoice
                ,NPLSVR LeaseVersion
                ,NPYEOV NPYEOV
                ,NPMCU BusinessUnit
                ,NPUNIT Unit
                ,NPDBAN NPDBAN
                ,NPPRDC NPPRDC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NPEFTB) DateBeginningEffective
                ,CAST(NPBKPT/100 AS DECIMAL(18,2)) ProductDollarBreakpt
                ,NPBCI BillingControlId
                ,NPDCTO OrderType
                ,NPSTNR NPSTNR
                ,NPULI NPULI
                ,NPULI2 NPULI2
                ,NPSET NPSET
                ,NPCMPM SalesOverageComputa
                ,NPPDUE PercentageDueOnSales
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NPEFTE) DateEndingEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NPSUDT) DateSuspended
                ,NPBCI3 BillingControlIDThird
                ,NPVRSC VersionSuspendCode
                ,NPSUSP SuspendCode
                ,NPSBJR NPSBJR
                ,NPPRRO NPPRRO
                ,NPGLC BillCode
                ,NPLRYF NPLRYF
                ,NPLRYT NPLRYT
                ,NPYT NPYT
                ,CAST(NPSUSA/100 AS DECIMAL(18,2)) SalesUsableArea
                ,NPMCUS CostCenterSubsequent
                ,NPDEAL DealNumber
                ,NPSOPC NPSOPC
                ,NPSRLS NPSRLS
                ,NPRM13 NPRM13
                ,NPRM14 NPRM14
                ,NPRM15 NPRM15
                ,NPRM24 NPRM24
                ,NPRM25 NPRM25
                ,NPURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NPURDT) UserReservedDate
                ,CAST(NPURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NPURAB UserReservedNumber
                ,NPURRF UserReservedReference
                ,NPUSER UserId
                ,NPPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NPUPMJ) DateUpdated
                ,NPUPMT TimeLastUpdated
                ,NPJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NPENTJ) NPENTJ
                ,NPTORG TransactionOriginator
                ,NPCRR NPCRR
                ,NPCRRM NPCRRM
                ,NPCRCD CurrencyCodeFrom
                ,NPACRL NPACRL
                ,NPARMT NPARMT
                
FROM datalake_dev.pre_governed.jde_f15014b_1950 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
