
  create or replace  view datalake_dev.governed.JDE_CUSTOMER_MASTER_BY_LINE_OF_BUSINESS  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f03012_1956)
SELECT AIAN8 AddressNumber
                ,AICO Company
                ,AIARC AIARC
                ,AIMCUR AIMCUR
                ,AIOBAR AIOBAR
                ,AIAIDR AIAIDR
                ,AIKCOR AIKCOR
                ,AIDCAR AIDCAR
                ,AIDTAR AIDTAR
                ,AICRCD CurrencyCodeFrom
                ,AITXA1 TaxArea1
                ,AIEXR1 TaxExplanationCode1
                ,AIACL AIACL
                ,AIHDAR AIHDAR
                ,AITRAR PaymentTermsAR
                ,AISTTO AISTTO
                ,AIRYIN AIRYIN
                ,AISTMT AISTMT
                ,AIARPY AIARPY
                ,AIATCS AIATCS
                ,AISITO AISITO
                ,AISQNL SequenceForLedgrInq
                ,AIALGM AIALGM
                ,AICYCN AICYCN
                ,AIBO AIBO
                ,AITSTA AITSTA
                ,AICKHC AICKHC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDLC) AIDLC
                ,AIDNLT AIDNLT
                ,AIPLCR AIPLCR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIRVDJ) AIRVDJ
                ,AIDSO AIDSO
                ,AICMGR AICMGR
                ,AICLMG AICLMG
                ,AIDLQT AIDLQT
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDLQJ) AIDLQJ
                ,AINBRR AINBRR
                ,AICOLL AICOLL
                ,AINBR1 AINBR1
                ,AINBR2 AINBR2
                ,AINBR3 AINBR3
                ,AINBCL AINBCL
                ,AIAFC AIAFC
                ,AIFD AIFD
                ,CAST(AIFP/100 AS DECIMAL(18,2)) AIFP
                ,AICFCE AICFCE
                ,AIAB2 AIAB2
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDT1J) AIDT1J
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDFIJ) AIDFIJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDLIJ) AIDLIJ
                ,AIABC1 AIABC1
                ,AIABC2 AIABC2
                ,AIABC3 AIABC3
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIFNDJ) AIFNDJ
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDLP) AIDLP
                ,AIDB AIDB
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDNBJ) AIDNBJ
                ,AITRW AITRW
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AITWDJ) AITWDJ
                ,AIAVD AIAVD
                ,AICRCA CurrencyCodeAmounts
                ,CAST(AIAD/100 AS DECIMAL(18,2)) AIAD
                ,CAST(AIAFCP/100 AS DECIMAL(18,2)) AIAFCP
                ,CAST(AIAFCY/100 AS DECIMAL(18,2)) AIAFCY
                ,CAST(AIASTY/100 AS DECIMAL(18,2)) AIASTY
                ,CAST(AISPYE/100 AS DECIMAL(18,2)) AISPYE
                ,CAST(AIAHB/100 AS DECIMAL(18,2)) AIAHB
                ,CAST(AIALP/100 AS DECIMAL(18,2)) AIALP
                ,CAST(AIABAM/100 AS DECIMAL(18,2)) AmountAddressBook
                ,CAST(AIABA1/100 AS DECIMAL(18,2)) Amount1
                ,CAST(AIAPRC/100 AS DECIMAL(18,2)) AmountOpenOrder
                ,AIMAXO MaximumOrderValue
                ,AIMINO MinimumOrderValue
                ,AIOYTD AIOYTD
                ,AIOPY AIOPY
                ,AIPOPN AIPOPN
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIDAOJ) AIDAOJ
                ,AIAN8R RelatedAddressNo
                ,AIBADT BillingAddressType
                ,AICPGP GroupCustomerPriceGp
                ,AIORTP OrderTemplate
                ,CAST(AITRDC/100 AS DECIMAL(18,2)) AITRDC
                ,AIINMG PrintMessage1
                ,AIEXHD AIEXHD
                ,AIHOLD HoldOrdersCode
                ,AIROUT RouteCode
                ,AISTOP StopCode
                ,AIZON ZoneNumber
                ,AICARS Carrier
                ,AIDEL1 DeliveryInstructLine1
                ,AIDEL2 DeliveryInstructLine2
                ,AILTDT LeadtimeTransit
                ,AIFRTH FreightHandlingCode
                ,AIAFT AIAFT
                ,AIAPTS AIAPTS
                ,AISBAL AISBAL
                ,AIBACK AIBACK
                ,AIPORQ AIPORQ
                ,AIPRIO AIPRIO
                ,AIARTO AIARTO
                ,AIINVC InvoiceCopies
                ,AIICON AIICON
                ,AIBLFR AIBLFR
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AINIVD) AINIVD
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AILEDJ) AILEDJ
                ,AIPLST PricePickListYN
                ,AIMORD AIMORD
                ,AICMC1 AICMC1
                ,CAST(AICMR1/100 AS DECIMAL(18,2)) AICMR1
                ,AICMC2 AICMC2
                ,CAST(AICMR2/100 AS DECIMAL(18,2)) AICMR2
                ,AIPALC AIPALC
                ,AIVUMD UnitOfMeasureVolDisp
                ,AIWUMD UnitOfMeasureWhtDisp
                ,AIEDPM BatchProcessingMode
                ,AIEDII ItemTypeIdentifier
                ,AIEDCI CustomerTypeIdentifier
                ,AIEDQD QuantityDecimals
                ,AIEDAD AmountDecimals
                ,AIEDF1 DeliveryNote
                ,AIEDF2 ItemRestrictions
                ,AISI01 AISI01
                ,AISI02 AISI02
                ,AISI03 AISI03
                ,AISI04 AISI04
                ,AISI05 AISI05
                ,AIURCD UserReservedCode
                ,CAST(AIURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,AIURAB UserReservedNumber
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIURDT) UserReservedDate
                ,AIURRF UserReservedReference
                ,AICP01 AICP01
                ,AIASN PriceAdjustmentScheduleN
                ,AIDSPA AIDSPA
                ,AICRMD CorrespondenceMethod
                ,AIPLY AIPLY
                ,AIMAN8 AIMAN8
                ,AIARL AIARL
                ,AIAMCR AIAMCR
                ,AIAC01 AIAC01
                ,AIAC02 AIAC02
                ,AIAC03 AIAC03
                ,AIAC04 AIAC04
                ,AIAC05 AIAC05
                ,AIAC06 AIAC06
                ,AIAC07 AIAC07
                ,AIAC08 AIAC08
                ,AIAC09 AIAC09
                ,AIAC10 AIAC10
                ,AIAC11 AIAC11
                ,AIAC12 AIAC12
                ,AIAC13 AIAC13
                ,AIAC14 AIAC14
                ,AIAC15 AIAC15
                ,AIAC16 AIAC16
                ,AIAC17 AIAC17
                ,AIAC18 AIAC18
                ,AIAC19 AIAC19
                ,AIAC20 AIAC20
                ,AIAC21 AIAC21
                ,AIAC22 AIAC22
                ,AIAC23 AIAC23
                ,AIAC24 AIAC24
                ,AIAC25 AIAC25
                ,AIAC26 AIAC26
                ,AIAC27 AIAC27
                ,AIAC28 AIAC28
                ,AIAC29 AIAC29
                ,AIAC30 AIAC30
                ,AISLPG AISLPG
                ,AISLDW AISLDW
                ,AICFPP AICFPP
                ,AICFSP AICFSP
                ,AICFDF AICFDF
                ,AIRQ01 AIRQ01
                ,AIRQ02 AIRQ02
                ,AIDR03 AIDR03
                ,AIDR04 AIDR04
                ,AIRQ03 AIRQ03
                ,AIRQ04 AIRQ04
                ,AIRQ05 AIRQ05
                ,AIRQ06 AIRQ06
                ,AIRQ07 AIRQ07
                ,AIRQ08 AIRQ08
                ,AIDR08 AIDR08
                ,AIDR09 AIDR09
                ,AIRQ09 AIRQ09
                ,AIUSER UserId
                ,AIPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(AIUPMJ) DateUpdated
                ,AIUPMT TimeLastUpdated
                ,AIJOBN WorkStationId
                ,AIPRGF AIPRGF
                ,AIBYAL AIBYAL
                ,AIBSC AIBSC
                ,AIASHL AIASHL
                ,AIPRSN AIPRSN
                ,AIOPBO AIOPBO
                ,AIAPSB AIAPSB
                ,AITIER1 AITIER1
                ,AIPWPCP AIPWPCP
                ,AICUSTS AICUSTS
                ,AISTOF AISTOF
                ,AITERRID AITERRID
                ,AICIG AICIG
                ,AITORG TransactionOriginator
                ,AIDTEE AIDTEE
                ,AISYNCS SynchronizationStatus
                ,AICAAD ServerStatus
                ,AIGOPASF AIGOPASF
                
FROM datalake_dev.pre_governed.jde_f03012_1956 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
