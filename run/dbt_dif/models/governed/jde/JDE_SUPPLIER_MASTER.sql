
  create or replace  view datalake_dev.governed.JDE_SUPPLIER_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f0401_1951)
SELECT A6AN8 AddressNumber
                ,A6APC APClass
                ,A6MCUP CostCenterApDefault
                ,A6OBAP ObjectAcctsPayable
                ,A6AIDP SubsidiaryAcctsPayable
                ,A6KCOP CompanyKeyAPModel
                ,A6DCAP DocApDefaultJe
                ,A6DTAP DocTyApDefaultJe
                ,A6CRRP CurrencyCodeAP
                ,A6TXA2 TaxArea2
                ,A6EXR2 TaxExemptReason2
                ,A6HDPY HoldPaymentCode
                ,A6TXA3 TaxRateArea3Withholding
                ,A6EXR3 TaxExplCode3Withhholding
                ,A6TAWH TaxAuthorityAp
                ,A6PCWH PercentWithholding
                ,A6TRAP PaymentTermsAP
                ,A6SCK MultipleChecksYN
                ,A6PYIN PaymentInstrument
                ,A6SNTO AddressNumberSentTo
                ,A6AB1 MiscCode1
                ,A6FLD FloatDaysForChecks
                ,A6SQNL SequenceForLedgrInq
                ,A6CRCA CurrencyCodeAmounts
                ,CAST(A6AYPD/100 AS DECIMAL(18,2)) AmountVoucheredYtd
                ,CAST(A6APPD/100 AS DECIMAL(18,2)) AmountVoucheredPye
                ,CAST(A6ABAM/100 AS DECIMAL(18,2)) AmountAddressBook
                ,CAST(A6ABA1/100 AS DECIMAL(18,2)) Amount1
                ,CAST(A6APRC/100 AS DECIMAL(18,2)) AmountOpenOrder
                ,A6MINO MinimumOrderValue
                ,A6MAXO MaximumOrderValue
                ,A6AN8R RelatedAddressNo
                ,A6BADT BillingAddressType
                ,A6CPGP GroupCustomerPriceGp
                ,A6ORTP OrderTemplate
                ,A6INMG PrintMessage1
                ,A6HOLD HoldOrdersCode
                ,A6ROUT RouteCode
                ,A6STOP StopCode
                ,A6ZON ZoneNumber
                ,A6ANCR CarrierNumber
                ,A6CARS Carrier
                ,A6DEL1 DeliveryInstructLine1
                ,A6DEL2 DeliveryInstructLine2
                ,A6LTDT LeadtimeTransit
                ,A6FRTH FreightHandlingCode
                ,A6INVC InvoiceCopies
                ,A6PLST PricePickListYN
                ,A6WUMD UnitOfMeasureWhtDisp
                ,A6VUMD UnitOfMeasureVolDisp
                ,A6PRP5 PurchasingReportCode5
                ,A6EDPM BatchProcessingMode
                ,A6EDCI CustomerTypeIdentifier
                ,A6EDII ItemTypeIdentifier
                ,A6EDQD QuantityDecimals
                ,A6EDAD AmountDecimals
                ,A6EDF1 DeliveryNote
                ,A6EDF2 ItemRestrictions
                ,A6VI01 SpecialInst01
                ,A6VI02 SpecialInstr02
                ,A6VI03 SpecialInst03
                ,A6VI04 SpecialInst04
                ,A6VI05 SpecialInst05
                ,A6MNSC MinimumCheckAmtCode
                ,A6ATO AddTypeCode5Owner
                ,A6RVNT RevenueNettedYN
                ,A6URCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(A6URDT) UserReservedDate
                ,CAST(A6URAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,A6URAB UserReservedNumber
                ,A6URRF UserReservedReference
                ,A6USER UserId
                ,A6PID ProgramId
                ,A6JOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(A6UPMJ) DateUpdated
                ,A6UPMT TimeLastUpdated
                ,A6ASN PriceAdjustmentScheduleN
                ,A6CRMD CorrespondenceMethod
                ,A6AVCH CodeAutomaticVoucher
                ,A6ATRL AutomationRule
                
FROM datalake_dev.pre_governed.jde_f0401_1951 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
