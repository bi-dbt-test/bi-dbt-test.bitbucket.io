
  create or replace  view datalake_dev.governed.JDE_LOG_DETAIL_MASTER  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.jde_f1521_2971)
SELECT NBLGLV LogLevel
                ,NBLGNM LogNumber
                ,NBDOCO DocumentOrderInvoice
                ,NBLSVR LeaseVersion
                ,NBMCU BusinessUnit
                ,NBUNIT Unit
                ,NBLGCL PropertyLogClass
                ,NBDL01 Description
                ,NBEXR NameRemarkExplanation
                ,NBSTSC NBSTSC
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NBEFTB) DateBeginningEffective
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NBEXPR) ExpiredDate
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NBTKDT) TickleDate
                ,NBEPCD NBEPCD
                ,NBAMID AmenityIdentification
                ,CAST(NBPMU1/100 AS DECIMAL(18,2)) PropmanUnits001
                ,NBUM UnitOfMeasure
                ,NBRO01 NBRO01
                ,NBRO02 NBRO02
                ,NBRQ NBRQ
                ,NBJOB_NUM NBJOB_NUM
                ,NBSUSC NBSUSC
                ,NBAN8 AddressNumber
                ,NBANP NBANP
                ,NBFLOR PropmanFloor
                ,NBFOTY NBFOTY
                ,NBARGC NBARGC
                ,NBARGV NBARGV
                ,NBURCD UserReservedCode
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NBURDT) UserReservedDate
                ,CAST(NBURAT/100 AS DECIMAL(18,2)) UserReservedAmount
                ,NBURAB UserReservedNumber
                ,NBURRF UserReservedReference
                ,NBUSER UserId
                ,NBPID ProgramId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NBUPMJ) DateUpdated
                ,NBUPMT TimeLastUpdated
                ,NBJOBN WorkStationId
                ,SEMANTIC.JDE_JULIAN_TO_DATE(NBENTJ) NBENTJ
                ,NBTORG TransactionOriginator
                ,NBNUMB NBNUMB
                ,NBLZNPA NBLZNPA
                
FROM datalake_dev.pre_governed.jde_f1521_2971 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
