
  create or replace  view datalake_dev.governed.KRONOS_DAILY_LABOR_REPORT  as (
    






WITH t_f as (SELECT MAX(IngestionDate) max_ingdate FROM  datalake_dev.pre_governed.kronos_daily_labor_report_1079)
SELECT Date Date
                ,Employee_ID Employee_ID
                ,Employee Employee
                ,Position Position
                ,Cost_Center Cost_Center
                ,Line_of_Business Line_of_Business
                ,Job_Profile Job_Profile
                ,Job_Profile_ID Job_Profile_ID
                ,Location Location
                ,Earnings Earnings
                ,Hours Hours
                ,Dollars Dollars
                
FROM datalake_dev.pre_governed.kronos_daily_labor_report_1079 t 
INNER JOIN t_f ON t.IngestionDate = t_f.max_ingdate

  );
